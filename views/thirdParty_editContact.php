<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

<form action="<?=$urlSave?>" method="POST">

    <input type="hidden" name="ID" class="form-control" value="<?=$item['ID']?>" />
    <input type="hidden" name="Active" class="form-control" value="0" />

    <div id="headerbar" style="position:fixed;width:100%;background-color:white;z-index:99;">

        <div class="pull-left">
            <h1><?=t('EditContact').' - '.$item['Name'].' '.$item['Firstname']?></h1>
        </div>
        <div class="pull-right btn-group">
            <a class="btn btn-sm btn-default" href="<?=$urlCancel?>" onclick="history.back();return false;">
                <i class="glyphicon glyphicon-remove"></i> <?=t('Cancel')?>
            </a>
            <button class="btn btn-sm btn-primary">
                <i class="fa fa-edit"></i> <?=t('Save')?>
            </button>
        </div>

    </div>

    <div class="container-fluid" style="padding-top:45px;">

            <div class="row">
                <br />
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-8">
                    <label><?=t('Name')?> * : </label><br />
                    <div class="input-group input-group-lg">
                          <input type="text" name="Name" required="required" class="form-control" value="<?=$item['Name']?>" />
                          <span class="input-group-addon">
                            <input type="checkbox" name="Active" value="1" <?=($item['Active'] == 1 ? 'checked="checked"' : '')?> /> <?=t('Active')?>
                          </span>
                    </div>
                    <br />
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <label><?=t('Firstname')?> : </label><br />
                    <input type="text" name="Firstname" class="form-control" value="<?=$item['Firstname']?>" /><br />
                </div>

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                    <label><?=t('Function')?> : </label><br />
                    <input type="text" name="Function" class="form-control" value="<?=$item['Function']?>" /><br />
                </div>
            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-6">
                    <fieldset>
                        <legend><?=t('ContactInformations')?></legend>

                        <div class="form-group">
                            <label><?=t('Phone')?> : </label>
                            <div class="controls">
                                <input type="text" name="Phone" class="form-control input-sm" value="<?=$item['Phone']?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label><?=t('Mobile')?>: </label>
                            <div class="controls">
                                <input type="text" name="Mobile" class="form-control input-sm" value="<?=$item['Mobile']?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label><?=t('Fax')?> : </label>
                            <div class="controls">
                                <input type="text" name="Fax" class="form-control input-sm" value="<?=$item['Fax']?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label><?=t('Email')?> : </label>
                            <div class="controls">
                                <input type="text" name="Email" class="form-control input-sm" value="<?=$item['Email']?>" />
                            </div>
                        </div>

                    </fieldset>
                </div>

        </div>

    </div>

</form>

<script>
<!--
-->
</script>

<?php include 'footer.php';