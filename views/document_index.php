<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

    <script type="text/javascript">
        var today = new Date().toISOString().slice(0, 10);
    </script>

<?php
    $rOptions = array();
    //Repeater Id
    $rOptions['id'] = 'documentList';
    //url
    $rOptions['url'] = $urlLoadList;
    //Default Type
    $rOptions['type'] = $listOptionType;
    //Array Types
    $rOptions['arrTypes'] = $rTypes;
    //default filter
    $rOptions['filter'] = $listOptionFilter;
    //Array Filters
    $rOptions['arrFilters'] = array();
    $rOptions['arrFilters'][1] = array();
    $rOptions['arrFilters'][1][] = array(
        'value'     => 'all',
        'label'     => t('All')
    );
    $rOptions['arrFilters'][1][] = array(
        'value'     => 'expired',
        'label'     => t('Expired')
    );
    foreach($quoteStatutes as $status) {
        $rOptions['arrFilters'][1][] = array(
            'value'     => $status['ID'],
            'label'     => $status['Name']
        );
    }

    $rOptions['arrFilters'][2][] = array(
        'value'     => 'all',
        'label'     => t('All')
    );
    $rOptions['arrFilters'][2][] = array(
        'value'     => 'expired',
        'label'     => t('Expired')
    );
    foreach($orderStatutes as $status) {
        $rOptions['arrFilters'][2][] = array(
            'value'     => $status['ID'],
            'label'     => $status['Name']
        );
    }

    $rOptions['arrFilters'][4][] = array(
        'value'     => 'all',
        'label'     => t('All')
    );
    $rOptions['arrFilters'][4][] = array(
        'value'     => 'unpaid',
        'label'     => t('Unpaid')
    );
    $rOptions['arrFilters'][4][] = array(
        'value'     => 'overdue',
        'label'     => t('Overdue')
    );
    foreach($invoiceStatutes as $status) {
        $rOptions['arrFilters'][4][] = array(
            'value'     => $status['ID'],
            'label'     => $status['Name']
        );
    }

    $rOptions['arrFilters'][5][] = array(
        'value'     => 'all',
        'label'     => t('All')
    );
    foreach($purchaseStatutes as $status) {
        $rOptions['arrFilters'][5][] = array(
            'value'     => $status['ID'],
            'label'     => $status['Name']
        );
    }
    //Settings for action button
    $rOptions['actionSettings'] = array(
        'label' => t('New'),
        'icon' => 'plus'
    );
    //Array Actions
    $rOptions['arrActions'] = array(
        0 => array(
            'label'     => t('Quote'),
            'url'       => 'javascript:void(0); newQuote();'
        ),
        1 => array(
            'label'     => t('Order'),
            'url'       => 'javascript:void(0); newOrder();'
        ),
        3 => array(
            'label'     => t('Invoice'),
            'url'       => 'javascript:void(0); newInvoice();'
        ),
        4 => array(
            'label'     => t('Purchase'),
            'url'       => 'javascript:void(0); newPurchase();'
        )
    );
    //Object contenant les descriptions des colonnes en fonction du type
    $rOptions['arrColumns'] = array(
        '1' => array(
            array(
                'label'     => t('Reference'),
                'property'  => 'Reference',
                'className' => 'valign-middle text-center',
                'width'     => 100,
                'sortable'  => true
            ),
            array(
                'label'     => t('State'),
                'property'  => 'State',
                'className' => 'valign-middle',
                'width'     => 150,
                'sortable'  => true
            ),
            array(
                'label'     => t('Validated'),
                'property'  => 'DateValidated',
                'className' => 'valign-middle text-center',
                'width'     => 105,
                'sortable'  => true
            ),
            array(
                'label'     => t('ThirdPartyName'),
                'property'  => 'ThirdPartyName',
                'className' => 'valign-middle',
                'sortable'  => false
            ),
            array(
                'label'     => t('Expire'),
                'property'  => 'DateDue',
                'className' => 'valign-middle text-center',
                'width'     => 105,
                'sortable'  => false
            ),
            array(
                'label'     => t('Amount'),
                'property'  => 'Total',
                'className' => 'valign-middle',
                'width'     => 100,
                'sortable'  => true
            ),
            array(
                'label'     => t('Action'),
                'property'  => 'Action',
                'className' => 'valign-middle text-center',
                'width'     => 115,
                'sortable'  => false
            )
        ),
        '2' => array(
            array(
                'label'     => t('Reference'),
                'property'  => 'Reference',
                'className' => 'valign-middle text-center',
                'width'     => 100,
                'sortable'  => true
            ),
            array(
                'label'     => t('State'),
                'property'  => 'State',
                'className' => 'valign-middle',
                'width'     => 150,
                'sortable'  => true
            ),
            array(
                'label'     => t('Validated'),
                'property'  => 'DateValidated',
                'className' => 'valign-middle text-center',
                'width'     => 105,
                'sortable'  => true
            ),
            array(
                'label'     => t('ThirdPartyName'),
                'property'  => 'ThirdPartyName',
                'className' => 'valign-middle',
                'sortable'  => false
            ),
            array(
                'label'     => t('Expire'),
                'property'  => 'DateDue',
                'className' => 'valign-middle text-center',
                'width'     => 105,
                'sortable'  => false
            ),
            array(
                'label'     => t('Amount'),
                'property'  => 'Total',
                'className' => 'valign-middle',
                'width'     => 100,
                'sortable'  => true
            ),
            array(
                'label'     => t('Action'),
                'property'  => 'Action',
                'className' => 'valign-middle text-center',
                'width'     => 115,
                'sortable'  => false
            )
        ),
        '4' => array(
            array(
                'label'     => t('Reference'),
                'property'  => 'Reference',
                'className' => 'valign-middle text-center',
                'width'     => 100,
                'sortable'  => true
            ),
            array(
                'label'     => t('State'),
                'property'  => 'State',
                'className' => 'valign-middle',
                'width'     => 150,
                'sortable'  => true
            ),
            array(
                'label'     => t('Validated'),
                'property'  => 'DateValidated',
                'className' => 'valign-middle text-center',
                'width'     => 105,
                'sortable'  => true
            ),
            array(
                'label'     => t('ThirdPartyName'),
                'property'  => 'ThirdPartyName',
                'className' => 'valign-middle',
                'sortable'  => false
            ),
            array(
                'label'     => t('Due'),
                'property'  => 'DateDue',
                'className' => 'valign-middle text-center',
                'width'     => 105,
                'sortable'  => false
            ),
            array(
                'label'     => t('Amount'),
                'property'  => 'Total',
                'className' => 'valign-middle',
                'width'     => 100,
                'sortable'  => true
            ),
            array(
                'label'     => t('Balance'),
                'property'  => 'Paid',
                'className' => 'valign-middle',
                'width'     => 100,
                'sortable'  => true
            ),
            array(
                'label'     => t('Action'),
                'property'  => 'Action',
                'className' => 'valign-middle text-center',
                'width'     => 115,
                'sortable'  => false
            )
        ),
        '5' => array(
            array(
                'label'     => t('Reference'),
                'property'  => 'Reference',
                'className' => 'valign-middle text-center',
                'width'     => 100,
                'sortable'  => true
            ),
            array(
                'label'     => t('State'),
                'property'  => 'State',
                'className' => 'valign-middle',
                'width'     => 150,
                'sortable'  => true
            ),
            array(
                'label'     => t('Validated'),
                'property'  => 'DateValidated',
                'className' => 'valign-middle text-center',
                'width'     => 105,
                'sortable'  => true
            ),
            array(
                'label'     => t('ThirdPartyName'),
                'property'  => 'ThirdPartyName',
                'className' => 'valign-middle',
                'sortable'  => false
            ),
            array(
                'label'     => t('Receipt'),
                'property'  => 'DateReceived',
                'className' => 'valign-middle text-center',
                'width'     => 105,
                'sortable'  => false
            ),
            array(
                'label'     => t('Due'),
                'property'  => 'DateDue',
                'className' => 'valign-middle text-center',
                'width'     => 105,
                'sortable'  => false
            ),
            array(
                'label'     => t('Amount'),
                'property'  => 'Total',
                'className' => 'valign-middle',
                'width'     => 100,
                'sortable'  => true
            ),
            array(
                'label'     => t('Balance'),
                'property'  => 'Paid',
                'className' => 'valign-middle',
                'width'     => 100,
                'sortable'  => true
            ),
            array(
                'label'     => t('Action'),
                'property'  => 'Action',
                'className' => 'valign-middle text-center',
                'width'     => 115,
                'sortable'  => false
            )
        )
    );
    //View render
    $rOptions['arrColumnsRenderer'] = array(
        '1' => array(
            'Reference'         => "<a href=\"".$urlEditQuote."'+rowData.ID+'\">'+(rowData.Reference == '' ? '".htmlspecialchars(t('Draft'))."' : rowData.Reference)+'</a>",
            'State'             => "<div class=\"label\" style=\"width:100%;background-color:#'+rowData.DocStatusColor+';display:block;\">'+rowData.DocStatusName+'</div>",
            'DateValidated'     => "'+(rowData.DateValidated != null ? printDate('".$settings['GENERAL']['DateFormat']."', rowData.DateValidated) : '<b title=\"".htmlspecialchars(t('DocumentNotValidatedDateCreatedDisplayed'), ENT_QUOTES)."\">*</b>' + printDate('".$settings['GENERAL']['DateFormat']."', rowData.DateCreated))+'",
            'ThirdPartyName'    => "<a href=\"".$urlViewCustomer."'+rowData.ThirdPartyID+'\">' + rowData.ThirdPartyName + ' ' + rowData.ThirdPartyFirstname + '</a>",
            'DateDue'           => "<div class=\"'+(rowData.DocStatusDraft == 0 && rowData.DocStatusClosed == 0 && rowData.DateDue < today ? 'text-danger text-bold' : '')+'\">' + printDate('".$settings['GENERAL']['DateFormat']."', rowData.DateDue) + '</div>",
            'Total'             => "<div class=\"amount\">' + currency_format(rowData.Total, '".$settings['GENERAL']['Decimals']."', '".$settings['GENERAL']['DecimalSeparator']."', '', '".$settings['GENERAL']['CurrencySymbol']."', '".$settings['GENERAL']['CurrencyPlacement']."') + '</div>",
            'Action'            => "<div class=\"btn-group text-center\" style=\"min-width:85px\" data-id=\"'+rowData.ID+'\" >
                                    <a role=\"button\" class=\"btn btn-default btn-sm\" href=\"".$urlEditQuote."'+rowData.ID+'\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span> ".t('Edit')."</a>
                                    <button type=\"button\" class=\"btn btn-default btn-sm dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><span class=\"caret\"></span><span class=\"sr-only\">Toggle Dropdown</span></button>
                                    <ul class=\"dropdown-menu dropdown-menu-right\">
                                    <li><a href=\"".$urlPDFQuote."'+rowData.ID+'\"><span class=\"fa fa-print\" aria-hidden=\"true\"></span> ".t('ViewPDF')."</a></li>
                                    <!--<li><a href=\"".$urlSendQuote."'+rowData.ID+'\"><span class=\"glyphicon glyphicon-envelope\" aria-hidden=\"true\"></span> ".t('SendEmail')."</a></li>-->
                                    <li class=\"'+(rowData.DocStatusID!=1?'hidden':'')+'\"><a href=\"".$urlDeleteQuote."'+rowData.ID+'\" style=\"color:red !important;\"><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span> ".t('Delete')."</a></li></ul></div>"
        ),
        '2' => array(
            'Reference'         => "<a href=\"".$urlEditOrder."'+rowData.ID+'\">'+(rowData.Reference == '' ? '".htmlspecialchars(t('Draft'))."' : rowData.Reference)+'</a>",
            'State'             => "<div class=\"label\" style=\"width:100%;background-color:#'+rowData.DocStatusColor+';display:block;\">'+rowData.DocStatusName+'</div>",
            'DateValidated'     => "'+(rowData.DateValidated != null ? printDate('".$settings['GENERAL']['DateFormat']."', rowData.DateValidated) : '<b title=\"".htmlspecialchars(t('DocumentNotValidatedDateCreatedDisplayed'), ENT_QUOTES)."\">*</b>' + printDate('".$settings['GENERAL']['DateFormat']."', rowData.DateCreated))+'",
            'ThirdPartyName'    => "<a href=\"".$urlViewCustomer."'+rowData.ThirdPartyID+'\">' + rowData.ThirdPartyName + ' ' + rowData.ThirdPartyFirstname + '</a>",
            'DateDue'           => "<div class=\"' + (rowData.DocStatusDraft == 0 && rowData.DocStatusClosed == 0 && rowData.DateDue < today ? 'text-danger text-bold' : '') + '\">' +  printDate('".$settings['GENERAL']['DateFormat']."', rowData.DateDue) + '</div>",
            'Total'             => "<div class=\"amount\">' + currency_format(rowData.Total, '".$settings['GENERAL']['Decimals']."', '".$settings['GENERAL']['DecimalSeparator']."', '', '".$settings['GENERAL']['CurrencySymbol']."', '".$settings['GENERAL']['CurrencyPlacement']."') + '</div>",
            'Action'            => "<div class=\"btn-group text-center\" style=\"min-width:85px\" data-id=\"'+rowData.ID+'\" >
                                    <a role=\"button\" class=\"btn btn-default btn-sm\" href=\"".$urlEditOrder."'+rowData.ID+'\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span> ".t('Edit')."</a>
                                    <button type=\"button\" class=\"btn btn-default btn-sm dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><span class=\"caret\"></span><span class=\"sr-only\">Toggle Dropdown</span></button>
                                    <ul class=\"dropdown-menu dropdown-menu-right\">
                                    <li><a href=\"".$urlPDFOrder."'+rowData.ID+'\"><span class=\"fa fa-print\" aria-hidden=\"true\"></span> ".t('ViewPDF')."</a></li>
                                    <!--<li><a href=\"".$urlSendOrder."'+rowData.ID+'\"><span class=\"glyphicon glyphicon-envelope\" aria-hidden=\"true\"></span> ".t('SendEmail')."</a></li>--></ul></div>"
        ),
        '4' => array(
            'Reference'         => "<a href=\"".$urlEditInvoice."'+rowData.ID+'\">'+(rowData.Reference == '' ? '".htmlspecialchars(t('Draft'))."' : rowData.Reference)+'</a>",
            'State'             => "<div class=\"label\" style=\"width:100%;background-color:#'+rowData.DocStatusColor+';display:block;\">'+rowData.DocStatusName+'</div>",
            'DateValidated'     => "'+(rowData.DateValidated != null ? printDate('".$settings['GENERAL']['DateFormat']."', rowData.DateValidated) : '<b title=\"".htmlspecialchars(t('DocumentNotValidatedDateCreatedDisplayed'), ENT_QUOTES)."\">*</b>' + printDate('".$settings['GENERAL']['DateFormat']."', rowData.DateCreated))+'",
            'ThirdPartyName'    => "<a href=\"".$urlViewCustomer."'+rowData.ThirdPartyID+'\">' + rowData.ThirdPartyName + ' ' + rowData.ThirdPartyFirstname + '</a>",
            'DateDue'           => "<div class=\"' + (rowData.DocStatusDraft == 0 && rowData.DocStatusClosed == 0 && rowData.DateDue < today ? 'text-danger text-bold' : '') + '\">' + printDate('".$settings['GENERAL']['DateFormat']."', rowData.DateDue) + '</div>",
            'Total'             => "<div class=\"amount ' + (rowData.DocStatusDraft == 0 && rowData.DocStatusClosed == 0 && rowData.DateDue < today ? 'text-danger text-bold' : '') + '\">' + currency_format(rowData.Total, '".$settings['GENERAL']['Decimals']."', '".$settings['GENERAL']['DecimalSeparator']."', '', '".$settings['GENERAL']['CurrencySymbol']."', '".$settings['GENERAL']['CurrencyPlacement']."') + '</div>",
            'Paid'              => "'+(rowData.Total==rowData.Paid ? '<div class=\"label\" style=\"background-color:#58a959;display:block;\">".htmlspecialchars(t('Paid'))."</div>' : '<div class=\"amount ' + (rowData.DocStatusDraft == 0 && rowData.DocStatusClosed == 0 && rowData.DateDue < today ? 'text-danger text-bold' : '') + '\">' + currency_format(rowData.Total - rowData.Paid, '".$settings['GENERAL']['Decimals']."', '".$settings['GENERAL']['DecimalSeparator']."', '', '".$settings['GENERAL']['CurrencySymbol']."', '".$settings['GENERAL']['CurrencyPlacement']."') + '</div>')+'",
            'Action'            => "<div class=\"btn-group text-center\" style=\"min-width:85px\" data-id=\"'+rowData.ID+'\" >
                                        <a role=\"button\" class=\"btn btn-default btn-sm\" href=\"".$urlEditInvoice."'+rowData.ID+'\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span> ".t('Edit')."</a>
                                        <button type=\"button\" class=\"btn btn-default btn-sm dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><span class=\"caret\"></span><span class=\"sr-only\">Toggle Dropdown</span></button>
                                        <ul class=\"dropdown-menu dropdown-menu-right\">
                                        <li><a href=\"".$urlPDFInvoice."'+rowData.ID+'\"><span class=\"fa fa-print\" aria-hidden=\"true\"></span> ".t('ViewPDF')."</a></li>
                                        <!--<li><a href=\"".$urlSendInvoice."'+rowData.ID+'\"><span class=\"glyphicon glyphicon-envelope\" aria-hidden=\"true\"></span> ".t('SendEmail')."</a></li>-->
                                        <li class=\"'+(rowData.Total == rowData.Paid ? 'hidden' : '')+'\"><a href=\"".$urlNewPayment."'+rowData.ID+'\"><span class=\"fa fa-credit-card\" aria-hidden=\"true\"></span> ".t('EnterPayment')."</a></li></ul></div>"
        ),
        '5' => array(
            'Reference'         => "<a href=\"".$urlEditPurchase."'+rowData.ID+'\">'+(rowData.Reference == '' ? '".htmlspecialchars(t('Draft'))."' : rowData.Reference)+'</a>",
            'State'             => "<div class=\"label\" style=\"width:100%;background-color:#'+rowData.DocStatusColor+';display:block;\">'+rowData.DocStatusName+'</div>",
            'DateValidated'     => "'+(rowData.DateValidated != null ? printDate('".$settings['GENERAL']['DateFormat']."', rowData.DateValidated) : '<b title=\"".htmlspecialchars(t('DocumentNotValidatedDateCreatedDisplayed'), ENT_QUOTES)."\">*</b>' + printDate('".$settings['GENERAL']['DateFormat']."', rowData.DateCreated))+'</div>",
            'ThirdPartyName'    => "<a href=\"".$urlViewSupplier."'+rowData.ThirdPartyID+'\">' + rowData.ThirdPartyName + ' ' + rowData.ThirdPartyFirstname + '</a>",
            'DateReceived'      => "<div class=\"' + (rowData.Received == 0 && rowData.DateReceived != '0000-00-00' && rowData.DateReceived < today ? 'text-danger text-bold' : '') + '\">' + (rowData.DateReceived != '0000-00-00' ? printDate('".$settings['GENERAL']['DateFormat']."', rowData.DateReceived)+rowData.DateReceived : '-') + '</div>",
            'DateDue'           => "<div class=\"' + (rowData.DocStatusDraft == 0 && rowData.DocStatusClosed == 0 && rowData.Total != rowData.Paid && rowData.DateDue < today ? 'text-danger text-bold' : '') + '\">' + printDate('".$settings['GENERAL']['DateFormat']."', rowData.DateDue) + '</div>",
            'Total'             => "<div class=\"amount ' + (rowData.DocStatusDraft == 0 && rowData.DocStatusClosed == 0 && rowData.Total != rowData.Paid && rowData.DateDue < today ? 'text-danger text-bold' : '') + '\">' + currency_format(rowData.Total, '".$settings['GENERAL']['Decimals']."', '".$settings['GENERAL']['DecimalSeparator']."', '', '".$settings['GENERAL']['CurrencySymbol']."', '".$settings['GENERAL']['CurrencyPlacement']."') + '</div>",
            'Paid'              => "'+(rowData.Total==rowData.Paid ? '<div class=\"label\" style=\"background-color:#58a959;display:block;\">".htmlspecialchars(t('Paid'))."</div>' : '<div class=\"amount ' + (rowData.DocStatusDraft == 0 && rowData.DocStatusClosed == 0 && rowData.Total != rowData.Paid && rowData.DateDue < today ? 'text-danger text-bold' : '') + '\">' + currency_format(rowData.Total - rowData.Paid, '".$settings['GENERAL']['Decimals']."', '".$settings['GENERAL']['DecimalSeparator']."', '', '".$settings['GENERAL']['CurrencySymbol']."', '".$settings['GENERAL']['CurrencyPlacement']."') + '</div>')+'",
            'Action'            => "<div class=\"btn-group text-center\" style=\"min-width:85px\" data-id=\"'+rowData.ID+'\" >
                                        <a role=\"button\" class=\"btn btn-default btn-sm\" href=\"".$urlEditPurchase."'+rowData.ID+'\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span> ".t('Edit')."</a>
                                        <button type=\"button\" class=\"btn btn-default btn-sm dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><span class=\"caret\"></span><span class=\"sr-only\">Toggle Dropdown</span></button>
                                        <ul class=\"dropdown-menu dropdown-menu-right\">
                                        <li><a href=\"".$urlPDFPurchase."'+rowData.ID+'\"><span class=\"fa fa-print\" aria-hidden=\"true\"></span> ".t('ViewPDF')."</a></li>
                                        <li class=\"'+(rowData.Total == rowData.Paid ? 'hidden' : '')+'\"><a href=\"".$urlNewPayment."'+rowData.ID+'\"><span class=\"fa fa-credit-card\" aria-hidden=\"true\"></span> ".t('EnterPayment')."</a></li>
                                        <li class=\"'+(rowData.Received == 0? 'hidden' : '')+'\"><a href=\"".$urlReceive."'+rowData.ID+'\"><span class=\"fa fa-truck\" aria-hidden=\"true\"></span> ".t('Receive')."</a></li></ul></div>"
        )
    );
    HLX_View::hlxRepeater($rOptions);
?>

<script>
<!--

-->
</script>

<?php include 'footer.php';