<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

<div id="headerbar" style="position:fixed;width:100%;background-color:white;z-index:99;">

    <div class="pull-left">
        <h1><?=t('ProductInformations')?></h1>
    </div>

    <div class="pull-right">
        <a href="<?=$urlDeleteProduct?>" onclick="return confirm('<?=t('AreYouSure')?>')" class="btn btn-sm btn-danger">
            <i class="fa fa-remove"></i> <?=t('Delete')?>
        </a>
        <a href="<?=$urlEditProduct?>" class="btn btn-sm btn-primary">
            <i class="fa fa-edit"></i> <?=t('Edit')?>
        </a>
    </div>

</div>

<div class="container-fluid" style="padding-top:45px;">

    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <h3><?=(($item['Active']==1)?'<span class="glyphicon glyphicon-ok text-success"></span>':'<span class="glyphicon glyphicon-remove text-danger"></span>')?> <?=$item['Reference'].' '.$item['Name']?></h3>

            <br />

            <p>
                <?=nl2br($item['Description'])?><br />
            </p>

            <br />

            <div class="row">
                <div class="col-xs-6">
                    <table class="table table-condensed table-striped">
                        <tr>
                            <td><?=t('Type')?></td>
                            <td><?=($item['Type'] == 1 ? '<span class="fa fa-cube"></span> '.t('Product') : '<span class="fa fa-hand-paper-o"></span> '.t('Service'))?></td>
                        </tr>
                        <tr >
                            <td><?=t('OnSale')?></td>
                            <td><?=($item['OnSale'] == 1 ? '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>')?></a></td>
                        </tr>
                        <tr>
                            <td><?=t('Unity')?></td>
                            <td><?=$item['UnityValue']?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <hr />

    <div class="row">

        <div class="col-xs-12 col-sm-6">
            <fieldset>
                <legend><?=t('Sell')?></legend>
                <table class="table table-condensed table-striped">
                    <tr class="<?=($settings['GENERAL']['UseVAT'] == 0 ? 'hidden' : '')?>">
                        <td><?=t('PriceExclTax')?></td>
                        <td class="amount"><?=currency_format($item['Price'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></td>
                    </tr>
                    <tr class="<?=($settings['GENERAL']['UseVAT'] == 0 ? 'hidden' : '')?>">
                        <td><?=t('VAT')?></td>
                        <td class="text-right"><?=$item['TaxName']?></a></td>
                    </tr>
                    <tr>
                        <td><?=t('PriceInclTax')?></td>
                        <td class="amount"><?=currency_format($item['PriceTTC'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></td>
                    </tr>
                </table>
            </fieldset>
        </div>

        <div class="col-xs-12 col-sm-6">
            <fieldset>
                <legend><?=t('Buy')?></legend>
                <table class="table table-condensed table-striped">
                    <tr class="<?=($settings['GENERAL']['UseVAT'] == 0 ? 'hidden' : '')?>">
                        <td><?=t('PriceExclTax')?></td>
                        <td class="amount"><?=currency_format($item['PurchasePrice'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></td>
                    </tr>
                    <tr class="<?=($settings['GENERAL']['UseVAT'] == 0 ? 'hidden' : '')?>">
                        <td><?=t('VAT')?></td>
                        <td class="text-right"><?=$item['PurchaseTaxName']?></a></td>
                    </tr>
                    <tr>
                        <td><?=t('PriceInclTax')?></td>
                        <td class="amount"><?=currency_format($item['PurchasePriceTTC'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></td>
                    </tr>
                </table>
            </fieldset>
        </div>

    </div>

</div>

<script>
<!--
    $(document).keyup(function(e)
    {
        if ($('input:focus').length > 0 || $('textarea:focus').length > 0)
        {
            return false;
        }

        if (e.keyCode == true)
        {
            var key = e.keyCode;
        }
        else
        {
            var key = e.which;
        }

        switch (key)
        {
            case 113://F2
                window.location.href = "<?=$urlEditProduct?>";
                return false;
                break;
        }
    });
-->
</script>

<?php include 'footer.php';