<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

<style type="text/css">

    #paymentList .note {
        position: absolute;
        top: 5px;
        width: 95%;
        height: 80%;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .repeater-list-wrapper > table.table td {
        position: relative;
    }

</style>

<?php
    $rOptions = array();
    //Repeater Id
    $rOptions['id'] = 'paymentList';
    //url
    $rOptions['url'] = $urlLoadList;
    //Default Type
    $rOptions['type'] = 'payments';
    //Array Types
    $rOptions['arrTypes'] = array();
    //default filter
    $rOptions['filter'] = '';
    //Array Filters
    $rOptions['arrFilters'] = array();
    //Settings for action button
    $rOptions['actionSettings'] = array(
        'label' => t('New'),
        'icon' => 'plus'
    );
    //Array Actions
    $rOptions['arrActions'] = array(
        0 => array(
            'label'     => t('New').' '.t('Payment'),
            'url'       => $urlNewPayment
        )
    );
    //Object contenant les descriptions des colonnes en fonction du type
    $rOptions['arrColumns'] = array(
        'payments' => array(
            array(
                'label'     => t('ID'),
                'property'  => 'ID',
                'className' => 'valign-middle',
                'width'     => 50,
                'sortable'  => true
            ),
            array(
                'label'     => t('PaymentDate'),
                'property'  => 'Date',
                'className' => 'valign-middle',
                'width'     => 150,
                'sortable'  => true
            ),
            array(
                'label'     => t('Invoice'),
                'property'  => 'DocumentReference',
                'className' => 'valign-middle',
                'width'     => 105,
                'sortable'  => true
            ),
            array(
                'label'     => t('InvoiceDate'),
                'property'  => 'DocumentDateCreated',
                'className' => 'valign-middle',
                'width'     => 150,
                'sortable'  => true
            ),
            array(
                'label'     => t('Customer'),
                'property'  => 'ThirdPartyName',
                'className' => 'valign-middle',
                'sortable'  => true
            ),
            array(
                'label'     => t('Amount'),
                'property'  => 'Amount',
                'className' => 'valign-middle',
                'width'     => 120,
                'sortable'  => false
            ),
            array(
                'label'     => t('PaymentMethod'),
                'property'  => 'PaymentMethodName',
                'className' => 'valign-middle text-center',
                'width'     => 200,
                'sortable'  => true
            ),
            array(
                'label'     => t('Note'),
                'property'  => 'Note',
                'sortable'  => true
            ),
            array(
                'label'     => t('Action'),
                'property'  => 'Action',
                'className' => 'valign-middle text-center',
                'width'     => 115,
                'sortable'  => false
            )
        )
    );

    //View render
    $rOptions['arrColumnsRenderer'] = array(
        'payments' => array(
            'ID'                    => "<div class=\"text-center\"><a href=\"".$urlViewPayment."'+rowData.ID+'\">#'+rowData.ID+'</a></div>",
            'PaymentMethodName'     => "<div class=\"text-center\">'+rowData.PaymentMethodName+'</div>",
            'Date'                  => "<div class=\"text-center\">' + printDate('".$settings['GENERAL']['DateFormat']."', rowData.Date) + '</div>",
            'DocumentReference'     => "<div class=\"text-center\"><a href=\"'+(rowData.DocTypeID == 4 ? '".$urlViewInvoice."' : '".$urlViewPurchase."')+rowData.DocumentID+'\">'+rowData.DocumentReference+'</a></div>",
            'DocumentDateCreated'   => "<div class=\"text-center\">' + printDate('".$settings['GENERAL']['DateFormat']."', rowData.DocumentDateCreated) + '</div>",
            'ThirdPartyName'        => "<a href=\"'+(rowData.DocTypeID != 5 ? '".$urlViewCustomer."' : '".$urlViewSupplier."')+rowData.ThirdPartyID+'\">' + rowData.ThirdPartyName + ' ' + rowData.ThirdPartyFirstname + '</a>",
            'Amount'                => "<div class=\"amount\">'+(rowData.DocTypeID == 5 ? '- ' : '')+ currency_format(rowData.Amount, '".$settings['GENERAL']['Decimals']."', '".$settings['GENERAL']['DecimalSeparator']."', '', '".$settings['GENERAL']['CurrencySymbol']."', '".$settings['GENERAL']['CurrencyPlacement']."') + '</div>",
            'Note'                  => "<div class=\"note\">'+nl2br(rowData.Note)+'</div>",
            'Action'                => "<a role=\"button\" class=\"btn btn-default btn-sm\" href=\"".$urlViewPayment."'+rowData.ID+'\"><span class=\"glyphicon glyphicon-eye-open\" aria-hidden=\"true\"></span> ".t('View')."</a>",
        )
    );
    HLX_View::hlxRepeater($rOptions);
?>

<script>
<!--

    $(document).keyup(function(e)
    {
        if ($('input:focus').length > 0 || $('textarea:focus').length > 0)
        {
            return false;
        }

        if (e.keyCode == true)
        {
            var key = e.keyCode;
        }
        else
        {
            var key = e.which;
        }

        switch (key)
        {
            case 113://F2
                window.location.href = "<?=$urlNewPayment?>";
                return false;
                break;
        }
    });

-->
</script>

<?php include 'footer.php';