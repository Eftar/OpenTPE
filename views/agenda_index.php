<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

<style>
#calendar {
}

.fc th {
    height: 34px;
    vertical-align: middle;
    background-color:#969696 !important;
    color: white !important;
}

.fc-sun { background-color:#D9D9D9; }
.fc-sat { background-color:#D9D9D9;  }

.fc-toolbar {
margin:0;
padding: 10px;
}

.fc-toolbar h2 {
    font-size:19px;
}
</style>

<!-- Tab content-->
<div class="tab-content">

    <div id="repeater" class="repeater">
        <div class="repeater-header">
                <div class="repeater-header-left">

                    <div class="pull-left">
                        <button type="button" class="btn btn-primary btn-sm">
                            <?=t('Customers')?>
                        </button>
                        <button type="button" class="btn btn-default btn-sm" style="border: none;color:#1E90FF">
                            <?=t('Contacts')?>
                        </button>
                        <button type="button" class="btn btn-default btn-sm" style="border: none;color:#1E90FF">
                            <?=t('Suppliers')?>
                        </button>
                    </div>

                </div>
                <div class="repeater-header-right">

                    <div class="btn-group">
                        <button type="button" class="btn btn-primary btn-sm">
                            <span class="glyphicon glyphicon-plus"></span>
                            <?=t('Create new event')?>
                        </button>
                    </div>

                </div>
            </div>
        <div id="calendar">
        </div>
    </div>

</div>

<script>
<!--
    $('#calendar').fullCalendar({
        height: 650,
        header: {
            left: 'today prev,next',
            center: 'title',
            right: 'agendaDay,agendaWeek,month'
        },
        defaultDate: '2015-02-12',
        lang: 'fr',
        buttonIcons: true, // show the prev/next text
        weekNumbers: true,
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: [
            {
                title: 'All Day Event',
                start: '2015-02-01'
            },
            {
                title: 'Long Event',
                start: '2015-02-07',
                end: '2015-02-10'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: '2015-02-09T16:00:00'
            },
            {
                id: 999,
                title: 'Repeating Event',
                start: '2015-02-16T16:00:00'
            },
            {
                title: 'Conference',
                start: '2015-02-11',
                end: '2015-02-13'
            },
            {
                title: 'Meeting',
                start: '2015-02-12T10:30:00',
                end: '2015-02-12T12:30:00'
            },
            {
                title: 'Lunch',
                start: '2015-02-12T12:00:00'
            },
            {
                title: 'Meeting',
                start: '2015-02-12T14:30:00'
            },
            {
                title: 'Happy Hour',
                start: '2015-02-12T17:30:00'
            },
            {
                title: 'Dinner',
                start: '2015-02-12T20:00:00'
            },
            {
                title: 'Birthday Party',
                start: '2015-02-13T07:00:00'
            },
            {
                title: 'Click for Google',
                url: 'http://google.com/',
                start: '2015-02-28'
            }
        ]
    });

    $('.fc-button').className ="btn btn-default btn-sm";

    //Add event onResize
    var onResize = function() {
        //$('.repeater-list-wrapper').css('height',  jQuery(window).height() - 95 -85 + 'px');
        $('#calendar').fullCalendar('option', 'height', jQuery(window).height() - 120 -10);
    }

    $(window).resize(onResize);

    onResize();

-->
</script>

<?php include 'footer.php';