<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

<style>

</style>

<div id="headerbar" style="position:fixed;width:100%;background-color:white;z-index:99;">
    <h1><?=t('Reports')?></h1>
</div>

<div style="padding-top: 45px;">
    <table id="reportTable" class="table table-condensed table-bordered" >
        <thead>
            <tr>
                <th><?=t('Report')?></th>
                <th><?=t('Description')?></th>
                <th style="width:110px;"><?=t('Action')?></th>
            </tr>
        </thead>

        <tbody class="item">
            <?php foreach ($rReports as $report) { ?>
            <tr data-id="<?=$report['ID']?>" data-variables=<?=json_encode($report['Variables'], true)?>>
                <td><?=$report['Name']?></td>
                <td><?=$report['Description']?></td>
                <td class="text-center"><a class="btn btn-sm btn-default btnGenerate" ><?=t('Generate')?></a></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="reportModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="reportForm" action="" method="post" target="_blank">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?=t('ReportSettings')?></h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?=t('Cancel')?></button>
                    <input type="submit" class="btn btn-primary" value="<?=t('Generate')?>" />
                </div>
            <form>
        </div>
    </div>
</div>

<script>
<!--
    $('#reportTable .item .btnGenerate').click(function() {
        var tr = $(this).closest('tr'),
            id = tr.data('id'),
            variables = tr.data('variables');

        if (variables && variables.length) {
            $('#reportForm').attr('action', '<?=$urlGenerateReport?>'+id);
            $('#reportModal .modal-body').html('');
            $('#reportModal').modal('show');
            $('#reportModal .modal-body').load('<?=$urlSettingReport?>'+id);
        }
        else {
            window.open('<?=$urlGenerateReport?>'+id, '_blank');
        }
    });
-->
</script>

<?php include 'footer.php';