<!DOCTYPE html>
<html lang="<?=BFWK_LANG?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=HLX_NAME?></title>
    <link rel="icon" type="image/png" href="<?=BFWK_SERVER_ROOT?>favicon.png" />
    <link type="text/css" href="views/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="views/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="views/css/hlx.css" type="text/css" />
    <script type="text/javascript" src="views/js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="views/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="views/js/functions.js"></script>
</head>

<body>

<div class="container">
    <div class="panel panel-default">
    <div class="panel-body">

        <h1><img src="<?=BFWK_SERVER_ROOT?>views/img/logo.png" style="display: inline-block;margin-top: -5px;" /> <?=HLX_NAME?></h1>

        <form method="post" action="<?=$urlForm?>">

            <legend><?=t('SetupChooseLanguage')?></legend>

            <div class="row">
                <div class="col-xs-6">
                    <select name="lang" class="form-control input-sm">
                        <?php foreach ($rLlanguage as $code => $language) { ?>
                            <option value="<?=$code?>" <?=($code == 'FR' ?'selected="selected"' : '')?>><?=ucfirst($language)?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <br/>

            <input class="btn btn-primary" type="submit" value="<?=t('Continue')?>">
            <br />
            <br />

        </form>

    </div>
    </div>
</div>