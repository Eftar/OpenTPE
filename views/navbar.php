<!-- Nav Bar -->
<!--<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">-->
<nav class="navbar navbar-default navbar-static-top navbar-inverse navbar-fixed-top" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only"><?=t('Toggle navigation')?></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand <?=($navBar['active']=='home'?'active':'')?>" href="<?=$navBar['urlHome']?>" style="padding:8px 5px; 5px 10px; ">
            <img src="views/img/logo-white.png" alt="<?=HLX_NAME?>">
        </a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li <?=($navBar['active']=='home'?'class="active"':'')?> ><a href="<?=$navBar['urlHome']?>"><?=t('Dashboard')?></a></li>
            <li <?=($navBar['active']=='thirdParty'?'class="active"':'')?> ><a href="<?=$navBar['urlThirdParty']?>"><?=t('Third party')?></a></li>
    <!--        <li <?=($navBar['active']=='relation'?'class="active"':'')?> ><a href="<?=$navBar['urlRelation']?>"><?=t('Relation')?></a></li>-->
            <li <?=($navBar['active']=='document'?'class="active"':'')?> ><a href="<?=$navBar['urlDocument']?>"><?=t('Documents')?></a></li>
            <li <?=($navBar['active']=='payment'?'class="active"':'')?> ><a href="<?=$navBar['urlPayment']?>"><?=t('Payments')?></a></li>
            <li <?=($navBar['active']=='product'?'class="active"':'')?> ><a href="<?=$navBar['urlProduct']?>"><?=t('Products')?></a></li>
    <!--        <li <?=($navBar['active']=='agenda'?'class="active"':'')?> ><a href="<?=$navBar['urlAgenda']?>"><?=t('Agenda')?></a></li>-->
            <li <?=($navBar['active']=='report'?'class="active"':'')?> ><a href="<?=$navBar['urlReport']?>"><?=t('Reports')?></a></li>
    <!--        <li <?=($navBar['active']=='impexp'?'class="active"':'')?> ><a href="<?=$navBar['urlImpexp']?>"><?=t('Import/Export')?></a></li>-->
            <li <?=($navBar['active']=='impexp'?'class="active"':'')?> >
                 <a class="dropdown-toggle" data-toggle="dropdown" title="<?=t('New')?>" href="#">
                    <span class="glyphicon glyphicon-plus"></span>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="<?=$navBar['urlNewCustomer']?>"><?=t('New Customer')?></a>
                    </li>
                    <li>
                        <a href="<?=$navBar['urlNewSupplier']?>"><?=t('New Supplier')?></a>
                    </li>
                    <!--<li>
                        <a href="javascript:void(0); newContact();"><?=t('New Contact')?></a>
                    </li>-->
                    <li>
                        <a href="javascript:void(0); newQuote();"><?=t('New Quote')?></a>
                    </li>
                    <li>
                        <a href="javascript:void(0); newOrder();"><?=t('New Order')?></a>
                    </li>
                    <li>
                        <a href="javascript:void(0); newInvoice();"><?=t('New Invoice')?></a>
                    </li>
                    <li>
                        <a href="javascript:void(0); newPurchase();"><?=t('New Purchase')?></a>
                    </li>
                    <li>
                        <a href="<?=$navBar['urlNewProduct']?>"><?=t('New Product')?></a>
                    </li>
                    <li>
                        <a href="<?=$navBar['urlNewPayment']?>"><?=t('New Payment')?></a>
                    </li>
                </ul>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown <?=(in_array($navBar['active'], array('documentation', 'mynote'))?'active':'')?>">
                <a class="dropdown-toggle" data-toggle="dropdown" title="<?=t('Help')?>" href="#">
                    <span class="glyphicon glyphicon-question-sign"></span>
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li <?=($navBar['active']=='documentation'?'class="active"':'')?>>
                        <a href="<?=$navBar['urlDocumentation']?>"><?=t('Documentation')?></a>
                    </li>
                    <li <?=($navBar['active']=='myNote'?'class="active"':'')?>>
                        <a href="<?=$navBar['urlMyNote']?>"><?=t('MyNote')?></a>
                    </li>
                    <li <?=($navBar['active']=='about'?'class="active"':'')?>>
                        <a href="<?=$navBar['urlAbout']?>"><?=t('About')?></a>
                    </li>
                </ul>
            </li>
            <?php if ($navBar['isAdmin'] == true) { ?>
                <li class="dropdown <?=(in_array($navBar['active'], array('setting', 'user'))?'active':'')?>">
                    <a class="dropdown-toggle" data-toggle="dropdown" title="<?=t('Settings')?>" href="#">
                        <span class="glyphicon glyphicon-cog"></span>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li <?=($navBar['active']=='setting'?'class="active"':'')?>>
                            <a href="<?=$navBar['urlSetting']?>"><?=t('Settings')?></a>
                        </li>
                        <li <?=($navBar['active']=='user'?'class="active"':'')?>>
                            <a href="<?=$navBar['urlUser']?>"><?=t('Users')?></a>
                        </li>
                    </ul>
                </li>
            <?php } ?>
            <li>
                <a title="<?=t('Disconnect')?>" href="<?=$navBar['urlLogout']?>">
                    <?=t('Disconnect')?>&nbsp;
                    <span class="glyphicon glyphicon-off"></span>&nbsp;&nbsp;&nbsp;
                </a>
            </li>
        </ul>
    </div>
</nav>

<?php include 'item_new.php'; ?>

<?=((isset($msg) && $msg!='')?'<div class="alert alert-info" style="margin-bottom:0">'.$msg.'</div>':'')?>
<?=((isset($err) && $err!='')?'<div class="alert alert-danger" style="margin-bottom:0">'.$err.'</div>':'')?>