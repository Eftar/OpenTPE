<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

<script type="text/javascript" src="views/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="views/js/bootstrap-datepicker-lang.min.js"></script>
<script type="text/javascript" src="views/js/sortable.min.js"></script>
<link rel="stylesheet" href="views/css/bootstrap-datepicker3.min.css" type="text/css" />

<style type="text/css">

    #docProductsList .description {
        position: absolute;
        top: 5px;
        width: 95%;
        height: 80%;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .repeater-list-wrapper > table.table td {
        position: relative;
    }

</style>

<!--Use hidden instead of display:none for UseDiscount / UseAmount-->

<form action="<?=$urlSave?>" id="formDocument" onsubmit="return false" method="POST">

    <input type="hidden" name="ID" id="ID" value="<?=$item['ID']?>" />
    <input type="hidden" name="DocTypeID" value="<?=$item['DocTypeID']?>" />
    <input type="hidden" name="DocStatusReadOnly" id="DocStatusReadOnly" value="<?=$item['DocStatusReadOnly']?>" />
    <input type="hidden" name="type" id="type" value="<?=$type?>" />
    <input type="hidden" name="new" value="<?=($new == true ? '1' : '0')?>" />
    <input type="hidden" name="ThirdPartyID" value="<?=$item['ThirdPartyID']?>" />
    <input type="submit" name="submit" style="display:none;" />

    <div id="headerbar" style="position:fixed;width:100%;background-color:white;z-index:99;">

        <div class="pull-left">
            <h1><?=t('Edit').' '.t('purchase').' - <span id="spanReference">'.$item['Reference'].'</span> - '.$item['ThirdPartyName']?></h1>
        </div>

        <div class="pull-right btn-group">
            <?php if ($new == true) { ?>
                <a class="btn btn-sm btn-default" onclick="history.back();return false;">
                    <i class="glyphicon glyphicon-remove"></i> <?=t('Cancel')?>
                </a>
            <?php } else { ?>
                <a class="btn btn-sm btn-default" onclick="history.back();return false;">
                    <i class="glyphicon glyphicon-chevron-left"></i> <?=t('Back')?>
                </a>
            <?php } ?>
            <?php if ($item['DocStatusReadOnly'] == 0) { ?>
                <button class="btn btn-sm btn-primary" onclick="postForm()">
                    <i class="fa fa-edit"></i> <?=t('Save')?>
                </button>
            <?php } ?>
        </div>

        <?php if ($new == false) { ?>

            <div class="pull-right btn-group hidden-sm hidden-xs" style="margin-right:10px">
                <a href="<?=$urlViewPDF?>" class="btn btn-sm btn-default">
                    <i class="fa fa-print"></i> <?=t('ViewPDF')?>
                </a>
                <button onclick="window.location='<?=$urlNewPayment?>';return false;" <?=($item['Total'] == $item['Paid'] ? 'class="btn btn-sm btn-default disabled" disabled="disabled" title="'.t('AlreadyPaid').'"' : 'class="btn btn-sm btn-default"')?>>
                    <i class="fa fa-credit-card"></i> <?=t('EnterPayment')?>
                </button>
                <a href="<?=$urlReceive?>" <?=($item['Received'] == 1 ? 'class="btn btn-sm btn-default disabled" disabled="disabled" title="'.t('AlreadyReceived').'"' : 'class="btn btn-sm btn-default"')?>>
                    <i class="fa fa-truck"></i> <?=t('Receive')?>
                </a>
                <button onclick="window.location='<?=$urlDeletePurchase?>';return false;" <?=($item['DocStatusID'] != 1 ? 'class="btn btn-sm btn-danger disabled" disabled="disabled" title="'.t('DisallowedActionOnlyDraftDocumentCanBeDeleted').'"' : 'class="btn btn-sm btn-danger"')?>>
                    <i class="glyphicon glyphicon-remove"></i> <?=t('Delete')?>
                </button>
            </div>

            <div class="pull-right visible-sm-block visible-xs-block" style="margin-right:10px">
                <div class="btn-group btn-sm" style="padding:0" data-resize="auto">
                    <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
                        <span class="selected-label"><?=t('Actions')?></span>
                        <span class="caret"></span>
                        <span class="sr-only"><?=t('ToggleAction')?></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="<?=$urlViewPDF?>"><i class="fa fa-print"></i> <?=t('ViewPDF')?></a></li>
                        <li <?=($item['Total'] == $item['Paid'] ? 'class="disabled"' : '')?>><a href="<?=($item['Total'] == $item['Paid'] ? 'javascript:void(0);' : $urlNewPayment)?>" title="<?=($item['Total'] == $item['Paid'] ? t('AlreadyPaid') : '')?>"><i class="fa fa-credit-card"></i> <?=t('EnterPayment')?></a></li>
                        <li <?=($item['Received'] == 1 ? 'class="disabled"' : '')?>><a href="<?=($item['Received'] == 1 ? 'javascript:void(0);' : $urlReceive)?>" <?=($item['Received'] == 1 ? 'title="'.t('AlreadyReceived').'"' : '')?>><i class="fa fa-truck"></i> <?=t('Receive')?></a></li>
                        <li <?=($item['DocStatusID'] != 1 ? 'class="disabled"' : '')?>><a href="<?=($item['DocStatusID'] != 1 ? 'javascript:void(0);' : $urlDeletePurchase)?>" <?=($item['DocStatusID'] != 1 ? 'title="'.t('DisallowedActionOnlyDraftDocumentCanBeDeleted').'"' : '')?>><i class="glyphicon glyphicon-remove"></i> <?=t('Delete')?></a></li>
                    </ul>
                </div>
            </div>

        <?php } ?>

        <?php if ($item['DocStatusReadOnly'] == 1) { ?>
            <div class="pull-right" style="margin:4px 10px 0 0;">
                <span class="label label-warning"><?=t('ReadOnly')?></span>
            </div>
        <?php } ?>

    </div>

    <div class="container-fluid" style="padding-top: 45px;">

        <div class="row">
            <div class="col-xs-12 col-sm-8" style="margin-bottom:15px;">

                <ul id="documentsTabs" class="nav nav-tabs nav-tabs-noborder" style="margin-left:-15px;margin-top:0px;margin-right:-15px;">
                    <li class="active"><a href="#general" data-id="documentInfo" data-toggle="tab"><?=t('General')?></a></li>
                    <?=($settings['GENERAL']['UseBillingAddress'] == 1 ? '<li><a href="#invoice" data-id="documentInfo" data-toggle="tab">'.t('Invoice').'</a></li>':'')?>
                </ul>

                <div class="tab-content">

                    <div role="tabpanel" class="tab-pane active" id="general">

                        <h2>
                            <a href="<?=$urlViewSupplier.$item['ThirdPartyID']?>"><?=trim($item['ThirdPartyName'].' '.$item['ThirdPartyFirstname'])?></a>
                            <a class="small" style="font-size:0.5em;color:#337ab7" href="<?=$urlEditSupplier.$item['ThirdPartyID']?>"><span class="glyphicon glyphicon-pencil"></span></a>
                        </h2>

                        <?=(($item['ThirdPartyAddress1'] != '') ? $item['ThirdPartyAddress1'].'<br />' : '')?>
                        <?=(($item['ThirdPartyAddress2'] != '') ? $item['ThirdPartyAddress2'].'<br />' : '')?>
                        <?=(($item['ThirdPartyAddress3'] != '') ? $item['ThirdPartyAddress3'].'<br />' : '')?>
                        <?=$item['ThirdPartyPostalCode'].' '.$item['ThirdPartyCity']?><br />
                        <?=HLX_Country::getCountryName($item['ThirdPartyCountryID'])?><br /><br />

                        <strong><?=t('Phone')?> :</strong> <?=$item['ThirdPartyPhone']?><br />
                        <strong><?=t('Email')?> :</strong> <?=$item['ThirdPartyEmail']?><br />

                    </div>

                    <div role="tabpanel" class="tab-pane" id="invoice">

                        <br />
                        <div class="row">
                            <div class="form-group col-lg-8 col-md-6 col-sm-12">
                                <label><?=t('Name')?> :</label>
                                <input type="text" name="InvoiceName" class="form-control input-sm" value="<?=$item['InvoiceName']?>" />
                            </div>
                            <div class="form-group col-lg-4 col-md-6 col-sm-12">
                                <label><?=t('Firstname')?> :</label>
                                <input type="text" name="InvoiceFirstname" class="form-control input-sm" value="<?=$item['InvoiceFirstname']?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label><?=t('Address')?> :</label>
                            <input type="text" name="InvoiceAddress1" class="form-control input-sm" value="<?=$item['InvoiceAddress1']?>" />
                            <input type="text" name="InvoiceAddress2" class="form-control input-sm" value="<?=$item['InvoiceAddress2']?>" />
                            <input type="text" name="InvoiceAddress3" class="form-control input-sm" value="<?=$item['InvoiceAddress3']?>" />
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-3 col-sm-4">
                                <label><?=t('PostalCode')?> :</label>
                                <input type="text" name="InvoicePostalCode" class="form-control input-sm" value="<?=$item['InvoicePostalCode']?>" />
                            </div>

                            <div class="form-group col-lg-9 col-sm-8">
                                <label><?=t('City')?> :</label>
                                <input type="text" name="InvoiceCity" class="form-control input-sm" value="<?=$item['InvoiceCity']?>" />
                            </div>
                         </div>

                         <div class="row">
    <!--
                            <div class="form-group col-xs-6">
                                <label><?=t('State')?> :</label>
                                <input type="text" name="InvoiceState" class="form-control input-sm" value="<?=$item['InvoiceState']?>" />
                            </div>
    -->
                            <div class="form-group col-xs-6">
                                <label><?=t('Country')?> :</label>
                                <select id="InvoiceCountryID" name="InvoiceCountryID" class="selectpicker" data-size="10" data-width="100%" data-live-search="true">
                                    <option value="" ><?=t('Select')?></option>
                                    <?php foreach($countries as $code => $country) { ?>
                                        <option value="<?=$code?>" <?=($code==$item['InvoiceCountryID'] ? 'selected="selected"' : '')?>><?=htmlspecialchars($country)?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

            <div class="col-xs-12 col-sm-4">

                <div class="row">

                    <div class="col-xs-12 col-sm-12">

                        <fieldset style="margin-top:11px;">
                            <legend><?=t('Settings')?></legend>

                            <div class="form-group">
                                <label for="quote_number">
                                    <?=t('Purchase')?> #
                                </label>
                                <input type="text" id="Reference" name="Reference" class="form-control input-sm" readonly="readonly" value="<?=$item['Reference']?>">
                            </div>

                            <div class="form-group">
                                <label for="ExternalReference"><?=t('ExternalReference')?></label>
                                <input type="text" class="form-control input-sm" id="ExternalReference" name="ExternalReference" value="<?=$item['ExternalReference']?>" />
                            </div>
<!--
                            <div class="form-group">
                                <label for="DateValidated"><?=t('CreatedDate')?></label>
                                <input type="text" class="form-control input-sm" readonly="readonly" id="DateCreated" name="DateCreated" value="<?=printDate($settings['GENERAL']['DateFormat'], $item['DateCreated'])?>" />
                            </div>
-->
                            <div class="form-group">
                                <label for="DateCreated"><?=t('ValidatedDate')?></label>
                                <input type="text" class="form-control input-sm" readonly="readonly" id="DateValidated" name="DateValidated" value="<?=($item['DateValidated'] ? printDate($settings['GENERAL']['DateFormat'], $item['DateValidated']) : '')?>" />
                            </div>

                            <div class="form-group">
                                <label for="DocStatusID">
                                    <?=t('Status')?>
                                </label>
                                <select name="DocStatusID" id="DocStatusID" style="color:#fff;"class="form-control input-sm">
                                    <?php foreach($docStatutes as $status) { ?>
                                        <option style="border-left:10px solid #<?=$status['Color']?>;background:#fff;color:#000;padding:5px 10px;" data-color="<?=$status['Color']?>" value="<?=$status['ID']?>" <?=($item['DocStatusID']==$status['ID']?'selected="selected"':'')?>  ><?=$status['Name']?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group" style="<?=($settings['GENERAL']['UseVAT'] == 0 ? 'display:none;' : '')?>">
                                <label for="TaxID">
                                    <?=t('VAT')?>
                                </label>
                                <select name="TaxID" id="TaxID" onchange="updateDocument()" class="form-control input-sm">
                                    <?php foreach($taxes as $taxe) { ?>
                                        <option value="<?=$taxe['ID']?>" <?=($item['TaxID']==$taxe['ID']?'selected="selected"':'')?> data-rate="<?=$taxe['Rate']?>" ><?=$taxe['Name']?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="DateDue"><?=t('DueDate')?></label>
                                <div class="input-group date datepicker">
                                    <input type="text" class="form-control input-sm" id="DateDue" name="DateDue" value="<?=printDate($settings['GENERAL']['DateFormat'], $item['DateDue'])?>" ><span class="input-group-addon input-sm "><i class="fa fa-calendar fa-fw"></i></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="DateReceived"><?=t('ReceiptDate')?></label>
                                <div class="input-group date datepicker">
                                    <input type="text" class="form-control input-sm" id="DateReceived" name="DateReceived" value="<?=($item['DateReceived'] ? printDate($settings['GENERAL']['DateFormat'], $item['DateReceived']) : '')?>" ><span class="input-group-addon input-sm "><i class="fa fa-calendar fa-fw"></i></span>
                                </div>
                            </div>
                            <br />
                        </fieldset>

                    </div>

                </div>

            </div>

        </div>

        <div id="divLinesTable">

            <table id="linesTable" class="table table-condensed table-bordered" >
                <thead>
                    <tr>
                        <th style="width:28px;"></th>
                        <th><?=t('ItemDescrtiption')?></th>
                        <th style="width:110px;"><?=t('QtyUnity')?></th>
                        <th style="width:100px;"><?=t('Price')?></th>
                        <th style="width:100px;<?=($settings['GENERAL']['UseVAT'] == 0 ? 'display:none;' : '')?>"><?=t('VAT')?></th>
                        <th style="width:110px;"><?=t('Total')?></th>
                    </tr>
                </thead>

                <?php foreach($item['Lines'] as $line) { ?>

                    <tbody class="item">
                        <tr id="item_tr-<?=$line['Number']?>" data-number="<?=$line['Number']?>" class="tr1">
                            <td rowspan="2" class="sorter"><i class="fa fa-arrows cursor-move"></i></td>
                            <td rowspan="2">
                                <div class="btn-group-xs btn-group-vertical btnGroupType pull-left" role="group" >
                                    <button type="button" class="btn item_BtnType-<?=$line['Number']?> <?=($line['Type']=='1' ? 'btn-primary' : 'btn-default')?>" data-value="1" onclick="typeLineChange(this)"><span class="fa fa-cube"></span></button>
                                    <button type="button" class="btn item_BtnType-<?=$line['Number']?> <?=($line['Type']=='2' ? 'btn-primary' : 'btn-default')?>" data-value="2" onclick="typeLineChange(this)"><span class="fa fa-hand-paper-o"></span></button>
                                    <button type="button" class="btn item_BtnType-<?=$line['Number']?> <?=($line['Type']=='0' ? 'btn-primary' : 'btn-default')?>" data-value="0" onclick="typeLineChange(this)"><span class="fa fa-comment"></span></button>
                                </div>
                                <input type="hidden" name="item_Type-<?=$line['Number']?>" id="item_Type-<?=$line['Number']?>" value="<?=$line['Type']?>" />

                                <div class="pull-left" style="margin-left:4px;max-width:75px;">
                                    <input type="text" name="item_Reference-<?=$line['Number']?>" id="item_Reference-<?=$line['Number']?>" class="input-sm form-control" value="<?=$line['Reference']?>" onkeypress="checkReference(event, this)" />
                                </div>
                                <button style="margin-left:4px" class="btn_add_product btn btn-sm btn-default pull-left" id="item_searchProduct-<?=$line['Number']?>" onclick="return searchProduct(this);"><i class="glyphicon glyphicon-search"></i></button>
                                <div style="margin-left:145px;">
                                    <input type="text" name="item_Name-<?=$line['Number']?>" id="item_Name-<?=$line['Number']?>" class="input-sm form-control" style="font-weight:bold;" value="<?=$line['Name']?>" />
                                </div>
                                <div style="margin-top:5px;margin-left:28px;">
                                    <textarea name="item_Description-<?=$line['Number']?>" id="item_Description-<?=$line['Number']?>" class="input-sm form-control autoheight" onkeypress="autoheight(this)"><?=$line['Description']?></textarea>
                                </div>
                            </td>
                            <td style="height:42px;">
                                <input type="text" name="item_Quantity-<?=$line['Number']?>" id="item_Quantity-<?=$line['Number']?>" onchange="updateLine(this)" class="input-sm form-control amount" value="<?=$line['Quantity']?>">
                            </td>
                            </td>
                            <td>
                                <input type="text" name="item_Price-<?=$line['Number']?>" id="item_Price-<?=$line['Number']?>" onchange="updateLine(this)" class="input-sm form-control amount" value="<?=$line['Price']?>">
                            </td>
                            <td class="hidden">
                                <div class="input-group">
                                    <input type="text" name="item_DiscountValue-<?=$line['Number']?>" id="item_DiscountValue-<?=$line['Number']?>" onchange="updateLine(this)" class="form-control input-sm amount" value="<?=$line['DiscountValue']?>" />
                                    <input type="hidden" name="item_DiscountType-<?=$line['Number']?>" id="item_DiscountType-<?=$line['Number']?>" onchange="updateLine(this)" value="<?=$line['DiscountType']?>" />
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="item_DiscountTypeSpan-<?=$line['Number']?>"><?=$line['DiscountType']?></span> <span class="caret"></span></button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="#" data-type="%" onclick="return updateDiscountType(this);">%</a></li>
                                            <li><a href="#" data-type="<?=$settings['GENERAL']['CurrencySymbol']?>" onclick="return updateDiscountType(this);"><?=$settings['GENERAL']['CurrencySymbol']?></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </td>
                            <td style="<?=($settings['GENERAL']['UseVAT'] == 0 ? 'display:none;' : '')?>">
                                <select name="item_TaxID-<?=$line['Number']?>" id="item_TaxID-<?=$line['Number']?>" onchange="updateLine(this)" class="col-xs-12 form-control input-sm" >
                                    <option value="0" selected="selected" data-rate="0" ><?=t('NoVAT')?></option>
                                    <?php foreach($taxes as $taxe) { ?>
                                        <option value="<?=$taxe['ID']?>" <?=($line['TaxID']==$taxe['ID']?'selected="selected"':'')?> data-rate="<?=$taxe['Rate']?>" ><?=$taxe['Name']?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td>
                                <div class="btn-group text-center" style="min-width:85px">
                                    <button class="btn btn-default btn-sm text-danger" id="item_DeleteRow-<?=$line['Number']?>" onclick="return removeLine(this)"><span class="glyphicon glyphicon-remove text-danger" aria-hidden="true"></span> <span class="text-danger"><?=t('Del')?></span></button>
                                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span><span class="sr-only"><?=t('ToggleDropdown')?></span></button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <!--<li><a href="#" onclick="return duplicateLine(this)"><span class="glyphicon glyphicon-retweet" aria-hidden="true"></span> <?=t('Duplicate')?></a></li>-->
                                        <!--<li><a href="#"><span class="glyphicon glyphicon-retweet" aria-hidden="true"></span> <?=t('SaveAsProduct')?></a></li>-->
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <select name="item_UnityID-<?=$line['Number']?>" id="item_UnityID-<?=$line['Number']?>" class="form-control input-sm">
                                    <?php if (isset($settings['UNITY'])) { ?>
                                        <?php foreach($settings['UNITY'] as $unit) { ?>
                                            <option value="<?=$unit['ID']?>" <?=($line['UnityID']==$unit['ID']?'selected="selected"':'')?> ><?=$unit['Name']?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </td>
                            <td class="td-amount text-right">
                                <span><?=t('Subtotal')?></span><br />
                                <span id="item_Subtotal-<?=$line['Number']?>" class="amount item_Subtotal"><?=currency_format($line['Subtotal'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></span>
                            </td>
                            <td class="td-amount text-right hidden">
                                <span><?=t('Discount')?></span><br />
                                <span id="item_DiscountAmount-<?=$line['Number']?>" class="amount item_DiscountAmount"><?=currency_format($line['DiscountAmount'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></span>
                            </td>
                            <td class="td-amount text-right" style="<?=($settings['GENERAL']['UseVAT'] == 0 ? 'display:none;' : '')?>">
                                <span><?=t('VAT')?></span><br />
                                <span id="item_TaxAmount-<?=$line['Number']?>" class="amount item_TaxAmount"><?=currency_format($line['TaxAmount'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></span>
                            </td>
                            <td class="td-amount text-right">
                                <span><?=t('Total')?></span><br />
                                <span id="item_Total-<?=$line['Number']?>" class="amount item_Total"><?=currency_format($line['Total'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></span>
                            </td>
                        </tr>
                    </tbody>

                <?php } ?>

                <tfoot id="newRow" style="display: none;">
                    <tr id="item_tr-$x" data-number="$x" class="tr1" >
                        <td rowspan="2" class="sorter"><i class="fa fa-arrows cursor-move"></i></td>
                        <td rowspan="2">
                            <div class="btn-group-xs btn-group-vertical btnGroupType pull-left" role="group" >
                                <button type="button" class="btn item_BtnType-$x btn-primary" data-value="1" onclick="typeLineChange(this)"><span class="fa fa-cube"></span></button>
                                <button type="button" class="btn item_BtnType-$x btn-default" data-value="2" onclick="typeLineChange(this)"><span class="fa fa-hand-paper-o"></span></button>
                                <button type="button" class="btn item_BtnType-$x btn-default" data-value="0" onclick="typeLineChange(this)"><span class="fa fa-comment"></span></button>
                            </div>
                            <input type="hidden" name="item_Type-$x" id="item_Type-$x" value="1" />

                            <div class="pull-left" style="margin-left:4px;max-width:75px;">
                                <input type="text" name="item_Reference-$x" id="item_Reference-$x" class="input-sm form-control" value="" onkeypress="checkReference(event, this)" />
                            </div>
                            <button style="margin-left:4px;" class="btn_add_product btn btn-sm btn-default pull-left" id="item_searchProduct-$x" onclick="return searchProduct(this);"><i class=" glyphicon glyphicon-search"></i></button>
                            <div style="margin-left:145px;">
                                <input type="text" name="item_Name-$x" id="item_Name-$x" class="input-sm form-control" style="font-weight:bold;" value="" />
                            </div>
                            <div style="margin-top:5px;margin-left:28px">
                                <textarea name="item_Description-$x" id="item_Description-$x" class="input-sm form-control"  onkeypress="autoheight(this)"></textarea>
                            </div>
                        </td>
                        <td style="height:42px;">
                            <input type="text" name="item_Quantity-$x" id="item_Quantity-$x" onchange="updateLine(this)" class="input-sm form-control amount" value="1.00">
                        </td>
                        </td>
                        <td>
                            <input type="text" name="item_Price-$x" id="item_Price-$x" onchange="updateLine(this)" class="input-sm form-control amount" value="">
                        </td>
                        <td class="hidden">
                            <div class="input-group">
                                <input type="text" name="item_DiscountValue-$x" id="item_DiscountValue-$x" onchange="updateLine(this)" class="form-control input-sm amount" value="0.00" />
                                <input type="hidden" name="item_DiscountType-$x" id="item_DiscountType-$x" onchange="updateLine(this)" value="%" />
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="item_DiscountTypeSpan-$x">%</span> <span class="caret"></span></button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="#" data-type="%" onclick="return updateDiscountType(this);">%</a></li>
                                        <li><a href="#" data-type="<?=$settings['GENERAL']['CurrencySymbol']?>" onclick="return updateDiscountType(this);"><?=$settings['GENERAL']['CurrencySymbol']?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </td>
                        <td style="<?=($settings['GENERAL']['UseVAT'] == 0 ? 'display:none;' : '')?>">
                            <select name="item_TaxID-$x" id="item_TaxID-$x" onchange="updateLine(this)" class="col-xs-12 form-control input-sm" >
                                <option value="0" selected="selected" data-rate="0" ><?=t('NoVAT')?></option>
                                <?php foreach($taxes as $taxe) { ?>
                                    <option value="<?=$taxe['ID']?>" data-rate="<?=$taxe['Rate']?>" <?=($taxe['ID'] == $item['TaxID'] ? 'selected="selected"' : '')?> ><?=$taxe['Name']?></option>
                                <?php } ?>
                            </select>
                        </td>
                        <td>
                            <div class="btn-group text-center" style="min-width:85px">
                                <button class="btn btn-default btn-sm text-danger" id="item_DeleteRow-$x" onclick="return removeLine(this)"><span class="glyphicon glyphicon-remove text-danger" aria-hidden="true"></span> <span class="text-danger"><?=t('Del')?></span></button>
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span><span class="sr-only"><?=t('ToggleDropdown')?></span></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <!--<li><a href="#" onclick="return duplicateLine(this)"><span class="glyphicon glyphicon-retweet" aria-hidden="true"></span> <?=t('Duplicate')?></a></li>-->
                                    <!--<li><a href="#"><span class="glyphicon glyphicon-retweet" aria-hidden="true"></span> <?=t('SaveAsProduct')?></a></li>-->
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <select name="item_UnityID-$x" id="item_UnityID-$x" class="form-control input-sm">
                                <?php if (isset($settings['UNITY'])) { ?>
                                    <?php foreach($settings['UNITY'] as $unit) { ?>
                                        <option value="<?=$unit['ID']?>" ><?=$unit['Name']?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                        <td class="td-amount text-right">
                            <span><?=t('Subtotal')?></span><br />
                            <span id="item_Subtotal-$x" class="amount item_Subtotal"></span>
                        </td>
                        <td class="td-amount text-right hidden">
                            <span><?=t('Discount')?></span><br />
                            <span id="item_DiscountAmount-$x" class="amount item_DiscountAmount"></span>
                        </td>
                        <td class="td-amount text-right" style="<?=($settings['GENERAL']['UseVAT'] == 0 ? 'display:none;' : '')?>">
                            <span><?=t('VAT')?></span><br />
                            <span id="item_TaxAmount-$x" class="amount item_TaxAmount"></span>
                        </td>
                        <td class="td-amount text-right">
                            <span><?=t('Total')?></span><br />
                            <span id="item_Total-$x" class="amount item_Total"></span>
                        </td>

                    </tr>
                </tfoot>

            </table>

        </div>

        <div class="row">
            <div class="col-xs-12 col-md-4">
                <div class="btn-group">
                    <button id="addNewRow" class="btn_add_row btn btn-sm btn-default">
                        <i class="fa fa-plus"></i> <?=t('AddNewRow')?>
                    </button>
                    <button id="addNewProduct" class="btn_add_product btn btn-sm btn-default">
                        <i class="fa fa-database"></i> <?=t('AddProduct')?>
                    </button>
                </div>
                <br />
            </div>
            <div class="col-xs-12 col-md-6 col-md-offset-2 col-lg-4 col-lg-offset-4">
                <table class="table table-condensed text-right">
                    <tr style="<?=($settings['GENERAL']['UseVAT'] == 0 ? 'display:none;' : '')?>">
                        <td style="width: 40%;"><?=t('SubtotalItems')?></td>
                        <td style="width: 60%;" id="SubtotalItems" class="amount"><?=currency_format($item['SubtotalItems'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></td>
                    </tr>
                    <tr class="hidden">
                        <td class="td-vert-middle"><?=t('Discount')?></td>
                        <td class="clearfix">
                            <div class="input-group amount">
                                <input type="hidden" class="form-control input-sm amount" name="DiscountType" id="DiscountType" onchange="updateDocument()" value="<?=$item['DiscountType']?>">
                                <input type="text" class="form-control input-sm amount" name="DiscountValue" id="DiscountValue" onchange="updateDocument()" value="<?=$item['DiscountValue']?>">
                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="DiscountTypeSpan"><?=$item['DiscountType']?></span> <span class="caret"></span></button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="#" onclick="$('#DiscountTypeSpan').html('%');$('#DiscountType').val('%').change();return false;">%</a></li>
                                        <li><a href="#" onclick="$('#DiscountTypeSpan').html('<?=$settings['GENERAL']['CurrencySymbol']?>');$('#DiscountType').val('<?=$settings['GENERAL']['CurrencySymbol']?>').change();return false;"><?=$settings['GENERAL']['CurrencySymbol']?></a></li>
                                    </ul>
                                </div><!-- /input-group-btn -->
                            </div><!-- /input-group -->
                        </td>
                    </tr>
                    <tr class="hidden">
                        <td class="td-vert-middle"><?=t('DiscountAmount')?></td>
                        <td class="clearfix">
                            <span id="DiscountAmount" class="amount"><?=currency_format($item['DiscountAmount'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></span>
                        </td>
                    </tr>
                    <tr class="hidden">
                        <td style="width: 40%;"><?=t('Subtotal')?></td>
                        <td style="width: 60%;" id="Subtotal" class="amount"><?=currency_format($item['Subtotal'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></td>
                    </tr>
                    <tr style="<?=($settings['GENERAL']['UseVAT'] == 0 ? 'display:none;' : '')?>">
                        <td><?=t('VAT')?></td>
                        <td id="TaxAmount" class="amount"><?=currency_format($item['TaxAmount'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></td>
                    </tr>
                    <tr>
                        <td><b><?=t('Total')?></b></td>
                        <td><b id="Total" class="amount"><?=currency_format($item['Total'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></b></td>
                    </tr>
                    <tr>
                        <td><?=t('Paid')?></td>
                        <td id="Paid" class="amount"><?=currency_format($item['Paid'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></td>
                    </tr>
                    <tr>
                        <td><?=t('Balance')?></td>
                        <td id="Balance" class="amount"><?=currency_format($item['Total'] - $item['Paid'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></td>
                    </tr>
                </table>
            </div>
        </div>

        <?php if ( ! empty($item['Payments'])) { ?>
            <div class="col-xs-12 col-md-6">
                <label><?=t('Payments')?> :</label>
                <table id="linkedPays" class="table table-condensed table-bordered">
                    <tr>
                        <th><?=t('Reference')?></th>
                        <th><?=t('Created')?></th>
                        <th><?=t('Invoice')?></th>
                        <th><?=t('InvoiceDate')?></th>
                        <th><?=t('Amount')?></th>
                        <th><?=t('PaymentMethod')?></th>
                    </tr>
                    <?php foreach($item['Payments'] as $payment) { ?>
                        <tr>
                            <td class="text-center"><a href="<?=$urlViewPayment.$payment['ID']?>">#<?=$payment['ID']?></a></td>
                            <td class="text-center"><?=printDate($settings['GENERAL']['DateFormat'], $payment['Date'])?></td>
                            <td class="text-center"><a href="<?=$urlEditDocument.($payment['DocTypeID']==1?'quote':($payment['DocTypeID']==2?'order':($payment['DocTypeID']==3?'deposit':'invoice'))).'/'.$payment['DocumentID']?>"><?=$payment['DocumentReference']?></a></td>
                            <td class="text-center"><?=printDate($settings['GENERAL']['DateFormat'], $payment['DocumentDateValidated'])?></td>
                            <td class="text-right amount"><?=currency_format($payment['Amount'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></td>
                            <td class="text-center"><?=$payment['PaymentMethodName']?></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        <?php } ?>

    </div>

</form>

<div id="searchProductModal" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?=t('SearchProduct')?></h4>
            </div>
            <div class="modal-body" style="padding:0;">
                <input type="hidden" name="currentLineNumber" id="currentLineNumber" />
                <input type="hidden" name="newRowLineNumber" id="newRowLineNumber" />
                <?php
                    $rOptions = array();
                    //Repeater Id
                    $rOptions['id'] = 'docProductsList';
                    //url
                    $rOptions['url'] = $urlLoadList;
                    //Default Type
                    $rOptions['type'] = 'products';
                    //default filter
                    $rOptions['filter'] = 'active';
                    //Array Filters
                    $rOptions['arrFilters'] = array(
                        'products' => array(
                            '0' => array(
                                'value'     => 'all',
                                'label'     => t('All')
                            ),
                            1 => array(
                                'value'     => 'active',
                                'label'     => t('Active')
                            ),
                            2 => array(
                                'value'     => 'inactive',
                                'label'     => t('Inactive')
                            ),
                            3 => array(
                                'value'     => 'onSale',
                                'label'     => t('On sale')
                            ),
                            4 => array(
                                'value'     => 'notOnSale',
                                'label'     => t('Not on sale')
                            )
                        )
                    );
                    //Array Filters
                    $rOptions['arrActions'] = array();

                    //Object contenant les descriptions des colonnes en fonction du type
                    $rOptions['arrColumns'] = array(
                        'products' => array(
                            array(
                                'label'     => '',
                                'property'  => 'ID',
                                'className' => 'valign-middle',
                                'width'     => 50,
                                'sortable'  => false
                            ),
                            array(
                                'label'     => t('Type'),
                                'property'  => 'Type',
                                'className' => 'valign-middle text-center',
                                'width'     => 50,
                                'sortable'  => true
                            ),
                            array(
                                'label'     => t('Reference'),
                                'property'  => 'Reference',
                                'className' => 'valign-middle',
                                'width'     => 105,
                                'sortable'  => true
                            ),
                            array(
                                'label'     => t('Label'),
                                'property'  => 'Name',
                                'className' => 'valign-middle',
                                'sortable'  => true
                            ),
                            array(
                                'label'     => t('Description'),
                                'property'  => 'Description',
                                'className' => 'valign-middle',
                                'sortable'  => false
                            ),
                            array(
                                'label'     => t('Unity'),
                                'property'  => 'UnityValue',
                                'className' => 'valign-middle text-center',
                                'width'     => 70,
                                'sortable'  => true
                            ),
                            array(
                                'label'     => t('Price'),
                                'property'  => 'Price',
                                'className' => 'valign-middle',
                                'width'     => 110,
                                'sortable'  => true
                            ),
                            array(
                                'label'     => t('VAT'),
                                'property'  => 'TaxName',
                                'className' => ($settings['GENERAL']['UseVAT'] == 0 ? 'hidden valign-middle' : 'valign-middle'),
                                'width'     => 85,
                                'sortable'  => true
                            )
                        )
                    );

                    //View render
                    $rOptions['arrColumnsRenderer'] = array(
                        'products' => array(
                            'Type'          => "'+(rowData.Type == 1 ? '<span class=\"fa fa-cube\"></span>' : '<span class=\"fa fa-hand-paper-o\"></span>')+'",
                            'ID'            => "<div class=\"text-center\"><input type=\"radio\" name=\"selectedProduct\" data-json=\"'+JSON.stringify(rowData).replace(/\"/g, '&quot;')+'\" value=\"'+rowData.ID+'\" /></div>",
                            'Name'          => "<strong>'+rowData.Name+'</strong>",
                            'Reference'     => "<strong>'+rowData.Reference+'</strong>",
                            'Price'         => "<div class=\"amount\">' + currency_format(rowData.Price, '".$settings['GENERAL']['Decimals']."', '".$settings['GENERAL']['DecimalSeparator']."', '', '".$settings['GENERAL']['CurrencySymbol']."', '".$settings['GENERAL']['CurrencyPlacement']."') + '</div>",
                            'Description'   => "<div class=\"description\">'+nl2br(rowData.Description)+'</div>"
                        )
                    );

                    $rOptions['noloading'] = true;
                    $rOptions['noresize'] = true;
                    $rOptions['height'] = 500;
                    HLX_View::hlxRepeater($rOptions);
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><?=t('Cancel')?></button>
                <button type="button" class="btn btn-primary btn-sm" id="selectProduct" ><?=t('Select')?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal searchProduct-->

<div id="loading" style="display: none">
    <div class="loadingContent">
        <i class="fa fa-cog fa-spin"></i>
        <div class="hide">

        </div>
    </div>
</div>

<script>
<!--
    //Set form readonly
    var readOnly = $('#DocStatusReadOnly').val();
    if (readOnly == 1)
    {
        $('#formDocument input, #formDocument select, #formDocument button, #formDocument textarea').attr('readonly', true).attr('disabled', true);
        $('.sorter').html('');
    }
    else
    {
        Sortable.create(document.getElementById('linesTable'), {
            handle: '.sorter',
            onUpdate: function (event){
                reorderLines();
            }
        });
    }
    //Tabs general invoice delivery
    $('#documentsTabs a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    //Put cursor at the end of the search field
    $('#docProductsList .repeater-search input[type="search"]').focus(function(){
        this.selectionStart = this.selectionEnd = this.value.length;
    });

    //Add focus on search input
    $('#searchProductModal').on('shown.bs.modal', function () {
        $('#docProductsList .repeater-search input[type="search"]').focus();
    })

    //Clear newRowLineNumber on close
    $('#searchProductModal').on('hide.bs.modal', function () {
        var lineNumber = $('#newRowLineNumber').val();
        if (lineNumber != '')
            removeLine($('#item_DeleteRow-'+lineNumber)[0], true);
        $('#newRowLineNumber').val('');
    })

    //Date pickers
    $('.input-group.date.datepicker').datepicker({
        format: '<?=str_replace(array('d','m','y','Y'), array('dd','mm','yy','yyyy'), $settings['GENERAL']['DateFormat'])?>',
        todayBtn: 'linked',
        language: 'fr',//TODO set language according
        daysOfWeekHighlighted: '0,6'
    });

    //Add row
    $('#addNewRow').click(function() {//Increment tabindex
        var lineContent = $('#newRow').html(),
            counter = $('#linesTable tbody').length;
        lineContent = lineContent.replace(/(\$x)/g, counter);
        $('#linesTable').append('<tbody class="item">'+lineContent+'</tbody>');
        return false;
    });

    //Add product
    $('#addNewProduct').click(function() {
        $('#addNewRow').click();
        var lineNumber = ($('#linesTable tbody').length - 1);
        $('#item_searchProduct-'+lineNumber).click();
        $('#newRowLineNumber').val(lineNumber);
        return false;
    });

    //automatic height for textarea
    autoheight = function(e) {
        $(e).css({'height':'46px','overflow-y':'hidden'}).height(e.scrollHeight);
    }
    $('textarea.autoheight').keypress();

    updateDiscountType = function(elem) {
        var elem = $(elem),
            lineNumber = $(elem.closest('tr')).data('number');
        $('button.btn.btn-default.dropdown-toggle > #item_DiscountTypeSpan-'+lineNumber).html(elem.data('type'));
        $('#item_DiscountType-'+lineNumber).val(elem.data('type')).change();
        return false;
    }

    updateLine = function(elem) {

        var lineNumber = $($(elem).closest('tr')).data('number'),
            itemQty = $('#item_Quantity-'+lineNumber).val().replace(',', '.'),
            itemPrice = $('#item_Price-'+lineNumber).val().replace(',', '.'),
            itemDiscountValue = $('#item_DiscountValue-'+lineNumber).val().replace(',', '.'),
            itemDiscountType = $('#item_DiscountType-'+lineNumber).val(),
            itemTaxRate = $($('#item_TaxID-'+lineNumber)[0].options[ $('#item_TaxID-'+lineNumber)[0].selectedIndex ]).data('rate'),
            itemTaxRate = !itemTaxRate ? 0.00 : itemTaxRate,
            itemSubtotal = 0.00,
            itemDiscountAmount = 0.00,
            itemTaxAmount = 0.00,
            itemTotal = 0.00;

        itemSubtotal = itemQty * itemPrice;
        if (itemDiscountValue != 0)
            itemDiscountAmount = (itemDiscountType == '%') ? itemSubtotal * itemDiscountValue / 100 : itemDiscountValue;
        else
            itemDiscountAmount = 0.00;

        itemTaxAmount = (itemSubtotal - itemDiscountAmount) * itemTaxRate / 100;
        itemTotal = itemSubtotal - itemDiscountAmount + itemTaxAmount;

        $('#item_Subtotal-'+lineNumber).html(currency_format(itemSubtotal, '<?=$settings['GENERAL']['Decimals']?>', '<?=$settings['GENERAL']['DecimalSeparator']?>', '', '<?=$settings['GENERAL']['CurrencySymbol']?>', '<?=$settings['GENERAL']['CurrencyPlacement']?>'));
        $('#item_DiscountAmount-'+lineNumber).html(currency_format(itemDiscountAmount, '<?=$settings['GENERAL']['Decimals']?>', '<?=$settings['GENERAL']['DecimalSeparator']?>', '', '<?=$settings['GENERAL']['CurrencySymbol']?>', '<?=$settings['GENERAL']['CurrencyPlacement']?>'));
        $('#item_TaxAmount-'+lineNumber).html(currency_format(itemTaxAmount, '<?=$settings['GENERAL']['Decimals']?>', '<?=$settings['GENERAL']['DecimalSeparator']?>', '', '<?=$settings['GENERAL']['CurrencySymbol']?>', '<?=$settings['GENERAL']['CurrencyPlacement']?>'));
        $('#item_Total-'+lineNumber).html(currency_format(itemTotal, '<?=$settings['GENERAL']['Decimals']?>', '<?=$settings['GENERAL']['DecimalSeparator']?>', '', '<?=$settings['GENERAL']['CurrencySymbol']?>', '<?=$settings['GENERAL']['CurrencyPlacement']?>'));

        updateDocument();
    }

    updateDocument = function() {

        var TaxRate = $($('#TaxID')[0].options[ $('#TaxID')[0].selectedIndex ]).data('rate'),
            DiscountType = $('#DiscountType').val(),
            DiscountValue = $('#DiscountValue').val().replace(',', '.'),
            DiscountTax = 0.00,
            DiscountAmount = 0.00,
            SubtotalItems = 0.00,
            Subtotal = 0.00,
            TaxAmount = 0.00,
            Total = 0.00;

        $('.item_Subtotal').each(function(idx, elem) {
            SubtotalItems = $(elem).html().replace(/[^0-9.,]/g, '').replace(',', '.') * 1 + SubtotalItems;
        });

        if (DiscountValue != 0)
        {
            DiscountAmount = (DiscountType == '%') ? SubtotalItems * DiscountValue / 100 : DiscountValue;
            DiscountTax = DiscountAmount * TaxRate / 100;
            Subtotal = SubtotalItems - DiscountAmount;
        }
        else
        {
            DiscountAmount = 0.00;
            Subtotal = SubtotalItems;
        }

        $('.item_TaxAmount').each(function(idx, elem) {
            TaxAmount = $(elem).html().replace(/[^0-9.,]/g, '').replace(',', '.') * 1 + TaxAmount;
        });
        TaxAmount -= DiscountTax;

        Total = Subtotal + TaxAmount;

        $('#SubtotalItems').html(currency_format(SubtotalItems, '<?=$settings['GENERAL']['Decimals']?>', '<?=$settings['GENERAL']['DecimalSeparator']?>', '', '<?=$settings['GENERAL']['CurrencySymbol']?>', '<?=$settings['GENERAL']['CurrencyPlacement']?>'));
        $('#DiscountAmount').html(currency_format(DiscountAmount, '<?=$settings['GENERAL']['Decimals']?>', '<?=$settings['GENERAL']['DecimalSeparator']?>', '', '<?=$settings['GENERAL']['CurrencySymbol']?>', '<?=$settings['GENERAL']['CurrencyPlacement']?>'));
        $('#Subtotal').html(currency_format(Subtotal, '<?=$settings['GENERAL']['Decimals']?>', '<?=$settings['GENERAL']['DecimalSeparator']?>', '', '<?=$settings['GENERAL']['CurrencySymbol']?>', '<?=$settings['GENERAL']['CurrencyPlacement']?>'));
        $('#TaxAmount').html(currency_format(TaxAmount, '<?=$settings['GENERAL']['Decimals']?>', '<?=$settings['GENERAL']['DecimalSeparator']?>', '', '<?=$settings['GENERAL']['CurrencySymbol']?>', '<?=$settings['GENERAL']['CurrencyPlacement']?>'));
        $('#Total').html(currency_format(Total, '<?=$settings['GENERAL']['Decimals']?>', '<?=$settings['GENERAL']['DecimalSeparator']?>', '', '<?=$settings['GENERAL']['CurrencySymbol']?>', '<?=$settings['GENERAL']['CurrencyPlacement']?>'));

        var Paid = $('#Paid')
        if (Paid.length > 0)
        {
            Paid = Paid.html().replace(/[^0-9.,]/g, '').replace(',', '.') * 1;
            $('#Balance').html(currency_format(Total - Paid, '<?=$settings['GENERAL']['Decimals']?>', '<?=$settings['GENERAL']['DecimalSeparator']?>', '', '<?=$settings['GENERAL']['CurrencySymbol']?>', '<?=$settings['GENERAL']['CurrencyPlacement']?>'));
        }

    }

    removeLine = function(elem, confirmAction) {

        if (typeof(confirmAction) == 'undefined')
            confirmAction = false;

        if (confirmAction || confirm('<?=t('AreYouSure')?>'))
        {
            var lineNumber = $($(elem).closest('tr')).data('number');
            $($('#item_tr-'+lineNumber).parent()).remove();
            //reorder line
            reorderLines();
            //recalc document
            updateDocument();
            return false;
        }

        return false;
    }

    duplicateLine = function(elem) {
        //TODO write function
        alert('duplicateLine');
        var lineNumber = $($(elem).closest('tr')).data('number');
        return false;
    }

    reorderLines = function() {

        //Fix value in html dom
        $('#linesTable tbody input').each(function(idx, elem) {
            $(elem).attr('value', $(elem).val());
        });
        $('#linesTable tbody textarea').each(function(idx, elem) {
            $(elem).text($(elem).val());
        });
        $('#linesTable tbody select').each(function(idx, elem) {
            var opt = $(elem).find('option:selected');
            $(elem).find('option').removeAttr('selected');
            opt.attr('selected', 'selected');
        });
        //Reorder lines
        var i = 0;
        $('#linesTable tbody').each(function(idx, elem) {
            elem = $(elem);
            var currentNumber = $(elem.find('tr.tr1')).data('number'),
                lineContent = elem.html(),
                regxp1 = new RegExp('(item_[a-zA-Z]*)-('+currentNumber+')', 'g'),
                regxp2 = new RegExp('(data-number="'+currentNumber+'")');
            lineContent = lineContent.replace(regxp1, '$1-'+i).replace(regxp2, 'data-number="'+i+'"');
            elem.html(lineContent);
            i++;
        });

    }

    //Click on type line
    typeLineChange = function(elem) {
        var val = $(elem).data('value');
            tr = $($(elem).closest('tr'));
            lineNumber = tr.data('number');
        $('.item_BtnType-'+lineNumber).removeClass('btn-primary').addClass('btn-default');
        $(elem).removeClass('btn-default').addClass('btn-primary');
        $('#item_Type-'+lineNumber).val(val);
        //Hide qty / price if it's a comment
        if (val == '0') {
            $('#item_Quantity-'+lineNumber+', #item_UnityID-'+lineNumber+', #item_Subtotal-'+lineNumber+', #item_Price-'+lineNumber+', .inputGroupDiscount-'+lineNumber+', #item_TaxID-'+lineNumber+', #item_DiscountAmount-'+lineNumber+', #item_TaxAmount-'+lineNumber+', #item_Total-'+lineNumber+'').hide();
            $('#item_Quantity-'+lineNumber).val(0);
            updateLine(tr);
        }
        else
            $('#item_Quantity-'+lineNumber+', #item_UnityID-'+lineNumber+', #item_Subtotal-'+lineNumber+', #item_Price-'+lineNumber+', .inputGroupDiscount-'+lineNumber+', #item_TaxID-'+lineNumber+', #item_DiscountAmount-'+lineNumber+', #item_TaxAmount-'+lineNumber+', #item_Total-'+lineNumber+'').show();
    }
    //Hide qty / price for comments line
    $('.btnGroupType .btn-primary[data-value="0"]').click();

    //search reference in products
    checkReference = function(e, elem) {
        if (e.which == 13 || e.keyCode == 13) {
            e.preventDefault();
            $('#item_searchProduct-'+$(elem).data('number')).click();
            return false;
        }
    }

    //Click on search product
    searchProduct = function(elem) {

        var lineNumber = $($(elem).closest('tr')).data('number'),
            ref = $('#item_Reference-'+lineNumber).val();
        if (ref == '')
        {
            $('#docProductsList .repeater-search input[type="search"]').val('');
            $('#docProductsList .repeater-search div').removeClass('searched');
            $('#docProductsList .repeater-search button span.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-search');
        }
        else
        {
            $('#docProductsList .repeater-search input[type="search"]').val(ref);
            $('#docProductsList .repeater-search div').addClass('searched');
            $('#docProductsList .repeater-search button span.glyphicon').removeClass('glyphicon-search').addClass('glyphicon-remove');
        }
        $('#currentLineNumber').val(lineNumber);
        $('#searchProductModal').modal();
        hlxRepeater('docProductsList');
        return false;

    }

    //Click on product list line
    $('#docProductsList').bind('click', function(event) {

        var tr = $($(event.target).closest('tr'));
        $('#docProductsList table tr').removeClass('selectedTr');
        $('#docProductsList table tr input').prop('checked', false);
        tr.addClass('selectedTr');
        $(tr.find('input')).prop('checked', true);

    });

    $('#selectProduct').click(function() {
        //Check if there is a selection
        if ($('#docProductsList .selectedTr').length == 0)
        {
            alert('<?=t('Please select a product !')?>');
        }
        else
        {
            var product = $('#docProductsList table tr.selectedTr input').data('json'),
                lineNumber = $('#currentLineNumber').val();

            $('#item_Reference-'+lineNumber).val(product.Reference);
            $('#item_Name-'+lineNumber).val(product.Name);
            $('#item_Description-'+lineNumber).val(product.Description);
            if ($('#item_Qty-'+lineNumber).val() == 0)
                $('#item_Qty-'+lineNumber).val(1);
            $('#item_UnityID-'+lineNumber).val(product.UnityID);
            $('#item_Price-'+lineNumber).val(product.Price);
            $('#item_TaxID-'+lineNumber).val(product.TaxID).change();;

            //Click line type
            $('.item_BtnType-'+lineNumber+'[data-value="'+product.Type+'"]').click();

            //Clear new Row line Number
            $('#newRowLineNumber').val('');
            $('#searchProductModal').modal('hide');
        }
    });

    $('#DocStatusID').change(function() {

        var color = $(this.options[ this.selectedIndex ]).data('color');
        $(this).css('background-color', '#'+color);

    });
    $('#DocStatusID').change();

    postForm = function()
    {
        $('#loading').show();
        $.ajax({
            type: 'POST',
            url: $("#formDocument").attr("action"),
            data: $("#formDocument").serialize(),
            success: function(response) {
                //TODO les tests : $('#Reference').val() == '' || $('#DateValidated').val() == '' ne doivent être fait que si le statut choisi est 'Validated'
                //TODO gestion des erreurs néantes !!!
                if ($('#ID').val() == '0' || $('#Reference').val() == '' || $('#DateValidated').val() == '')
                {
                    var item = JSON.parse(response);
                    window.location = '<?=$urlEditDocument?>'+item.ID;
                }
                else
                    $('#loading').hide();
            },
        });
        return false;
    }

    <?=(count($item['Lines']) == 0 ? "$('#addNewRow').click();" : '')?>

-->
</script>

<?php include 'footer.php';