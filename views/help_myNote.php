<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

<div id="headerbar" style="position:fixed;width:100%;background-color:white;z-index:99;">

    <div class="pull-left">
        <h1><?=t('MyNote')?></h1>
    </div>

    <div class="pull-right btn-group">
        <a class="btn btn-sm btn-primary" onclick="createNote();return false;">
            <i class="glyphicon glyphicon-plus"></i> <?=t('NewNote')?>
        </a>
    </div>

</div>

<div class="container-fluid" style="padding:45px 0 0 0;">

<br />

<?php foreach($rNotes as $note) { ?>
    <form action="<?=$urlSaveNote?>" method="post">
    <input type="hidden" name="ID" value="<?=htmlspecialchars($note['ID'])?>" />
    <input type="text" name="Name" style="font-weight:bold" class="form-control" value="<?=htmlspecialchars($note['Name'])?>" />
    <div class="row">
        <div class="col-sm-10">
            <textarea name="Description" class="form-control autoheight" onkeypress="autoheight(this)"><?=htmlspecialchars($note['Description'])?></textarea>
        </div>
        <div class="col-sm-2">
            <a class="form-control btn btn-default" href="<?=$urlDeleteNote.$note['ID']?>" ><span class="text-danger"><?=t('Delete')?></span></a>
            <input type="submit" class="form-control btn btn-default" value="<?=t('Update')?>" />
        </div>
    </div>
    <hr /></form>

<?php } ?>

</div>

<script>
<!--
    function createNote() {

        html = '<form action="<?=$urlSaveNote?>" id ="newNote" method="post">';
        html += '<input type="hidden" name="ID" />';
        html += '<input type="text" name="Name" style="font-weight:bold" class="form-control" placeholder="<?=t('Title')?>" value="" />';
        html += '<div class="row">';
        html += '    <div class="col-sm-10">';
        html += '        <textarea name="Description" class="form-control autoheight" placeholder="<?=t('Note')?>" onkeypress="autoheight(this)"></textarea>';
        html += '    </div>';
        html += '    <div class="col-sm-2">';
        html += '        <a class="form-control btn btn-default" onclick="cancelNote();return false;" ><span class="text-danger"><?=t('Cancel')?></span></a>';
        html += '        <input type="submit" class="form-control btn btn-default" value="<?=t('Save')?>" />';
        html += '    </div>';
        html += '</div>';
        html += '<hr /></form>';

        $('.container-fluid').append(html);
    }

    function cancelNote() {

        $('#newNote').remove();

    }

    //automatic height for textarea
    autoheight = function(e) {
        $(e).css({'height':'46px','overflow-y':'hidden'}).height(e.scrollHeight);
    }
    $('textarea.autoheight').keypress();
-->
</script>

<?php include 'footer.php';