<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

<script type="text/javascript" src="views/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="views/js/bootstrap-datepicker-lang.min.js"></script>
<link rel="stylesheet" href="views/css/bootstrap-datepicker3.min.css" type="text/css" />



<div id="headerbar" style="position:fixed;width:100%;background-color:white;z-index:99;">

    <div class="pull-left">
        <h1><?=t('View payment')?></h1>
    </div>

    <?php if ( ! empty($documents)) { ?>
        <div class="pull-right btn-group">
            <a class="btn btn-sm btn-default" href="#" onclick="history.back();return false;">
                <i class="glyphicon glyphicon-chevron-left"></i> <?=t('Back')?>
            </a>
        </div>
    <?php } ?>

</div>

<div class="container-fluid" style="padding-top:45px;">

    <div class="row">
        <br />
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-4">
            <label><?=t('Invoice')?> : </label><br />
            <a href="<?=$urlViewInvoice.$item['DocumentID']?>"><?=$item['DocumentReference']?> - <?=trim($item['ThirdPartyName'].' '.$item['ThirdPartyFirstname'])?> - <?=currency_format($item['DocumentTotal'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></a>
        </div>
    </div>

    <div class="row">
        <br />
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-4">
            <label><?=t('PaymentMethod')?> : </label><br />
            <?=$item['PaymentMethodName']?>
        </div>
    </div>

    <div class="row">
        <br />
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-4">
            <label><?=t('Date')?> : </label><br />
            <?=printDate($settings['GENERAL']['DateFormat'], $item['Date'])?>
        </div>
    </div>

    <div class="row">
        <br />
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-4">
            <label><?=t('Amount')?> : </label><br />
            <?=currency_format($item['Amount'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?>
        </div>
    </div>

    <div class="row">
        <br />
        <div class="col-xs-12 col-sm-10 col-md-8 col-lg-7">
            <label><?=t('Note')?> : </label>
            <?=nl2br($item['Note'])?>
        </div>
    </div>

</div>


<?php include 'footer.php';