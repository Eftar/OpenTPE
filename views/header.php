<!DOCTYPE html>
<html lang="<?=BFWK_LANG?>">
<head>
    <?=(isset($header['headStart'])?$header['headStart']:'')?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=HLX_NAME?></title>
    <link rel="icon" type="image/png" href="<?=BFWK_SERVER_ROOT?>favicon.png" />
    <link type="text/css" href="views/css/bootstrap.min.css" rel="stylesheet" />
    <link type="text/css" href="views/css/fuelux.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="views/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="views/css/hlx.css" type="text/css" />
    <script type="text/javascript" src="views/js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="views/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="views/js/functions.js"></script>
    <script type="text/javascript" src="views/js/fuelux.min.js"></script>
    <?=(isset($header['headEnd'])?$header['headEnd']:'')?>
</head>

<body>