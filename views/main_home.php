<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

<script type="text/javascript" src="views/js/chart.min.js"></script>

<style type="text/css">
    .panel-heading.panel-grey {
        background-color: #969696;
        color: white;
        height:35px;
    }
    .quickAction {
        padding:15px;
    }
</style>


<div class="container-fluid">

    <div class="row quickAction">
        <div class="btn-group btn-group-justified" role="group">
            <a href="<?=$urlNewCustomer?>" class="btn btn-default">
                <i class="fa fa-user fa-margin"></i>
                <span class="hidden-xs"><?=t('AddCustomer')?></span>
            </a>
            <a href="javascript:void(0); newQuote();" class="create-quote btn btn-default">
                <i class="fa fa-file fa-margin"></i>
                <span class="hidden-xs"><?=t('CreateQuote')?></span>
            </a>
            <a href="javascript:void(0); newOrder();" class="create-quote btn btn-default">
                <i class="fa fa-file fa-margin"></i>
                <span class="hidden-xs"><?=t('CreateOrder')?></span>
            </a>
            <a href="javascript:void(0); newInvoice();" class="create-invoice btn btn-default">
                <i class="fa fa-file-text fa-margin"></i>
                <span class="hidden-xs"><?=t('CreateInvoice')?></span>
            </a>
            <a href="<?=$urlNewPayment?>" class="btn btn-default">
                <i class="fa fa-credit-card fa-margin"></i>
                <span class="hidden-xs"><?=t('EnterPayment')?></span>
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-12 col-lg-6">

            <div class="panel panel-default overview">

                <div class="panel-heading panel-grey">
                    <b><i class="fa fa-bar-chart fa-margin"></i> <?=t('Activity')?></b>
                </div>

                <canvas id="barChart" height="112"></canvas>

            </div>

        </div>
        <div class="col-xs-12 col-md-12 col-lg-6">

            <div class="panel panel-default overview">

                <div class="panel-heading panel-grey">
                    <b><i class="fa fa-bar-chart fa-margin"></i> <?=t('Activity')?></b>
                    <span class="pull-right"><?=t('ThisMonth')?></span>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <canvas id="pieChartQuote"></canvas>
                    </div>
                    <div class="col-xs-6">
                        <canvas id="pieChartOrder"></canvas>
                    </div>
                </div>

            </div>

            <div class="panel panel-default panel-heading">
                <?=($totalOverdueInvoice > 0 ? '<strong><a href="'.$urlOverdueInvoices.'" class="text-danger">'.$totalOverdueInvoice.' '.t('OverdueInvoices').' </a></strong>' : '<span class="text-muted">'.t('NoOverdueInvoice').'</span>')?>
                <span class="pull-right">
                    <?=($totalExpiredQuote > 0 ? '<strong><a href="'.$urlExpiredQuotes.'" class="text-danger">'.$totalExpiredQuote.' '.t('ExpiredQuotes').' </a></strong>' : '<span class="text-muted">'.t('NoExpiredQuote').'</span>')?>
                </span>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-md-6">

            <div class="panel panel-default">

                <div class="panel-heading  panel-grey">
                    <b><i class="fa fa-history fa-margin"></i> <?=t('RecentQuotes')?></b>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-condensed no-margin">
                        <thead>
                        <tr>
                            <th style="min-width: 15%;"><?=t('Reference')?> </th>
                            <th style="min-width: 15%;"><?=t('Status')?></th>
                            <th><?=t('Date')?></th>
                            <th style="min-width: 35%;"><?=t('Customer')?></th>
                            <th style="text-align: right;"><?=t('Total')?></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php foreach($rQuote as $quote) { ?>
                                <tr>
                                    <td class="text-center">
                                        <a href="<?=$urlViewQuote.$quote['ID']?>"><?=($quote['Reference']=='' ? t('Draft') : $quote['Reference'])?></a>
                                    </td>
                                    <td>
                                        <span class="label" style="width:100%;display:block;background-color:#<?=$quote['DocStatusColor']?>"><?=$quote['DocStatusName']?></span>
                                    </td>
                                    <td class="text-center">
                                        <?=printDate($settings['GENERAL']['DateFormat'], $quote['DateCreated'])?>
                                    </td>
                                    <td>
                                        <a href="<?=$urlViewCustomer.$quote['ThirdPartyID']?>"><?=$quote['ThirdPartyName'] . ' ' . $quote['ThirdPartyFirstname']?></a>
                                    </td>
                                    <td class="amount">
                                        <?=currency_format($quote['Total'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="6" class="text-right small">
                                    <a href="<?=$urlViewAllQuote?>"><?=t('ShowAll')?></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <div class="col-xs-12 col-md-6">

            <div class="panel panel-default">

                <div class="panel-heading panel-grey">
                    <b><i class="fa fa-history fa-margin"></i> <?=t('RecentInvoices')?></b>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-condensed no-margin">
                        <thead>
                        <tr>
                            <th style="min-width: 15%;"><?=t('Reference')?> </th>
                            <th style="min-width: 15%;"><?=t('Status')?></th>
                            <th><?=t('Date')?></th>
                            <th style="min-width: 35%;"><?=t('Customer')?></th>
                            <th style="text-align: right;"><?=t('Total')?></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php foreach($rInvoice as $invoice) { ?>
                                <tr>
                                    <td class="text-center">
                                        <a href="<?=$urlViewInvoice.$invoice['ID']?>"><?=($invoice['Reference']=='' ? t('Draft') : $invoice['Reference'])?></a>
                                    </td>
                                    <td>
                                        <span class="label" style="width:100%;display:block;background-color:#<?=$invoice['DocStatusColor']?>"><?=$invoice['DocStatusName']?></span>
                                    </td>
                                    <td class="text-center">
                                        <?=printDate($settings['GENERAL']['DateFormat'], $invoice['DateCreated'])?>
                                    </td>
                                    <td>
                                        <a href="<?=$urlViewCustomer.$invoice['ThirdPartyID']?>"><?=$invoice['ThirdPartyName'] . ' ' . $invoice['ThirdPartyFirstname']?></a>
                                    </td>
                                    <td class="amount">
                                        <?=currency_format($invoice['Total'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td colspan="6" class="text-right small">
                                    <a href="<?=$urlViewAllInvoice?>"><?=t('ShowAll')?></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>

<script>
    <!--
        <?php
            $rMonth = array();
            $rMonthName = array(
                1   => t('January'),
                2   => t('February'),
                3   => t('March'),
                4   => t('April'),
                5   => t('May'),
                6   => t('June'),
                7   => t('July'),
                8   => t('August'),
                9   => t('September'),
                10   => t('October'),
                11   => t('November'),
                12   => t('December')
            );
            foreach ($rTotalDocumentsYear[1] as $month => $data) {
                $rMonth[] = $rMonthName[ $month ];
            }

        ?>
/*#8EB7D7
#FFCE1C
#9FCD6C*/
        var ctx = document.getElementById("barChart");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: <?=json_encode($rMonth)?>,
                datasets: [
                    {
                        label: "<?=t('Invoice')?>",
                        fill: true,
                        lineTension: 0.1,
                        backgroundColor: "rgba(88,169,89,0.1)",
                        //borderColor: "#58a959",
                        borderColor: "#9FCD6C",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        //pointBorderColor: "#58a959",
                        pointBorderColor: "#9FCD6C",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        //pointHoverBackgroundColor: "#58a959",
                        pointHoverBackgroundColor: "#9FCD6C",
                        //pointHoverBorderColor: "#58a959",
                        pointHoverBorderColor: "#9FCD6C",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: <?=json_encode(array_values($rTotalDocumentsYear[4]))?>,
                        spanGaps: false,
                    },
                    {
                        label: "<?=t('Order')?>",
                        fill: true,
                        lineTension: 0.1,
                        backgroundColor: "rgba(84,160,198, 0.1)",
                        //borderColor: "#54a0c6",
                        borderColor: "#8EB7D7",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        //pointBorderColor: "#54a0c6",
                        pointBorderColor: "#8EB7D7",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        //pointHoverBackgroundColor: "#54a0c6",
                        pointHoverBackgroundColor: "#8EB7D7",
                        //pointHoverBorderColor: "#54a0c6",
                        pointHoverBorderColor: "#8EB7D7",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: <?=json_encode(array_values($rTotalDocumentsYear[2]))?>,
                        spanGaps: false,
                    },
                    {
                        label: "<?=t('Quote')?>",
                        fill: true,
                        lineTension: 0.1,
                        backgroundColor: "rgba(255,165,0, 0.1)",
                        //borderColor: "#ffa500",
                        borderColor: "#FFCE1C",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        //pointBorderColor: "#ffa500",
                        pointBorderColor: "#FFCE1C",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        //pointHoverBackgroundColor: "#ffa500",
                        pointHoverBackgroundColor: "#FFCE1C",
                        //pointHoverBorderColor: "#ffa500",
                        pointHoverBorderColor: "#FFCE1C",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: <?=json_encode(array_values($rTotalDocumentsYear[1]))?>,
                        spanGaps: false,
                    }
                ]
            },
            options : {
                legend: {
                    position: "top",
                    labels: {}
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            userCallback: function(value, index, values) {
                                return currency_format(value.toString(), '<?=$settings['GENERAL']['Decimals']?>', '<?=$settings['GENERAL']['DecimalSeparator']?>', '', '<?=$settings['GENERAL']['CurrencySymbol']?>', '<?=$settings['GENERAL']['CurrencyPlacement']?>')
                            }
                        }
                    }]
                }
            }

        });

        var ctx = document.getElementById("pieChartQuote");
        // And for a doughnut chart
        var myDoughnutChartQuote = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: [
                    "<?=t('Draft')?>",
                    "<?=t('Canceled')?>",
                    "<?=t('Opened')?>",
                    "<?=t('Closed')?>"
                ],
                datasets: [
                    {
                        data: <?=json_encode(array_values($rQuoteDivision))?>,
                        backgroundColor: [
                            "#b3b3b3",
                            "#c76e6d",
                            //"#54a0c6",
                            //"#58a959",
                            "#8EB7D7",
                            "#9FCD6C",
                        ],
                        hoverBackgroundColor: [
                            "#b3b3b3",
                            "#c76e6d",
                            //"#54a0c6",
                            //"#58a959",
                            "#8EB7D7",
                            "#9FCD6C",
                        ]
                    }]
            },
            options: {
                title: {
                    display: true,
                    fontColor: '#333',
                    fontSize: 14,
                    text: '<?=t('DivisionOfQuote')?>'

                }
            }
        });

        var ctx = document.getElementById("pieChartOrder");
        // And for a doughnut chart
        var myDoughnutChartOrder = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: [
                    "<?=t('Draft')?>",
                    "<?=t('Validated')?>",
                    "<?=t('Paid')?>",
                    "<?=t('Overdue')?>"
                ],
                datasets: [
                    {
                        data: <?=json_encode(array_values($rInvoiceDivision))?>,
                        backgroundColor: [
                            "#b3b3b3",
                            //"#54a0c6",
                            //"#58a959",
                            "#8EB7D7",
                            "#9FCD6C",
                            "#c76e6d"
                        ],
                        hoverBackgroundColor: [
                            "#b3b3b3",
                            //"#54a0c6",
                            //"#58a959",
                            "#8EB7D7",
                            "#9FCD6C",
                            "#c76e6d"
                        ]
                    }]
            },
            options: {
                title: {
                    display: true,
                    fontColor: '#333',
                    fontSize: 14,
                    text: '<?=t('DivisionOfInvoice')?>'

                }
            }
        });

    -->
</script>

<?php include 'footer.php';