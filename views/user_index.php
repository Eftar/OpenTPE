<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

<form action="<?=$urlSave?>" enctype="multipart/form-data" method="POST" onsubmit="return checkForm();">

    <div id="headerbar" style="position:fixed;width:100%;background-color:white;z-index:99;">

        <div class="pull-left">
            <h1><?=t('EditUsers')?></h1>
        </div>

        <div class="pull-right btn-group">
            <a class="btn btn-sm btn-default" href="<?=$urlCancel?>">
                <i class="glyphicon glyphicon-remove"></i> <?=t('Cancel')?>
            </a>
            <button class="btn btn-sm btn-primary">
                <i class="fa fa-edit"></i> <?=t('Save')?>
            </button>
        </div>

    </div>

    <div class="container-fluid" style="padding:45px 0 0 0;">

        <table class="settingsTable table table-bordered table-condensed" id="userTable">
            <tr>
                <th><?=t('Name')?></th>
                <th><?=t('Email')?></th>
                <th style="width:150px;"><?=t('Password')?></th>
                <th style="width:150px;"><?=t('Role')?></th>
                <th style="width:110px;"><?=t('Active')?></th>
                <th style="width:110px;"><?=t('Action')?></th>
            </tr>
            <?php foreach($users as $user) { ?>
                <tr>
                    <td>
                        <input type="hidden" name="ID[]" value="<?=$user['ID']?>" />
                        <input type="text" required="required" name="Name[]" value="<?=$user['Name']?>" class="form-control input-sm" />
                    </td>
                    <td><input type="email" required="required" name="Email[]" value="<?=$user['Email']?>" class="form-control input-sm" /></td>
                    <td class="text-center">
                        <input type="password" name="Password[]" value="" class="form-control input-sm" />
                    </td>
                    <td class="text-center">
                        <select name="Role[]" class="form-control input-sm" >
                            <option value="user" <?=($user['Role'] == 'user' ? 'selected="selected"' : '')?>><?=t('User')?></option>
                            <option value="admin" <?=($user['Role'] == 'admin' ? 'selected="selected"' : '')?>><?=t('Administrator')?></option>
                        </select>
                    </td>
                    <td class="text-center">
                        <select name="Active[]" class="form-control input-sm" >
                            <option value="1" <?=$user['Active']==1?'selected="selected"':''?>><?=t('YesStr')?></option>
                            <option value="0" <?=$user['Active']==0?'selected="selected"':''?>><?=t('NoStr')?></option>
                        </select>
                    </td>
                    <td class="text-center"></td>
                </tr>
            <?php } ?>
            <tr class="newRow" style="display:none;">
                <td>
                    <input type="hidden" name="ID[]" value="" />
                    <input type="text" name="Name[]" value="" class="form-control input-sm required" />
                </td>
                <td><input type="email" name="Email[]" value="" class="form-control input-sm required" /></td>
                <td class="text-center">
                    <input type="password" name="Password[]" value="" class="form-control input-sm required" />
                </td>
                <td class="text-center">
                    <select name="Role[]" class="form-control input-sm" >
                        <option value="user" selected="selected"><?=t('User')?></option>
                        <option value="admin"><?=t('Administrator')?></option>
                    </select>
                </td>
                <td class="text-center">
                    <select name="Active[]" class="form-control input-sm" >
                        <option value="1" selected="selected"><?=t('YesStr')?></option>
                        <option value="0"><?=t('NoStr')?></option>
                    </select>
                </td>
                <td class="text-center"><button type="button" class="btn btn-default btn-danger btn-sm" onclick="removeRow(this);return false;"><?=t('Delete')?></button></td>
            </tr>
            <tr class="actionTr">
                <td colspan="5"><button type="button" class="btn btn-default btn-sm" onclick="addRow();return false;"><i class="fa fa-plus"></i> <?=t('AddNewUser')?></button></td>
            </tr>
        </table>

    </div>

</form>

<script>
<!--

    addRow = function(idTable) {
        $('#userTable .newRow').clone().removeClass('newRow').insertBefore('#userTable .newRow').show().find('.required').attr('required', 'required');
    }

    removeRow = function(elem) {
        $($(elem).closest('tr')).remove();
    }

    checkForm = function() {

        return true;

    }

-->
</script>

<?php include 'footer.php';