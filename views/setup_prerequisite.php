<!DOCTYPE html>
<html lang="<?=BFWK_LANG?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=HLX_NAME?></title>
    <link rel="icon" type="image/png" href="<?=BFWK_SERVER_ROOT?>favicon.png" />
    <link type="text/css" href="views/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="views/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="views/css/hlx.css" type="text/css" />
    <script type="text/javascript" src="views/js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="views/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="views/js/functions.js"></script>
</head>

<body>

<div class="container">
    <div class="panel panel-default">
    <div class="panel-body">

        <h1><img src="<?=BFWK_SERVER_ROOT?>views/img/logo.png" style="display: inline-block;margin-top: -5px;" /> <?=HLX_NAME?></h1>

        <form method="post" action="<?=$urlForm?>">

            <legend><?=t('SetupPrerequisites')?></legend>

            <?php foreach ($basics as $basic) {
                if ($basic['success']) { ?>
                    <p><i class="fa fa-check text-success"></i> <?=$basic['message']?></p>
                <?php } elseif ($basic['warning']) { ?>
                    <p class="text-warning"><i class="fa fa-exclamation-triangle"></i> <?=$basic['message']?></p>
                <?php } else { ?>
                    <p class="text-danger"><i class="fa fa-close"></i> <?=$basic['message']?></p>
                <?php }
            } ?>

            <?php foreach ($writables as $writable) {
                if ($writable['success']) { ?>
                    <p><i class="fa fa-check text-success"></i> <?=$writable['message']?></p>
                <?php } else { ?>
                    <p class="text-danger"><i class="fa fa-close"></i> <?=$writable['message']?></p>
                <?php }
            } ?>

            <br />

            <?php if ($errors) { ?>
                <a href="javascript:location.reload(true)" class="btn btn-danger">
                    <?=t('TryAgain')?>
                </a>
            <?php } else { ?>
                <input class="btn btn-primary" name="btnContinue" type="submit" value="<?=t('Continue')?>">
            <?php } ?>
            <br />
            <br />

        </form>

    </div>
    </div>
</div>