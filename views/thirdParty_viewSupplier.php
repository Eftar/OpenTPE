<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

<div id="headerbar" style="position:fixed;width:100%;background-color:white;z-index:99;">

    <div class="pull-left">
        <h1><?=t('SupplierAccount').' - '.$item['Name'].' '.$item['Firstname']?></h1>
    </div>

    <div class="pull-right btn-group">
        <a href="<?=$urlEditSupplier?>" class="btn btn-sm btn-primary">
            <i class="fa fa-edit"></i> <?=t('EditSupplier')?>
        </a>
    </div>

    <div class="pull-right btn-group hidden-sm hidden-xs" style="margin-right:10px">
         <a href="<?=$urlNewContact?>" class="btn btn-sm btn-default">
            <i  class="glyphicon glyphicon-user"></i> <?=t('CreateContact')?>
        </a>
        <a href="<?=$urlNewPurchase?>" class="btn btn-sm btn-default">
            <i class="fa fa-file"></i> <?=t('CreatePurchase')?>
        </a>
     </div>

     <div class="pull-right visible-sm-block visible-xs-block" style="margin-right:10px">
        <div class="btn-group btn-sm" style="padding:0" data-resize="auto">
            <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown">
                <span class="selected-label"><?=t('Actions')?></span>
                <span class="caret"></span>
                <span class="sr-only"><?=t('ToggleType')?></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li><a href="<?=$urlNewContact?>"><i  class="glyphicon glyphicon-user"></i> <?=t('CreateContact')?></a></li>
                <li><a href="<?=$urlNewPurchase?>"><i class="fa fa-file"></i> <?=t('CreatePurchase')?></a></li>
            </ul>
        </div>
    </div>

</div>

<ul id="supplierTabs" class="nav nav-tabs nav-tabs-noborder" style="padding-top: 45px;">
    <li class="active"><a data-toggle="tab" data-id="supplierDetails" href="#supplierDetails"><?=t('Details')?></a></li>
    <li><a data-toggle="tab" data-id="supplierContacts" href="#supplierContacts"><?=t('Contacts')?></a></li>
    <li><a data-toggle="tab" data-id="supplierDocuments" href="#supplierDocuments"><?=t('Documents')?></a></li>
    <li><a data-toggle="tab" data-id="supplierPayments" href="#supplierPayments"><?=t('Payments')?></a></li>
</ul>

<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="supplierDetails">

        <div class="container-fluid">

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-8">

                    <h3><?=(($item['Active']==1)?'<span class="glyphicon glyphicon-ok text-success"></span>':'<span class="glyphicon glyphicon-remove text-danger"></span>')?> <?=$item['Name'].' '.$item['Firstname']?></h3>

                    <p>
                        <?=$item['Address1']?><br />
                        <?=$item['Address2']?><br />
                        <?=$item['Address3']?><br />
                        <?=$item['PostalCode'].' '.$item['City']?><br />
                        <!--<?=$item['State']?><br />-->
                        <?=HLX_Country::getCountryName($item['CountryID'])?>
                    </p>

                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
                    <br />
                    <table class="table table-condensed table-bordered">
                        <tr>
                            <td>
                                <b><?=t('TotalPurchased')?></b>
                            </td>
                            <td class="amount">
                                <?=currency_format($item['Purchased'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b><?=t('TotalPaid')?></b>
                            </td>
                            <td class="amount">
                                <?=currency_format($item['Paid'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b><?=t('Balance')?></b>
                            </td>
                            <td class="amount">
                                <?=currency_format($item['Purchased'] - $item['Paid'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <hr/>

            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <h4><?=t('ContactInformations')?></h4>
                    <br/>
                    <table class="table table-condensed table-striped">
                        <tr>
                            <td><?=t('Phone')?></td>
                            <td><?=$item['Phone']?></td>
                        </tr>
                        <tr>
                            <td><?=t('Mobile')?></td>
                            <td><?=$item['Mobile']?></td>
                        </tr>
                        <tr>
                            <td><?=t('Fax')?></td>
                            <td><?=$item['Fax']?></td>
                        </tr>
                        <tr>
                            <td><?=t('Email')?></td>
                            <td><a href="mailto:<?=$item['Email']?>"><?=$item['Email']?></a></td>
                        </tr>
                        <tr>
                            <td><?=t('Web')?></td>
                            <td><a href="<?=$item['Web']?>"><?=$item['Web']?></a></td>
                        </tr>
                    </table>
                </div>

<!--
                <div class="col-xs-12 col-md-6">
                    <h4><?=t('Taxes')?></h4>
                    <br/>
                    <table class="table table-condensed table-striped">
                        <tr>
                            <td><?=t('VATNumber')?></td>
                            <td><?=$item['VAT']?></td>
                        </tr>
                    </table>
                </div>
-->

            </div>
    <!--
            <div class="row">
                <div class="col-xs-12 col-md-6">
                    <h4><?=t('Custom Fields')?></h4>
                    <br/>
                    <table class="table table-condensed table-striped">
                    </table>
                </div>
            </div>

            <hr/>

            <div>
                <h4><?=t('Notes')?></h4>
                <br/>

                <div id="notes_list">
                    <div class="alert alert-default">
                        <p><b>07/06/2015</b>&nbsp;
                            Une note ne peut être enlevée        </p>
                    </div>
                <div class="alert alert-default">
                    <p><b>07/06/2015</b>&nbsp;
                        Une note        </p>
                </div>
                </div>
                <div class="panel panel-default panel-body">
                    <form class="row">
                        <div class="col-xs-12 col-md-10">
                            <input type="hidden" name="client_id" id="client_id"
                                   value="1">
                            <textarea id="client_note" class="form-control" rows="1"></textarea>
                        </div>
                        <div class="col-xs-12 col-md-2 text-center">
                            <input type="button" id="save_client_note" class="btn btn-default btn-block"
                                   value="Ajouter une note">
                        </div>
                    </form>
                </div>
            </div>
-->
        </div>

    </div>
    <div role="tabpanel" class="tab-pane" id="supplierContacts">
        <!--INIT Contact List-->
        <?php

            $rOptions = array();
            //Repeater Id
            $rOptions['id'] = 'supplContactList';
            //No loading
            $rOptions['noloading'] = true;
            //url
            $rOptions['url'] = $urlLoadContacts;
            //Default Type
            $rOptions['type'] = 'contacts';
            //Array Types
            $rOptions['arrTypes'] = array();
            //default filter
            $rOptions['filter'] = 'active';
            //Array Filters
            $rOptions['arrFilters'] = array();
            $rOptions['arrFilters']['contacts'] = array(
                '0' => array(
                    'value'     => 'all',
                    'label'     => t('All')
                ),
                1 => array(
                    'value'     => 'active',
                    'label'     => t('Active')
                ),
                2 => array(
                    'value'     => 'inactive',
                    'label'     => t('Inactive')
                )
            );
            //Array Actions
            $rOptions['arrActions'] = array(
                0 => array(
                    'label'     => t('New').' '.t('Contact'),
                    'url'       => $urlNewContact
                )
            );
            //Object contenant les descriptions des colonnes en fonction du type
            $rOptions['arrColumns'] = array(
                'contacts' => array(
                    array(
                        'label'     => t('Name'),
                        'property'  => 'Name',
                        'className' => 'valign-middle',
                        'sortable'  => true
                    ),
                    array(
                        'label'     => t('Firstname'),
                        'property'  => 'Firstname',
                        'className' => 'valign-middle',
                        'sortable'  => true
                    ),
                    array(
                        'label'     => t('Function'),
                        'property'  => 'Function',
                        'className' => 'valign-middle',
                        'sortable'  => true
                    ),
                    array(
                        'label'     => t('Phone'),
                        'property'  => 'Phone',
                        'className' => 'valign-middle',
                        'width'     => 105,
                        'sortable'  => false
                    ),
                    array(
                        'label'     => t('Mobile'),
                        'property'  => 'Mobile',
                        'className' => 'valign-middle',
                        'width'     => 105,
                        'sortable'  => false
                    ),
                    array(
                        'label'     => t('Active'),
                        'property'  => 'Active',
                        'className' => 'valign-middle',
                        'width'     => 82,
                        'sortable'  => true
                    ),
                    array(
                        'label'     => t('Action'),
                        'property'  => 'Action',
                        'className' => 'valign-middle',
                        'width'     => 110,
                        'sortable'  => false
                    )
                )
            );
            // ColumnRender
            $rOptions['arrColumnsRenderer'] = array(
                'contacts' => array(
                    'Name'      => "<a href=\"".$urlViewContact."'+rowData.ID+'\">'+rowData.Name+'</a>",
                    'Phone'     => "<div class=\"text-center\">'+rowData.Phone+'</div>",
                    'Mobile'    => "<div class=\"text-center\">'+rowData.Mobile+'</div>",
                    'Action'    => "<div class=\"btn-group text-center\" style=\"min-width:85px\" data-id=\"'+rowData.ID+'\" >
                                    <a href=\"".$urlEditContact."'+rowData.ID+'\" type=\"button\" class=\"btn btn-default btn-sm\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span> ".t('Edit')."</a>
                                    <button type=\"button\" class=\"btn btn-default btn-sm dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><span class=\"caret\"></span><span class=\"sr-only\">Toggle Dropdown</span></button>
                                    <ul class=\"dropdown-menu dropdown-menu-right\">
                                    <li><a href=\"".$urlViewContact."'+rowData.ID+'\"><span class=\"glyphicon glyphicon-eye-open\" aria-hidden=\"true\"></span> ".t('View')."</a></li></ul>",
                    'Active'    => "<div class=\"text-center\">'+(rowData.Active == 1 ? '".t('YesStr')."' : '".t('NoStr')."')+'</div>"
                ),
            );
            HLX_View::hlxRepeater($rOptions);
        ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="supplierDocuments">
        <!--INIT Document List-->
        <script type="text/javascript">
            var today = new Date().toISOString().slice(0, 10);
        </script>
        <?php
            $rOptions = array();
            //Repeater Id
            $rOptions['id'] = 'supplPurchaseList';
            //No loading
            $rOptions['noloading'] = true;
            //url
            $rOptions['url'] = $urlLoadDocuments;
            //Default Type
            $rOptions['type'] = $listDocOptionType;
            //Array Types
            $rOptions['arrTypes'] = array();
            //default filter
            $rOptions['filter'] = $listDocOptionFilter;
            //Array Filters
            $rOptions['arrFilters'] = array();
            $rOptions['arrFilters'][5] = array();
            $rOptions['arrFilters'][5][] = array(
                'value'     => 'all',
                'label'     => t('All')
            );
            foreach($purchaseStatutes as $status) {
                $rOptions['arrFilters'][5][] = array(
                    'value'     => $status['ID'],
                    'label'     => $status['Name']
                );
            }

            //Settings for action button
            $rOptions['actionSettings'] = array(
                'label' => t('New'),
                'icon' => 'plus'
            );
            //Array Actions
            $rOptions['arrActions'] = array(
                0 => array(
                    'label'     => t('Purchase'),
                    'url'       => $urlNewPurchase
                )
            );
            //Object contenant les descriptions des colonnes en fonction du type
            $rOptions['arrColumns'] = array(
                5 => array(
                    array(
                        'label'     => t('Reference'),
                        'property'  => 'Reference',
                        'className' => 'valign-middle text-center',
                        'width'     => 100,
                        'sortable'  => true
                    ),
                    array(
                        'label'     => t('State'),
                        'property'  => 'State',
                        'className' => 'valign-middle',
                        'width'     => 150,
                        'sortable'  => true
                    ),
                    array(
                        'label'     => t('Validated'),
                        'property'  => 'DateValidated',
                        'className' => 'valign-middle text-center',
                        'width'     => 105,
                        'sortable'  => true
                    ),
                    array(
                        'label'     => t('ThirdPartyName'),
                        'property'  => 'ThirdPartyName',
                        'className' => 'valign-middle',
                        'sortable'  => false
                    ),
                    array(
                        'label'     => t('Receipt'),
                        'property'  => 'DateReceived',
                        'className' => 'valign-middle text-center',
                        'width'     => 105,
                        'sortable'  => false
                    ),
                    array(
                        'label'     => t('Due'),
                        'property'  => 'DateDue',
                        'className' => 'valign-middle text-center',
                        'width'     => 105,
                        'sortable'  => false
                    ),
                    array(
                        'label'     => t('Amount'),
                        'property'  => 'Total',
                        'className' => 'valign-middle',
                        'width'     => 100,
                        'sortable'  => true
                    ),
                    array(
                        'label'     => t('Balance'),
                        'property'  => 'Paid',
                        'className' => 'valign-middle',
                        'width'     => 100,
                        'sortable'  => true
                    ),
                    array(
                        'label'     => t('Action'),
                        'property'  => 'Action',
                        'className' => 'valign-middle text-center',
                        'width'     => 115,
                        'sortable'  => false
                    )
                )
            );
            //View render
            $rOptions['arrColumnsRenderer'] = array(
                5 => array(
                    'Reference'         => "<a href=\"".$urlEditPurchase."'+rowData.ID+'\">'+(rowData.Reference == '' ? '".htmlspecialchars(t('Draft'))."' : rowData.Reference)+'</a>",
                    'State'             => "<div class=\"label\" style=\"width:100%;background-color:#'+rowData.DocStatusColor+';display:block;\">'+rowData.DocStatusName+'</div>",
                    'DateValidated'     => "'+(rowData.DateValidated != null ? printDate('".$settings['GENERAL']['DateFormat']."', rowData.DateValidated) : '<b title=\"".htmlspecialchars(t('DocumentNotValidatedDateCreatedDisplayed'), ENT_QUOTES)."\">*</b>' + printDate('".$settings['GENERAL']['DateFormat']."', rowData.DateCreated))+'</div>",
                    'ThirdPartyName'    => "<a href=\"".$urlViewSupplier."'+rowData.ThirdPartyID+'\">' + rowData.ThirdPartyName + ' ' + rowData.ThirdPartyFirstname + '</a>",
                    'DateReceived'      => "<div class=\"' + (rowData.Received == 0 && rowData.DateReceived != '0000-00-00' && rowData.DateReceived < today ? 'text-danger text-bold' : '') + '\">' + (rowData.DateReceived != '0000-00-00' ? printDate('".$settings['GENERAL']['DateFormat']."', rowData.DateReceived) : '-') + '</div>",
                    'DateDue'           => "<div class=\"' + (rowData.DocStatusDraft == 0 && rowData.DocStatusClosed == 0 && rowData.Total != rowData.Paid && rowData.DateDue < today ? 'text-danger text-bold' : '') + '\">' + printDate('".$settings['GENERAL']['DateFormat']."', rowData.DateDue) + '</div>",
                    'Total'             => "<div class=\"amount ' + (rowData.DocStatusDraft == 0 && rowData.DocStatusClosed == 0 && rowData.Total != rowData.Paid && rowData.DateDue < today ? 'text-danger text-bold' : '') + '\">' + currency_format(rowData.Total, '".$settings['GENERAL']['Decimals']."', '".$settings['GENERAL']['DecimalSeparator']."', '', '".$settings['GENERAL']['CurrencySymbol']."', '".$settings['GENERAL']['CurrencyPlacement']."') + '</div>",
                    'Paid'              => "'+(rowData.Total==rowData.Paid ? '<div class=\"label\" style=\"background-color:#58a959;display:block;\">".htmlspecialchars(t('Paid'))."</div>' : '<div class=\"amount ' + (rowData.DocStatusDraft == 0 && rowData.DocStatusClosed == 0 && rowData.Total != rowData.Paid && rowData.DateDue < today ? 'text-danger text-bold' : '') + '\">' + currency_format(rowData.Total - rowData.Paid, '".$settings['GENERAL']['Decimals']."', '".$settings['GENERAL']['DecimalSeparator']."', '', '".$settings['GENERAL']['CurrencySymbol']."', '".$settings['GENERAL']['CurrencyPlacement']."') + '</div>')+'",
                    'Action'            => "<div class=\"btn-group text-center\" style=\"min-width:85px\" data-id=\"'+rowData.ID+'\" >
                                                <a role=\"button\" class=\"btn btn-default btn-sm\" href=\"".$urlEditPurchase."'+rowData.ID+'\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span> ".t('Edit')."</a>
                                                <button type=\"button\" class=\"btn btn-default btn-sm dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><span class=\"caret\"></span><span class=\"sr-only\">Toggle Dropdown</span></button>
                                                <ul class=\"dropdown-menu dropdown-menu-right\">
                                                <li><a href=\"".$urlPDFPurchase."'+rowData.ID+'\"><span class=\"fa fa-print\" aria-hidden=\"true\"></span> ".t('ViewPDF')."</a></li>
                                                <li class=\"'+(rowData.Total == rowData.Paid ? 'hidden' : '')+'\"><a href=\"".$urlNewPurchasePayment."'+rowData.ID+'\"><span class=\"fa fa-credit-card\" aria-hidden=\"true\"></span> ".t('EnterPayment')."</a></li>
                                                <li class=\"'+(rowData.Received == 0? 'hidden' : '')+'\"><a href=\"".$urlReceive."'+rowData.ID+'\"><span class=\"fa fa-truck\" aria-hidden=\"true\"></span> ".t('Receive')."</a></li></ul></div>"
                )
            );
            HLX_View::hlxRepeater($rOptions);
        ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="supplierPayments">
        <!--INIT Contact List-->
        <?php
            $rOptions = array();
            //Repeater Id
            $rOptions['id'] = 'supplPaymentList';
            //No loading
            $rOptions['noloading'] = true;
            //url
            $rOptions['url'] = $urlLoadPayments;
            //Default Type
            $rOptions['type'] = 'payments';
            //Array Types
            $rOptions['arrTypes'] = array();
            //default filter
            $rOptions['filter'] = 'all';
            //Array Filters
            $rOptions['arrFilters'] = array();
            //Array Actions
            $rOptions['arrActions'] = array(
                0 => array(
                    'label'     => t('New').' '.t('Payment'),
                    'url'       => $urlNewPayment
                )
            );
            //Object contenant les descriptions des colonnes en fonction du type
            $rOptions['arrColumns'] = array(
                'payments' => array(
                    array(
                        'label'     => t('ID'),
                        'property'  => 'ID',
                        'className' => 'valign-middle',
                        'width'     => 50,
                        'sortable'  => true
                    ),
                    array(
                        'label'     => t('PaymentDate'),
                        'property'  => 'Date',
                        'className' => 'valign-middle',
                        'width'     => 150,
                        'sortable'  => true
                    ),
                    array(
                        'label'     => t('Invoice'),
                        'property'  => 'DocumentReference',
                        'className' => 'valign-middle',
                        'width'     => 105,
                        'sortable'  => true
                    ),
                    array(
                        'label'     => t('InvoiceDate'),
                        'property'  => 'DocumentDateCreated',
                        'className' => 'valign-middle',
                        'width'     => 150,
                        'sortable'  => true
                    ),
                    array(
                        'label'     => t('Amount'),
                        'property'  => 'Amount',
                        'className' => 'valign-middle',
                        'width'     => 120,
                        'sortable'  => false
                    ),
                    array(
                        'label'     => t('PaymentMethod'),
                        'property'  => 'PaymentMethodName',
                        'className' => 'valign-middle',
                        'width'     => 170,
                        'sortable'  => true
                    ),
                    array(
                        'label'     => t('Note'),
                        'property'  => 'Note',
                        'sortable'  => true
                    ),
                    array(
                        'label'     => t('Action'),
                        'property'  => 'Action',
                        'className' => 'valign-middle',
                        'width'     => 115,
                        'sortable'  => false
                    )
                )
            );
            // ColumnRender
            $rOptions['arrColumnsRenderer'] = array(
                'payments' => array(
                    'ID'                    => "<div class=\"text-center\"><a href=\"".$urlViewPayment."'+rowData.ID+'\">#'+rowData.ID+'</a></div>",
                    'PaymentMethodName'     => "<div class=\"text-center\">'+rowData.PaymentMethodName+'</div>",
                    'Date'                  => "<div class=\"text-center\">' + printDate('".$settings['GENERAL']['DateFormat']."', rowData.Date) + '</a></div>",
                    'DocumentReference'     => "<div class=\"text-center\"><a href=\"".$urlViewPurchase."'+rowData.DocumentID+'\">'+rowData.DocumentReference+'</a></div>",
                    'DocumentDateCreated'   => "<div class=\"text-center\">' + printDate('".$settings['GENERAL']['DateFormat']."', rowData.DocumentDateCreated) + '</div>",
                    'Amount'                => "<div class=\"amount\" >' + currency_format(rowData.Amount, '".$settings['GENERAL']['Decimals']."', '".$settings['GENERAL']['DecimalSeparator']."', '', '".$settings['GENERAL']['CurrencySymbol']."', '".$settings['GENERAL']['CurrencyPlacement']."') + '</div>",
                    'Note'                  => "<div class=\"note\">'+nl2br(rowData.Note)+'</div>",
                    'Action'                => "<div class=\"text-center\"><a role=\"button\" class=\"btn btn-default btn-sm\" href=\"".$urlViewPayment."'+rowData.ID+'\"><span class=\"glyphicon glyphicon-eye-open\" aria-hidden=\"true\"></span> ".t('View')."</a></div>",
                )
            );
            HLX_View::hlxRepeater($rOptions);

        ?>
    </div>
</div>
<script>
<!--

    ignoreNextHash = false;

    $('#supplierTabs a').click(function (e) {
        e.preventDefault();

        var id = $(this).data('id');
        $(this).tab('show');
        switch(id) {
            case 'supplierContacts':
                hlxRepeater('supplContactList');
                break;
            case'supplierDocuments':
                hlxRepeater('supplPurchaseList');
                break;
            case'supplierPayments':
                hlxRepeater('supplPaymentList');
                break;
        }
        ignoreNextHash = true;
        window.location = '#'+id;
        return false;
    });

    //Got to last opened tab
    var lastOpened = function() {
        var url = document.location.toString();
        if (url.match('#')) {
            $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').click();
        }
        else {
            $('.nav-tabs a:first').click();
        }
    }
    lastOpened();
    //Check browser back with anchor
    $(window).on('hashchange', function() {
        if ( ! ignoreNextHash)
            lastOpened();
        else
            ignoreNextHash = false;
    });

-->
</script>

<?php include 'footer.php';