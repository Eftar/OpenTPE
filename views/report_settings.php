
<div id="reportSettings">
    <?php foreach($report['Variables'] as $idx => $variable) { ?>
        <div class="form-group">
            <label for="<?=$variable['name']?>"><?=$variable['label']?></label>
            <?php if ($variable['type'] == 'date') { ?>
                <input type="date" class="form-control" id="<?=$variable['name']?>" name="<?=$variable['name']?>" value="<?=($variable['default']!=''?printDate($settings['GENERAL']['DateFormat'], $variable['default']):'')?>" placeholder="<?=$settings['GENERAL']['DateFormat']?>" />
            <?php } else { ?>
                <input type="text" class="form-control" id="<?=$variable['name']?>" name="<?=$variable['name']?>" value="<?=$variable['default']?>" placeholder="<?=$variable['placeholder']?>" />
            <?php } ?>
        </div>
    <?php } ?>
</div>