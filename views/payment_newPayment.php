<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

<script type="text/javascript" src="views/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="views/js/bootstrap-datepicker-lang.min.js"></script>
<link rel="stylesheet" href="views/css/bootstrap-datepicker3.min.css" type="text/css" />

<form action="<?=$urlSave?>" method="POST">

    <div id="headerbar" style="position:fixed;width:100%;background-color:white;z-index:99;">

        <div class="pull-left">
            <h1><?=t('NewPayment')?></h1>
        </div>

        <?php if ( ! empty($documents)) { ?>
            <div class="pull-right btn-group">
                <a class="btn btn-sm btn-default" href="#" onclick="history.back();return false;">
                    <i class="glyphicon glyphicon-remove"></i> <?=t('Cancel')?>
                </a>
                <button class="btn btn-sm btn-primary">
                    <i class="fa fa-edit"></i> <?=t('Save')?>
                </button>
            </div>
        <?php } ?>

    </div>

    <div class="container-fluid" style="padding-top:45px;">

        <?php if (empty($documents)) { ?>

            <br /><br />
            <div class="alert alert-danger" role="alert">

                <a class="btn btn-sm btn-default pull-right" style="margin-top: -5px;" href="<?=$urlCancel?>" onclick="history.back();return false;">
                    <i class="glyphicon glyphicon-chevron-left"></i> <?=t('Back')?>
                </a>

                <?=t('NoDocumentToPay')?>

            </div>

        <?php } else { ?>

            <div class="row">
                <br />
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-4">
                    <label><?=t('Invoice')?> * : </label><br />
                    <div class="form-group">
                        <select name="DocumentID" required="required" id="DocumentID" class="col-xs-12 form-control input-sm">
                            <?=(count($documents) > 1 ? '<option value="" data-amount="">'.t('Select').'</option>' : '' )?>
                            <?php foreach($documents as $document) { ?>
                                <option value="<?=$document['ID']?>" data-amount="<?=currency_format($document['Total'] - $document['Paid'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'])?>"><?=$document['Reference']?>   |   <?=trim($document['ThirdPartyName'].' '.$document['ThirdPartyFirstname'])?>   |   <?=t('Total').' : '.currency_format($document['Total'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?>   |   <?=t('Paid').' : '.currency_format($document['Paid'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?>   |   <?=t('Balance').' : '.currency_format($document['Total'] - $document['Paid'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <br />
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-4">
                    <label><?=t('PaymentMethod')?> * : </label><br />
                    <div class="form-group">
                        <select name="PaymentMethodID" class="col-xs-12 form-control input-sm">
                            <?php foreach($settings['PAYMENT'] as $payment) { ?>
                                <option value="<?=$payment['ID']?>" ><?=$payment['Name']?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <br />
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-4">
                    <label><?=t('Date')?> : </label><br />
                    <div class="input-group date datepicker">
                        <input type="text" class="form-control input-sm" id="Date" name="Date" value="<?=printDate($settings['GENERAL']['DateFormat'], date('Y-m-d'))?>" /><span class="input-group-addon input-sm "><i class="fa fa-calendar fa-fw"></i></span>
                    </div>
                    <br />
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-4">
                    <label><?=t('Amount')?> * : </label><br />
                    <div class="form-group">
                        <input type="text" name="Amount" required="required" id="Amount" class="form-control input-sm" value="" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-10 col-md-8 col-lg-7">
                    <label><?=t('Note')?> : </label>
                    <div class="form-group">
                        <textarea name="Note" class="form-control input-sm"></textarea>
                    </div>
                </div>
            </div>

        <?php } ?>

    </div>

</form>

<script>
<!--
    //Date pickers
    $('.input-group.date.datepicker').datepicker({
        format: "<?=str_replace(array('d','m','y','Y'), array('dd','mm','yy','yyyy'), $settings['GENERAL']['DateFormat'])?>",
        todayBtn: "linked",
        language: "fr",//TODO set language according
        daysOfWeekHighlighted: "0,6"
    });
    $('#DocumentID').change(function() {
        var amount = $(this).find(":selected").data('amount');
        if (amount)
            $('#Amount').val(amount);
    });
    $('#DocumentID').change();
-->
</script>

<?php include 'footer.php';