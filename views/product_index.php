<?php include 'header.php'; ?>

<style type="text/css">

    #productsList .description {
        position: absolute;
        top: 5px;
        width: 95%;
        height: 80%;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
    }

    .repeater-list-wrapper > table.table td {
        position: relative;
    }

</style>

<?php include 'navbar.php';

    $rOptions = array();
    //Repeater Id
    $rOptions['id'] = 'productsList';
    //url
    $rOptions['url'] = $urlLoadList;
    //Default Type
    $rOptions['type'] = 'products';
    //Array Types
    $rOptions['arrTypes'] = array();
    //default filter
    $rOptions['filter'] = 'active';
    //Array Filters
    $rOptions['arrFilters'] = array(
        'products' => array(
            '0' => array(
                'value'     => 'all',
                'label'     => t('All')
            ),
            1 => array(
                'value'     => 'active',
                'label'     => t('Active')
            ),
            2 => array(
                'value'     => 'inactive',
                'label'     => t('Inactive')
            ),
            3 => array(
                'value'     => 'onSale',
                'label'     => t('OnSale')
            ),
            4 => array(
                'value'     => 'notOnSale',
                'label'     => t('NotOnSale')
            )
        )
    );
    //Settings for action button
    $rOptions['actionSettings'] = array(
        'label' => t('New'),
        'icon' => 'plus'
    );
    //Array Actions
    $rOptions['arrActions'] = array(
        0 => array(
            'label'     => t('New').' '.t('Product'),
            'url'       => $urlNewProduct
        )
    );
    //Object contenant les descriptions des colonnes en fonction du type
    $rOptions['arrColumns'] = array(
        'products' => array(
            array(
                'label'     => t('Type'),
                'property'  => 'Type',
                'className' => 'valign-middle text-center',
                'width'     => 50,
                'sortable'  => true
            ),
            array(
                'label'     => t('Reference'),
                'property'  => 'Reference',
                'className' => 'valign-middle',
                'width'     => 105,
                'sortable'  => true
            ),
            array(
                'label'     => t('Label'),
                'property'  => 'Name',
                'className' => 'valign-middle',
                'sortable'  => true
            ),
            array(
                'label'     => t('Description'),
                'property'  => 'Description',
                'sortable'  => false
            ),
            array(
                'label'     => t('Unity'),
                'property'  => 'UnityValue',
                'className' => 'valign-middle text-center',
                'width'     => 70,
                'sortable'  => true
            ),
            array(
                'label'     => t('Price'),
                'property'  => 'Price',
                'className' => 'valign-middle',
                'width'     => 120,
                'sortable'  => true
            ),
            array(
                'label'     => t('VAT'),
                'property'  => 'TaxName',
                'className' => ($settings['GENERAL']['UseVAT'] == 0 ? 'hidden valign-middle' : 'valign-middle'),
                'width'     => 85,
                'sortable'  => true
            ),
            array(
                'label'     => t('Active'),
                'property'  => 'Active',
                'className' => 'valign-middle',
                'width'     => 82,
                'sortable'  => true
            ),
            array(
                'label'     => t('Action'),
                'property'  => 'Action',
                'className' => 'valign-middle',
                'width'     => 115,
                'sortable'  => false
            )
        )
    );

    //View render
    $rOptions['arrColumnsRenderer'] = array(
        'products' => array(
            'Type'          => "'+(rowData.Type == 1 ? '<span class=\"fa fa-cube\"></span>' : '<span class=\"fa fa-hand-paper-o\"></span>')+'",
            'Reference'     => "<a href=\"".$urlViewProduct."'+rowData.ID+'\">'+rowData.Reference+'</a>",
            'Name'          => "<a href=\"".$urlViewProduct."'+rowData.ID+'\">'+rowData.Name+'</a>",
            'Price'         => "<div class=\"amount\">' + currency_format(rowData.Price, '".$settings['GENERAL']['Decimals']."', '".$settings['GENERAL']['DecimalSeparator']."', '', '".$settings['GENERAL']['CurrencySymbol']."', '".$settings['GENERAL']['CurrencyPlacement']."') + '</div>",
            'Description'   => "<div class=\"description\">'+nl2br(rowData.Description)+'</div>",
            'Active'        => "<div class=\"text-center\">'+(rowData.Active == 1 ? '".t('YesStr')."' : '".t('NoStr')."')+'</div>",
            'Action'        => "<div class=\"text-center\"><div class=\"btn-group text-center\" style=\"min-width:85px\" data-id=\"'+rowData.ID+'\" >
                                        <a role=\"button\" class=\"btn btn-default btn-sm\" href=\"".$urlEditProduct."'+rowData.ID+'\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span> ".t('Edit')."</a>
                                        <button type=\"button\" class=\"btn btn-default btn-sm dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><span class=\"caret\"></span><span class=\"sr-only\">Toggle Dropdown</span></button>
                                        <ul class=\"dropdown-menu dropdown-menu-right\">
                                        <li><a href=\"".$urlViewProduct."'+rowData.ID+'\"><span class=\"glyphicon glyphicon-eye-open\" aria-hidden=\"true\"></span> ".t('View')."</a></li>
                                        <li><a href=\"".$urlDeleteProduct."'+rowData.ID+'\" style=\"color:red !important;\"><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span> ".t('Delete')."</a></li></ul></div></div>",
        )
    );
    HLX_View::hlxRepeater($rOptions);
?>

<script>
<!--

    $(document).keyup(function(e)
    {
        if ($('input:focus').length > 0 || $('textarea:focus').length > 0)
        {
            return false;
        }

        if (e.keyCode == true)
        {
            var key = e.keyCode;
        }
        else
        {
            var key = e.which;
        }

        switch (key)
        {
            case 113://F2
                window.location.href = "<?=$urlNewProduct?>";
                return false;
                break;
        }
    });

-->
</script>

<?php include 'footer.php';