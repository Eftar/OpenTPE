<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

<div id="headerbar" style="position:fixed;width:100%;background-color:white;z-index:99;">

    <div class="pull-left">
        <h1><?=t('ContactAccount').' - '.$item['Name'].' '.$item['Firstname']?></h1>
    </div>

    <div class="pull-right btn-group">
        <a href="<?=$urlEditContact?>"class="btn btn-sm btn-primary">
            <i class="fa fa-edit"></i> <?=t('EditContact')?>
        </a>
    </div>

</div>

<div class="container-fluid" style="padding-top:45px;">

    <div class="row">

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-8">
            <h3><?=(($item['Active']==1)?'<span class="glyphicon glyphicon-ok text-success"></span>':'<span class="glyphicon glyphicon-remove text-danger"></span>')?> <?=$item['Name'].' '.$item['Firstname']?></h3>

            <p>
                <?=$item['Function']?><br>
            </p>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
            <br />
            <table class="table table-condensed table-bordered">
                <tr>
                    <td>
                        <b><?=t('Company')?></b>
                    </td>
                    <td>
                        <a href="<?=$urlViewCustomer.$item['CompanyID']?>"><?=$item['CompanyName']?></a>
                    </td>
                </tr>
            </table>
        </div>

    </div>

    <hr/>

    <div class="row">
        <div class="col-xs-12 col-md-6">
            <h4><?=t('ContactInformations')?></h4>
            <br/>
            <table class="table table-condensed table-striped">
                    <tr>
                        <td><?=t('Phone')?></td>
                        <td><?=$item['Phone']?></td>
                    </tr>
                    <tr>
                        <td><?=t('Mobile')?></td>
                        <td><?=$item['Mobile']?></td>
                    </tr>
                    <tr>
                        <td><?=t('Email')?></td>
                        <td><a href="mailto:<?=$item['Email']?>"><?=$item['Email']?></a></td>
                    </tr>
            </table>
        </div>
    </div>
<!--
            <hr/>

            <div>
                <h4>Note</h4>
                <br/>

                <div id="notes_list">
                        <div class="alert alert-default">
        <p><b>07/06/2015</b>&nbsp;
            Une note ne peut être enlevée        </p>
    </div>
    <div class="alert alert-default">
        <p><b>07/06/2015</b>&nbsp;
            Une note        </p>
    </div>
                </div>
                <div class="panel panel-default panel-body">
                    <form class="row">
                        <div class="col-xs-12 col-md-10">
                            <input type="hidden" name="client_id" id="client_id"
                                   value="1">
                            <textarea id="client_note" class="form-control" rows="1"></textarea>
                        </div>
                        <div class="col-xs-12 col-md-2 text-center">
                            <input type="button" id="save_client_note" class="btn btn-default btn-block"
                                   value="Ajouter une note">
                        </div>
                    </form>
                </div>
            </div>

        </div>
-->
</div>

<script>
<!--

-->
</script>

<?php include 'footer.php';