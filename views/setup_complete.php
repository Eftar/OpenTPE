<!DOCTYPE html>
<html lang="<?=BFWK_LANG?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hélix GC</title>
    <link rel="icon" type="image/png" href="<?=BFWK_SERVER_ROOT?>favicon.png" />
    <link type="text/css" href="views/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="views/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="views/css/hlx.css" type="text/css" />
    <script type="text/javascript" src="views/js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="views/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="views/js/functions.js"></script>
</head>

<body>

<div class="container">
    <div class="panel panel-default">
    <div class="panel-body">

        <h1><img src="<?=BFWK_SERVER_ROOT?>views/img/logo.png" style="display: inline-block;margin-top: -5px;" /> <?=HLX_NAME?></h1>

        <?php if ($save == true) { ?>

            <div class="alert alert-info"><?=t('DataRecordedGoToLogin', $urlLogin)?></div>

        <?php } else { ?>

            <form method="post" action="<?=$urlForm?>">

                <legend><?=t('SetupSettings')?></legend>

                <br />

                <div class="row">

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label><?=t('CompanyName')?> :</label>
                            <input type="text" required="required" value="" name="CompanyName" class="form-control input-sm" />
                        </div>

                        <div class="form-group">
                            <label for="CompanyType" class="control-label"><?=t('CompanyType')?></label>
                            <input type="text" value="" name="CompanyType" id="CompanyType" class="input-sm form-control" />
                        </div>

                        <div class="form-group">
                            <label for="CompanyCapital" class="control-label"><?=t('Capital')?></label>
                            <input type="text" value="" name="CompanyCapital" id="CompanyCapital" class="input-sm form-control" />
                        </div>

                        <div class="form-group">
                            <label for="CompanyRegistrationNumber" class="control-label"><?=t('RegistrationNumber')?></label>
                            <input type="text" value="" name="CompanyRegistrationNumber" id="CompanyRegistrationNumber" class="input-sm form-control" />
                        </div>

                        <div class="form-group">
                            <label for="CompanyRegister" class="control-label"><?=t('Register')?></label>
                            <input type="text" value="" name="CompanyRegister" id="CompanyRegister" class="input-sm form-control" />
                        </div>

                        <div class="form-group">
                            <label for="CompanyClassification" class="control-label"><?=t('Classification')?></label>
                            <input type="text" value="" name="CompanyClassification" id="CompanyClassification" class="input-sm form-control" />
                        </div>

                        <div class="form-group">
                            <label for="CompanyTaxNumber" class="control-label"><?=t('VATNumber')?></label>
                            <input type="text" value="" name="CompanyTaxNumber" id="CompanyTaxNumber" class="input-sm form-control" />
                        </div>

                        <div class="form-group">
                            <label><?=t('Address')?> :</label>
                            <input type="mail" value="" name="CompanyAddress1" class="form-control input-sm" />
                            <input type="mail" value="" name="CompanyAddress2" class="form-control input-sm" />
                            <input type="mail" value="" name="CompanyAddress3" class="form-control input-sm" />
                        </div>

                        <div class="form-group">
                            <label><?=t('PostalCode')?> :</label>
                            <input type="text" value="" name="CompanyPostalCode" class="form-control input-sm" />
                        </div>

                        <div class="form-group">
                            <label><?=t('City')?> :</label>
                            <input type="text" value="" name="CompanyCity" class="form-control input-sm" />
                        </div>

                        <fieldset>
                            <legend><?=t('Contact')?></legend>
                            <div class="form-group">
                                <label for="CompanyPhone" class="control-label"><?=t('Phone')?></label>
                                <input type="text" value="" name="CompanyPhone" id="CompanyPhone" class="input-sm form-control" />
                            </div>
                            <div class="form-group">
                                <label for="CompanyFax" class="control-label"><?=t('Fax')?></label>
                                <input type="text" value="" name="CompanyFax" id="CompanyFax" class="input-sm form-control" />
                            </div>
                            <div class="form-group">
                                <label for="CompanyEmail" class="control-label"><?=t('Email')?></label>
                                <input type="text" value="" name="CompanyEmail" id="CompanyEmail" class="input-sm form-control" />
                            </div>
                            <div class="form-group">
                                <label for="CompanyWeb" class="control-label"><?=t('Web')?></label>
                                <input type="text" value="" name="CompanyWeb" id="CompanyWeb" class="input-sm form-control" />
                            </div>
                        </fieldset>

                    </div>

                    <div class="col-xs-6">
                        <fieldset>
                            <legend><?=t('Features')?></legend>
                            <div class="form-group">
                                <label for="UseBillingAddress" class="control-label"><?=t('UseBillingAddress')?></label>
                                <select name="UseBillingAddress" id="UseBillingAddress" class="input-sm form-control">
                                    <option value="1" selected="selected"><?=t('YesStr')?></option>
                                    <option value="0"><?=t('NoStr')?></option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="UseDeliveryAddress" class="control-label"><?=t('UseDeliveryAddress')?></label>
                                <select name="UseDeliveryAddress" id="UseDeliveryAddress" class="input-sm form-control">
                                    <option value="1" selected="selected"><?=t('YesStr')?></option>
                                    <option value="0"><?=t('NoStr')?></option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="UseDiscount" class="control-label"><?=t('UseDiscount')?></label>
                                <select name="UseDiscount" id="UseDiscount" class="input-sm form-control">
                                    <option value="1" selected="selected"><?=t('YesStr')?></option>
                                    <option value="0"><?=t('NoStr')?></option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="UseVAT"><?=t('UseVAT')?></label>
                                <select name="UseVAT" id="UseVAT" class="form-control input-sm">
                                    <option value="1" selected="selected"><?=t('YesStr')?></option>
                                    <option value="0" ><?=t('NoStr')?></option>
                                </select>
                            </div>
                            <br />
                        </fieldset>
                        <fieldset>
                            <legend><?=t('AutoSettings')?></legend>
                            <div class="form-group">
                                <label for="AutoSettings"><?=t('AutoSettings')?></label>
                                <select name="AutoSettings" id="AutoSettings" class="form-control input-sm">
                                    <option value="" selected="selected"><?=t('Select')?></option>
                                    <option value="FR_SARL" >SARL / EURL France</option>
                                    <option value="FR_EI" >Entreprise individuelle France</option>
                                    <option value="FR_MICRO" >Micro-entreprise France</option>
                                </select>
                                <div class="text-muted text-info"><?=t('AutoSettingsInformation')?></div>
                            </div>
                        </fieldset>
                    </div>

                </div>

                <br />

                <a href="<?=$urlLogin?>" class="btn btn-default">
                    <?=t('PassStep')?>
                </a>

                <input class="btn btn-primary" name="btnValidate" type="submit" value="<?=t('Validate')?>">

            </form>

            <script type="text/javascript">

                $('#AutoSettings').change(function() {
                    if ($(this).val() == 'FR_MICRO') {
                        $('#UseVAT').val('0');
                    }
                });

            </script>

        <?php } ?>

    </div>
    </div>
</div>