<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

<script type="text/javascript" src="views/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="views/css/bootstrap-select.min.css" type="text/css" />

<form action="<?=$urlSave?>" method="POST">

    <input type="hidden" name="ID" class="form-control" value="<?=$item['ID']?>" />
    <input type="hidden" name="Type" class="form-control" value="S" />
    <input type="hidden" name="Active" value="0" />

    <div id="headerbar" style="position:fixed;width:100%;background-color:white;z-index:99;">

        <div class="pull-left">
            <h1><?=t('EditSupplier').' - '.$item['Name'].' '.$item['Firstname']?></h1>
        </div>

        <div class="pull-right btn-group">
            <a class="btn btn-sm btn-default" href="<?=$urlCancel?>" onclick="history.back();return false;">
                <i class="glyphicon glyphicon-remove"></i> <?=t('Cancel')?>
            </a>
            <button class="btn btn-sm btn-primary">
                <i class="fa fa-edit"></i> <?=t('Save')?>
            </button>
        </div>

    </div>

    <div class="container-fluid" style="padding-top:45px;">

        <div class="row">
            <br />
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-8">
                <label><?=t('Name')?> * :</label><br />
                <div class="input-group input-group-lg">
                      <input type="text" name="Name" required="required" class="form-control" value="<?=$item['Name']?>" />
                      <span class="input-group-addon">
                            <input type="checkbox" name="Active" value="1" <?=($item['Active'] == 1 ? 'checked="checked"' : '')?> /> <?=t('Active')?>
                      </span>
                </div>
                <br />
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-8">
                <label><?=t('Firstname')?> :</label>
                <input type="text" name="Firstname" class="form-control" value="<?=$item['Firstname']?>" />
            </div>
        </div>

        <br />

        <div class="row">

            <div class="col-xs-12 col-sm-6">
                <fieldset>
                    <legend><?=t('Address')?></legend>

                    <div class="form-group">
                        <label><?=t('Address')?> :</label>
                        <input type="text" name="Address1" class="form-control input-sm" value="<?=$item['Address1']?>" />
                        <input type="text" name="Address2" class="form-control input-sm" value="<?=$item['Address2']?>" />
                        <input type="text" name="Address3" class="form-control input-sm" value="<?=$item['Address3']?>" />
                    </div>

                    <div class="row">
                        <div class="form-group col-lg-3 col-sm-4">
                            <label><?=t('PostalCode')?> :</label>
                            <input type="text" name="PostalCode" class="form-control input-sm" value="<?=$item['PostalCode']?>" />
                        </div>

                        <div class="form-group col-lg-9 col-sm-8">
                            <label><?=t('City')?> :</label>
                            <input type="text" name="City" class="form-control input-sm" value="<?=$item['City']?>" />
                        </div>
                     </div>

                     <div class="row">
    <!--
                        <div class="form-group col-xs-6">
                            <label><?=t('State')?> :</label>
                            <input type="text" name="State" class="form-control input-sm" value="<?=$item['State']?>" />
                        </div>
    -->
                        <div class="form-group col-xs-6">
                            <label><?=t('Country')?> :</label>
                            <select id="CountryID" name="CountryID" class="selectpicker" data-size="10" data-width="100%" data-live-search="true">
                                <option value="" ><?=t('Select')?></option>
                                <?php foreach($countries as $code => $country) { ?>
                                    <option value="<?=$code?>" <?=($code==$item['CountryID'] ? 'selected="selected"' : '')?>><?=htmlspecialchars($country)?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                 </fieldset>
            </div>

            <div class="col-xs-12 col-sm-6">
                <fieldset>

                    <legend><?=t('ContactInformations')?></legend>

                    <div class="form-group">
                        <label><?=t('Phone')?> :</label>
                        <input type="text" name="Phone" class="form-control input-sm" value="<?=$item['Phone']?>" />
                    </div>

                    <div class="form-group">
                        <label><?=t('Mobile')?> :</label>
                        <input type="text" name="Mobile" class="form-control input-sm" value="<?=$item['Mobile']?>" />
                    </div>

                    <div class="form-group">
                        <label><?=t('Fax')?> :</label>
                        <input type="text" name="Fax" class="form-control input-sm" value="<?=$item['Fax']?>" />
                    </div>

                    <div class="form-group">
                        <label><?=t('Email')?> :</label>
                        <input type="text" name="Email" class="form-control input-sm" value="<?=$item['Email']?>" />
                    </div>

                    <div class="form-group">
                        <label><?=t('Web')?> :</label>
                        <input type="text" name="Web" class="form-control input-sm" value="<?=$item['Web']?>" />
                    </div>

                </fieldset>
            </div>
<!--
            <div class="col-xs-12 col-sm-6">
                <fieldset>
                    <legend><?=t('Taxe information')?></legend>

                    <div class="form-group">
                        <label><?=t('VAT ID')?> :</label>
                        <input type="text" name="VAT" class="form-control input-sm" value="<?=$item['VAT']?>" />
                    </div>

                </fieldset>
            </div>
-->
        </div>

    </div>

</form>

<script>
<!--
-->
</script>

<?php include 'footer.php';