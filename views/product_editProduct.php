<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

<form action="<?=$urlSave?>" method="POST">

    <input type="hidden" name="ID" class="form-control" value="<?=$item['ID']?>" />
    <input type="hidden" name="Active" class="form-control" value="0" />

    <div id="headerbar" style="position:fixed;width:100%;background-color:white;z-index:99;">

        <div class="pull-left">
            <h1><?=t('EditProduct').' - '.$item['Name']?></h1>
        </div>

        <div class="pull-right btn-group">
            <a class="btn btn-sm btn-default" href="<?=$urlCancel?>" onclick="history.back();return false;">
                <i class="glyphicon glyphicon-remove"></i> <?=t('Cancel')?>
            </a>
            <button class="btn btn-sm btn-primary">
                <i class="fa fa-edit"></i> <?=t('Save')?>
            </button>
        </div>

    </div>

    <div class="container-fluid" style="padding-top:45px;">

            <div class="row">
                <br />
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-4">
                    <label><?=t('Reference')?> * : </label><br />
                    <div class="input-group input-group-lg">
                          <input type="text" name="Reference" required="required" class="form-control" value="<?=$item['Reference']?>" />
                          <span class="input-group-addon">
                            <input type="checkbox" name="Active" value="1" <?=($item['Active'] == 1 ? 'checked="checked"' : '')?> /> <?=t('Active')?>
                          </span>
                    </div>
                </div>
            </div>

            <div class="row">
                <br />
                <div class="col-xs-12 col-sm-10 col-md-8 col-lg-7">
                    <label><?=t('Label')?> * : </label><br />
                    <div class="controls">
                          <input type="text" name="Name" required="required" class="form-control input-lg" value="<?=$item['Name']?>" />
                    </div>
                    <br />
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-10 col-md-8 col-lg-7">
                    <label><?=t('Description')?> : </label><br />
                    <textarea name="Description" class="form-control autoheight"><?=$item['Description']?></textarea><br />
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-md-8 col-lg-7">
                    <label><?=t('Type')?> * : </label>
                    <div class="controls">
                        <select name="Type" class="form-control input-sm">
                            <option value="1" <?=($item['Type']== '1'?'selected="selected"':'')?> ><?=t('Product')?></option>
                            <option value="2" <?=($item['Type']== '2'?'selected="selected"':'')?> ><?=t('Service')?></option>
                        </select>
                    </div>
                    <br />
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-md-8 col-lg-7">
                    <label><?=t('OnSale')?> * : </label>
                    <div class="controls">
                        <select name="OnSale" class="form-control input-sm">
                            <option value="1" <?=($item['OnSale']== '1'?'selected="selected"':'')?> ><?=t('YesStr')?></option>
                            <option value="0" <?=($item['OnSale']== '0'?'selected="selected"':'')?> ><?=t('NoStr')?></option>
                        </select>
                    </div>
                    <br />
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-md-8 col-lg-7">
                    <label><?=t('Unity')?> * : </label>
                    <div class="controls">
                        <select name="UnityID" class="form-control input-sm">
                            <?php if (isset($settings['UNITY'])) { ?>
                                <?php foreach($settings['UNITY'] as $unit) { ?>
                                    <option value="<?=$unit['ID']?>" <?=($item['UnityID']==$unit['ID']?'selected="selected"':'')?> ><?=$unit['Name']?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <br />
                    <br />
                </div>
            </div>

            <div class="row">

                <div class="col-xs-12 col-sm-6">
                    <fieldset>
                        <legend><?=t('Sell')?></legend>

                            <div class="form-group <?=($settings['GENERAL']['UseVAT'] == 0 ? 'hidden' : '')?>">
                                <label><?=t('PriceExclTax')?>: </label>
                                <div class="row">
                                    <div class="controls col-xs-6 col-sm-6 col-md-5 col-lg-4">
                                        <input type="text" name="Price" id="Price" class="form-control input-sm" value="<?=currency_format($item['Price'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'])?>" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group <?=($settings['GENERAL']['UseVAT'] == 0 ? 'hidden' : '')?>">
                                <label><?=t('VAT')?>: </label>
                                <div class="row">
                                    <div class="controls col-xs-6 col-sm-6 col-md-5 col-lg-4">
                                        <select name="TaxID" id="TaxID" class="col-xs-12 form-control input-sm">
                                            <?php foreach($taxes as $taxe) { ?>
                                                <option value="<?=$taxe['ID']?>" <?=($item['TaxID']==$taxe['ID']?'selected="selected"':'')?> data-rate="<?=$taxe['Rate']?>" ><?=$taxe['Name']?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label><?=t('PriceInclTax')?>: </label>
                                <div class="row">
                                    <div class="controls col-xs-6 col-sm-6 col-md-5 col-lg-4">
                                        <input type="text" name="PriceTTC" id="PriceTTC" class="form-control input-sm" value="<?=currency_format($item['PriceTTC'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'])?>" />
                                    </div>
                                </div>
                            </div>

                    </fieldset>
                </div>

                <div class="col-xs-12 col-sm-6">
                    <fieldset>
                        <legend><?=t('Buy')?></legend>

                            <div class="form-group <?=($settings['GENERAL']['UseVAT'] == 0 ? 'hidden' : '')?>">
                                <label><?=t('PriceExclTax')?>: </label>
                                <div class="row">
                                    <div class="controls col-xs-6 col-sm-6 col-md-5 col-lg-4">
                                        <input type="text" name="PurchasePrice" id="PurchasePrice" class="form-control input-sm" value="<?=currency_format($item['PurchasePrice'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'])?>" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group <?=($settings['GENERAL']['UseVAT'] == 0 ? 'hidden' : '')?>">
                                <label><?=t('VAT')?>: </label>
                                <div class="row">
                                    <div class="controls col-xs-6 col-sm-6 col-md-5 col-lg-4">
                                        <select name="PurchaseTaxID" id="PurchaseTaxID" class="col-xs-12 form-control input-sm">
                                            <?php foreach($taxes as $taxe) { ?>
                                                <option value="<?=$taxe['ID']?>" <?=($item['PurchaseTaxID']==$taxe['ID']?'selected="selected"':'')?>  data-rate="<?=$taxe['Rate']?>" ><?=$taxe['Name']?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label><?=t('PriceInclTax')?>: </label>
                                <div class="row">
                                    <div class="controls col-xs-6 col-sm-6 col-md-5 col-lg-4">
                                        <input type="text" name="PurchasePriceTTC" id="PurchasePriceTTC" class="form-control input-sm" value="<?=currency_format($item['PurchasePriceTTC'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'])?>" />
                                    </div>
                                </div>
                            </div>

                    </fieldset>
                </div>

        </div>

    </div>

</form>

<script>
<!--
    //automatic height for textarea
    $('.autoheight').on('keypress', function(e) {
        $(e.currentTarget).css({'height':'46px','overflow-y':'hidden'}).height(e.currentTarget.scrollHeight);
    });

    $('#Price, #TaxID, #PriceTTC').on('change', function() {
        var     taxItem = $('#TaxID')[0].options[ $('#TaxID')[0].selectedIndex ],
                taxRate = $(taxItem).attr('data-rate').replace(',', '.')*1.0;
        if (taxRate)
        {
            switch(this.id) {
                case 'Price':
                    this.value = this.value.replace(',', '.') * 1.0;
                    $('#PriceTTC').val( currency_format(this.value * (100 + taxRate) / 100, '<?=$settings['GENERAL']['Decimals']?>', '<?=$settings['GENERAL']['DecimalSeparator']?>'));
                    break;
                case 'TaxID':
                    var value = $('#Price').val().replace(',', '.')*1;
                    $('#PriceTTC').val( currency_format(value * (100 + taxRate) / 100, '<?=$settings['GENERAL']['Decimals']?>', '<?=$settings['GENERAL']['DecimalSeparator']?>'));
                    break;
                case 'PriceTTC':
                    this.value = this.value.replace(',', '.') * 1.0;
                    $('#Price').val( currency_format((this.value*1) / ((100 + taxRate) / 100), '<?=$settings['GENERAL']['Decimals']?>', '<?=$settings['GENERAL']['DecimalSeparator']?>'));
                    break;
            }
        }
    });
    $('#PurchasePrice, #PurchaseTaxID, #PurchasePriceTTC').on('change', function() {
        var     taxItem = $('#PurchaseTaxID')[0].options[ $('#PurchaseTaxID')[0].selectedIndex ],
                taxRate = $(taxItem).attr('data-rate').replace(',', '.')*1.0;
                taxRate = !taxRate ? 0.00 : taxRate;
        if (taxRate)
        {
            switch(this.id) {
                case 'PurchasePrice':
                    this.value = this.value.replace(',', '.') * 1.0;
                    $('#PurchasePriceTTC').val( currency_format(this.value * (100 + taxRate) / 100, '<?=$settings['GENERAL']['Decimals']?>', '<?=$settings['GENERAL']['DecimalSeparator']?>'));
                    break;
                case 'PurchaseTaxID':
                    var value = $('#PurchasePrice').val().replace(',', '.')*1;
                    $('#PurchasePriceTTC').val( currency_format(value * (100 + taxRate) / 100, '<?=$settings['GENERAL']['Decimals']?>', '<?=$settings['GENERAL']['DecimalSeparator']?>'));
                    break;
                case 'PurchasePriceTTC':
                    this.value = this.value.replace(',', '.') * 1.0;
                    $('#PurchasePrice').val( currency_format((this.value*1) / ((100 + taxRate) / 100), '<?=$settings['GENERAL']['Decimals']?>', '<?=$settings['GENERAL']['DecimalSeparator']?>'));
                    break;
            }
        }
    });
    $(document).keyup(function(e)
    {
        if ($('input:focus').length > 0 || $('textarea:focus').length > 0)
        {
            return false;
        }

        if (e.keyCode == true)
        {
            var key = e.keyCode;
        }
        else
        {
            var key = e.which;
        }

        switch (key)
        {
            case 27://ESC
                history.back();
                return false;
                break;
        }
    });
-->
</script>

<?php include 'footer.php';