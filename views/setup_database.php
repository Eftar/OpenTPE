<!DOCTYPE html>
<html lang="<?=BFWK_LANG?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=HLX_NAME?></title>
    <link rel="icon" type="image/png" href="<?=BFWK_SERVER_ROOT?>favicon.png" />
    <link type="text/css" href="views/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="views/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="views/css/hlx.css" type="text/css" />
    <script type="text/javascript" src="views/js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="views/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="views/js/functions.js"></script>
</head>

<body>

<div class="container">
    <div class="panel panel-default">
    <div class="panel-body">

        <h1><img src="<?=BFWK_SERVER_ROOT?>views/img/logo.png" style="display: inline-block;margin-top: -5px;" /> <?=HLX_NAME?></h1>

        <form method="post" action="<?=$urlForm?>">

            <legend><?=t('SetupDatabase')?></legend>

            <br />
            <?php if ($firstTry == false) { ?>

                <div class="alert <?=($database['success']==1 ? 'alert-info' : 'alert-danger')?>" role="alert"><?=$database['message']?></div><br />

            <?php } ?>

            <?php if ($database['success'] == 1) { ?>

                <input class="btn btn-primary" name="btnCreateTable" type="submit" value="<?=t('CreateTables')?>">

            <?php } else { ?>

                <div class="row">

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label><?=t('DatabaseType')?></label>
                            <select name="databaseType" id="databaseType" onchange="typeChange()" class="form-control input-sm">
                                <option value="sqlite" selected="selected">SQLite</option>
                                <option value="mysql" >MySQL / MariaDB</option>
                            </select>
                        </div>
                        <br />

                        <div id="sqlite">
                            <div class="form-group">
                                <label><?=t('Path')?></label>
                                <input type="text" value="store/db/OpenTPE.db" name="databasePath" class="form-control input-sm" />
                                <div class="text-muted text-info"><?=t('SecurityInformation')?></div>
                            </div>
                        </div>

                        <div id="mysql" class="hidden">
                            <div class="form-group">
                                <label><?=t('Host')?></label>
                                <input type="text" value="localhost" name="databaseHost" class="form-control input-sm" />
                            </div>
                            <div class="form-group">
                                <label><?=t('User')?></label>
                                <input type="text" value="" name="databaseUser" class="form-control input-sm" />
                            </div>
                            <div class="form-group">
                                <label><?=t('Password')?></label>
                                <input type="password" value="" name="databasePassword" class="form-control input-sm" />
                            </div>
                            <div class="form-group">
                                <label><?=t('DatabaseName')?></label>
                                <input type="text" value="" name="databaseName" class="form-control input-sm" />
                            </div>
                        </div>
                    </div>

                </div>

                <br />

                <input class="btn btn-default" name="btnTest" type="submit" value="<?=t('Test')?>">

            <?php } ?>

        </form>

    </div>
    </div>
</div>

<script type="text/javascript">

    typeChange = function() {

        if ($('#databaseType').val() == 'sqlite')
        {
            $('#sqlite').show();
            $('#mysql').hide();
        }
        else
        {
            $('#sqlite').hide();
            $('#mysql').show().removeClass('hidden');
        }

    }

</script>