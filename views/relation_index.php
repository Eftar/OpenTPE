<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

<style>
.repeater {

	bottom: 0;
}


</style>

<!-- Tab content-->
<div class="tab-content">

	<div>

		<div class="repeater" id="repeater">
			<div class="repeater-header">
				<div class="repeater-header-left">

					<div class="pull-left">
						<button type="button" class="btn btn-primary btn-sm">
							<?=t('Quote')?>
						</button>
						<button type="button" class="btn btn-default btn-sm" style="border: none;color:#1E90FF">
							<?=t('Invoice')?>
						</button>
					</div>
					<div class="pull-left" style="margin-left:20px;">
						<div class="search input-group input-group-sm">
							<input type="search" class="form-control control-sm" placeholder="Search"/>
							<span class="input-group-btn input-group-btn-sm">
								<button class="btn btn-default btn-sm" type="button">
									<span class="glyphicon glyphicon-search"></span>
									<span class="sr-only">Search</span>
								</button>
							</span>
						</div>
					</div>

				</div>
				<div class="repeater-header-right">

					<div class="btn-group">
						<button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span class="glyphicon glyphicon-plus"></span>
							<?=t('Create new document')?> <span class="caret"></span>
						</button>
						<ul class="dropdown-menu">
							<li><a href="#"><?=t('Quote')?></a></li>
							<li><a href="#"><?=t('Invoice')?></a></li>
						</ul>
					</div>

				</div>
			</div>
			<div class="repeater-viewport">
				<div class="repeater-canvas"></div>
				<div class="loader repeater-loader"></div>
			</div>
			<div class="repeater-footer">
				<div class="repeater-footer-left">
					<div class="repeater-itemization">
						<span><span class="repeater-start"></span> - <span class="repeater-end"></span> of <span class="repeater-count"></span> items</span>
						<div class="btn-group selectlist" data-resize="auto">
							<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
								<span class="selected-label">&nbsp;</span>
								<span class="caret"></span>
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<ul class="dropdown-menu" role="menu">
								<li data-value="5"><a href="#">5</a></li>
								<li data-value="10" data-selected="true"><a href="#">10</a></li>
								<li data-value="20"><a href="#">20</a></li>
								<li data-value="50" data-foo="bar" data-fizz="buzz"><a href="#">50</a></li>
								<li data-value="100"><a href="#">100</a></li>
							</ul>
							<input class="hidden hidden-field" name="itemsPerPage" readonly="readonly" aria-hidden="true" type="text"/>
						</div>
						<span>Per Page</span>
					</div>
				</div>
				<div class="repeater-footer-right">
					<div class="repeater-pagination">
						<button type="button" class="btn btn-default btn-xs repeater-prev">
							<span class="glyphicon glyphicon-chevron-left"></span>
							<span class="sr-only">Previous Page</span>
						</button>
						<label class="page-label" id="myPageLabel">Page</label>
						<div class="repeater-primaryPaging active">
							<div class="input-group input-append dropdown combobox input-group-sm">
								<input type="text" class="form-control" aria-labelledby="myPageLabel">
								<div class="input-group-btn input-group-btn-sm">
									<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
										<span class="caret"></span>
										<span class="sr-only">Toggle Dropdown</span>
									</button>
									<ul class="dropdown-menu dropdown-menu-right"></ul>
								</div>
							</div>
						</div>
						<input type="text" class="form-control repeater-secondaryPaging" aria-labelledby="myPageLabel">
						<span>of <span class="repeater-pages"></span></span>
						<button type="button" class="btn btn-default btn-xs repeater-next">
							<span class="glyphicon glyphicon-chevron-right"></span>
							<span class="sr-only">Next Page</span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	<!--

	columns = [
		{
			label: 'Name &amp; Description',
			property: 'name',
			sortable: true
		},
		{
			label: 'Key',
			property: 'key',
			sortable: true
		},
		{
			label: 'Status',
			property: 'status',
			sortable: true
		}
	];




	dataSource = function(options, callback){
			// set options
			var pageIndex = options.pageIndex;
			var pageSize = options.pageSize;
			var options = {
				pageIndex: pageIndex,
				pageSize: pageSize,
				sortDirection: options.sortDirection,
				sortBy: options.sortProperty,
				filterBy: options.filter.value || '',
				searchBy: options.search || ''
			};

			// call API, posting options
			/*$.ajax({
				type: 'post',
				url: '/repeater/data',
				data: options
			})
			.done(function(data) {

				var items = data.items;
				var totalItems = data.total;
				var totalPages = Math.ceil(totalItems / pageSize);
				var startIndex = (pageIndex * pageSize) + 1;
				var endIndex = (startIndex + pageSize) - 1;

				if(endIndex > items.length) {
					endIndex = items.length;
				}

				// configure datasource
				var dataSource = {
					page: pageIndex,
					pages: totalPages,
					count: totalItems,
					start: startIndex,
					end: endIndex,
					columns: columns,
					items: items
				};

				// invoke callback to render repeater
				callback(dataSource);
			});*/
			var dataSource = {
					page: 0,
					pages: 10,
					count: 200,
					start: 1,
					end: 200,
					columns: columns,
					items: [
						{
							name: 'Name',
							key: 'Key',
							status: 'Status'
						},
						{
							name: 'Name1',
							key: 'Key',
							status: 'Status'
						},
						{
							name: 'Name2',
							key: 'Key',
							status: 'Status'
						},
						{
							name: 'Name2',
							key: 'Key',
							status: 'Status'
						},
						{
							name: 'Name2',
							key: 'Key',
							status: 'Status'
						},
						{
							name: 'Name2',
							key: 'Key',
							status: 'Status'
						},
						{
							name: 'Name2',
							key: 'Key',
							status: 'Status'
						},
						{
							name: 'Name2',
							key: 'Key',
							status: 'Status'
						},
						{
							name: 'Name2',
							key: 'Key',
							status: 'Status'
						},
						{
							name: 'Name2',
							key: 'Key',
							status: 'Status'
						},
						{
							name: 'Name2',
							key: 'Key',
							status: 'Status'
						},
						{
							name: 'Name2',
							key: 'Key',
							status: 'Status'
						},
						{
							name: 'Name2',
							key: 'Key',
							status: 'Status'
						},
						{
							name: 'Name2',
							key: 'Key',
							status: 'Status'
						},
					]
				};

				// invoke callback to render repeater
				callback(dataSource);

    };

	$('#repeater').repeater({dataSource: dataSource, /*staticHeight: true*/});

	//Select navBarItem according to current anchor
	try {
		var anchor = window.location.hash.substring(1,window.location.hash.length);
		if (anchor != '')
			$('.navbar a[href="#'+anchor+'"]').click();
	} catch(e) {}

	//Create modal from ajax content specified in data-remote
	$('[data-toggle="modal"]').click(function(e) {
		e.preventDefault();
		$.get($(this).attr('data-remote'), function(data) {
			$(data).modal();
		});
	});
/*
	deleteItem = function(type, id) {
		if (confirm('<?=t('AreYouSure')?>'))
		{
			var url = '';
			switch(type)
			{
				case 'execs': 		url = '<?=$urlDeleteExec?>';		break;
				case 'sources': 	url = '<?=$urlDeleteSource?>';		break;
				case 'targets': 	url = '<?=$urlDeleteTarget?>';		break;
				case 'connectors': 	url = '<?=$urlDeleteConnector?>';	break;
				case 'users': 		url = '<?=$urlDeleteUser?>';		break;

			}
			$.get(url+id, function(data) {
				//TODO add a test before show modal prevent BlackScreenOfTheDeath
				$(data).modal().on('hide.bs.modal', function (e) {
					location.href = location.href.substr(0, location.href.length - location.hash.length) + '#' + type;
					location.reload();
				});
			});
		}
		else
			return false;
	}*/

	//Add event onResize
	var onResize = function() {
		$('.repeater-list-wrapper').css('height',  jQuery(window).height() - 95 -85 + 'px');
	}

	$(window).resize(onResize);

	onResize();
-->
</script>

<?php include 'footer.php';