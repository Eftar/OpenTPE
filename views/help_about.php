<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

<div id="headerbar" style="position:fixed;width:100%;background-color:white;z-index:99;">

    <div class="pull-left">
        <h1><?=t('About')?></h1>
    </div>

    <div class="pull-right btn-group">
        <a class="btn btn-sm btn-default" onclick="history.back();return false;">
            <i class="glyphicon glyphicon-chevron-left"></i> <?=t('Back')?>
        </a>
    </div>

</div>

<div class="container-fluid" style="padding-top:45px;">

    <div class="row">
        <div class="col-xs-10">
            <p>
                <br />
                <?=t('AboutText', HLX_VERSION)?>
                <?=t('YourCanal', HLX_CANAL)?>
            </p>
        </div>
    </div>

</div>

<script>
<!--

-->
</script>

<?php include 'footer.php';