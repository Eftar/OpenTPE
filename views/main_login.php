<!DOCTYPE html>

<html lang="<?=BFWK_LANG?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?=HLX_NAME?></title>
    <link rel="icon" type="image/png" href="<?=BFWK_SERVER_ROOT?>favicon.png" />
    <link type="text/css" href="views/css/bootstrap.min.css" rel="stylesheet" />
    <script type="text/javascript" src="views/js/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" src="views/js/bootstrap.min.js"></script>
</head>
<body>

    <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">

                    <h1><a href="<?=BFWK_SERVER_ROOT?>" style="color:rgb(51, 51, 51);text-decoration:none;" ><img src="views/img/logo.png" style="margin-top:-7px;" /> <?=HLX_NAME?></a></h1>
                    <hr />
                    <?=(($msg!='')?'<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$msg.'</div>':'')?>
                    <?=(($err!='')?'<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$err.'</div>':'')?>

                    <form method="post" action="<?=$urlForm?>">
                        <div class="form-group">
                            <label for="email"><?=t('Email')?></label>
                            <input type="email" name="Email" value="" id="email" class="form-control" autofocus placeholder="<?=t('Email')?>">
                        </div>
                        <div class="form-group">
                            <label for="password"><?=t('Pass')?></label>
                            <input type="password" name="Password" value="" id="password" class="form-control" placeholder="<?=t('Pass')?>">
                        </div>
                        <br />
                        <button type="submit" class="btn btn-primary btn-block"><?=t('Connexion')?></button>
                    </form>

                    <div class="login-help">
                        <!--<a href="#">Forgot Password</a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(window).load(function(){
            $('#loginModal').modal({
                backdrop: 'static',
                keyboard: false
            }).modal('show');
            $('#loginModal').on('shown.bs.modal', function () {
                $('#email').focus()
            });
        });
    </script>

</body>
</html>