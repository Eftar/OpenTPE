<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

<style type="text/css">

    .glyphicon-refresh-animate {
        -animation: spin .9s infinite linear;
        -webkit-animation: spin2 .9s infinite linear;
    }

    @-webkit-keyframes spin2 {
        from { -webkit-transform: rotate(0deg);}
        to { -webkit-transform: rotate(360deg);}
    }

    @keyframes spin {
        from { transform: scale(1) rotate(0deg);}
        to { transform: scale(1) rotate(360deg);}
    }

    #result pre {
        height: 252px;
    }

</style>

<div id="headerbar" style="position:fixed;width:100%;background-color:white;z-index:99;">

    <div class="pull-left">
        <h1><?=t('Update')?></h1>
    </div>

    <div class="pull-right btn-group">
        <a class="btn btn-sm btn-default" onclick="history.back();return false;">
            <i class="glyphicon glyphicon-chevron-left"></i> <?=t('Back')?>
        </a>
    </div>

</div>

<div class="container-fluid" style="padding-top:55px;">


    <div class="alert alert-warning">
        <?php if ($rChangeLog && ! empty($rChangeLog)) { ?>
            <div class="pull-left" id="upgradeDiv">
                <button class="btn btn-primary" id="upgradeButton" style="margin-top: -6px">Mettre à jour</button>&nbsp;&nbsp;&nbsp;
            </div>
        <?php } ?>
        La mise à jour est une étape dangereuse pour votre application et vos données, nous vous conseillons, avant de lancer la procédure, de <b>sauvegarder toutes les données</b> (voir <a href="<?=$urlHelp?>">documentation</a>).<br />
    </div>

    <br />

    <div id="result">
        <?php if ($rChangeLog === false) { ?>

            Erreur lors de la récupération de la liste des mises à jour

        <?php } elseif ($rChangeLog && ! empty($rChangeLog)) { ?>

                Des mises à jour sont disponibles :
                <br />
                <br />

<pre><?php foreach($rChangeLog as $version => $rInfo) { ?>
<b>Version V<?=$version?> (<?=$rInfo['date']?>):</b>
<?=implode("\n", $rInfo['changelog'])."\n\n"?>
<?php } ?></pre>

        <?php } else { ?>

            Votre application utilise déjà la dernière version disponible

        <?php } ?>
    </div>

    <br />

    <!--<blockquote style="font-size:0.85em">
        Il existe deux sortes de personne :<br />
         - Ceux qui ont déjà perdu des données <br />
         - Ceux qui en perdront un jour <br />
    </blockquote>-->
</div>

<script>
<!--
    $('#upgradeButton').click(function() {

        if (confirm('Êtes vous sûr de vouloir mettre à jour votre application ?'))
        {
            $('#upgradeDiv').html('<button class="btn btn-warning" style="margin-top: -6px"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Mise à jour en cours ...</button>&nbsp;&nbsp;&nbsp;');

            $.ajax({
                url: '<?=$urlUpgrade?>',
                success: function(data) {

                    $('#upgradeDiv').html('');
                    if (data.match('Successful update'))
                    {
                        $("#result").html('<div class="alert alert-info">Mise à jour effectuée</div>'+data);
                    }
                    else
                    {
                        $("#result").html('<div class="alert alert-danger">Nous sommes désolés une erreur s\'est produite lors de la mise à jour</div>'+data);
                    }
                },
                error: function() {
                    $("#result").html('Error lors de la mise à jour');
                }
            });
        }
    });
-->
</script>

<?php include 'footer.php';