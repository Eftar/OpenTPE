<?php include 'header.php'; ?>

<?php include 'navbar.php';

    $rOptions = array();
    //Repeater Id
    $rOptions['id'] = 'thirdPartyList';
    //url
    $rOptions['url'] = $urlLoadList;
    //Default Type
    $rOptions['type'] = $listOptionType;
    //Array Types
    $rOptions['arrTypes'] = array(
        'customers' => t('Customers'),
        'suppliers' => t('Suppliers'),
        'contacts'  => t('Contacts')
    );
    //default filter
    $rOptions['filter'] = $listOptionFilter;
    //Array Filters
    $rOptions['arrFilters'] = array(
        'all' => array(
            '0' => array(
                'value'     => 'all',
                'label'     => t('All')
            ),
            1 => array(
                'value'     => 'active',
                'label'     => t('Active')
            ),
            2 => array(
                'value'     => 'inactive',
                'label'     => t('Inactive')
            )
        )
    );
    $rOptions['arrFilters']['customers']    = $rOptions['arrFilters']['all'];
    $rOptions['arrFilters']['contacts']     = $rOptions['arrFilters']['all'];
    $rOptions['arrFilters']['suppliers']    = $rOptions['arrFilters']['all'];
    //Settings for action button
    $rOptions['actionSettings'] = array(
        'label' => t('New'),
        'icon' => 'plus'
    );
    //Array Actions
    $rOptions['arrActions'] = array(
        0 => array(
            'label'     => t('Customer'),
            'url'       => $urlNewCustomer
        ),
        1 => array(
            'label'     => t('Supplier'),
            'url'       => $urlNewSupplier
        )
    );
    //Object contenant les descriptions des colonnes en fonction du type
    $rOptions['arrColumns'] = array(
        'customers' => array(
            array(
                'label'     => t('Name'),
                'property'  => 'Name',
                'className' => 'valign-middle',
                'sortable'  => true
            ),
            array(
                'label'     => t('Address'),
                'property'  => 'Address',
                'sortable'  => true
            ),
            array(
                'label'     => t('PostalCode'),
                'property'  => 'PostalCode',
                'className' => 'valign-middle',
                'width'     => 130,
                'sortable'  => true
            ),
            array(
                'label'     => t('City'),
                'property'  => 'City',
                'className' => 'valign-middle',
                'width'     => 135,
                'sortable'  => true
            ),
            array(
                'label'     => t('Phone'),
                'property'  => 'Phone',
                'className' => 'valign-middle',
                'width'     => 105,
                'sortable'  => true
            ),
            array(
                'label'     => t('Balance'),
                'property'  => 'Balance',
                'className' => 'valign-middle',
                'width'     => 110,
                'sortable'  => true
            ),
            array(
                'label'     => t('Active'),
                'property'  => 'Active',
                'className' => 'valign-middle',
                'width'     => 82,
                'sortable'  => true
            ),
            array(
                'label'     => t('Action'),
                'property'  => 'Action',
                'className' => 'valign-middle',
                'width'     => 120,
                'sortable'  => true
            )
        ),
        'contacts' => array(
            array(
                'label'     => t('Name'),
                'property'  => 'Name',
                'className' => 'valign-middle',
                'sortable'  => true
            ),
            array(
                'label'     => t('Firstname'),
                'property'  => 'Firstname',
                'className' => 'valign-middle',
                'sortable'  => true
            ),
            array(
                'label'     => t('Function'),
                'property'  => 'Function',
                'className' => 'valign-middle',
                'sortable'  => true
            ),
            array(
                'label'     => t('Email'),
                'property'  => 'Email',
                'className' => 'valign-middle',
                'sortable'  => true
            ),
            array(
                'label'     => t('Company'),
                'property'  => 'CompanyName',
                'className' => 'valign-middle',
                'sortable'  => true
            ),
            array(
                'label'     => t('Phone'),
                'property'  => 'Phone',
                'className' => 'valign-middle',
                'width'     => 105,
                'sortable'  => false
            ),
            array(
                'label'     => t('Mobile'),
                'property'  => 'Mobile',
                'className' => 'valign-middle',
                'width'     => 105,
                'sortable'  => false
            ),
            array(
                'label'     => t('Active'),
                'property'  => 'Active',
                'className' => 'valign-middle',
                'width'     => 82,
                'sortable'  => true
            ),
            array(
                'label'     => t('Action'),
                'property'  => 'Action',
                'className' => 'valign-middle',
                'width'     => 120,
                'sortable'  => false
            )
        ),
        'suppliers' => array(
            array(
                'label'     => t('Name'),
                'property'  => 'Name',
                'className' => 'valign-middle',
                'sortable'  => true
            ),
            array(
                'label'     => t('Address'),
                'property'  => 'Address',
                'sortable'  => true
            ),
            array(
                'label'     => t('PostalCode'),
                'property'  => 'PostalCode',
                'className' => 'valign-middle',
                'width'     => 130,
                'sortable'  => true
            ),
            array(
                'label'     => t('City'),
                'property'  => 'City',
                'className' => 'valign-middle',
                'width'     => 135,
                'sortable'  => true
            ),
            array(
                'label'     => t('Phone'),
                'property'  => 'Phone',
                'className' => 'valign-middle',
                'width'     => 105,
                'sortable'  => true
            ),
            array(
                'label'     => t('Balance'),
                'property'  => 'Balance',
                'className' => 'valign-middle',
                'width'     => 110,
                'sortable'  => true
            ),
            array(
                'label'     => t('Active'),
                'property'  => 'Active',
                'className' => 'valign-middle',
                'width'     => 82,
                'sortable'  => true
            ),
            array(
                'label'     => t('Action'),
                'property'  => 'Action',
                'className' => 'valign-middle',
                'width'     => 120,
                'sortable'  => true
            )
        )
    );
    //View render
    $rOptions['arrColumnsRenderer'] = array(
        'customers' => array(
            'Name'          => "<a href=\"".$urlViewCustomer."'+rowData.ID+'\">'+rowData.Name+' '+rowData.Firstname+'</a>",
            'Address'       => "<div style=\"font-size:12px;\">' + rowData.Address1 + '</div><div class=\"small text-muted\">' + rowData.Address2 + '</div>",
            'PostalCode'    => "<div class=\"text-center\">'+rowData.PostalCode+'</div>",
            'Phone'         => "<div class=\"text-center\">'+rowData.Phone+'</div>",
            'Balance'       => "<div class=\"amount\" >' + currency_format(rowData.Balance, '".$settings['GENERAL']['Decimals']."', '".$settings['GENERAL']['DecimalSeparator']."', '', '".$settings['GENERAL']['CurrencySymbol']."', '".$settings['GENERAL']['CurrencyPlacement']."') + '</div>",
            'Action'        => "<div class=\"text-center\"><div class=\"btn-group text-center\" style=\"min-width:85px\" data-id=\"'+rowData.ID+'\" >
                                    <a  href=\"".$urlEditCustomer."'+rowData.ID+'\" role=\"button\" class=\"btn btn-default btn-sm\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span> ".t('Edit')."</a>
                                    <button type=\"button\" class=\"btn btn-default btn-sm dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><span class=\"caret\"></span><span class=\"sr-only\">Toggle Dropdown</span></button>
                                    <ul class=\"dropdown-menu dropdown-menu-right\">
                                    <li><a href=\"".$urlViewCustomer."'+rowData.ID+'\"><span class=\"glyphicon glyphicon-eye-open\" aria-hidden=\"true\"></span> ".t('View')."</a></li>
                                    <li><a href=\"".$urlNewQuote."'+rowData.ID+'\"><span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span> ".t('CreateQuote')."</a></li>
                                    <li><a href=\"".$urlNewOrder."'+rowData.ID+'\"><span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span> ".t('CreateOrder')."</a></li>
                                    <li><a href=\"".$urlNewInvoice."'+rowData.ID+'\"><span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span> ".t('CreateInvoice')."</a></li></ul></div></div>",

            'Active'        => "<div class=\"text-center\">'+(rowData.Active == 1 ? '".t('YesStr')."' : '".t('NoStr')."')+'</div>"
        ),
        'contacts' => array(
            'Name'          => "<a href=\"".$urlViewContact."'+rowData.ID+'\">'+rowData.Name+'</a>",
            'Email'         => "<a href=\"mailto:'+rowData.Email+'\">'+rowData.Email+'</a>",
            'CompanyName'   => "<a href=\"'+(rowData.CompanyType=='C' ? '".$urlViewCustomer."' : '".$urlViewSupplier."')+''+rowData.CompanyID+'\">'+rowData.CompanyName+'</a>",
            'Phone'         => "<div class=\"text-center\">'+rowData.Phone+'</div>",
            'Mobile'         => "<div class=\"text-center\">'+rowData.Mobile+'</div>",
            'Action'        => "<div class=\"text-center\"><div class=\"btn-group text-center\" style=\"min-width:85px\" data-id=\"'+rowData.ID+'\" >
                                    <a  href=\"".$urlEditContact."'+rowData.ID+'\" role=\"button\" class=\"btn btn-default btn-sm\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span> ".t('Edit')."</a>
                                    <button type=\"button\" class=\"btn btn-default btn-sm dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><span class=\"caret\"></span><span class=\"sr-only\">Toggle Dropdown</span></button>
                                    <ul class=\"dropdown-menu dropdown-menu-right\">
                                    <li><a href=\"".$urlViewContact."'+rowData.ID+'\"><span class=\"glyphicon glyphicon-eye-open\" aria-hidden=\"true\"></span> ".t('View')."</a></li></ul></div></div>",
            'Active'        => "<div class=\"text-center\">'+(rowData.Active == 1 ? '".t('YesStr')."' : '".t('NoStr')."')+'</div>"
        ),
        'suppliers' => array(
            'Name'          => "<a href=\"".$urlViewSupplier."'+rowData.ID+'\">'+rowData.Name+' '+rowData.Firstname+'</a>",
            'Address'       => "<div style=\"font-size:12px;\">' + rowData.Address1 + '</div><div class=\"small text-muted\">' + rowData.Address2 + '</div>",
            'PostalCode'    => "<div class=\"text-center\">'+rowData.PostalCode+'</div>",
            'Phone'         => "<div class=\"text-center\">'+rowData.Phone+'</div>",
            'Balance'       => "<div class=\"amount\" >' + currency_format(rowData.Purchased - rowData.Paid, '".$settings['GENERAL']['Decimals']."', '".$settings['GENERAL']['DecimalSeparator']."', '', '".$settings['GENERAL']['CurrencySymbol']."', '".$settings['GENERAL']['CurrencyPlacement']."') + '</div>",
            'Action'        => "<div class=\"text-center\"><div class=\"btn-group text-center\" style=\"min-width:85px\" data-id=\"'+rowData.ID+'\" >
                                    <a  href=\"".$urlEditSupplier."'+rowData.ID+'\" role=\"button\" class=\"btn btn-default btn-sm\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span> ".t('Edit')."</a>
                                    <button type=\"button\" class=\"btn btn-default btn-sm dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"><span class=\"caret\"></span><span class=\"sr-only\">Toggle Dropdown</span></button>
                                    <ul class=\"dropdown-menu dropdown-menu-right\">
                                    <li><a href=\"".$urlViewSupplier."'+rowData.ID+'\"><span class=\"glyphicon glyphicon-eye-open\" aria-hidden=\"true\"></span> ".t('View')."</a></li>
                                    <li><a href=\"".$urlNewPurchase."'+rowData.ID+'\"><span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span> ".t('CreatePurchase')."</a></li></ul></div></div>",

            'Active'        => "<div class=\"text-center\">'+(rowData.Active == 1 ? '".t('YesStr')."' : '".t('NoStr')."')+'</div>"
        )
    );
    HLX_View::hlxRepeater($rOptions);
?>

<script>
<!--

-->
</script>

<?php include 'footer.php';