<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

<div id="headerbar" style="position:fixed;width:100%;background-color:white;z-index:99;">

    <div class="pull-left">
        <h1><?=t('Help')?></h1>
    </div>

    <div class="pull-right btn-group">
        <a class="btn btn-sm btn-default" onclick="history.back();return false;">
            <i class="glyphicon glyphicon-chevron-left"></i> <?=t('Back')?>
        </a>
    </div>

</div>

<div class="container-fluid" style="padding:45px 0 0 0;">

    <div style="position:fixed;top:95px;bottom:0;left:0;right:0;">
        <iframe style="width: 100%;height: 100%;border: none" src="views/js/pdfjs/web/viewer.html?file=<?=urlencode($urlPDF)?>#pagemode=bookmarks"></iframe>
    </div>

</div>

<script>
<!--

-->
</script>

<?php include 'footer.php';