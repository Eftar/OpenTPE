<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

<script type="text/javascript" src="views/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="views/js/bootstrap-datepicker-lang.min.js"></script>
<link rel="stylesheet" href="views/css/bootstrap-datepicker3.min.css" type="text/css" />

<script type="text/javascript" src="views/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="views/css/bootstrap-select.min.css" type="text/css" />

<form action="<?=$urlSave?>" enctype="multipart/form-data" method="POST">

    <div id="headerbar" style="position:fixed;width:100%;background-color:white;z-index:99;">

        <div class="pull-left">
            <h1><?=t('EditSettings')?></h1>
        </div>

        <div class="pull-right btn-group">
            <a class="btn btn-sm btn-default" href="<?=$urlCancel?>">
                <i class="glyphicon glyphicon-remove"></i> <?=t('Cancel')?>
            </a>
            <button class="btn btn-sm btn-primary">
                <i class="fa fa-edit"></i> <?=t('Save')?>
            </button>
        </div>

    </div>

    <ul class="nav nav-tabs nav-tabs-noborder" id="settingsTab" style="padding-top: 45px;">
        <li class="active"><a data-toggle="tab" href="#generalSettings"><?=t('General')?></a></li>
        <li><a data-toggle="tab" href="#companySettings" data-id="companySettings"><?=t('Company')?></a></li>
        <li><a data-toggle="tab" href="#quoteSettings" data-id="quoteSettings"><?=t('Quote')?></a></li>
        <li><a data-toggle="tab" href="#orderSettings" data-id="orderSettings"><?=t('Order')?></a></li>
        <li><a data-toggle="tab" href="#depositSettings" data-id="depositSettings"><?=t('Deposit')?></a></li>
        <li><a data-toggle="tab" href="#invoiceSettings" data-id="invoiceSettings"><?=t('Invoice')?></a></li>
        <li><a data-toggle="tab" href="#purchaseSettings" data-id="purchaseSettings"><?=t('Purchase')?></a></li>
        <li><a data-toggle="tab" href="#productSettings" data-id="productSettings"><?=t('Product')?></a></li>
        <li><a data-toggle="tab" href="#taxSettings" data-id="taxSettings"><?=t('Taxes')?></a></li>
        <li><a data-toggle="tab" href="#paymentSettings" data-id="paymentSettings"><?=t('Payment')?></a></li>
        <li><a data-toggle="tab" href="#unitSettings" data-id="unitSettings"><?=t('Unit')?></a></li>
        <!--<li><a data-toggle="tab" href="#emailSettings" data-id="emailSettings"><?=t('Email')?></a></li>-->
        <li><a data-toggle="tab" href="#updateSettings" data-id="updateSettings"><?=t('Update')?></a></li>
        <li><a data-toggle="tab" href="#backupSettings" data-id="backupSettings"><?=t('Backup')?></a></li>
    </ul>

    <div class="tab-content">

        <div role="tabpanel" class="tab-pane active" id="generalSettings" style="padding-top: 140px; margin-top: -140px;">
            <div class="container-fluid">

                <div class="row">
                    <br />
                    <div class="col-xs-12 col-lg-8">
                        <fieldset>
                            <legend><?=t('Language')?></legend>
                            <div class="row">
                                <div class="col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="Language" class="control-label"><?=t('Language')?></label>
                                        <select name="Language" class="input-sm form-control">
                                            <?php foreach($settings['LANGUAGE'] as $language) { ?>
                                                <option value="<?=$language['Value']?>" <?=($settings['GENERAL']['Language'] == $language['Value'] ? 'selected="selected"' : '')?>><?=$language['Name']?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="row">
                    <br />
                    <div class="col-xs-12 col-lg-8">
                        <fieldset>
                            <legend><?=t('Date')?></legend>
                            <div class="row">
                                <div class="col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="FirstDayOfWeek" class="control-label"><?=t('FirstDayOfWeek')?></label>
                                        <select name="FirstDayOfWeek" id="FirstDayOfWeek" class="input-sm form-control">
                                            <option value="0" <?=($settings['GENERAL']['FirstDayOfWeek'] == '0' ? 'selected="selected"' : '')?>><?=t('Sunday')?></option>
                                            <option value="1" <?=($settings['GENERAL']['FirstDayOfWeek'] == '1' ? 'selected="selected"' : '')?>><?=t('Monday')?></option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="DateFormat" class="control-label"><?=t('DateFormat')?></label>
                                        <input type="text" value="<?=$settings['GENERAL']['DateFormat']?>" name="DateFormat" id="DateFormat" class="input-sm form-control" />
                                        <small class="text-muted text-info"><?=t('DateFormatHelp')?></small>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>

                <div class="row">
                    <br />
                    <div class="col-xs-12 col-lg-8">
                        <fieldset>
                            <legend><?=t('Amount')?></legend>
                            <div class="row">
                                <div class="col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="CurrencySymbol" class="control-label"><?=t('CurrencySymbol')?></label>
                                        <input type="text" value="<?=$settings['GENERAL']['CurrencySymbol']?>" name="CurrencySymbol" id="CurrencySymbol" class="input-sm form-control" />
                                    </div>
                                </div>

                                <div class="col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="CurrencyPlacement" class="control-label"><?=t('CurrencySymbolPosition')?></label>
                                        <select class="input-sm form-control" id="CurrencyPlacement" name="CurrencyPlacement">
                                            <option value="before" <?=($settings['GENERAL']['CurrencyPlacement'] == 'before' ? 'selected="selected"' : '')?>><?=t('BeforeAmount')?></option>
                                            <option value="after" <?=($settings['GENERAL']['CurrencyPlacement'] == 'after' ? 'selected="selected"' : '')?>><?=t('AfterAmount')?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="Decimals" class="control-label"><?=t('NumberOfDecimals')?></label>
                                        <input type="text" value="<?=$settings['GENERAL']['Decimals']?>" name="Decimals" id="Decimals" class="input-sm form-control" />
                                    </div>
                                </div>
                                <div class="col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="DecimalSeparator" class="control-label"><?=t('DecimalSeparator')?></label>
                                        <select class="input-sm form-control" id="DecimalSeparator" name="DecimalSeparator">
                                            <option value="." <?=($settings['GENERAL']['DecimalSeparator'] == '.' ? 'selected="selected"' : '')?>>.</option>
                                            <option value="," <?=($settings['GENERAL']['DecimalSeparator'] == ',' ? 'selected="selected"' : '')?>>,</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--
                            <div class="row">
                                <div class="col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="thousandSeparator" class="control-label">Séparateur de millier</label>
                                        <input type="text" value=" " name="thousandSeparator" id="thousandSeparator" class="input-sm form-control" />
                                    </div>
                                </div>
                            </div>
                            !-->
                        </fieldset>
                    </div>
                </div>

                <div class="row">
                    <br />
                    <div class="col-xs-12 col-lg-8">
                        <fieldset>
                            <legend><?=t('Features')?></legend>
                            <div class="row">
                                <div class="col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="UseBillingAddress" class="control-label"><?=t('UseBillingAddress')?></label>
                                        <select name="UseBillingAddress" class="input-sm form-control">
                                            <option value="1" <?=($settings['GENERAL']['UseBillingAddress'] == '1' ? 'selected="selected"' : '')?>><?=t('YesStr')?></option>
                                            <option value="0" <?=($settings['GENERAL']['UseBillingAddress'] == '0' ? 'selected="selected"' : '')?>><?=t('NoStr')?></option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="UseDiscount" class="control-label"><?=t('UseDiscount')?></label>
                                        <select name="UseDiscount" class="input-sm form-control">
                                            <option value="1" <?=($settings['GENERAL']['UseDiscount'] == '1' ? 'selected="selected"' : '')?>><?=t('YesStr')?></option>
                                            <option value="0" <?=($settings['GENERAL']['UseDiscount'] == '0' ? 'selected="selected"' : '')?>><?=t('NoStr')?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="UseDeliveryAddress" class="control-label"><?=t('UseDeliveryAddress')?></label>
                                        <select name="UseDeliveryAddress" class="input-sm form-control">
                                            <option value="1" <?=($settings['GENERAL']['UseDeliveryAddress'] == '1' ? 'selected="selected"' : '')?>><?=t('YesStr')?></option>
                                            <option value="0" <?=($settings['GENERAL']['UseDeliveryAddress'] == '0' ? 'selected="selected"' : '')?>><?=t('NoStr')?></option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="UseVAT" class="control-label"><?=t('UseVAT')?></label>
                                        <select name="UseVAT" class="input-sm form-control">
                                            <option value="1" <?=($settings['GENERAL']['UseVAT'] == '1' ? 'selected="selected"' : '')?>><?=t('YesStr')?></option>
                                            <option value="0" <?=($settings['GENERAL']['UseVAT'] == '0' ? 'selected="selected"' : '')?>><?=t('NoStr')?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>

            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="companySettings" style="padding-top: 140px; margin-top: -140px;">
            <div class="container-fluid">
                <div class="row">
                    <br />
                    <div class="col-xs-12 col-lg-6">
                        <div class="form-group">
                            <label for="CompanyName" class="control-label"><?=t('CompanyName')?></label>
                            <input type="text" value="<?=$settings['GENERAL']['CompanyName']?>" name="CompanyName" id="CompanyName" class="input-sm form-control" />
                        </div>
                        <div class="form-group">
                            <label for="CompanyType" class="control-label"><?=t('CompanyType')?></label>
                            <input type="text" value="<?=$settings['GENERAL']['CompanyType']?>" name="CompanyType" id="CompanyType" class="input-sm form-control" />
                        </div>
                        <div class="form-group">
                            <label for="CompanyCapital" class="control-label"><?=t('Capital')?></label>
                            <input type="text" value="<?=$settings['GENERAL']['CompanyCapital']?>" name="CompanyCapital" id="CompanyCapital" class="input-sm form-control" />
                        </div>
                        <div class="form-group">
                            <label for="CompanyRegistrationNumber" class="control-label"><?=t('RegistrationNumber')?></label>
                            <input type="text" value="<?=$settings['GENERAL']['CompanyRegistrationNumber']?>" name="CompanyRegistrationNumber" id="CompanyRegistrationNumber" class="input-sm form-control" />
                        </div>
                        <div class="form-group">
                            <label for="CompanyRegister" class="control-label"><?=t('Register')?></label>
                            <input type="text" value="<?=$settings['GENERAL']['CompanyRegister']?>" name="CompanyRegister" id="CompanyRegister" class="input-sm form-control" />
                        </div>
                        <div class="form-group">
                            <label for="CompanyClassification" class="control-label"><?=t('Classification')?></label>
                            <input type="text" value="<?=$settings['GENERAL']['CompanyClassification']?>" name="CompanyClassification" id="CompanyClassification" class="input-sm form-control" />
                        </div>
                        <div class="form-group">
                            <label for="CompanyTaxNumber" class="control-label"><?=t('VATNumber')?></label>
                            <input type="text" value="<?=$settings['GENERAL']['CompanyTaxNumber']?>" name="CompanyTaxNumber" id="CompanyTaxNumber" class="input-sm form-control" />
                        </div>
                        <fieldset>
                            <legend><?=t('Contact')?></legend>
                            <div class="form-group">
                                <label for="CompanyPhone" class="control-label"><?=t('Phone')?></label>
                                <input type="text" value="<?=$settings['GENERAL']['CompanyPhone']?>" name="CompanyPhone" id="CompanyPhone" class="input-sm form-control" />
                            </div>
                            <div class="form-group">
                                <label for="CompanyFax" class="control-label"><?=t('Fax')?></label>
                                <input type="text" value="<?=$settings['GENERAL']['CompanyFax']?>" name="CompanyFax" id="CompanyFax" class="input-sm form-control" />
                            </div>
                            <div class="form-group">
                                <label for="CompanyEmail" class="control-label"><?=t('Email')?></label>
                                <input type="text" value="<?=$settings['GENERAL']['CompanyEmail']?>" name="CompanyEmail" id="CompanyEmail" class="input-sm form-control" />
                            </div>
                            <div class="form-group">
                                <label for="CompanyWeb" class="control-label"><?=t('Web')?></label>
                                <input type="text" value="<?=$settings['GENERAL']['CompanyWeb']?>" name="CompanyWeb" id="CompanyWeb" class="input-sm form-control" />
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-xs-12 col-lg-2">
                        <label for="CompanyLogo"><?=t('Logo')?> : </label>
                        <?php if (isset($companyLogo) && $companyLogo != false) { ?>
                            <br /><br /><img src="<?=$companyLogo.'?'.date('dHis')?>" style="max-width:100%;box-shadow: -1px 2px 5px 1px rgba(0, 0, 0, 0.7); " /><br /><br />
                            <a href="<?=$urlRemoveLogo?>"><?=t('Remove')?></a><br /><br />
                        <?php } ?>
                        <input type="file" name="CompanyLogo" id="CompanyLogo" class="" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-lg-8">
                        <fieldset>
                            <legend><?=t('Address')?></legend>
                            <div class="form-group">
                                <label for="CompanyAddress1" class="control-label"><?=t('Address')?></label>
                                <input type="text" value="<?=$settings['GENERAL']['CompanyAddress1']?>" name="CompanyAddress1" id="CompanyAddress1" class="input-sm form-control" />
                                <input type="text" value="<?=$settings['GENERAL']['CompanyAddress2']?>" name="CompanyAddress2" id="CompanyAddress2" class="input-sm form-control" />
                                <input type="text" value="<?=$settings['GENERAL']['CompanyAddress3']?>" name="CompanyAddress3" id="CompanyAddress3" class="input-sm form-control" />
                            </div>
                            <div class="row">
                                 <div class="form-group col-lg-3 col-sm-4">
                                    <label for="CompanyPostalCode"><?=t('PostalCode')?> : </label>
                                    <input type="text" name="CompanyPostalCode" class="form-control input-sm" value="<?=$settings['GENERAL']['CompanyPostalCode']?>" />
                                </div>

                                <div class="form-group col-lg-9 col-sm-8">
                                    <label for="CompanyCity"><?=t('City')?> : </label>
                                    <input type="text" name="CompanyCity" class="form-control input-sm" value="<?=$settings['GENERAL']['CompanyCity']?>" />
                                </div>
                            </div>
                            <!--
                            <div class="form-group">
                                <label for="CompanyState" class="control-label"><?=t('Etat')?></label>
                                <input type="text" value="<?=$settings['GENERAL']['CompanyState']?>" name="QuoteExpirationDelay" id="QuoteExpirationDelay" class="input-sm form-control" />
                            </div>
                            -->
                            <div class="form-group">
                                <label for="CompanyCountry" class="control-label"><?=t('Country')?></label>
                                <select id="CompanyCountry" name="CompanyCountry" class="selectpicker" data-size="10" data-width="100%" data-live-search="true">
                                    <option value="" ><?=t('Select')?></option>
                                    <?php foreach($countries as $code => $country) { ?>
                                        <option value="<?=$code?>" <?=($code==$settings['GENERAL']['CompanyCountry'] ? 'selected="selected"' : '')?>><?=htmlspecialchars($country)?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="quoteSettings" style="padding-top: 140px; margin-top: -140px;">
            <div class="container-fluid">
                <div class="row">
                    <br />
                    <div class="col-xs-12 col-lg-8">
                        <div class="form-group">
                            <label for="QuoteReferenceFormat" class="control-label"><?=t('ReferenceFormat')?></label>
                            <input type="text" value="<?=$settings['GENERAL']['QuoteReferenceFormat']?>" name="QuoteReferenceFormat" id="QuoteReferenceFormat" class="input-sm form-control" />
                            <small class="text-muted text-info"><?=t('ReferenceFormatHelp')?></small>
                        </div>
                        <div class="form-group">
                            <label for="QuoteTaxDefault" class="control-label"><?=t('DefaultVAT')?></label>
                            <select class="input-sm form-control" id="QuoteTaxDefault" name="QuoteTaxDefault">
                                 <?php foreach($taxes as $tax) { ?>
                                    <option value="<?=$tax['ID']?>" <?=($settings['GENERAL']['QuoteTaxDefault'] == $tax['ID'] ? 'selected="selected"' : '')?>><?=$tax['Name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="QuoteExpirationDelay" class="control-label"><?=t('ExpirationDelayDay')?></label>
                            <input type="text" value="<?=$settings['GENERAL']['QuoteExpirationDelay']?>" name="QuoteExpirationDelay" id="QuoteExpirationDelay" class="input-sm form-control" />
                        </div>
                        <div class="form-group">
                            <label for="QuoteDefaultStatus" class="control-label"><?=t('DefaultStatus')?></label>
                            <select name="QuoteDefaultStatus" class="input-sm form-control">
                                <?php foreach($quoteStatutes as $status) { ?>
                                    <option value="<?=$status['ID']?>" <?=($settings['GENERAL']['QuoteDefaultStatus'] == $status['ID'] ? 'selected="selected"' : '')?>><?=$status['Name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="QuoteTemplate" class="control-label"><?=t('DocumentTemplate')?></label>
                            <select name="QuoteTemplate" class="input-sm form-control"><!--TODO replace by list with desc-->
                                <?php foreach($quoteTemplates as $template) { ?>
                                    <option value="<?=$template['Name']?>" <?=($settings['GENERAL']['QuoteTemplate'] == $template['Name'] ? 'selected="selected"' : '')?>><?=$template['Name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="QuoteNote" class="control-label"><?=t('FootNote')?></label>
                            <textarea name="QuoteNote" style="height:180px" id="QuoteNote" class="form-control"><?=$settings['GENERAL']['QuoteNote']?></textarea>
                            <div class="text-muted text-info"><?=t('FootNoteReplacement')?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="orderSettings" style="padding-top: 140px; margin-top: -140px;">
            <div class="container-fluid">
                <div class="row">
                    <br />
                    <div class="col-xs-12 col-lg-8">
                        <div class="form-group">
                            <label for="OrderReferenceFormat" class="control-label"><?=t('ReferenceFormat')?></label>
                            <input type="text" value="<?=$settings['GENERAL']['OrderReferenceFormat']?>" name="OrderReferenceFormat" id="OrderReferenceFormat" class="input-sm form-control" />
                            <small class="text-muted"><?=t('ReferenceFormatHelp')?></small>
                        </div>
                        <div class="form-group">
                            <label for="OrderTaxDefault" class="control-label"><?=t('DefaultVAT')?></label>
                            <select class="input-sm form-control" id="OrderTaxDefault" name="OrderTaxDefault">
                                 <?php foreach($taxes as $tax) { ?>
                                    <option value="<?=$tax['ID']?>" <?=($settings['GENERAL']['OrderTaxDefault'] == $tax['ID'] ? 'selected="selected"' : '')?>><?=$tax['Name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="OrderDueDelay" class="control-label"><?=t('DueDelayDay')?></label>
                            <input type="text" value="<?=$settings['GENERAL']['OrderDueDelay']?>" name="OrderDueDelay" id="OrderDueDelay" class="input-sm form-control" />
                        </div>
                        <div class="form-group">
                            <label for="OrderDefaultStatus" class="control-label"><?=t('DefaultStatus')?></label>
                            <select name="OrderDefaultStatus" class="input-sm form-control">
                                <?php foreach($orderStatutes as $status) { ?>
                                    <option value="<?=$status['ID']?>" <?=($settings['GENERAL']['OrderDefaultStatus'] == $status['ID'] ? 'selected="selected"' : '')?>><?=$status['Name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="OrderTemplate" class="control-label"><?=t('DocumentTemplate')?></label>
                            <select name="OrderTemplate" class="input-sm form-control"><!--TODO replace by list with desc-->
                                <?php foreach($orderTemplates as $template) { ?>
                                    <option value="<?=$template['Name']?>" <?=($settings['GENERAL']['OrderTemplate'] == $template['Name'] ? 'selected="selected"' : '')?>><?=$template['Name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="OrderNote" class="control-label"><?=t('FootNote')?></label>
                            <textarea name="OrderNote" style="height:180px" id="OrderNote" class="form-control"><?=$settings['GENERAL']['OrderNote']?></textarea>
                            <div class="text-muted text-info"><?=t('FootNoteReplacement')?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="depositSettings" style="padding-top: 140px; margin-top: -140px;">
            <div class="container-fluid">
                <div class="row">
                    <br />
                    <div class="col-xs-12 col-lg-8">
                        <div class="form-group">
                            <label for="DepositDefaultUnity" class="control-label"><?=t('DefaultUnity')?></label>
                            <select name="DepositDefaultUnity" id="DepositDefaultUnity" class="input-sm form-control">
                                 <?php foreach($settings['UNITY'] as $unity) { ?>
                                    <option value="<?=$unity['ID']?>" <?=($settings['GENERAL']['DepositDefaultUnity'] == $unity['ID'] ? 'selected="selected"' : '')?>><?=$unity['Name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="DepositDueDelay" class="control-label"><?=t('DueDelayDay')?></label>
                            <input type="text" value="<?=$settings['GENERAL']['DepositDueDelay']?>" name="DepositDueDelay" id="DepositDueDelay" class="input-sm form-control" />
                        </div>
                        <div class="form-group">
                            <label for="DepositNote" class="control-label"><?=t('FootNote')?></label>
                            <textarea name="DepositNote" style="height:180px" id="DepositNote" class="form-control"><?=$settings['GENERAL']['DepositNote']?></textarea>
                            <div class="text-muted text-info"><?=t('FootNoteReplacement')?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="invoiceSettings" style="padding-top: 140px; margin-top: -140px;">
            <div class="container-fluid">
                <div class="row">
                    <br />
                    <div class="col-xs-12 col-lg-8">
                        <div class="form-group">
                            <label for="InvoiceReferenceFormat" class="control-label"><?=t('ReferenceFormat')?></label>
                            <input type="text" value="<?=$settings['GENERAL']['InvoiceReferenceFormat']?>" name="InvoiceReferenceFormat" id="InvoiceReferenceFormat" class="input-sm form-control" />
                            <small class="text-muted"><?=t('ReferenceFormatHelp')?></small>
                        </div>
                        <div class="form-group">
                            <label for="InvoiceTaxDefault" class="control-label"><?=t('DefaultVAT')?></label>
                            <select class="input-sm form-control" id="InvoiceTaxDefault" name="InvoiceTaxDefault">
                                 <?php foreach($taxes as $tax) { ?>
                                    <option value="<?=$tax['ID']?>" <?=($settings['GENERAL']['InvoiceTaxDefault'] == $tax['ID'] ? 'selected="selected"' : '')?>><?=$tax['Name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="InvoiceDueDelay" class="control-label"><?=t('DueDelayDay')?></label>
                            <input type="text" value="<?=$settings['GENERAL']['InvoiceDueDelay']?>" name="InvoiceDueDelay" id="InvoiceDueDelay" class="input-sm form-control" />
                        </div>
                        <div class="form-group">
                            <label for="InvoiceDefaultStatus" class="control-label"><?=t('DefaultStatus')?></label>
                            <select name="InvoiceDefaultStatus" class="input-sm form-control">
                                <?php foreach($invoiceStatutes as $status) { ?>
                                    <option value="<?=$status['ID']?>" <?=($settings['GENERAL']['InvoiceDefaultStatus'] == $status['ID'] ? 'selected="selected"' : '')?>><?=$status['Name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="InvoiceTemplate" class="control-label"><?=t('DocumentTemplate')?></label>
                            <select name="InvoiceTemplate" class="input-sm form-control"><!--TODO replace by list with desc-->
                                <?php foreach($invoiceTemplates as $template) { ?>
                                    <option value="<?=$template['Name']?>" <?=($settings['GENERAL']['InvoiceTemplate'] == $template['Name'] ? 'selected="selected"' : '')?>><?=$template['Name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="InvoiceNote" class="control-label"><?=t('FootNote')?></label>
                            <textarea name="InvoiceNote" style="height:180px" id="InvoiceNote" class="form-control"><?=$settings['GENERAL']['InvoiceNote']?></textarea>
                            <div class="text-muted text-info"><?=t('FootNoteReplacement')?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="purchaseSettings" style="padding-top: 140px; margin-top: -140px;">
            <div class="container-fluid">
                <div class="row">
                    <br />
                    <div class="col-xs-12 col-lg-8">

                        <div class="form-group">
                            <label for="PurchaseReferenceFormat" class="control-label"><?=t('ReferenceFormat')?></label>
                            <input type="text" value="<?=$settings['GENERAL']['PurchaseReferenceFormat']?>" name="PurchaseReferenceFormat" id="PurchaseReferenceFormat" class="input-sm form-control" />
                            <small class="text-muted"><?=t('ReferenceFormatHelp')?></small>
                        </div>
                        <div class="form-group">
                            <label for="PurchaseTaxDefault" class="control-label"><?=t('DefaultVAT')?></label>
                            <select class="input-sm form-control" id="PurchaseTaxDefault" name="PurchaseTaxDefault">
                                 <?php foreach($taxes as $tax) { ?>
                                    <option value="<?=$tax['ID']?>" <?=($settings['GENERAL']['PurchaseTaxDefault'] == $tax['ID'] ? 'selected="selected"' : '')?>><?=$tax['Name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="PurchaseDefaultStatus" class="control-label"><?=t('DefaultStatus')?></label>
                            <select name="PurchaseDefaultStatus" class="input-sm form-control">
                                <?php foreach($purchaseStatutes as $status) { ?>
                                    <option value="<?=$status['ID']?>" <?=($settings['GENERAL']['PurchaseDefaultStatus'] == $status['ID'] ? 'selected="selected"' : '')?>><?=$status['Name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="PurchaseTemplate" class="control-label"><?=t('DocumentTemplate')?></label>
                            <select name="PurchaseTemplate" class="input-sm form-control"><!--TODO replace by list with desc-->
                                <?php foreach($purchaseTemplates as $template) { ?>
                                    <option value="<?=$template['Name']?>" <?=($settings['GENERAL']['PurchaseTemplate'] == $template['Name'] ? 'selected="selected"' : '')?>><?=$template['Name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="productSettings" style="padding-top: 140px; margin-top: -140px;">
            <div class="container-fluid">
                <div class="row">
                    <br />
                    <div class="col-xs-12 col-lg-8">
                        <div class="form-group">
                            <label for="ProductDefaultTaxSell" class="control-label"><?=t('DefaultVATSell')?></label>
                            <select class="input-sm form-control" id="ProductDefaultTaxSell" name="ProductDefaultTaxSell">
                                <?php foreach($taxes as $tax) { ?>
                                    <option value="<?=$tax['ID']?>" <?=($settings['GENERAL']['ProductDefaultTaxSell'] == $tax['ID'] ? 'selected="selected"' : '')?>><?=$tax['Name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="ProductDefaultTaxBuy" class="control-label"><?=t('DefaultVATBuy')?></label>
                            <select class="input-sm form-control" id="ProductDefaultTaxBuy" name="ProductDefaultTaxBuy">
                                <?php foreach($taxes as $tax) { ?>
                                    <option value="<?=$tax['ID']?>" <?=($settings['GENERAL']['ProductDefaultTaxBuy'] == $tax['ID'] ? 'selected="selected"' : '')?>><?=$tax['Name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="ProductDefaultUnity" class="control-label"><?=t('DefaultUnity')?></label>
                            <select name="ProductDefaultUnity" id="ProductDefaultUnity" class="input-sm form-control">
                                 <?php foreach($settings['UNITY'] as $unity) { ?>
                                    <option value="<?=$unity['ID']?>" <?=($settings['GENERAL']['ProductDefaultUnity'] == $unity['ID'] ? 'selected="selected"' : '')?>><?=$unity['Name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="taxSettings" style="padding-top: 140px; margin-top: -140px;">

            <table class="settingsTable table table-bordered table-condensed" id="taxTable">
                <tr>
                  <th><?=t('Label')?></th>
                  <th><?=t('Rate')?></th>
                  <th style="width:150px;"><?=t('ValidFrom')?></th>
                  <th style="width:150px;"><?=t('ValidTo')?></th>
                  <th style="width:110px;"><?=t('Action')?></th>
                </tr>
                <?php foreach($taxes as $tax) { ?>
                    <tr>
                        <td>
                            <input type="hidden" name="IDTax[]" value="<?=$tax['ID']?>" />
                            <input type="text" name="NameTax[]" value="<?=$tax['Name']?>" class="form-control input-sm" />
                        </td>
                        <td><input type="text" name="RateTax[]" value="<?=$tax['Rate']?>" class="form-control input-sm" /></td>
                        <td class="text-center">
                            <div class="input-group date datepicker">
                                <input type="text" class="form-control input-sm" id="ActiveFromTax[]" name="ActiveFromTax[]" value="<?=printDate($settings['GENERAL']['DateFormat'], $tax['ActiveFrom'])?>" ><span class="input-group-addon input-sm "><i class="fa fa-calendar fa-fw"></i></span>
                            </div>
                        </td>
                        <td class="text-center">
                            <div class="input-group date datepicker">
                                <input type="text" class="form-control input-sm" id="ActiveToTax[]" name="ActiveToTax[]" value="<?=printDate($settings['GENERAL']['DateFormat'], $tax['ActiveTo'])?>" ><span class="input-group-addon input-sm "><i class="fa fa-calendar fa-fw"></i></span>
                            </div>
                        </td>
                        <td class="text-center"></td>
                    </tr>
                <?php } ?>
                <tr class="newRow" style="display:none;">
                    <td>
                        <input type="hidden" name="IDTax[]" value="" />
                        <input type="text" name="NameTax[]" value="" class="form-control input-sm" />
                    </td>
                    <td><input type="text" name="RateTax[]" value="" class="form-control input-sm" /></td>
                    <td class="text-center">
                        <div class="input-group date datepicker">
                            <input type="text" class="form-control input-sm" id="ActiveFromTax[]" name="ActiveFromTax[]" value="" ><span class="input-group-addon input-sm "><i class="fa fa-calendar fa-fw"></i></span>
                        </div>
                    </td>
                    <td class="text-center">
                        <div class="input-group date datepicker">
                            <input type="text" class="form-control input-sm" id="ActiveToTax[]" name="ActiveToTax[]" value="" ><span class="input-group-addon input-sm "><i class="fa fa-calendar fa-fw"></i></span>
                        </div>
                    </td>
                    <td class="text-center"><button type="button" class="btn btn-default btn-danger btn-sm" onclick="removeRow(this);return false;"><?=t('Delete')?></button></td>
                </tr>
                <tr class="actionTr">
                    <td colspan="5"><button type="button" class="btn btn-default btn-sm" onclick="addRow('taxTable');return false;"><i class="fa fa-plus"></i> <?=t('AddNewRow')?></button></td>
                </tr>
            </table>

        </div>

        <div role="tabpanel" class="tab-pane" id="paymentSettings" style="padding-top: 140px; margin-top: -140px;">

            <table class="settingsTable table table-bordered table-condensed" id="paymentTable">
                <tr>
                  <th><?=t('Label')?></th>
                  <th style="width:150px;"><?=t('ValidFrom')?></th>
                  <th style="width:150px;"><?=t('ValidTo')?></th>
                  <th style="width:110px;"><?=t('Action')?></th>
                </tr>
                <?php if (isset($settings['PAYMENT'])) { ?>
                    <?php foreach($settings['PAYMENT'] as $payment) { ?>
                        <tr>
                            <td>
                                <input type="hidden" name="IDPayment[]" value="<?=$payment['ID']?>" />
                                <input type="text" name="NamePayment[]" value="<?=$payment['Name']?>" class="form-control input-sm" />
                            </td>
                            <td class="text-center">
                                <div class="input-group date datepicker">
                                    <input type="text" class="form-control input-sm" id="ActiveFromPayment[]" name="ActiveFromPayment[]" value="<?=printDate($settings['GENERAL']['DateFormat'], $payment['ActiveFrom'])?>" ><span class="input-group-addon input-sm "><i class="fa fa-calendar fa-fw"></i></span>
                                </div>
                            </td>
                            <td class="text-center">
                                <div class="input-group date datepicker">
                                    <input type="text" class="form-control input-sm" id="ActiveToPayment[]" name="ActiveToPayment[]" value="<?=printDate($settings['GENERAL']['DateFormat'], $payment['ActiveTo'])?>" ><span class="input-group-addon input-sm "><i class="fa fa-calendar fa-fw"></i></span>
                                </div>
                            </td>
                            <td class="text-center"></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                <tr class="newRow" style="display:none;">
                    <td>
                        <input type="hidden" name="IDPayment[]" value="" />
                        <input type="text" name="NamePayment[]" value="" class="form-control input-sm" />
                    </td>
                    <td class="text-center">
                        <div class="input-group date datepicker">
                            <input type="text" class="form-control input-sm" id="ActiveFromPayment[]" name="ActiveFromPayment[]" value="" ><span class="input-group-addon input-sm "><i class="fa fa-calendar fa-fw"></i></span>
                        </div>
                    </td>
                    <td class="text-center">
                        <div class="input-group date datepicker">
                            <input type="text" class="form-control input-sm" id="ActiveToPayment[]" name="ActiveToPayment[]" value="" ><span class="input-group-addon input-sm "><i class="fa fa-calendar fa-fw"></i></span>
                        </div>
                    </td>
                    <td class="text-center"><button type="button" class="btn btn-default btn-danger btn-sm" onclick="removeRow(this);return false;"><?=t('Delete')?></button></td>
                </tr>
                <tr class="actionTr">
                    <td colspan="3"><button type="button" class="btn btn-default btn-sm" onclick="addRow('paymentTable');return false;"><i class="fa fa-plus"></i> <?=t('AddNewRow')?></button></td>
                </tr>
            </table>

        </div>

        <div role="tabpanel" class="tab-pane" id="unitSettings" style="padding-top: 140px; margin-top: -140px;">

            <table class="settingsTable table table-bordered table-condensed" id="unitTable">
                <tr>
                  <th><?=t('Label')?></th>
                  <th style="width:150px;"><?=t('ValidFrom')?></th>
                  <th style="width:150px;"><?=t('ValidTo')?></th>
                  <th style="width:110px;"><?=t('Action')?></th>
                </tr>
                <?php if (isset($settings['UNITY'])) { ?>
                    <?php foreach($settings['UNITY'] as $unity) { ?>
                        <tr>
                            <td>
                                <input type="hidden" name="IDUnity[]" value="<?=$unity['ID']?>" />
                                <input type="text" name="NameUnity[]" value="<?=$unity['Name']?>" class="form-control input-sm" />
                            </td>
                            <td class="text-center">
                                <div class="input-group date datepicker">
                                    <input type="text" class="form-control input-sm" id="ActiveFromUnity[]" name="ActiveFromUnity[]" value="<?=printDate($settings['GENERAL']['DateFormat'], $unity['ActiveFrom'])?>" ><span class="input-group-addon input-sm "><i class="fa fa-calendar fa-fw"></i></span>
                                </div>
                            </td>
                            <td class="text-center">
                                <div class="input-group date datepicker">
                                    <input type="text" class="form-control input-sm" id="ActiveToUnity[]" name="ActiveToUnity[]" value="<?=printDate($settings['GENERAL']['DateFormat'], $unity['ActiveTo'])?>" ><span class="input-group-addon input-sm "><i class="fa fa-calendar fa-fw"></i></span>
                                </div>
                            </td>
                            <td class="text-center"></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                <tr class="newRow" style="display:none;">
                    <td>
                        <input type="hidden" name="IDUnity[]" value="" />
                        <input type="text" name="NameUnity[]" value="" class="form-control input-sm" />
                    </td>
                    <td class="text-center">
                        <div class="input-group date datepicker">
                            <input type="text" class="form-control input-sm" id="ActiveFromUnity[]" name="ActiveFromUnity[]" value="" ><span class="input-group-addon input-sm "><i class="fa fa-calendar fa-fw"></i></span>
                        </div>
                    </td>
                    <td class="text-center">
                        <div class="input-group date datepicker">
                            <input type="text" class="form-control input-sm" id="ActiveToUnity[]" name="ActiveToUnity[]" value="" ><span class="input-group-addon input-sm "><i class="fa fa-calendar fa-fw"></i></span>
                        </div>
                    </td>
                    <td class="text-center"><button type="button" class="btn btn-default btn-danger btn-sm" onclick="removeRow(this);return false;"><?=t('Delete')?></button></td>
                </tr>
                <tr class="actionTr">
                    <td colspan="3"><button type="button" class="btn btn-default btn-sm" onclick="addRow('unitTable');return false;"><i class="fa fa-plus"></i> <?=t('AddNewRow')?></button></td>
                </tr>
            </table>

        </div>
<!--
        <div role="tabpanel" class="tab-pane" id="emailSettings">

            <div class="container-fluid">
                <div class="row">
                    <br />
                    <div class="col-xs-12 col-lg-8">
                        <div class="form-group">
                            <label for="MailCopyAdmin" class="control-label"><?=t('Send copy of email to admin ?')?></label>
                            <select name="MailCopyAdmin" id="MailCopyAdmin" class="input-sm form-control">
                                <option value="0" <?=($settings['GENERAL']['MailCopyAdmin'] == '0' ? 'selected="selected"' : '' )?>><?=t('No')?></option>
                                <option value="1" <?=($settings['GENERAL']['MailCopyAdmin'] == '1' ? 'selected="selected"' : '' )?>><?=t('Yes')?></option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="MailMethod" class="control-label">Envoi des mails :</label>
                            <select name="MailMethod" id="MailMethod" class="input-sm form-control" onchange="mailMethodChange()">
                                <option value="PHP" <?=($settings['GENERAL']['MailCopyAdmin'] == 'PHP' ? 'selected="selected"' : '' )?>><?=t('PHP')?></option>
                                <option value="SMTP" <?=($settings['GENERAL']['MailCopyAdmin'] == 'SMTP' ? 'selected="selected"' : '' )?>><?=t('SMTP')?></option>
                            </select>
                        </div>

                        <div id="SMTPSettingsDiv" <?=($settings['GENERAL']['MailCopyAdmin'] == 'SMTP' ? '' : 'style="display:none;"' )?>>
                            <br />
                            <fieldset>
                                <legend><?=t('SMTP Settings')?></legend>
                                <div class="form-group">
                                    <label for="SMTPURL" class="control-label"><?=t('SMTP URL')?></label>
                                    <input type="text" value="<?=$settings['GENERAL']['SMTPURL']?>" name="SMTPURL" id="SMTPURL" class="input-sm form-control" />
                                </div>
                                <div class="form-group">
                                    <label for="SMTPNeedAuth" class="control-label"><?=t('SMTP need Auth ?')?></label>
                                    <select name="SMTPNeedAuth" id="SMTPNeedAuth" class="input-sm form-control">
                                        <option value="0" <?=($settings['GENERAL']['SMTPNeedAuth'] == '0' ? 'selected="selected"' : '' )?>><?=t('No')?></option>
                                        <option value="1" <?=($settings['GENERAL']['SMTPNeedAuth'] == '1' ? 'selected="selected"' : '' )?>><?=t('Yes')?></option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="SMTPUser" class="control-label"><?=t('SMTP user')?></label>
                                    <input type="text" value="<?=$settings['GENERAL']['SMTPUser']?>" name="SMTPUser" id="SMTPUser" class="input-sm form-control" />
                                </div>
                                <div class="form-group">
                                    <label for="SMTPPassword" class="control-label"><?=t('SMTP password')?></label>
                                    <input type="password" value="<?=$settings['GENERAL']['SMTPPassword']?>" name="SMTPPassword" id="SMTPPassword" class="input-sm form-control" />
                                </div>
                                <div class="form-group">
                                    <label for="SMTPPort" class="control-label"><?=t('SMTP Port')?></label>
                                    <input type="text" value="<?=$settings['GENERAL']['SMTPPort']?>" name="SMTPPort" id="SMTPPort" class="input-sm form-control" />
                                </div>
                                <div class="form-group">
                                    <label for="SMTPSecurity" class="control-label">Sécurité</label>
                                    <select name="SMTPSecurity" id="SMTPSecurity" class="input-sm form-control">
                                        <option value="0" <?=($settings['GENERAL']['SMTPSecurity'] == '0' ? 'selected="selected"' : '' )?>><?=t('None')?></option>
                                        <option value="SSL" <?=($settings['GENERAL']['SMTPSecurity'] == 'SSL' ? 'selected="selected"' : '' )?>>SSL</option>
                                        <option value="TLS" <?=($settings['GENERAL']['SMTPSecurity'] == 'TLS' ? 'selected="selected"' : '' )?>>TLS</option>
                                    </select>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>

        </div>
-->

        <div role="tabpanel" class="tab-pane" id="updateSettings" style="padding-top: 140px; margin-top: -140px;">
            <div class="container-fluid">
                <br />
                <?=t('UpdateManagement')?>.<br />
                <?=t('YourVersion', HLX_VERSION)?>. <?=t('YourCanal', HLX_CANAL)?>.
                <br />
                <br />
                <div id="checkUpdate"><button class="btn btn-primary"><?=t('CheckForUpdate')?></button></div>
            </div>
        </div>

        <div role="tabpanel" class="tab-pane" id="backupSettings" style="padding-top: 140px; margin-top: -140px;">
            <div class="container-fluid">
                <br />
                <?=t('BackupManagement')?>
                <br />
                <br />
                <div id="backup"><button class="btn btn-primary"><?=t('Backup')?></button></div>
            </div>
        </div>
    </div>

</form>

<script>
<!--
    ignoreNextHash = false;
    // Change hash for page-reload
    $('.nav-tabs#settingsTab a').on('shown.bs.tab', function (e) {
        ignoreNextHash = true;
        window.location.hash = e.target.hash;
    });
    //Got to last opened tab
    var lastOpened = function() {
        var url = document.location.toString();
        if (url.match('#')) {
            $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
        }
        else {
            $('.nav-tabs a:first').click();
        }
    }
    lastOpened();
    //Check browser back with anchor
    $(window).on('hashchange', function() {
        if ( ! ignoreNextHash)
            lastOpened();
        else
            ignoreNextHash = false;
    });

    var datepickerSettings = {
        format: '<?=str_replace(array('d','m','y','Y'), array('dd','mm','yy','yyyy'), $settings['GENERAL']['DateFormat'])?>',
        todayBtn: 'linked',
        language: 'fr',
        daysOfWeekHighlighted: '0,6',
        autoUpdateInput: false,
        clearBtn: true
    };

    var addRow = function(idTable) {
        $('#'+idTable+' .newRow').clone().removeClass('newRow').insertBefore('#'+idTable+' .actionTr').show().find('.datepicker').datepicker(datepickerSettings).val('');
    }

    var removeRow = function(elem) {
        $($(elem).closest('tr')).remove();
    }

    var mailMethodChange = function() {
        if ($('#MailMethod').val() == 'SMTP')
            $('#SMTPSettingsDiv').show();
        else
            $('#SMTPSettingsDiv').hide();
    }
    //Init Date pickers
    $('.input-group.date.datepicker').datepicker(datepickerSettings);
    $('.input-group.date.datepicker[value=""]').val('');

    //Check for update
    $('#checkUpdate button').click(function() {

        $('#checkUpdate').addClass('alert alert-warning').html('<span class="glyphicon glyphicon-refresh"></span> <?=t('Checking')?>');

        checkUpdate(function(res) {

            if (res.type == 'error')
                $('#checkUpdate').removeClass('alert-warning').addClass('alert-danger');
            if (res.type == 'info')
                $('#checkUpdate').removeClass('alert-warning').addClass('alert-info');

            $('#checkUpdate').html(res.msg);
        });

    });

    //Check update
    var checkUpdate = function(callback) {
        $.ajax({
            url: '<?=$urlUpdateJson?>',
            cache: false,
            success: function(data) {

                try {
                    data = JSON.parse(data);
                }catch(e) {
                    data = false;
                }

                if ( ! data) {
                    callback({
                        type:   'error',
                        msg:    '<?=addslashes(t('ErrorSearchingUpdate'))?>'
                    });
                }
                else {
                    var found = false;

                    $.each(data, function(version, rInfo) {
                        if (version > '<?=HLX_VERSION?>'
                            && ('<?=HLX_CANAL?>' == 'beta'
                                || ('<?=HLX_CANAL?>' == 'rc' && (rInfo.canal == 'rc' || rInfo.canal == 'stable'))
                                || ('<?=HLX_CANAL?>' == 'stable' && rInfo.canal == 'stable')))
                        {
                            callback({
                                type:   'info',
                                msg:    '<?=addslashes(t('NewVersion', $urlUpdate))?>'
                            });
                            found = true;
                        }
                    });

                    if (found == false) {
                        callback({
                            type:   'info',
                            msg:    '<?=addslashes(t('VersionUpToDate'))?>'
                        });
                    }
                }
                return true;
            },
            error: function(data) {
                callback({
                    type:   'error',
                    msg:    '<?=addslashes(t('ErrorSearchingUpdate'))?>'
                });
                return true;
            }
        });
    }

    //Check for update
    $('#backup button').click(function() {

        $('#backup').addClass('alert alert-warning').html('<span class="glyphicon glyphicon-refresh"></span> <?=t('Processing')?>');

        doBackup(function(res) {

            if (res.type == 'error')
                $('#backup').removeClass('alert-warning').addClass('alert-danger');
            if (res.type == 'info')
                $('#backup').removeClass('alert-warning').addClass('alert-info');

            $('#backup').html(res.msg);
        });

    });

    //Check update
    var doBackup = function(callback) {
        $.ajax({
            url: '<?=$urlBackup?>',
            dataType: 'json',
            cache: false,
            success: function(data) {
                if ( ! data || data.error == 1) {
                    callback({
                        type:   'error',
                        msg:    '<?=addslashes(t('ErrorDoingBackup'))?>'
                    });
                }
                else {
                    callback({
                        type:   'info',
                        msg:    '<?=addslashes(t('BackupFile'))?>'.replace('%1', data.file)
                    });
                }
            },
            error: function() {
                callback({
                    type:   'error',
                    msg:    '<?=addslashes(t('ErrorDoingBackup'))?>'
                });
                return true;
            }
        });
    }

-->
</script>

<?php include 'footer.php';