
<script type="text/javascript" src="views/js/bootstrap-select.min.js"></script>
<link rel="stylesheet" href="views/css/bootstrap-select.min.css" type="text/css" />

<div id="modalNewItem" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><?=t('New')?> <span id="spanTitle"></span></h4>
            </div>
            <div class="modal-body">

                <input type="hidden" name="typeNewItem" id="typeNewItem" />

                <div class="row" id="customerDiv">
                    <div class="col-xs-12">
                        <label id="labelSelect"></label><br />
                        <select id="thirdPartyIDNewItem" name="thirdPartyIDNewItem" class="selectpicker" data-none-selected-text="<?=t('Select')?>" data-size="10" data-width="100%" data-live-search="true">
                        </select>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><?=t('Cancel')?></button>
                <button type="button" class="btn btn-primary btn-sm" id="createNewItem" onclick="createNewItem();" ><?=t('Create')?></button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    <!--
    newQuote = function() {
        $('#modalNewItem').modal('show');
        $('#typeNewItem').val('quote');
        $('#spanTitle').html('<?=t('quote')?>');
        $('#labelSelect').html('<?=t('Customer')?> :');
        if ($('#thirdPartyIDNewItem')[0].options.length < 2)
        {
            loadCustomer();
        }
    }
    newOrder = function() {
        $('#modalNewItem').modal('show');
        $('#typeNewItem').val('order');
        $('#spanTitle').html('<?=t('order')?>');
        $('#labelSelect').html('<?=t('Customer')?> :');
        if ($('#thirdPartyIDNewItem')[0].options.length < 2)
        {
            loadCustomer();
        }
    }
    newInvoice = function() {
        $('#modalNewItem').modal('show');
        $('#typeNewItem').val('invoice');
        $('#spanTitle').html('<?=t('invoice')?>');
        $('#labelSelect').html('<?=t('Customer')?> :');
        if ($('#thirdPartyIDNewItem')[0].options.length < 2)
        {
            loadCustomer();
        }
    }
    newPurchase  = function() {
        $('#modalNewItem').modal('show');
        $('#typeNewItem').val('purchase');
        $('#spanTitle').html('<?=t('purchase')?>');
        $('#labelSelect').html('<?=t('Supplier')?> :');
        if ($('#thirdPartyIDNewItem')[0].options.length < 2)
        {
            loadSupplier();
        }
    }
    newContact = function() {
        $('#modalNewItem').modal('show');
        $('#typeNewItem').val('contact');
        $('#spanTitle').html('<?=t('Contact')?>');
        if ($('#thirdPartyIDNewItem')[0].options.length < 2)
        {
            loadCustomer();
        }
    }

    loadCustomer = function() {
        $.ajax({
            type: "GET",
            url: '<?=$navBar['urlLoadCustomer']?>',
            success: function (response) {
                var options = '<option selected hidden value=""><?=t('Select')?></option>';
                JSON.parse(response).forEach(function(item) {
                    options += '<option value="'+item.ID+'" data-subtext="'+escapeHtml(item.PostalCode+' - '+item.City)+'">'+escapeHtml(item.Name+' '+item.Firstname)+'</option>';
                });
                $('#thirdPartyIDNewItem').html(options);
                $('#thirdPartyIDNewItem').selectpicker('refresh');
            }
        });
    }
    loadSupplier = function() {
        $.ajax({
            type: "GET",
            url: '<?=$navBar['urlLoadSupplier']?>',
            success: function (response) {
                var options = '<option selected hidden value="" ><?=t('Select')?></option>';
                JSON.parse(response).forEach(function(item) {
                    options += '<option value="'+item.ID+'" data-subtext="'+escapeHtml(item.PostalCode+' - '+item.City)+'">'+escapeHtml(item.Name+' '+item.Firstname)+'</option>';
                });
                $('#thirdPartyIDNewItem').html(options);
                $('#thirdPartyIDNewItem').selectpicker('refresh');
            }
        });
    }

    createNewItem = function() {
        var thirdPartyID = $('#thirdPartyIDNewItem').val();

        if (thirdPartyID == '')
        {
            alert('<?=t('PleaseSelectAThird')?>');
        }
        else
        {
            switch ($('#typeNewItem').val())
            {
                case 'contact':
                    window.location = '<?=$navBar['urlNewContact']?>'+thirdPartyID;
                    break;
                case 'quote':
                    window.location = '<?=$navBar['urlNewQuote']?>'+thirdPartyID;
                    break;
                case 'order':
                    window.location = '<?=$navBar['urlNewOrder']?>'+thirdPartyID;
                    break;
                case 'invoice':
                    window.location = '<?=$navBar['urlNewInvoice']?>'+thirdPartyID;
                    break;
                case 'purchase':
                    window.location = '<?=$navBar['urlNewPurchase']?>'+thirdPartyID;
                    break;
            }
        }
    }
    //-->
</script>