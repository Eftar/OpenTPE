<?php include 'header.php'; ?>

<?php include 'navbar.php'; ?>

    <br /><br />
    <div class="alert alert-danger" role="alert">
        <?=$error?>
        <a class="btn btn-sm btn-default pull-right" onclick="history.back();return false;">
            <i class="glyphicon glyphicon-chevron-left"></i> <?=t('Back')?>
        </a>
    </div>

<script>
<!--

-->
</script>

<?php include 'footer.php';