<?php


/**
 *
 * */

class HLX_User {

    private static $instance;

    /**
     * Return the current instance or create the first one
     * */
    public static function getInstance()
    {
        if ( ! isset(self::$instance))
        {
            $c = __CLASS__;
            self::$instance = new $c;
        }

        return self::$instance;
    }

    /**
     *
     * */
    public function getUsers()
    {

        $arrReturn = array();

        $res = $this->pdo->query("SELECT * FROM hlx_users");
        if ( ! $res)
        {
            echo t('ErrorConnexionTableUsers');
            return false;
        }

        while ($row = $res->fetch(PDO::FETCH_ASSOC))
        {
            $row['pass'] = '';
            $arrReturn[ $row['login'] ] = $row;
        }

        $res->closeCursor();

        return $arrReturn;

    }

    /**
     *
     * */
    public function getUser($login)
    {

        $res = $this->pdo->query("SELECT * FROM hlx_users WHERE login = ".$this->pdo->quote($login));
        if ( ! $res)
        {
            echo t('ErrorConnexionTableUsers');
            return false;
        }

        $rowUser = $res->fetch(PDO::FETCH_ASSOC);
        //clear pass field
        $rowUser['pass'] = '';

        $res->closeCursor();

        return $rowUser;

    }

    /**
     *
     * */
    public function delete($login)
    {

        $res = $this->pdo->query("DELETE FROM hlx_users WHERE login = ".$this->pdo->quote($login));
        if ( ! $res)
        {
            $this->log->logMsg(t('Error deleting item table', 'users'), Log::TYPE_ERROR);
            return false;
        }
        else
        {
            return true;
        }

    }

    public function save($user)
    {

        //TODO forbid if not connected OR assigned role will be lower than role of connected user
        $res = $this->pdo->query("UPDATE hlx_users SET
                                        password = ".(($user['pass'] != '') ? "'".sha1($user['password'].md5(substr($user['login'], 0, strlen($user['password']))))."'" : "password").",
                                        role = ".$this->pdo->quote($user['role'])."
                                    WHERE login = ".$this->pdo->quote($user['login']));

        if ($res->rowCount() == 0)
            $res = $this->pdo->query("INSERT INTO hlx_users ('login', 'password', 'role') VALUES (".$this->pdo->quote($user['login']).", '".sha1($user['password'].md5(substr($user['login'], 0, strlen($user['password']))))."', ".$this->pdo->quote($user['role']).")");

        if ($res->rowCount() == 0)
        {
            $res->closeCursor();
            return false;
        }

        $res->closeCursor();

        return true;

    }

    private function initSession()
    {

        session_start();
        session_regenerate_id();
        //TODO see for adding test on IP / user agent / accepted language to avoid steal of session
        $this->isUserConnected = isset($_SESSION['HLX_User']);

    }

    /**
     *
     * */
    public function isConnected()
    {
        return $this->isUserConnected;
    }

    public function login($login, $password)
    {

        $res = $this->pdo->query("SELECT * FROM hlx_users WHERE login = ".$this->pdo->quote($login)." AND password = '".sha1($password.md5(substr($login, 0, strlen($password))))."'");
        if ( ! $res)
        {
            return false;
        }

        $row = $res->fetch(PDO::FETCH_ASSOC);

        if (isset($row['login']) && $row['login'] == $login)
        {
            $row['password'] = '';
            //TODO see to add  IP / user agent / accepted language for avoid steal of session
            $_SESSION['HLX_User'] = $row;
            return true;
        }

        return false;

    }

    public function logout()
    {

        unset($_SESSION['HLX_User']);
        session_destroy();

    }

}
