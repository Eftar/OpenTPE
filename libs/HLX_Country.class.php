<?php

class HLX_Country {

    static $rCountryList = array();

    /**
     * Returns an array list of iso => country, translated in the language $iso.
     * If there is no translated country list, return the english one
     * @param $iso
     * @return array
     */
    static function getCountryList($iso = false)
    {
        if ($iso == false)
        {
            require_once('models/settingModel.php');
            $settings = SettingModel::load(array('GENERAL'));
            $iso = $settings['GENERAL']['Language'];
        }

        if (isset($rCountryList[ $iso ]))
            return $rCountryList[ $iso ];
        if (file_exists(BFWK_LIB_PATH . 'countryList/' . $iso . '/country.php'))
        {
            return $rCountryList[ $iso ] = (include BFWK_LIB_PATH . 'countryList/' . $iso . '/country.php');
        }
        else
            return $rCountryList[ $iso ] = (include BFWK_LIB_PATH . 'countryList/en/country.php');

    }

    /**
     * Returns the countryname of a given $countrycode, , translated in the language $iso
     * @param $iso
     * @return string
     */
    static function getCountryName($countryCode, $iso = false)
    {
        $rCountry = self::getCountryList($iso);
        return (isset($rCountry[ $countryCode ]) ? $rCountry[ $countryCode ] : $countryCode);
    }

}