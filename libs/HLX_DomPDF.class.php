<?php
/*
require_once BFWK_LIB_PATH . 'dompdf/autoload.inc.php';
// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;
use Dompdf\FontMetrics;
*/

require_once BFWK_LIB_PATH . 'dompdf/lib/html5lib/Parser.php';
require_once BFWK_LIB_PATH . 'dompdf/lib/php-font-lib/src/FontLib/Autoloader.php';
require_once BFWK_LIB_PATH . 'dompdf/lib/php-svg-lib/src/autoload.php';
require_once BFWK_LIB_PATH . 'dompdf/src/Autoloader.php';

Dompdf\Autoloader::register();

use Dompdf\Dompdf;
use Dompdf\Options;


/**
 *
 * */

class HLX_DomPDF {

    private $dompdf;

    /**
     *
     * */
    function __construct()
    {
        // instantiate and use the dompdf class
        $options = new Options();
        $options->setIsPhpEnabled(true);
        $options->setDefaultPaperSize('a4');
        $options->setDefaultFont('Helvetica');

        $this->dompdf = new Dompdf($options);

    }

    /**
     *
     * */
    public function loadHtml($html)
    {
        $this->dompdf->loadHtml($html);
    }

    /**
     *
     * */
    public function render($file = false, $name = 'document')
    {

        // (Optional) Setup the paper size and orientation
        $this->dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $this->dompdf->render();

        if ( ! $file)
        {
            // Output the generated PDF to Browser
            $this->dompdf->stream($name, array('Attachment'=>0));
        }
        else
        {
            file_put_contents($file, $this->dompdf->output());
            // Output the generated PDF to Browser
            $this->dompdf->stream($name, array('Attachment'=>0));
        }

    }

}