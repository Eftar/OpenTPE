<?php

/*************************
 * COMPATIBILITY Functions
 *************************/

if ( ! function_exists('get_called_class')) {
    class class_tools {
        static $i = 0;
        static $fl = null;

        static function get_called_class() {
            $bt = debug_backtrace();

            if (self::$fl == $bt[2]['file'].$bt[2]['line']) {
                self::$i++;
            } else {
                self::$i = 0;
                self::$fl = $bt[2]['file'].$bt[2]['line'];
            }

            $lines = file($bt[2]['file']);

            preg_match_all('/([a-zA-Z0-9\_]+)::'.$bt[2]['function'].'/',
                $lines[$bt[2]['line']-1],$matches);

            return $matches[1][self::$i];
        }
    }

    function get_called_class() {
        return class_tools::get_called_class();
    }
}

if ( ! function_exists('str_getcsv')) {
    function str_getcsv($input, $delimiter = ',', $enclosure = '"')
    {
        if( ! preg_match("/[$enclosure]/", $input) ) {
          return (array)preg_replace(array("/^\\s*/", "/\\s*$/"), '', explode($delimiter, $input));
        }

        $token = "##"; $token2 = "::";
        //alternate tokens "\034\034", "\035\035", "%%";
        $t1 = preg_replace(array("/\\\[$enclosure]/", "/$enclosure{2}/",
             "/[$enclosure]\\s*[$delimiter]\\s*[$enclosure]\\s*/", "/\\s*[$enclosure]\\s*/"),
             array($token2, $token2, $token, $token), trim(trim(trim($input), $enclosure)));

        $a = explode($token, $t1);
        foreach($a as $k=>$v) {
            if ( preg_match("/^{$delimiter}/", $v) || preg_match("/{$delimiter}$/", $v) ) {
                $a[$k] = trim($v, $delimiter); $a[$k] = preg_replace("/$delimiter/", "$token", $a[$k]); }
        }
        $a = explode($token, implode($token, $a));
        return (array)preg_replace(array("/^\\s/", "/\\s$/", "/$token2/"), array('', '', $enclosure), $a);

    }
}

if ( ! function_exists('boolval')) {
    function boolval($val) {
        return (bool) $val;
    }
}

if ( ! function_exists('password_hash')) {
    function password_hash($password) {
        $salt = mcrypt_create_iv(22, MCRYPT_DEV_URANDOM);
        $salt = base64_encode($salt);
        $salt = str_replace('+', '.', $salt);
        return crypt($password, '$2y$10$'.$salt.'$');
    }
}

if ( ! function_exists('password_verify')) {
    function password_verify($password, $hash) {
        if (!function_exists('crypt')) {
            trigger_error("Crypt must be loaded for password_verify to function", E_USER_WARNING);
            return false;
        }
        $ret = crypt($password, $hash);
        if (!is_string($ret) || mb_strlen($ret) != mb_strlen($hash) || mb_strlen($ret) <= 13) {
            return false;
        }
        $status = 0;
        for ($i = 0; $i < mb_strlen($ret); $i++) {
            $status |= (ord($ret[$i]) ^ ord($hash[$i]));
        }
        return $status === 0;
    }
}

/*****************
 * UTILS Functions
 *****************/
function array_search_multi($value, $key, $array) {
   return array_search($value, array_column($array, $key));
}

function printDate($format, $dateString)//TODO rename this function it's neither print
{

    if ($dateString == '0000-00-00' || $dateString == '0000-00-00 00:00:00')
        return '';

    $arrStr = explode(' ', $dateString);
    $arrDate = explode('-', $arrStr[0]);
    if ( ! isset($arrDate[2]))
    {
        $arrDate = array(0, 0, 0);
    }
    if ( ! isset($arrStr[1]))
        $arrTime = array(0, 0);
    else
    {
        $arrTime = explode(':', $arrStr[1]);
        if ( ! isset($arrTime[1]))
            $arrTime = array(0, 0);
    }
    return date($format, mktime($arrTime[0], $arrTime[1], 0, $arrDate[1], $arrDate[2], $arrDate[0]));

}

function convertDateFormat($dateString, $inputFormat, $outputFormat, $check = false)
{
    if ($check == true)
    {
        //Check if date was already in output format
        $check = date_create_from_format($outputFormat, $dateString);
        if ($check !== false)
            return date_format($check, $outputFormat);
    }
    return date_format(date_create_from_format($inputFormat, $dateString), $outputFormat);
}

//From http://php.net/manual/fr/function.is-writable.php#118667
function is_writable_r($dir) {
    if (is_dir($dir)) {
        if(is_writable($dir)){
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (!is_writable_r($dir."/".$object)) return false;
                    else continue;
                }
            }
            return true;
        }else{
            return false;
        }

    }else if(file_exists($dir)){
        return (is_writable($dir));

    }
}

/*****************
 * VIEWS Functions
 *****************/

 /**
  * Return HTML content for displaying a modal window
  * @param $title
  * @param $content
  * @param $options
  * */
function showModal($title, $content, $options = false) {

    echo '<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="theModalWindow" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">'.$title.'</h4>
                </div>
                <div class="modal-body">
                    '.(isset($options['error'])?'<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$options['error'].'</div><br />':'').'
                    '.(isset($options['info'])?'<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$options['info'].'</div><br />':'').'
                    '.$content.'
                </div>
                <div class="modal-footer">
                    '.(isset($options['action'])?'<a href="'.$options['action']['url'].'" role="button" class="btn btn-default btn-primary">'.$options['action']['value'].'</a>':'').'
                    <button type="button" class="btn btn-default" data-dismiss="modal">'.t('Close').'</button>
                </div>
            </div>
        </div>
    </div>';

}

/**
 * Init vars for navBar
 * */
function navBarVars($active) {

    $array = array();
    $array['urlHome']           = BFWK_SERVER_ROOT.'?/main/home/';
    $array['urlThirdParty']     = BFWK_SERVER_ROOT.'?/thirdParty/index/';
    $array['urlRelation']       = BFWK_SERVER_ROOT.'?/relation/index/';
    $array['urlDocument']       = BFWK_SERVER_ROOT.'?/document/index/';
    $array['urlProduct']        = BFWK_SERVER_ROOT.'?/product/index/';
    $array['urlPayment']        = BFWK_SERVER_ROOT.'?/payment/index/';
    $array['urlAgenda']         = BFWK_SERVER_ROOT.'?/agenda/index/';
    $array['urlReport']         = BFWK_SERVER_ROOT.'?/report/index/';
    $array['urlImpexp']         = BFWK_SERVER_ROOT.'?/impexp/index/';
    $array['urlSetting']        = BFWK_SERVER_ROOT.'?/setting/edit/';
    $array['urlUser']           = BFWK_SERVER_ROOT.'?/user/index/';
    $array['urlLogout']         = BFWK_SERVER_ROOT.'?/main/logout/';
    $array['urlMyNote']         = BFWK_SERVER_ROOT.'?/help/myNote/';
    $array['urlAbout']          = BFWK_SERVER_ROOT.'?/help/about/';
    $array['urlDocumentation']  = BFWK_SERVER_ROOT.'?/help/documentation/';
    $array['urlUpdate']         = BFWK_SERVER_ROOT.'?setting/edit/#updateSettings';
    $array['urlNewCustomer']    = BFWK_SERVER_ROOT.'?/thirdParty/newCustomer/';
    $array['urlNewSupplier']    = BFWK_SERVER_ROOT.'?/thirdParty/newSupplier/';
    $array['urlNewContact']     = BFWK_SERVER_ROOT.'?/thirdParty/newContact/';
    $array['urlNewProduct']     = BFWK_SERVER_ROOT.'?/product/newProduct/';
    $array['urlNewQuote']       = BFWK_SERVER_ROOT.'?/document/newDocument/quote/';
    $array['urlNewOrder']       = BFWK_SERVER_ROOT.'?/document/newDocument/order/';
    $array['urlNewInvoice']     = BFWK_SERVER_ROOT.'?/document/newDocument/invoice/';
    $array['urlNewPurchase']    = BFWK_SERVER_ROOT.'?/document/newPurchase/';
    $array['urlNewPayment']     = BFWK_SERVER_ROOT.'?/payment/newPayment/';
    $array['urlLoadCustomer']   = BFWK_SERVER_ROOT.'?/thirdParty/loadCustomer/';
    $array['urlLoadSupplier']   = BFWK_SERVER_ROOT.'?/thirdParty/loadSupplier/';
    $array['isAdmin']           = HLX_Motor::getInstance()->user_isAdmin();
    $array['active']            = $active ;

    return $array;

}

function currency_format($number, $decimals = 2, $dec_point = '.', $thousand_sep = '', $currency = false, $currency_placement = false) {

    if ($currency_placement == false || $currency == false)
        return number_format($number, $decimals, $dec_point, $thousand_sep);
    else if ($currency_placement == 'before')
        return $currency.' '.number_format($number, $decimals, $dec_point, $thousand_sep);
    else
        return number_format($number, $decimals, $dec_point, $thousand_sep).' '.$currency;

}