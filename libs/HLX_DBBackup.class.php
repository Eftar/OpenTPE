<?php

class HLX_DBBackup {

    private $type;
    private $dsn;
    private $user;
    private $pass;
    private $pdo;

    public $error = false;

    function __construct($dsn, $user='', $pass='') {

        if (substr($dsn, 0, 5) == 'mysql')
            $this->type = 'mysql';
        else
            $this->type = 'sqlite';

        $this->dsn  = $dsn;
        $this->user = $user;
        $this->pass = $pass;
        $opt = array(
           PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
           PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        );
        try{
            $this->pdo = new PDO($this->dsn, $user, $pass, $opt);
        }
        catch(PDOException $e){
             $this->error = $e->getMessage();
             $this->error.= 'There was a problem with connection to db check credenctials';
        }

    }

    public function backupTables($tables = '*', $filename = 'default') {

        $data = "";
        //get all of the tables
        if($tables == '*')
        {
            $tables = array();

            if ($this->type == 'mysql')
                $result = $this->pdo->prepare("SHOW TABLES");
            else
                $result = $this->pdo->prepare("SELECT name FROM sqlite_master WHERE type='table'");

            $result->execute();
            while($row = $result->fetch(PDO::FETCH_NUM))
            {
                $tables[] = $row[0];
            }
        }
        else
        {
            $tables = is_array($tables) ? $tables : explode(',',$tables);
        }
        //cycle through
        foreach($tables as $table)
        {

            $data.= 'DROP TABLE IF EXIST '.$table.';'."\n\n";

            if ($this->type == 'mysql')
                $resultCreate = $this->pdo->prepare("SHOW CREATE TABLE ".$table);
            else
                $resultCreate = $this->pdo->prepare("SELECT name, sql FROM sqlite_master WHERE name = '".$table."'");

            $resultCreate->execute();
            $row = $resultCreate->fetch(PDO::FETCH_NUM);
            $data.= "\n\n".$row[1].";\n\n";

            $result = $this->pdo->prepare("SELECT * FROM ".$table);
            $result->execute();

            while($row = $result->fetch(PDO::FETCH_NUM))
            {
                $data.= 'INSERT INTO '.$table.' VALUES(';
                $first = true;
                foreach($row as $val)
                {
                    if(is_null($val)) {
                        $data .= ($first==false?',':'').'NULL';
                    } else {
                        $data .= ($first==false?',':'').'\''. $this->insertClean($val) . '\'';
                    }
                    $first = false;
                }
                $data.= ");\n";
            }
            $data.="\n";
        }
        //save filename
        if ($filename == 'default')
            $filename = 'db-backup-'.time().'.sql';

        if ($f = fopen($filename, "w+")) {
            fwrite($f, $data);
            fclose($f);
            return true;
        }
        else
        {
            $this->error = 'Error writing file';
            return false;
        }

    }

    function insertClean($str) {

        static $s1 = array("\\", "'", "\r", "\n", "\t");
        static $s2 = array("\\\\", "''", '\r', '\n', '\t');

        return str_replace($s1, $s2, $str);

    }

}