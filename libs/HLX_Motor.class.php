<?php


/**
 * Centralisation of DB access and user action
 * */

class HLX_Motor {

    private static $instance;

    private $pdo                = false;

    private $isUserConnected    = false;

    /**
     * Private constructor
     * */
    private function __construct()
    {

        $this->initDatabase();

        //Check user session
        $this->user_initSession();//TODO use users Class instead !

    }

    /**
     * Return the current instance or create the first one
     * */
    public static function getInstance()
    {
        if ( ! isset(self::$instance))
        {
            $c = __CLASS__;
            self::$instance = new $c;
        }

        return self::$instance;
    }

    /**
     *
     * */
    private function initDatabase() {

        //Create DB connection
        $this->pdo = new PDO(HLX_DBDSN, HLX_DBUSER, HLX_DBPWD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET sql_mode="PIPES_AS_CONCAT"'));

        if (BFWK_MODE != 2)
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

        if ( ! $this->pdo)
        {
            echo t('ErrorConnexionBDD');
            return false;
        }

    }

    /**
     * Return the current instance or create the first one
     * */
    public function getDatabase()
    {
        return $this->pdo;
    }

    private function user_initSession()
    {

        ini_set('session.gc_maxlifetime', 3600);

        // each client should remember their session id for EXACTLY 1 hour
        session_set_cookie_params(3600);

        session_start();
        session_regenerate_id();

        //TODO see for adding test on IP / user agent / accepted language to avoid steal of session
        $this->isUserConnected = isset($_SESSION['HLX_User']);

    }

    /**
     *
     * */
    public function user_isConnected()
    {
        return $this->isUserConnected;
    }

    /**
     *
     * */
    public function user_isAdmin()
    {
        return ($this->user_getParam('Role') == 'admin' ? true : false);
    }

    public function user_login($email, $password)
    {

        $res = $this->pdo->query("SELECT * FROM hlx_users WHERE Active = 1 AND Email = ".$this->pdo->quote($email));
        if ( ! $res)
        {
            return false;
        }

        $row = $res->fetch(PDO::FETCH_ASSOC);

        if (isset($row['Email']) && $row['Email'] == $email && password_verify($password, $row['Password']))
        {
            //Clear password field to not store it in session
            $row['Password'] = '';
            //TODO see to add IP / user agent / accepted language for avoid steal of session
            $_SESSION['HLX_User'] = $row;
            return true;
        }

        return false;

    }

    public function user_getParam($key)
    {

        if (isset($_SESSION['HLX_User'][ $key ]))
            return $_SESSION['HLX_User'][ $key ];

        return false;

    }

    public function user_logout()
    {

        unset($_SESSION['HLX_User']);
        session_destroy();

    }

}
