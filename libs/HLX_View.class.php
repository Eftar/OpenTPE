<?php

/**
 * Class Utilities Availables in view
 * */

class HLX_View {

    /**
     * Display A repeater
     * */
    static function hlxRepeater($rOptions) {

        //TODO init all Options
        echo "<div class=\"fuelux\">

                <script type=\"text/javascript\">
                <!--

                    var ".$rOptions['id']." = {};
                    //Id de l'élément
                    ".$rOptions['id']."['id']           = '".$rOptions['id']."';
                    //Type par défaut
                    ".$rOptions['id']."['type']         = '".$rOptions['type']."';
                    //filter par défaut
                    ".$rOptions['id']."['filter']       = '".$rOptions['filter']."';
                    //Object contenant les descriptions des colonnes en fonction du type
                    ".$rOptions['id']."['arrColumns']   = ".json_encode($rOptions['arrColumns']).";
                    //Callback for rendering field
                    ".$rOptions['id']."['customColumnRenderer'] = function(helpers, callback) {
                        // determine what column is being rendered
                        var column = helpers.columnAttr;

                        // get all the data for the entire row
                        var rowData = helpers.rowData;
                        var customMarkup = '';
                    ";

        foreach($rOptions['arrColumnsRenderer'] as $kType => $columnsRenderer)
        {

            echo "if (".$rOptions['id']."['type'] == '".$kType."') {

                    switch (column) {
                        ";

            foreach($columnsRenderer as $column => $renderer)
            {

                echo "case '".$column."':
                        customMarkup = '".str_replace("\n", '', $renderer)."';
                        break;
                    ";
            }
            echo "default:
                        // otherwise, just use the existing text value
                        customMarkup = helpers.item.text();
                        break;
                    }

                }
                ";

        }

        echo "helpers.item.html(customMarkup);
                callback();
            }
                    //Datasource
                    ".$rOptions['id']."['dataSource'] = function(options, callback) {

                        // set options
                        var pageIndex   = options.pageIndex;
                        var pageSize    = options.pageSize;

                        // getting filter By
                        var options = {
                            pageIndex:      pageIndex,
                            pageSize:       pageSize,
                            sortDirection:  options.sortDirection,
                            sortBy:         options.sortProperty || '',
                            type:           ".$rOptions['id']."['type'],
                            filterBy:       ($('.repeater-filters:visible input[name=filterSelection]').val() ? $('.repeater-filters:visible input[name=filterSelection]').val() : ".$rOptions['id']."['filter']),
                            searchBy:       options.search || ''
                        };

                        //store in a cookie
                        createCookie('".$rOptions['id']."', JSON.stringify(options));

                        // Call API, posting options
                        $.ajax({
                            type:   'post',
                            url:    '".$rOptions['url']."',
                            data:   options
                        }).done(function(data) {

                            try {
                                var data        = JSON.parse(data),
                                    items       = data.items,
                                    totalItems  = data.total,
                                    totalPages  = Math.ceil(totalItems / pageSize),
                                    startIndex  = (pageIndex * pageSize) + 1,
                                    endIndex    = (startIndex + pageSize) - 1;

                                if (endIndex > items.length) {
                                    endIndex = items.length;
                                }

                                // configure datasource
                                var dataSource = {
                                    page:       pageIndex,
                                    pages:      totalPages,
                                    count:      totalItems,
                                    start:      startIndex,
                                    end:        endIndex,
                                    columns:    ".$rOptions['id']."['arrColumns'][ ".$rOptions['id']."['type'] ],
                                    items:      items
                                };

                                // Invoke callback to render repeater
                                callback(dataSource);

                            }catch(e) { }

                        }).error(function(data) {

                            alert(data);

                        });

                    };

                -->
                </script>
                <div>
                    <div class=\"repeater\" id=\"".$rOptions['id']."\" style=\"border-bottom:1px solid #DDD;\" >
                        <div class=\"repeater-header\">

                            <div class=\"repeater-header-left\">
                                ";

        if ( ! empty($rOptions['arrTypes']))
        {
            echo "<div class=\"pull-left hidden-xs\" style=\"margin-right:10px;\">
                ";
            foreach($rOptions['arrTypes'] as $key => $label)
            {
                echo "<button type=\"button\" class=\"btn ".($key == $rOptions['type'] ? 'btn-primary' : 'btn-default')." btn-sm hlx-filter\" value=\"".$key."\">
                            ".$label."
                        </button>
                    ";
            }
            echo "</div>
                    <div class=\"pull-left visible-xs-block\">
                        <div class=\"btn-group btn-sm repeater-filters\" style=\"padding:0\" data-resize=\"auto\">
                            <button type=\"button\" class=\"btn btn-sm btn-default dropdown-toggle\" data-toggle=\"dropdown\">
                                <span class=\"selected-label\">&nbsp;</span>
                                <span class=\"caret\"></span>
                                <span class=\"sr-only\">".t('ToggleType')."</span>
                            </button>
                            <ul class=\"dropdown-menu\" role=\"menu\">
                ";
            foreach($rOptions['arrTypes'] as $key => $label)
            {
                echo "<li data-value=\"".$key."\" ".($key == $rOptions['type'] ? ' data-selected="true" ' : '')."><a href=\"#\">".$label."</a></li>
                ";
            }
            echo "</ul>
                        </div>
                    </div>
                ";
        }

        echo "<div class=\"pull-left repeater-search\">
                    <div class=\"search input-group input-group-sm\">
                        <input type=\"search\" class=\"form-control control-sm\" placeholder=\"".t('Search')."\"/>
                        <span class=\"input-group-btn input-group-btn-sm\">
                            <button class=\"btn btn-default btn-sm\" type=\"button\">
                                <span class=\"glyphicon glyphicon-search\"></span>
                                <span class=\"sr-only\">".t('Search')."</span>
                            </button>
                        </span>
                    </div>
                </div>

            </div>
                ";

        if ( ! empty($rOptions['arrActions']) || ! empty($rOptions['arrFilters']))
        {
            echo "<div class=\"repeater-header-right\">
                ";

            //Use of multi filters for type
            if ( ! empty($rOptions['arrFilters']))
            {
                foreach ($rOptions['arrFilters'] as $kType => $filters)
                {
                    echo "<div class=\"btn-group btn-sm repeater-filters div-filters\" id=\"filters-".$kType."\" style=\"padding:0;".($kType != $rOptions['type'] ? 'display:none;' : '')."\" data-resize=\"auto\">
                            <button type=\"button\" class=\"btn btn-sm btn-default dropdown-toggle\" data-toggle=\"dropdown\">
                                <span class=\"selected-label\">&nbsp;</span>
                                <span class=\"caret\"></span>
                                <span class=\"sr-only\">".t('ToggleFilters')."</span>
                            </button>
                            <ul class=\"dropdown-menu\" role=\"menu\">
                                                ";
                    foreach($filters as $filterSetting)
                    {
                        echo "<li data-value=\"".$filterSetting['value']."\" ".($filterSetting['value'] == $rOptions['filter'] ? 'data-selected="true"' : '')." ><a href=\"#\">".$filterSetting['label']."</a></li>
                            ";
                    }
                    echo "</ul>
                                <input class=\"hidden hidden-field\" name=\"".($kType == $rOptions['type'] ? 'filterSelection' : 'deactivated')."\" readonly=\"readonly\" aria-hidden=\"true\" type=\"text\"/>
                            </div>
                            ";
                }

            }

            if ( count($rOptions['arrActions']) == 1)
            {
                echo "<a role=\"button\" class=\"btn btn-primary btn-sm\"  href=\"".$rOptions['arrActions'][0]['url']."\">
                            <span class=\"glyphicon glyphicon-plus\"></span>".$rOptions['arrActions'][0]['label']."
                        </a>
                    ";
            }
            elseif ( ! empty($rOptions['arrActions']))
            {
                echo "<div class=\"btn-group\" style=\"padding:0\">
                        <button type=\"button\" class=\"btn btn-primary btn-sm dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                            <span class=\"glyphicon ".(isset($rOptions['actionSettings']['icon']) ? 'glyphicon-'.$rOptions['actionSettings']['icon'] : '')."\"></span>
                            ".(isset($rOptions['actionSettings']['label']) ? $rOptions['actionSettings']['label'] : t('Actions'))." <span class=\"caret\"></span>
                        </button>
                        <ul class=\"dropdown-menu dropdown-menu-right\">
                    ";
                foreach($rOptions['arrActions'] as $action)
                {
                    echo "<li><a href=\"".$action['url']."\">".$action['label']."</a></li>
                        ";
                }
                echo "</ul>
                    </div>
                    ";
            }
            echo "</div>
                ";
        }

        echo "</div>

                    <div class=\"repeater-viewport\">
                        <div class=\"repeater-canvas\"></div>
                        <div class=\"loader repeater-loader\"></div>
                    </div>

                    <div class=\"repeater-footer\">
                        <div class=\"repeater-footer-left form-inline\">
                            <div class=\"repeater-itemization\">
                                <div style=\"padding-top:8px\"><span class=\"repeater-start\"></span> - <span class=\"repeater-end\"></span> ".t('of')." <span class=\"repeater-count\"></span> ".t('items').".</div>
                            </div>
                        </div>
                        <div class=\"repeater-footer-right\">
                            <div class=\"repeater-pagination form-inline\">
                                <button type=\"button\" class=\"btn btn-default btn-sm repeater-prev\">
                                    <span class=\"glyphicon glyphicon-chevron-left\"></span>
                                    <span class=\"sr-only\">".t('PreviousPage')."</span>
                                </button>
                                <label class=\"page-label\">".t('Page')."</label>
                                <div class=\"repeater-primaryPaging active\">
                                    <div class=\"input-group input-append dropdown combobox\">
                                        <input type=\"text\" class=\"form-control input-sm\">
                                    </div>
                                </div>
                                <input type=\"text\" class=\"form-control repeater-secondaryPaging\">
                                <span>".t('of')." <span class=\"repeater-pages\"></span></span>
                                <button type=\"button\" class=\"btn btn-default btn-sm repeater-next\">
                                    <span class=\"glyphicon glyphicon-chevron-right\"></span>
                                    <span class=\"sr-only\">".t('NextPage')."</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script type=\"text/javascript\">
        <!--
        ";
        if ( ! isset($rOptions['noresize'])) {

            echo "if (typeof(onResize) == 'undefined') {

                //function onResize
                var onResize = function() {
                    if (typeof($('.repeater:visible').offset()) != 'undefined')
                    {
                        $('.repeater:visible').attr('data-staticheight', jQuery(window).height() - $('.repeater:visible').offset().top - 1);
                        $('.repeater:visible').repeater('resize');
                    }
                }

                //TODO recharger la liste entière && utiliser le modèle de présentation en fonction de l'affichage

                //Add event onResize
                $(window).resize(onResize);

            }
            ";
        }
        echo "if (typeof(hlxRepeater) == 'undefined') {

                var hlxRepeater = function(id) {

                    if ($('#'+id+' thead tr th').length > 0)
                    {
                        $('#'+id).repeater('render');
                    }
                    else
                    {
                        repeater = $('#'+id).repeater({
                            dataSource: window[ id ]['dataSource'],
                            preserveDataSourceOptions: true,
                            list_columnRendered: window[ id ]['customColumnRenderer'],
                            ".( ! isset($rOptions['noresize']) ? "list_rowRendered: function(helpers, callback) { onResize(); callback();}," : "")."
                            staticHeight: ".( isset($rOptions['height']) ? $rOptions['height'] : "-1").",
                            list_noItemsHTML: '".t('NoItemsFound')."'
                        });
                    }
                    ".( ! isset($rOptions['noresize']) ? "onResize();" : "")."
                }
            }
            ";

        if ( ! isset($rOptions['noloading']))
        {
            echo "hlxRepeater('".$rOptions['id']."');
            ";
        }

        //Add action on filter click //TODO testé si déjà référencé avant de redéclarer
        echo "$('#'+".$rOptions['id']."['id']+' .hlx-filter').on('click', function() {

                $('#'+".$rOptions['id']."['id']+' .hlx-filter.btn-primary').removeClass('btn-primary').addClass('btn-default');
                $(this).removeClass('btn-default').addClass('btn-primary');//Todo set disable / Todo Set element inverse XS || Other reflet the same selection
                ".$rOptions['id']."['type'] = $(this).val();
                //Display filter acording to type
                $('#'+".$rOptions['id']."['id']+' .div-filters').hide();
                $('#'+".$rOptions['id']."['id']+' .div-filters > input').attr('name', \"deactivated\");
                $('#'+".$rOptions['id']."['id']+' #filters-'+$(this).val()).show();
                $('#'+".$rOptions['id']."['id']+' #filters-'+$(this).val()+' > input').attr('name', \"filterSelection\");
                //Render
                $('#'+".$rOptions['id']."['id']).repeater('render');
            });
            ";
        echo "
        -->
        </script>
        ";

    }

    /**
     * Check $argument for developping purpose
     * */
    static function hlxRepeaterDebug($rOptions) {
        //TODO Write hlxRepeaterDebug function
    }

    //TODO Write some notifications function store && display

}
