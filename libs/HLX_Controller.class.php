<?php

/**
 * Override bfwk Controller to include HLX configuration and libraries
 * */

class HLX_Controller extends Controller {

    /**
     * Disable auto language detection
     * */
    protected $m_autoLang = false;

    /**
     * Store General setting locally
     * */
    protected $settings = array();

    /**
     * Link to HLX Motor class
     * */
    public $hlx_motor;

    function __construct() {

        //Load custom config
        if (is_file('config/hlx.conf.php'))
            require('config/hlx.conf.php');

        /**
         * Init Default Settings
         * */
        //DSN for database
        if ( ! defined('HLX_DBDSN'))
            define('HLX_DBDSN', '');

        //User for database
        if ( ! defined('HLX_DBUSER'))
            define('HLX_DBUSER', '');

        //Pass for database
        if ( ! defined('HLX_DBPWD'))
            define('HLX_DBPWD', '');

        //Path Data
        if ( ! defined('HLX_STORE'))
            define('HLX_STORE', 'store/');

        //Canal of Update ('beta', 'rc', 'stable')
        if ( ! defined('HLX_CANAL'))
            define('HLX_CANAL', 'stable');

        //URL For Update
        if ( ! defined('HLX_URL_UPDATE'))
            define('HLX_URL_UPDATE', 'https://www.opentpe.fr/releases/');

        //Current Version
        define('HLX_VERSION', '0.9.7-beta2');

        //HLX App Name
        define('HLX_NAME', 'OpenTPE');

        parent::__construct();

        /**
         * Include somes common functions
         * */
        require(BFWK_LIB_PATH.'HLX_Lib.functions.php');

        //No configuration ? Go to setup
        if (HLX_DBDSN == '' || get_class($this) == 'Setup')
        {
            if (get_class($this) != 'Setup')
            {
                header('Location: '.BFWK_SERVER_ROOT.'?/setup/index');
            }
        }
        else
        {
            $this->hlx_motor = HLX_Motor::getInstance();

            require_once(BFWK_LIB_PATH.'HLX_View.class.php');

            require_once('models/settingModel.php');
            $this->settings = SettingModel::load(array('GENERAL'));

            if ($this->settings['GENERAL']['Language'] != '')
                load_language($this->settings['GENERAL']['Language']);
            else
                load_language();
        }

    }

}
