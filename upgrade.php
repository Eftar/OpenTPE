<?php

$hlx_motor = HLX_Motor::getInstance();
$db = HLX_Motor::getInstance()->getDatabase();

if ( ! $db)
{
    return false;
}

//From update.php
$version = $version;
$curVersion = HLX_VERSION;
$rLog[] = 'Database upgrade';


/*******************
 * UPDATE FOR 0.9.4
 * *****************/
if ($curVersion < '0.9.4')
{
    $rLog[] = 'SQL Update for version 0.9.4';
    $rLog[] = 'Creating table Notes';
    //sql stuff for sqlite or mysql
    if ($db->getAttribute(PDO::ATTR_DRIVER_NAME) === 'sqlite') {

        //create table Note
        $res = $db->query("CREATE TABLE IF NOT EXISTS `hlx_notes` (
                              `ID` integer NOT NULL PRIMARY KEY AUTOINCREMENT
                            , `Name` varchar(256) NOT NULL DEFAULT ''
                            , `Tags` varchar(256) NOT NULL DEFAULT ''
                            , `Description` text NOT NULL DEFAULT ''
                            )");
        if ( ! $res)
            return false;

    }
    else
    {
        //create table Note
        $res = $db->query("CREATE TABLE IF NOT EXISTS `hlx_notes` (
                              `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
                              `Name` varchar(256) NOT NULL,
                              `Tags` varchar(256) NOT NULL,
                              `Description` text NOT NULL,
                              PRIMARY KEY (`ID`)
                            ) ENGINE=MyISAM  DEFAULT CHARSET=utf8");
        if ( ! $res)
            return false;
    }

    $rLog[] = 'Updating invoice address of document type 5 (purchase)';
    //UPDATE invoice Address in document type 5
    $sql = "UPDATE hlx_documents AS docs
            SET
                InvoiceName         = (SELECT thirds.Name FROM hlx_thirdparties AS thirds WHERE docs.ThirdPartyID = thirds.ID),
                InvoiceFirstname    = (SELECT thirds.Firstname FROM hlx_thirdparties AS thirds WHERE docs.ThirdPartyID = thirds.ID),
                InvoiceAddress1     = (SELECT thirds.Address1 FROM hlx_thirdparties AS thirds WHERE docs.ThirdPartyID = thirds.ID),
                InvoiceAddress2     = (SELECT thirds.Address2 FROM hlx_thirdparties AS thirds WHERE docs.ThirdPartyID = thirds.ID),
                InvoiceAddress3     = (SELECT thirds.Address3 FROM hlx_thirdparties AS thirds WHERE docs.ThirdPartyID = thirds.ID),
                InvoicePostalCode   = (SELECT thirds.PostalCode FROM hlx_thirdparties AS thirds WHERE docs.ThirdPartyID = thirds.ID),
                InvoiceCity         = (SELECT thirds.City FROM hlx_thirdparties AS thirds WHERE docs.ThirdPartyID = thirds.ID),
                InvoiceState        = (SELECT thirds.State FROM hlx_thirdparties AS thirds WHERE docs.ThirdPartyID = thirds.ID),
                InvoiceCountryID    = (SELECT thirds.CountryID FROM hlx_thirdparties AS thirds WHERE docs.ThirdPartyID = thirds.ID)
            WHERE docs.DocTypeID = 5";
    $res = $db->query($sql);
    if ( ! $res)
        return false;

}

if ($curVersion < '0.9.7')
{
    $rLog[] = 'SQL Update for version 0.9.7';

    $rLog[] = 'Add field \'IsDeposit\' in hlx_documents';
    if ($db->getAttribute(PDO::ATTR_DRIVER_NAME) === 'sqlite') {
        $sql = "ALTER TABLE `hlx_documents` ADD `IsDeposit` INTEGER NOT NULL DEFAULT '0';";
    }
    else {
        $sql = "ALTER TABLE `hlx_documents` ADD `IsDeposit` INT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `Received`;";
    }
    $res = $db->query($sql);
    if ( ! $res)
        return false;

    $rLog[] = 'Remove document status for deposit';
    $sql = "DELETE FROM hlx_documents_statutes WHERE DocTypeID = 3";
    $res = $db->query($sql);
    if ( ! $res)
        return false;

    $rLog[] = 'Remove settings for deposit';
    $sql = "DELETE FROM hlx_settings WHERE Name IN ('DepositTemplate', 'DepositTaxDefault', 'DepositReferenceFormat')";
    $res = $db->query($sql);
    if ( ! $res)
        return false;

    $rLog[] = 'Add settings for deposit default unity';
    $sql = "INSERT INTO hlx_settings VALUES (NULL, 'GENERAL', 'DepositDefaultUnity', '', '0000-00-00', '0000-00-00')";
    $res = $db->query($sql);
    if ( ! $res)
        return false;

    $rLog[] = 'Rewrite hlx.conf.php for 0.9.7';
    $confFile = "<?php

/**
 * DSN for database
 */
define('HLX_DBDSN', '".addslashes(HLX_DBDSN)."');

/**
 * User for database
 */
define('HLX_DBUSER', '".addslashes(HLX_DBUSER)."');

/**
 * Pass for database
 */
define('HLX_DBPWD', '".addslashes(HLX_DBPWD)."');";
    if ( ! file_put_contents('config/hlx.conf.php', $confFile))
        return false;
}

$rLog[] = 'Database upgraded';