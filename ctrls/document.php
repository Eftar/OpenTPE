<?php

/**
 *
 * */

class Document extends HLX_Controller
{

    function index($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();

        if (isset($_SESSION['msg']) && $_SESSION['msg'] != '')//TODO find another way to carry message
        {
            $data['msg'] = htmlspecialchars(t($_SESSION['msg']));
            unset($_SESSION['msg']);
        }
        if (isset($_SESSION['err']) && $_SESSION['err'] != '')
        {
            $data['err'] = htmlspecialchars(t($_SESSION['err']));
            unset($_SESSION['err']);
        }

        //Options for navBar
        $data['navBar'] = navBarVars('document');

        require_once('models/documentModel.php');
        $rTypesRaw = DocumentModel::getDocumentTypes('all');
        $data['rTypes'] = array_map('t', $rTypesRaw);

        require_once('models/documentStatusModel.php');
        $data['quoteStatutes']      = DocumentStatusModel::loadAll(1);
        $data['orderStatutes']      = DocumentStatusModel::loadAll(2);
        $data['invoiceStatutes']    = DocumentStatusModel::loadAll(4);
        $data['purchaseStatutes']   = DocumentStatusModel::loadAll(5);

        //Getting default configuration for list
        $data['listOptionType']     = DocumentModel::DOC_TYPE_QUOTE;
        $data['listOptionFilter']   = 'all';

        if (isset($_COOKIE['documentList'])) {
            $options = json_decode($_COOKIE['documentList'], true);
            if ($options)
            {
                $data['listOptionType']     = $options['type'];
                $data['listOptionFilter']   = htmlspecialchars($options['filterBy']);
            }
        }

        if (isset($args[0]) && $args[0] != '') {
            $data['listOptionType'] = array_search($args[0], $rTypesRaw);

            if (isset($args[1]) && $args[1] != '')
                $data['listOptionFilter'] = htmlspecialchars($args[1]);
        }

        if ( ! in_array($data['listOptionType'], array_keys($rTypesRaw)))
            $data['listOptionType'] = key($rTypesRaw);

        require_once('models/settingModel.php');
        $data['settings'] = SettingModel::load(array('GENERAL'));

        $data['urlLoadList']            = BFWK_SERVER_ROOT.'?/document/loadList/';
        $data['urlEditQuote']           = BFWK_SERVER_ROOT.'?/document/edit/quote/';
        $data['urlPDFQuote']            = BFWK_SERVER_ROOT.'?/document/viewPdf/quote/';
        $data['urlSendQuote']           = BFWK_SERVER_ROOT.'?/document/send/quote/';
        $data['urlDeleteQuote']         = BFWK_SERVER_ROOT.'?/document/delete/quote/';

        $data['urlEditOrder']           = BFWK_SERVER_ROOT.'?/document/edit/order/';
        $data['urlPDFOrder']            = BFWK_SERVER_ROOT.'?/document/viewPdf/order/';
        $data['urlSendOrder']           = BFWK_SERVER_ROOT.'?/document/send/order/';

        $data['urlEditInvoice']         = BFWK_SERVER_ROOT.'?/document/edit/invoice/';
        $data['urlPDFInvoice']          = BFWK_SERVER_ROOT.'?/document/viewPdf/invoice/';

        $data['urlEditPurchase']        = BFWK_SERVER_ROOT.'?/document/editPurchase/';
        $data['urlPDFPurchase']         = BFWK_SERVER_ROOT.'?/document/viewPdf/purchase/';
        $data['urlReceive']             = BFWK_SERVER_ROOT.'?/document/receive/';

        $data['urlNewPayment']          = BFWK_SERVER_ROOT.'?/payment/newPayment/document/';
        $data['urlSendInvoice']         = BFWK_SERVER_ROOT.'?/document/send/invoice/';
        $data['urlViewCustomer']        = BFWK_SERVER_ROOT.'?/thirdParty/viewCustomer/';
        $data['urlViewSupplier']        = BFWK_SERVER_ROOT.'?/thirdParty/viewSupplier/';

        $data['token'] = '';

        return $data;

    }

    /**
     * Ajax call for main List
     * */
    function loadList()
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        //TODO Add load with ID ThirdParty for Customer's documents tab
        //En fonction de Post récupère les données
        $args = $_POST;

        require_once('models/documentModel.php');
        echo json_encode(DocumentModel::getList($args));

        exit;

    }

    function edit($args)
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        require_once('models/documentModel.php');
        $rDocTypes = DocumentModel::getDocumentTypes();

        if ( ! isset($args[0]) || ! in_array($args[0], $rDocTypes))
        {
            //TODO error
        }
        else
            $type = $args[0] ;

        if ( ! isset($args[1]))
        {
            //TODO error
        }
        else
            $id = (int) $args[1];

        $data = array();
        //Options for navBar
        $data['navBar'] = navBarVars('document');

        $data['type'] = $type;
        $data['new'] = false;

        $data['item'] = DocumentModel::load($id);

        require_once('models/settingModel.php');
        $data['settings']  = SettingModel::load(array('GENERAL', 'STATE', 'COUNTRY', 'UNITY'), $data['item']['DateCreated']);

        if ($data['settings']['GENERAL']['UseVAT'] == 1)
        {
            require_once('models/taxModel.php');
            $data['taxes'] = TaxModel::loadAll($data['item']['DateCreated']);
        }
        else
            $data['taxes'] = array();

        require_once('models/documentStatusModel.php');
        $data['docStatutes'] = DocumentStatusModel::loadAll(array_search($type, $rDocTypes));

        require_once('libs/HLX_Country.class.php');
        $data['countries'] = HLX_Country::getCountryList();

        $data['urlSave']                = BFWK_SERVER_ROOT.'?/document/save/';
        $data['urlSendMail']            = BFWK_SERVER_ROOT.'?/document/sendMail/'.$id;
        $data['urlViewPDF']             = BFWK_SERVER_ROOT.'?/document/viewPDF/'.$type.'/'.$id;
        $data['urlDeleteQuote']         = BFWK_SERVER_ROOT.'?/document/deleteQuote/'.$id;
        $data['urlQuoteToOrder']        = BFWK_SERVER_ROOT.'?/document/quoteToOrder/'.$id;
        $data['urlOrderToInvoice']      = BFWK_SERVER_ROOT.'?/document/orderToInvoice/'.$id;
        $data['urlCopyDocument']        = BFWK_SERVER_ROOT.'?/document/copyDocument/'.$id;
        $data['urlCreateDeposit']       = BFWK_SERVER_ROOT.'?/document/createDeposit/'.$id.'/';
        $data['urlCreateCreditNote']    = BFWK_SERVER_ROOT.'?/document/createCreditNote/'.$id.'/';
        $data['urlEditDocument']        = BFWK_SERVER_ROOT.'?/document/edit/';
        $data['urlNewPayment']          = BFWK_SERVER_ROOT.'?/payment/newPayment/document/'.$id;
        $data['urlViewPayment']         = BFWK_SERVER_ROOT.'?/payment/viewPayment/';
        $data['urlViewCustomer']        = BFWK_SERVER_ROOT.'?/thirdParty/viewCustomer/';
        $data['urlEditCustomer']        = BFWK_SERVER_ROOT.'?/thirdParty/editCustomer/';

        //List product
        $data['urlLoadList']        = BFWK_SERVER_ROOT.'?/product/loadList/';
        $data['urlNewProduct']      = BFWK_SERVER_ROOT.'?/product/newProduct/';
        $data['urlViewProduct']     = BFWK_SERVER_ROOT.'?/product/viewProduct/';
        $data['urlEditProduct']     = BFWK_SERVER_ROOT.'?/product/editProduct/';
        $data['urlDeleteProduct']   = BFWK_SERVER_ROOT.'?/product/deleteProduct/';

        return $data;
    }

    function editPurchase($args)
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        if ( ! isset($args[0]))
        {
            //TODO error
        }
        else
            $id = (int) $args[0];

        $data = array();
        //Options for navBar
        $data['navBar'] = navBarVars('document');

        require_once('models/documentModel.php');
        $data['item'] = DocumentModel::load($id);
        $data['type'] = DocumentModel::DOC_TYPE_PURCHASE;
        $data['new'] = false;

        require_once('models/settingModel.php');
        $data['settings']  = SettingModel::load(array('GENERAL', 'STATE', 'COUNTRY', 'UNITY'), $data['item']['DateCreated']);

        if ($data['settings']['GENERAL']['UseVAT'] == 1)
        {
            require_once('models/taxModel.php');
            $data['taxes'] = TaxModel::loadAll($data['item']['DateCreated']);
        }
        else
            $data['taxes'] = array();

        require_once('models/documentStatusModel.php');
        $data['docStatutes'] = DocumentStatusModel::loadAll(5);

        require_once('libs/HLX_Country.class.php');

        $data['urlSave']            = BFWK_SERVER_ROOT.'?/document/save/';
        $data['urlSendMail']        = BFWK_SERVER_ROOT.'?/document/sendMail/'.$id;
        $data['urlViewPDF']         = BFWK_SERVER_ROOT.'?/document/viewPDF/purchase/'.$id;
        $data['urlDeleteQuote']     = BFWK_SERVER_ROOT.'?/document/deletePurchase/'.$id;
        $data['urlEditDocument']    = BFWK_SERVER_ROOT.'?/document/editPurchase/';
        $data['urlViewSupplier']    = BFWK_SERVER_ROOT.'?/thirdParty/viewSupplier/';
        $data['urlEditSupplier']    = BFWK_SERVER_ROOT.'?/thirdParty/editSupplier/';
        $data['urlNewPayment']      = BFWK_SERVER_ROOT.'?/payment/newPayment/document/'.$id;
        $data['urlReceive']         = BFWK_SERVER_ROOT.'?/document/receive/'.$id;

        //List product
        $data['urlLoadList']        = BFWK_SERVER_ROOT.'?/product/loadList/';
        $data['urlNewProduct']      = BFWK_SERVER_ROOT.'?/product/newProduct/';
        $data['urlViewProduct']     = BFWK_SERVER_ROOT.'?/product/viewProduct/';
        $data['urlEditProduct']     = BFWK_SERVER_ROOT.'?/product/editProduct/';
        $data['urlDeleteProduct']   = BFWK_SERVER_ROOT.'?/product/deleteProduct/';

        return $data;
    }

    function newPurchase($args)
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        require_once('models/documentModel.php');
        $rDocTypes = DocumentModel::getDocumentTypes('purchase');

        $data = array();
        $data['navBar'] = navBarVars('document');

        //ThirdPartyID
        if ( ! isset($args[0]))
        {
            $data['error'] = t('EmptyThirdpartyID');
            $this->_display('error_page', $data);
            die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
        }


        $data['type'] = DocumentModel::DOC_TYPE_PURCHASE;
        $data['new'] = true;
        $thirdPartyID = (int) $args[0];
        $docTypeID = $data['type'];

        require_once('models/thirdPartyModel.php');
        $customer = ThirdPartyModel::load($thirdPartyID);

        if ( ! $customer)
        {
            $data['error'] = t('ErrorLoadingCustomer');
            $this->_display('error_page', $data);
            die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
        }

        //For document
        $id = 0;

        require_once('models/documentModel.php');
        $data['item'] = documentModel::load($id);

        require_once('models/settingModel.php');
        $data['settings']  = SettingModel::load(array('GENERAL', 'PAYMENT', 'STATE', 'UNITY'));

        if ($data['settings']['GENERAL']['UseVAT'] == 1)
        {
            require_once('models/taxModel.php');
            $data['taxes'] = TaxModel::loadAll();
        }
        else
            $data['taxes'] = array();

        require_once('libs/HLX_Country.class.php');
        $data['countries'] = HLX_Country::getCountryList();

        require_once('models/documentStatusModel.php');
        $data['docStatutes'] = DocumentStatusModel::loadAll(5);

        require_once('libs/HLX_Country.class.php');

        //Add customer information
        $data['item']['ThirdPartyID']           = $customer['ID'];
        $data['item']['ThirdPartyName']         = $customer['Name'];
        $data['item']['ThirdPartyFirstname']    = $customer['Firstname'];
        $data['item']['ThirdPartyAddress1']     = $customer['Address1'];
        $data['item']['ThirdPartyAddress2']     = $customer['Address2'];
        $data['item']['ThirdPartyAddress3']     = $customer['Address3'];
        $data['item']['ThirdPartyPostalCode']   = $customer['PostalCode'];
        $data['item']['ThirdPartyCity']         = $customer['City'];
        $data['item']['ThirdPartyState']        = $customer['State'];
        $data['item']['ThirdPartyCountryID']    = $customer['CountryID'];
        $data['item']['ThirdPartyPhone']        = $customer['Phone'];
        $data['item']['ThirdPartyMobile']       = $customer['Mobile'];
        $data['item']['ThirdPartyFax']          = $customer['Fax'];
        $data['item']['ThirdPartyEmail']        = $customer['Email'];
        $data['item']['InvoiceName']            = $customer['Name'];
        $data['item']['InvoiceFirstname']       = $customer['Firstname'];
        $data['item']['InvoiceAddress1']        = $customer['Address1'];
        $data['item']['InvoiceAddress2']        = $customer['Address2'];
        $data['item']['InvoiceAddress3']        = $customer['Address3'];
        $data['item']['InvoicePostalCode']      = $customer['PostalCode'];
        $data['item']['InvoiceCity']            = $customer['City'];
        $data['item']['InvoiceState']           = $customer['State'];
        $data['item']['InvoiceCountryID']       = $customer['CountryID'];
        $data['item']['UserID']                 = HLX_Motor::getInstance()->user_getParam('ID');
        $data['item']['UserName']               = HLX_Motor::getInstance()->user_getParam('Name');
        $data['item']['DocTypeID']              = $docTypeID;
        $data['item']['DateCreated']            = date('Y-m-d');

        $data['item']['DateDue']        = date('Y-m-d', mktime(0,0,0,date('n'),date('d')+$data['settings']['GENERAL']['InvoiceDueDelay'],date('Y')));
        $data['item']['DocStatusID']    = $data['settings']['GENERAL']['InvoiceDefaultStatus'];
        $data['item']['TaxID']          = $data['settings']['GENERAL']['InvoiceTaxDefault'];

        $data['urlSave']            = BFWK_SERVER_ROOT.'?/document/save/';
        $data['urlViewCustomer']    = BFWK_SERVER_ROOT.'?/thirdParty/viewCustomer/';//TODO replace by thirdParty
        $data['urlEditSupplier']    = BFWK_SERVER_ROOT.'?/thirdParty/editSupplier/';
        $data['urlEditDocument']    = BFWK_SERVER_ROOT.'?/document/editPurchase/';

        //List product
        $data['urlLoadList']        = BFWK_SERVER_ROOT.'?/product/loadList/';
        $data['urlNewProduct']      = BFWK_SERVER_ROOT.'?/product/newProduct/';
        $data['urlViewProduct']     = BFWK_SERVER_ROOT.'?/product/viewProduct/';
        $data['urlEditProduct']     = BFWK_SERVER_ROOT.'?/product/editProduct/';
        $data['urlDeleteProduct']   = BFWK_SERVER_ROOT.'?/product/deleteProduct/';

        $this->_display('document_editPurchase', $data);
        die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
    }

    function newDocument($args)
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        require_once('models/documentModel.php');
        $rDocTypes = DocumentModel::getDocumentTypes();

        $data = array();
        $data['navBar'] = navBarVars('document');

        //typeName
        if ( ! isset($args[0]) || ! in_array($args[0], $rDocTypes))
        {
            $data['error'] = t('EmptyTypeDocument');
            $this->_display('error_page', $data);
            die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
        }
        //ThirdPartyID
        if ( ! isset($args[1]))
        {
            $data['error'] = t('EmptyThirdpartyID');
            $this->_display('error_page', $data);
            die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
        }


        $data['type'] = $args[0];
        $data['new'] = true;
        $thirdPartyID = (int) $args[1];
        $docTypeID = array_search($data['type'], $rDocTypes);

        require_once('models/thirdPartyModel.php');
        $customer = ThirdPartyModel::load($thirdPartyID);

        if ( ! $customer)
        {
            $data['error'] = t('ErrorLoadingCustomer');
            $this->_display('error_page', $data);
            die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
        }

        //For document
        $id = 0;

        require_once('models/documentModel.php');
        $data['item'] = documentModel::load($id);

        require_once('models/settingModel.php');
        $data['settings']  = SettingModel::load(array('GENERAL', 'PAYMENT', 'STATE', 'UNITY'));

        if ($data['settings']['GENERAL']['UseVAT'] == 1)
        {
            require_once('models/taxModel.php');
            $data['taxes'] = TaxModel::loadAll();
        }
        else
            $data['taxes'] = array();

        require_once('libs/HLX_Country.class.php');
        $data['countries'] = HLX_Country::getCountryList();

        require_once('models/documentStatusModel.php');
        $data['docStatutes'] = DocumentStatusModel::loadAll($docTypeID);

        //Add customer information
        $data['item']['ThirdPartyID']           = $customer['ID'];
        $data['item']['ThirdPartyName']         = $customer['Name'];
        $data['item']['ThirdPartyFirstname']    = $customer['Firstname'];
        $data['item']['ThirdPartyAddress1']     = $customer['Address1'];
        $data['item']['ThirdPartyAddress2']     = $customer['Address2'];
        $data['item']['ThirdPartyAddress3']     = $customer['Address3'];
        $data['item']['ThirdPartyPostalCode']   = $customer['PostalCode'];
        $data['item']['ThirdPartyCity']         = $customer['City'];
        $data['item']['ThirdPartyState']        = $customer['State'];
        $data['item']['ThirdPartyCountryID']    = $customer['CountryID'];
        $data['item']['ThirdPartyPhone']        = $customer['Phone'];
        $data['item']['ThirdPartyMobile']       = $customer['Mobile'];
        $data['item']['ThirdPartyFax']          = $customer['Fax'];
        $data['item']['ThirdPartyEmail']        = $customer['Email'];
        $data['item']['InvoiceName']            = $customer['Name'];
        $data['item']['InvoiceFirstname']       = $customer['Firstname'];
        $data['item']['InvoiceAddress1']        = $customer['Address1'];
        $data['item']['InvoiceAddress2']        = $customer['Address2'];
        $data['item']['InvoiceAddress3']        = $customer['Address3'];
        $data['item']['InvoicePostalCode']      = $customer['PostalCode'];
        $data['item']['InvoiceCity']            = $customer['City'];
        $data['item']['InvoiceState']           = $customer['State'];
        $data['item']['InvoiceCountryID']       = $customer['CountryID'];
        $data['item']['DeliveryName']           = $customer['Name'];
        $data['item']['DeliveryFirstname']      = $customer['Firstname'];
        $data['item']['DeliveryAddress1']       = $customer['Address1'];
        $data['item']['DeliveryAddress2']       = $customer['Address2'];
        $data['item']['DeliveryAddress3']       = $customer['Address3'];
        $data['item']['DeliveryPostalCode']     = $customer['PostalCode'];
        $data['item']['DeliveryCity']           = $customer['City'];
        $data['item']['DeliveryState']          = $customer['State'];
        $data['item']['DeliveryCountryID']      = $customer['CountryID'];
        $data['item']['UserID']                 = HLX_Motor::getInstance()->user_getParam('ID');
        $data['item']['UserName']               = HLX_Motor::getInstance()->user_getParam('Name');
        $data['item']['DocTypeID']              = $docTypeID;
        $data['item']['DateCreated']            = date('Y-m-d');
        if ($docTypeID == DocumentModel::DOC_TYPE_QUOTE)
        {
            $data['item']['DateDue']        = date('Y-m-d', mktime(0,0,0,date('n'),date('d')+$data['settings']['GENERAL']['QuoteExpirationDelay'],date('Y')));
            $data['item']['DocStatusID']    = $data['settings']['GENERAL']['QuoteDefaultStatus'];
            $data['item']['TaxID']          = $data['settings']['GENERAL']['QuoteTaxDefault'];
        }
        elseif ($docTypeID == DocumentModel::DOC_TYPE_ORDER)
        {
            $data['item']['DateDue']        = date('Y-m-d', mktime(0,0,0,date('n'),date('d')+$data['settings']['GENERAL']['OrderDueDelay'],date('Y')));
            $data['item']['DocStatusID']    = $data['settings']['GENERAL']['OrderDefaultStatus'];
            $data['item']['TaxID']          = $data['settings']['GENERAL']['OrderTaxDefault'];
        }
        elseif ($docTypeID == DocumentModel::DOC_TYPE_INVOICE)
        {
            $data['item']['DateDue']        = date('Y-m-d', mktime(0,0,0,date('n'),date('d')+$data['settings']['GENERAL']['InvoiceDueDelay'],date('Y')));
            $data['item']['DocStatusID']    = $data['settings']['GENERAL']['InvoiceDefaultStatus'];
            $data['item']['TaxID']          = $data['settings']['GENERAL']['InvoiceTaxDefault'];
        }

        $data['urlSave']            = BFWK_SERVER_ROOT.'?/document/save/';
        $data['urlViewCustomer']    = BFWK_SERVER_ROOT.'?/thirdParty/viewCustomer/';
        $data['urlEditCustomer']    = BFWK_SERVER_ROOT.'?/thirdParty/editCustomer/';
        $data['urlEditDocument']    = BFWK_SERVER_ROOT.'?/document/edit/';

        //List product
        $data['urlLoadList']        = BFWK_SERVER_ROOT.'?/product/loadList/';
        $data['urlNewProduct']      = BFWK_SERVER_ROOT.'?/product/newProduct/';
        $data['urlViewProduct']     = BFWK_SERVER_ROOT.'?/product/viewProduct/';
        $data['urlEditProduct']     = BFWK_SERVER_ROOT.'?/product/editProduct/';
        $data['urlDeleteProduct']   = BFWK_SERVER_ROOT.'?/product/deleteProduct/';

        $this->_display('document_edit', $data);
        die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
    }

    /**
     * TODO improve errors management
     * */
    function save($args)
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');//TODO do womethoing else when session end to catch it and ask user to reconnect

        if (isset($_POST)
            && isset($_POST['ThirdPartyID'])
            && isset($_POST['DocTypeID']))
        {

            require_once('models/documentModel.php');
            $document = DocumentModel::load($_POST['ID']);
            if ($document == false || $document['DocStatusReadOnly'] == 1)
                die('KO');

            //Rearrange data ans send it to model
            $data['ID']                 = $_POST['ID'];
            $data['Reference']          = $_POST['Reference'];
            $data['ExternalReference']  = $_POST['ExternalReference'];
            $data['DocTypeID']          = $_POST['DocTypeID'];
            $data['ThirdPartyID']       = $_POST['ThirdPartyID'];
            $data['DocStatusID']        = $_POST['DocStatusID'];
            $data['TaxID']              = (isset($_POST['TaxID']) ? $_POST['TaxID'] : 0);
            $data['DateValidated']      = $_POST['DateValidated'];
            $data['DateReceived']       = (isset($_POST['DateReceived']) ? $_POST['DateReceived'] : '0000-00-00');
            $data['DateDue']            = $_POST['DateDue'];
            $data['DiscountType']       = $_POST['DiscountType'];
            $data['DiscountValue']      = $_POST['DiscountValue'];

            $data['InvoiceName']        = (isset($_POST['InvoiceName'])         ? $_POST['InvoiceName'] : '');
            $data['InvoiceFirstname']   = (isset($_POST['InvoiceFirstname'])    ? $_POST['InvoiceFirstname'] : '');
            $data['InvoiceAddress1']    = (isset($_POST['InvoiceAddress1'])     ? $_POST['InvoiceAddress1'] : '');
            $data['InvoiceAddress2']    = (isset($_POST['InvoiceAddress2'])     ? $_POST['InvoiceAddress2'] : '');
            $data['InvoiceAddress3']    = (isset($_POST['InvoiceAddress3'])     ? $_POST['InvoiceAddress3'] : '');
            $data['InvoicePostalCode']  = (isset($_POST['InvoicePostalCode'])   ? $_POST['InvoicePostalCode'] : '');
            $data['InvoiceCity']        = (isset($_POST['InvoiceCity'])         ? $_POST['InvoiceCity'] : '');
            $data['InvoiceCountryID']   = (isset($_POST['InvoiceCountryID'])    ? $_POST['InvoiceCountryID'] : '');
            $data['DeliveryName']       = (isset($_POST['DeliveryName'])        ? $_POST['DeliveryName'] : '');
            $data['DeliveryFirstname']  = (isset($_POST['DeliveryFirstname'])   ? $_POST['DeliveryFirstname'] : '');
            $data['DeliveryAddress1']   = (isset($_POST['DeliveryAddress1'])    ? $_POST['DeliveryAddress1'] : '');
            $data['DeliveryAddress2']   = (isset($_POST['DeliveryAddress2'])    ? $_POST['DeliveryAddress2'] : '');
            $data['DeliveryAddress3']   = (isset($_POST['DeliveryAddress3'])    ? $_POST['DeliveryAddress3'] : '');
            $data['DeliveryPostalCode'] = (isset($_POST['DeliveryPostalCode'])  ? $_POST['DeliveryPostalCode'] : '');
            $data['DeliveryCity']       = (isset($_POST['DeliveryCity'])        ? $_POST['DeliveryCity'] : '');
            $data['DeliveryCountryID']  = (isset($_POST['DeliveryCountryID'])   ? $_POST['DeliveryCountryID'] : '');

            //Lines
            $data['Lines'] = array();
            $i = 0;
            while (isset($_POST['item_Reference-'.$i]))
            {
                $line['Reference']     = (isset($_POST['item_Reference-'.$i])       ? $_POST['item_Reference-'.$i] : '');
                $line['Type']          = (isset($_POST['item_Type-'.$i])            ? $_POST['item_Type-'.$i] : '');
                $line['Name']          = (isset($_POST['item_Name-'.$i])            ? $_POST['item_Name-'.$i] : '');
                $line['Description']   = (isset($_POST['item_Description-'.$i])     ? $_POST['item_Description-'.$i] : '');
                $line['Quantity']      = (isset($_POST['item_Quantity-'.$i])        ? $_POST['item_Quantity-'.$i] : '');
                $line['Price']         = (isset($_POST['item_Price-'.$i])           ? $_POST['item_Price-'.$i] : '');
                $line['DiscountValue'] = (isset($_POST['item_DiscountValue-'.$i])   ? $_POST['item_DiscountValue-'.$i] : '');
                $line['DiscountType']  = (isset($_POST['item_DiscountType-'.$i])    ? $_POST['item_DiscountType-'.$i] : '');
                $line['TaxID']         = (isset($_POST['item_TaxID-'.$i])           ? $_POST['item_TaxID-'.$i] : '');
                $line['UnityID']       = (isset($_POST['item_UnityID-'.$i])         ? $_POST['item_UnityID-'.$i] : '');

                $data['Lines'][] = $line;
                $i++;
            }

            $res = DocumentModel::save($data);
            if ($res !== false)
            {
                echo json_encode($res);
                die;
            }
            else
                die('KO');

        }
        else
        {
            die('KO');
        }
        die;

    }

    function delete($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();
        $data['navBar'] = navBarVars('document');

        if ( ! isset($args[0]) || $args[0] !== 'quote')
        {
            $data['error'] = t('EmptyTypeDocument');
            $this->_display('error_page', $data);
            die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
        }
        else
            $type = $args[0] ;

        if ( ! isset($args[1]))
        {
            $data['error'] = t('EmptyIDDocument');
            $this->_display('error_page', $data);
            die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
        }
        else
            $id = (int) $args[1];

        //TODO check only quote

        require_once('models/documentModel.php');
        $quote = DocumentModel::load($id);
        if ($quote == false || $quote['DocTypeID'] != DocumentModel::DOC_TYPE_QUOTE || $quote['DocStatusID'] != 1)
        {
            $data['error'] = t('DisallowedActionOnlyDraftDocumentCanBeDeleted');
            $this->_display('error_page', $data);
            die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
        }

        $res = DocumentModel::delete($id);

        if ($res === true)
        {
            $_SESSION['msg'] = 'DocumentDeleted';
            header('Location: '.BFWK_SERVER_ROOT.'?/document/index');
        }
        else
        { //TODO si mode édition retour sur la page édition non pas accueil
            $_SESSION['err'] = 'ErrorDeletingDocument';
            header('Location: '.BFWK_SERVER_ROOT.'?/document/index');
        }
        die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer

    }

    function quoteToOrder($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();
        $data['navBar'] = navBarVars('document');

        if ( ! isset($args[0]))
        {
            $data['error'] = t('EmptyIDDocument');
            $this->_display('error_page', $data);
            die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
        }
        else
            $id = $args[0] ;

        require_once('models/settingModel.php');
        $settings = SettingModel::load(array('GENERAL'));

        require_once('models/documentStatusModel.php');

        require_once('models/documentModel.php');
        $quote = DocumentModel::load($id);

        if ($quote == false || $quote['DocTypeID'] != 1 || $quote['Ordered'] == 1)
        {
            $data['error'] = t('DisallowedActionOnlyDraftDocumentCanBeDeleted');
            $this->_display('error_page', $data);
            die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
        }
        $order = $quote;

        //TODO check if document is really a quote
        $order['ID']              = 0;
        $order['QuoteID']         = $quote['ID'];
        $order['Reference']       = '';
        $order['DocTypeID']       = DocumentModel::DOC_TYPE_ORDER;
        $order['DateCreated']     = date($settings['GENERAL']['DateFormat']);
        $order['DateDue']         = date($settings['GENERAL']['DateFormat'], mktime(0,0,0,date('n'),date('d')+$settings['GENERAL']['OrderDueDelay'],date('Y')));
        $order['DocStatusID']     = $settings['GENERAL']['OrderDefaultStatus'];

        $order = DocumentModel::save($order);//TODO check error

        $quote['Ordered'] = 1;
        $quote['DocStatusID'] = DocumentStatusModel::QUOTE_CLOSED;
        $quote = DocumentModel::save($quote);//TODO check error

        header('Location: '.BFWK_SERVER_ROOT.'?/document/edit/order/'.$order['ID']);
        exit;

    }

    function orderToInvoice($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();
        $data['navBar'] = navBarVars('document');

        if ( ! isset($args[0]))
        {
            $data['error'] = t('EmptyIDDocument');
            $this->_display('error_page', $data);
            die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
        }
        else
            $id = $args[0] ;

        $invoice = self::invoiceFromOrder($id);//TODO check error

        header('Location: '.BFWK_SERVER_ROOT.'?/document/edit/invoice/'.$invoice['ID']);
        exit;

    }

    function receive($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();
        $data['navBar'] = navBarVars('document');

        if ( ! isset($args[0]))
        {
            $data['error'] = t('EmptyIDDocument');
            $this->_display('error_page', $data);
            die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
        }
        else
            $id = $args[0] ;

        require_once('models/documentModel.php');
        $purchase = DocumentModel::receivePurchase($id);//TODO check error

        header('Location: '.BFWK_SERVER_ROOT.'?/document/editPurchase/'.$purchase['ID']);
        exit;

    }

    /**
     * Internal method for creating an invoice from an order
     * */
    private function invoiceFromOrder($orderId) {

        require_once('models/settingModel.php');
        $settings  = SettingModel::load(array('GENERAL'));

        require_once('models/documentStatusModel.php');

        require_once('models/documentModel.php');
        $order = DocumentModel::load($orderId);

        if ($order == false || $order['DocTypeID'] != DocumentModel::DOC_TYPE_ORDER || $order['Invoiced'] == 1)
        {
            $data['error'] = t('ActionAllowedOnlyForNonInvoicedOrder');
            return false;//Return error text ?
        }

        $invoice = $order;

        //TODO check if document is really an order
        $invoice['ID']              = 0;
        $invoice['OrderID']         = $order['ID'];
        $invoice['Reference']       = '';
        $invoice['DocTypeID']       = DocumentModel::DOC_TYPE_INVOICE;
        $invoice['DateValidated']   = '';
        $invoice['DateCreated']     = date($settings['GENERAL']['DateFormat']);
        $invoice['DateDue']         = date($settings['GENERAL']['DateFormat'], mktime(0,0,0,date('n'),date('d')+$settings['GENERAL']['InvoiceDueDelay'],date('Y')));
        $invoice['DocStatusID']     = DocumentStatusModel::INVOICE_VALIDATED;

        $invoice = DocumentModel::save($invoice);//TODO check error

        $order['Invoiced'] = 1;
        $order['DocStatusID'] = DocumentStatusModel::ORDER_INVOICED;
        $order = DocumentModel::save($order);//TODO check error

        return $invoice;

    }

    /**
     * Création d'un accompte et de la facture finale
     * */
    function createDeposit($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();
        $data['navBar'] = navBarVars('document');

        if ( ! isset($args[0]))
        {
            $data['error'] = t('EmptyIDDocument');
            $this->_display('error_page', $data);
            die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
        }
        else
            $id = (int) $args[0];

        if ( ! isset($args[1]) || ! in_array($args[1], array('percent', 'amount')))
        {
            $data['error'] = t('EmptyTypeAmount');
            $this->_display('error_page', $data);
            die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
        }
        else
            $type = $args[1];

        if ( ! isset($args[2]))
        {
            $data['error'] = t('EmptyAmount');
            $this->_display('error_page', $data);
            die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
        }
        else
            $amount = $args[2] * 1.0;

        require_once('models/settingModel.php');
        $settings  = SettingModel::load(array('GENERAL'));

        require_once('models/documentStatusModel.php');

        require_once('models/documentModel.php');
        $order = DocumentModel::load($id);

        if ($order == false || $order['DocTypeID'] != DocumentModel::DOC_TYPE_ORDER || $order['Invoiced'] == 1)
        {
            $data['error'] = t('ActionAllowedOnlyForNonInvoicedOrder');
            $this->_display('error_page', $data);
            die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
        }

        $deposit = $order;

        $orderRef = $deposit['Reference'];

        //TODO check if document is really an order
        $deposit['ID']              = 0;
        $deposit['OrderID']         = $order['ID'];
        $deposit['Reference']       = '';
        $deposit['DocTypeID']       = DocumentModel::DOC_TYPE_INVOICE;
        $deposit['IsDeposit']       = '1';
        $deposit['DateCreated']     = '';
        $deposit['DateValidated']   = '';
        $deposit['DateDue']         = date($settings['GENERAL']['DateFormat'], mktime(0,0,0,date('n'),date('d')+$settings['GENERAL']['DepositDueDelay'],date('Y')));
        $deposit['DocStatusID']     = DocumentStatusModel::INVOICE_VALIDATED;

        $deposit['Lines'] = array();

        //TODO récupérer les TVA en fonction de la notion service / produit (TVA20% produit - TVA20% Service - TVA5% produit - ...)
        $i = 1;
        $nbTax = count($deposit['TaxSummary']);
        foreach($deposit['TaxSummary'] as $taxID => $taxDetail)
        {
            if ($amount > 0)
            {
                $line = array();
                $line['Reference']     = '#'.$i;
                $line['Type']          = DocumentModel::LINE_TYPE_SERVICE;
                $line['Name']          = t('DepositOnOrderNumber', $orderRef);
                if ($nbTax > 1)
                    $line['Description'] = t('VAT').' '.$taxDetail['Rate'].' %';
                else
                    $line['Description'] = '';
                $line['Quantity']      = 1;
                $line['DiscountValue'] = '';
                $line['DiscountType']  = '%';
                $line['TaxID']         = $taxID;
                $line['UnityID']       = $settings['GENERAL']['DepositDefaultUnity'];

                if ($type == 'percent')
                    $line['Price'] = sprintf('%.2f', $taxDetail['Base'] * $amount / 100);
                else
                    $line['Price'] = sprintf('%.2f', $taxDetail['Base'] / $deposit['Total'] * $amount);

                $deposit['Lines'][] = $line;
                $i++;
            }
        }

        $deposit = DocumentModel::save($deposit);

        $invoice = self::invoiceFromOrder($order['ID']);//TODO check error

        //Add line with deposit detail
        foreach($deposit['Lines'] as $line)
        {
            $line['Name'] = t('Deposit').' '.$deposit['Reference'];
            $line['Quantity'] = $line['Quantity'] * -1;
            $invoice['Lines'][] = $line;
        }
        $invoice = DocumentModel::save($invoice);//TODO check error

        //Update invoiceID
        $deposit['InvoiceID'] = $invoice['ID'];
        $deposit = DocumentModel::save($deposit);

        header('Location: '.BFWK_SERVER_ROOT.'?/document/edit/invoice/'.$deposit['ID']);
        exit;

    }

    function createCreditNote($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();
        $data['navBar'] = navBarVars('document');

        if ( ! isset($args[0]))
        {
            $data['error'] = t('EmptyIDDocument');
            $this->_display('error_page', $data);
            die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
        }
        else
            $id = (int) $args[0];

        require_once('models/settingModel.php');
        $settings  = SettingModel::load(array('GENERAL'));

        require_once('models/documentModel.php');
        $invoice = DocumentModel::load($id);

        if ($invoice == false || $invoice['DocTypeID'] != DocumentModel::DOC_TYPE_INVOICE)
        {
            $data['error'] = t('ActionAllowedOnlyForNonInvoicedOrder');
            $this->_display('error_page', $data);
            die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
        }

        $invoiceRef = $invoice['Reference'];

        $credit = $invoice;

        $credit['ID']           = 0;
        $credit['OrderID']      = $order['ID'];
        $credit['Reference']    = '';
        $credit['DocTypeID']    = DocumentModel::DOC_TYPE_INVOICE;
        $credit['DateCreated']  = date($settings['GENERAL']['DateFormat']);
        $credit['DateDue']      = date($settings['GENERAL']['DateFormat'], mktime(0,0,0,date('n'),date('d')+$settings['GENERAL']['CreditDueDelay'],date('Y')));
        $credit['DocStatusID']  = $settings['GENERAL']['InvoiceDefaultStatus'];
        $credit['Paid']         = 0.00;

        $i = 1;
        foreach($credit['Lines'] as &$line)
        {
            $line['Quantity'] = $line['Quantity'] * -1;
        }

        $credit = DocumentModel::save($credit);

        header('Location: '.BFWK_SERVER_ROOT.'?/document/edit/invoice/'.$credit['ID']);
        exit;

    }

    function copyDocument($args)
    {
/*TODO copyDocument à développer
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        if ( ! isset($args[0]))
        {
            //TODO error
        }
        else
            $id = $args[0] ;

        require_once('models/settingModel.php');
        $settings  = SettingModel::load(array('GENERAL'));

        require_once('models/documentModel.php');
        $document = DocumentModel::load($id);
        $rDocTypes = DocumentModel::getDocumentTypes();

        //TODO check if document is really an order
        $document['ID']             = 0;
        $document['Reference']      = '';
        $document['Ordered']        = '';
        $document['Invoiced']       = '';
        $document['Paid']           = 0.00;
        $document['DateCreated']    = date($settings['GENERAL']['DateFormat']);

        if ($document['DocTypeID'] == 1)
        {
            $document['DateDue']        = date($settings['GENERAL']['DateFormat'], mktime(0,0,0,date('n'),date('d')+$settings['GENERAL']['QuoteExpirationDelay'],date('Y')));
            $document['DocStatusID']    = $settings['GENERAL']['QuoteDefaultStatus'];
            $document['TaxID']          = $settings['GENERAL']['QuoteTaxDefault'];
        }
        elseif ($document['DocTypeID'] == 2)
        {
            $document['DateDue']        = date($settings['GENERAL']['DateFormat'], mktime(0,0,0,date('n'),date('d')+$settings['GENERAL']['OrderDueDelay'],date('Y')));
            $document['DocStatusID']    = $settings['GENERAL']['OrderDefaultStatus'];
            $document['TaxID']          = $settings['GENERAL']['OrderTaxDefault'];
        }
        elseif ($document['DocTypeID'] == 3)
        {
            $document['DateDue']        = date($settings['GENERAL']['DateFormat'], mktime(0,0,0,date('n'),date('d')+$settings['GENERAL']['DepositDueDelay'],date('Y')));
            $document['DocStatusID']    = $settings['GENERAL']['DepositDefaultStatus'];
            $document['TaxID']          = $settings['GENERAL']['DepositTaxDefault'];
        }
        elseif ($document['DocTypeID'] == 4)
        {
            $document['DateDue']        = date($settings['GENERAL']['DateFormat'], mktime(0,0,0,date('n'),date('d')+$settings['GENERAL']['InvoiceDueDelay'],date('Y')));
            $document['DocStatusID']    = $settings['GENERAL']['InvoiceDefaultStatus'];
            $document['TaxID']          = $settings['GENERAL']['InvoiceTaxDefault'];
        }

        $document = DocumentModel::save($document);

        header('Location: '.BFWK_SERVER_ROOT.'?/document/edit/'.$rDocTypes[ $document['DocTypeID'] ].'/'.$document['ID']);
        exit;
*/
    }

    function viewPDF($args)
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        require_once(BFWK_LIB_PATH . 'HLX_DomPDF.class.php');

        require_once('models/documentModel.php');
        $rDocTypes = DocumentModel::getDocumentTypes('all');

        $data = array();
        $data['navBar'] = navBarVars('document');

        if ( ! isset($args[0]) || ! in_array($args[0], $rDocTypes) || ! isset($args[1]))
        {
            $data['error'] = t('EmptyTypeDocument');
            $this->_display('error_page', $data);
            die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
        }

        $type = $args[0];
        $id = $args[1];

        require_once('models/documentModel.php');
        $data['document'] = DocumentModel::load($id);

        if ($data['document']['Reference'] != '')
        {
            $ref = mb_ereg_replace("([^a-zA-Z0-9\-_])", '-', $data['document']['Reference']);
            $dowloadName = $ref.'.pdf';
        }
        else
        {
            $ref = '#';
            $dowloadName = 'document.pdf';
        }

        if ($ref == '#')
            $filename = HLX_STORE.'documents/'.$type.'/#-'.$id.'.pdf';
        else
            $filename = HLX_STORE.'documents/'.$type.'/'.$ref.'-'.$id.'.pdf';

        //Check document archive validity
        if (is_file($filename))
        {
            $dateFile = date("Y-m-d H:i:s", filemtime($filename));
            if ($data['document']['DateUpdated'] <= $dateFile)
            {
                header("Content-type:application/pdf");
                header("Content-Disposition:inline;filename='".$dowloadName."'");
                readfile($filename);
                die;
            }
        }

        $data['type'] = $type;

        require_once('models/settingModel.php');
        $data['settings']       = SettingModel::load(array('GENERAL'));
        $data['companyLogo']    = SettingModel::getLogo();

        require_once('libs/HLX_Country.class.php');

        $rSearch = array('{CompanyName}', '{CompanyType}', '{CompanyCapital}', '{CompanyClassification}', '{CompanyRegistrationNumber}', '{CompanyRegister}', '{CompanyTaxNumber}');
        $rReplace = array($data['settings']['GENERAL']['CompanyName'], $data['settings']['GENERAL']['CompanyType'], $data['settings']['GENERAL']['CompanyCapital'], $data['settings']['GENERAL']['CompanyClassification'], $data['settings']['GENERAL']['CompanyRegistrationNumber'], $data['settings']['GENERAL']['CompanyRegister'], $data['settings']['GENERAL']['CompanyTaxNumber']);

        if ($type == 'quote')
        {
            $data['settings']['GENERAL']['QuoteNote'] = str_replace($rSearch, $rReplace, $data['settings']['GENERAL']['QuoteNote']);
            $template = HLX_STORE.'templates/quote/'.$data['settings']['GENERAL']['QuoteTemplate'].'.php';
        }
        elseif ($type == 'order')
        {
            $data['settings']['GENERAL']['OrderNote'] = str_replace($rSearch, $rReplace, $data['settings']['GENERAL']['OrderNote']);
            $template = HLX_STORE.'templates/order/'.$data['settings']['GENERAL']['OrderTemplate'].'.php';
        }
        elseif ($type == 'invoice')
        {
            $data['settings']['GENERAL']['InvoiceNote'] = str_replace($rSearch, $rReplace, $data['settings']['GENERAL']['InvoiceNote']);
            $template = HLX_STORE.'templates/invoice/'.$data['settings']['GENERAL']['InvoiceTemplate'].'.php';
        }
        elseif ($type == 'purchase')
        {
            $template = HLX_STORE.'templates/purchase/'.$data['settings']['GENERAL']['PurchaseTemplate'].'.php';
        }

        //Purge older files without reference
        if ($ref != '#')
        {
            $oldname = HLX_STORE.'documents/'.$type.'/#-'.$id.'.pdf';
            if (is_file($oldname))
                unlink($oldname);
        }
        $filename = HLX_STORE.'documents/'.$type.'/'.$ref.'-'.$id.'.pdf';

        $hlxDomPDF = new HLX_DomPDF();
        $hlxDomPDF->loadHtml($this->getHTMLDocument($data, $template));
        $hlxDomPDF->render($filename, $dowloadName);
        die;

    }

    function sendMail($args)
    {
        /**
         *TODO sendMail à développer
         * */
    }

    function getHTMLDocument($data, $template)
    {
        extract($data, EXTR_OVERWRITE);
        ob_start();
        require($template);
        $res = ob_get_contents();
        ob_clean();
        return $res;
    }
}
