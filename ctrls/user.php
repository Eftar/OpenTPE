<?php

/**
 *
 * */

class User extends HLX_Controller
{

    function index($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        //TODO add message erreur de role
        if ( ! $this->hlx_motor->user_isAdmin())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/home');

        $data = array();

        require_once('models/userModel.php');
        $data['users'] = UserModel::loadAll();

        //Options for navBar
        $data['navBar']         = navBarVars('user');
        $data['urlSave']        = BFWK_SERVER_ROOT.'?user/save';
        $data['urlCancel']      = BFWK_SERVER_ROOT.'?user/index';

        return $data;

    }

    function save($args)
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        //TODO add message erreur de role
        if ( ! $this->hlx_motor->user_isAdmin())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/home');

        require_once('models/userModel.php');

        $users = array();
        //Check form structure
        if (isset($_POST['ID']) && is_array($_POST['ID'])
            && isset($_POST['Name']) && is_array($_POST['Name'])
            && isset($_POST['Email']) && is_array($_POST['Email'])
            && isset($_POST['Password']) && is_array($_POST['Password'])
            && isset($_POST['Role']) && is_array($_POST['Role'])
            && isset($_POST['Active']) && is_array($_POST['Active']))
        {
            $i = 0;
            $c = min(count($_POST['ID']), count($_POST['Name']), count($_POST['Password']), count($_POST['Email']), count($_POST['Role']), count($_POST['Active']));
            while ($i < $c)
            {
                //Check champs obligatoires
                if ($_POST['Name'][ $i ] != ''
                    && $_POST['Email'][ $i ] != ''
                    && ($_POST['ID'][ $i ] != '' || $_POST['Password'][ $i ] != ''))
                {
                    //Force current user stay admin and actif
                    if ($_POST['ID'] == $this->hlx_motor->user_getParam('ID'))
                    {
                        $_POST['Role'] = 'admin';
                        $_POST['Active'] = '1';
                    }


                    $users[] = array(
                        'ID'        => (isset($_POST['ID'][ $i ]) ? $_POST['ID'][ $i ] : ''),
                        'Name'      => (isset($_POST['Name'][ $i ]) ? $_POST['Name'][ $i ] : ''),
                        'Email'     => (isset($_POST['Email'][ $i ]) ? $_POST['Email'][ $i ] : ''),
                        'Password'  => (isset($_POST['Password'][ $i ]) ? $_POST['Password'][ $i ] : ''),
                        'Role'      => (isset($_POST['Role'][ $i ]) ? $_POST['Role'][ $i ] : ''),
                        'Active'    => (isset($_POST['Active'][ $i ]) ? $_POST['Active'][ $i ] : '')
                    );
                }
                $i++;
            }
        }

        UserModel::saveAll($users);

        header('Location: '.BFWK_SERVER_ROOT.'?/user/index');
        exit;

    }

}
