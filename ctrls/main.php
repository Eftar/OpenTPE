<?php

/**
 *
 * */

class Main extends HLX_Controller
{

    function index()
    {

        header('Location: '.BFWK_SERVER_ROOT.'?/main/login');
        return;

    }

    /**
     * Login Form
     */
    function login($args)
    {

        $data = array();
        $data['msg'] = '';
        $data['err'] = '';

        if (isset($_GET['msg']))
            $data['msg'] = htmlspecialchars(t($_GET['msg']));

        if (isset($_POST['Email']) && isset($_POST['Password']))
        {
            if ($this->hlx_motor->user_login($_POST['Email'], $_POST['Password']))
            {
                header('Location: '.BFWK_SERVER_ROOT.'?/main/home');
                return false;
            }
            else
            {
                $data['err'] = t('AuthFailed');
            }
        }

        $data['urlForm'] = BFWK_SERVER_ROOT.'?/main/login';

        return $data;

    }

    /**
     * Main page of the application
     * */
    function home()
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();
        //Options for navBar
        $data['navBar'] = navBarVars('home');

        require_once('models/documentModel.php');
        $rDocTypes = DocumentModel::getDocumentTypes();

        //Get some statitics to populate graph
        $data['rTotalDocumentsYear']    = DocumentModel::getTotalDocumentsYear();
        $data['rQuoteDivision']         = DocumentModel::getQuoteDivision();
        $data['rInvoiceDivision']       = DocumentModel::getInvoiceDivision();
        $data['totalOverdueInvoice']    = DocumentModel::getTotalOverdueInvoice();
        $data['totalExpiredQuote']      = DocumentModel::getTotalExpiredQuote();

        //Get last quote
        $args = array(
            'sortBy'        => 'DateCreated',
            'sortDirection' => 'desc',
            'type'          => '1',
            'searchBy'      => '',
            'filterBy'      => 'all',
            'pageIndex'     => 0,
            'pageSize'      => 5
        );
        $rQuote = DocumentModel::getList($args);
        $data['rQuote'] = $rQuote['items'];
        //Get last invoice
        $args = array(
            'sortBy'        => 'DateCreated',
            'sortDirection' => 'desc',
            'type'          => '4',
            'searchBy'      => '',
            'filterBy'      => 'all',
            'pageIndex'     => 0,
            'pageSize'      => 5
        );
        $rInvoice = DocumentModel::getList($args);
        $data['rInvoice'] = $rInvoice['items'];

        require_once('models/settingModel.php');
        $data['settings'] = SettingModel::load(array('GENERAL'));

        require_once('models/documentStatusModel.php');
        $data['quoteStatutes']      = DocumentStatusModel::loadAll(1);
        $data['invoiceStatutes']    = DocumentStatusModel::loadAll(4);

        $data['urlNewPayment']      = BFWK_SERVER_ROOT.'?/payment/newPayment/';
        $data['urlNewCustomer']     = BFWK_SERVER_ROOT.'?/thirdParty/newCustomer/';
        $data['urlViewCustomer']    = BFWK_SERVER_ROOT.'?/thirdParty/viewCustomer/';
        $data['urlViewQuote']       = BFWK_SERVER_ROOT.'?/document/edit/quote/';
        $data['urlViewInvoice']     = BFWK_SERVER_ROOT.'?/document/edit/invoice/';

        $data['urlExpiredQuotes']   = BFWK_SERVER_ROOT.'?/document/index/quote/expired/';
        $data['urlOverdueInvoices'] = BFWK_SERVER_ROOT.'?/document/index/invoice/overdue/';

        $data['urlViewAllQuote']    = BFWK_SERVER_ROOT.'?/document/index/quote';
        $data['urlViewAllInvoice']  = BFWK_SERVER_ROOT.'?/document/index/invoice';

        return $data;

    }


    function logout()
    {

        $this->hlx_motor->user_logout();
        header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=LogoutOk');
        exit;

    }

}
