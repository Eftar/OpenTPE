<?php

/**
 *
 * */

class Report extends HLX_Controller
{

    function index($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();

        require_once('models/reportModel.php');
        $data['rReports'] = ReportModel::loadAll();

        //Options for navBar
        $data['navBar'] = navBarVars('report');
        $data['urlSettingReport'] = BFWK_SERVER_ROOT.'?/report/settings/';
        $data['urlGenerateReport'] = BFWK_SERVER_ROOT.'?/report/generate/';

        return $data;

    }

    function settings($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        if ( ! isset($args[0]))
        {
            //TODO error
        }
        else
            $id = (int) $args[0];

        $data = array();

        require_once('models/reportModel.php');
        $data['report'] = ReportModel::load($id);

        require_once('models/settingModel.php');
        $data['settings'] = SettingModel::load(array('GENERAL'));

        return $data;

    }

    function generate($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        if ( ! isset($args[0]))
        {
            //TODO error
        }
        else
            $id = (int) $args[0];

        require_once('models/settingModel.php');
        $settings = SettingModel::load(array('GENERAL'));

        require_once('models/reportModel.php');
        $report = ReportModel::load($id);

        if ( ! $report)
        {
            //TODO error
            die('error loading report');
        }
        //Check variables
        foreach($report['Variables'] as $variable)
        {
            if ( ! isset($_POST[ $variable['name'] ]))
                die('error loading variable '.$variable['name']);

            if ($variable['type'] == 'date')
                $report['Query'] = str_replace('{'.$variable['name'].'}', convertDateFormat($_POST[ $variable['name'] ], $settings['GENERAL']['DateFormat'], 'Y-m-d', true), $report['Query']);
            else
                $report['Query'] = str_replace('{'.$variable['name'].'}', $_POST[ $variable['name'] ], $report['Query']);
        }

        $db = HLX_Motor::getInstance()->getDatabase();
        $res = $db->query($report['Query']);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC) : false;
        if ($rows === false)
        {
            die('Error running query');
        }

        $csv = '';
        foreach($rows as $row) {
            foreach ($row as $col) {
                $csv .= '"'.str_replace('"', '""', $col).'";';
            }
            $csv .= "\n";
        }
        if ($csv == '')
            $csv = t('NoResult');

        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=report-".mb_ereg_replace("([^a-zA-Z0-9\-_])", '-', $report['Name']).".csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo $csv;

        die;

    }

}