<?php

/**
 *
 * */

class Relation extends HLX_Controller
{

	function index($args)
	{

		if ( ! $this->hlx_motor->user_isConnected())
			header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

		$data = array();

		//Options for navBar
		$data['navBar'] = navBarVars('relation');

		return $data;

	}

}
