<?php

/**
 *
 * */

class Update extends HLX_Controller
{

    function index($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        //TODO add message erreur de role
        if ( ! $this->hlx_motor->user_isAdmin())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/home');

        $data = array();
        //Options for navBar
        $data['navBar'] = navBarVars('setting');
        $data['rChangeLog'] = array();
        $data['urlHelp']    = BFWK_SERVER_ROOT.'?/help/index/';
        $data['urlUpgrade'] = BFWK_SERVER_ROOT.'?/update/upgrade/';

        //Check for an update
        try {

            $versions = @file_get_contents(HLX_URL_UPDATE.'/versions.json');
            if ( ! $versions)
                throw new Exception('Error opening versions file');
            $rVersions = @json_decode($versions, true);
            if ( ! $rVersions)
                throw new Exception('Error reading versions file');

            krsort($rVersions);
            foreach ($rVersions as $version => $rInfo)
            {
                if ($version > HLX_VERSION
                    && (HLX_CANAL == 'beta'
                        || (HLX_CANAL == 'rc' && ($rInfo['canal'] == 'rc' || $rInfo['canal'] == 'stable'))
                        || (HLX_CANAL == 'stable' && $rInfo['canal'] == 'stable')))
                {
                    $data['rChangeLog'][ $version ] = array(
                        'date'      => $rInfo['date'],
                        'changelog' => $rInfo['changelog']
                    );
                }
            }

        } catch (Exception $e) {

            $data['rChangeLog'] = false;

        }

        return $data;

    }

    function getUpdateJson($args)
    {
        echo @file_get_contents(HLX_URL_UPDATE.'/versions.json');
        die;
    }

    function upgrade($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        //TODO add message erreur de role
        if ( ! $this->hlx_motor->user_isAdmin())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/home');

        ini_set('max_execution_time', 200);

        $found = false;

        echo '<pre>';

        $rLog = array();
        $rLog[] = 'CURRENT VERSION: '.HLX_VERSION;
        $rLog[] = 'Reading Current Releases List';

        //Check for an update
        try {
            $versions = @file_get_contents(HLX_URL_UPDATE.'/versions.json');
            $rVersions = @json_decode($versions, true);
        } catch (Exception $e) {
            $rLog[] = 'Error getting / reading version list';
            echo implode("\n", $rLog);
            echo '</pre>';
            die;
        }

        if ($rVersions && is_array($rVersions))
        {
            //get last version
            $version = 0;
            ksort($rVersions);
            foreach ($rVersions as $kVersion => $rInfo)
            {
                if ($kVersion > HLX_VERSION
                    && (HLX_CANAL == 'beta'
                        || (HLX_CANAL == 'rc' && ($rInfo['canal'] == 'rc' || $rInfo['canal'] == 'stable'))
                        || (HLX_CANAL == 'stable' && $rInfo['canal'] == 'stable')))
                {
                    $version = $kVersion;
                    break;
                }
            }

            if ($version > HLX_VERSION)
            {

                $found = true;

                $rLog[] = 'New Update Found: V'.$version;

                //Download The File If We Do Not Have It
                if ( ! is_file(HLX_STORE.'update/OpenTPE'.$version.'.zip'))
                {
                    $rLog[] = 'Downloading archive';
                    try {
                        file_put_contents(HLX_STORE.'update/OpenTPE'.$version.'.zip', file_get_contents(HLX_URL_UPDATE.'OpenTPE'.$version.'.zip'));
                    } catch (Exception $e) {
                        $rLog[] = 'Error downloading archive';
                        echo implode("\n", $rLog);
                        echo '</pre>';
                        die;
                    }
                }
                else
                    $rLog[] = 'Archive already downloaded';

                //Download The signature If We Do Not Have It
                if ( ! is_file(HLX_STORE.'update/OpenTPE'.$version.'.sig'))
                {
                    $rLog[] = 'Downloading signature';
                    try {
                        file_put_contents(HLX_STORE.'update/OpenTPE'.$version.'.sig', file_get_contents(HLX_URL_UPDATE.'OpenTPE'.$version.'.sig'));
                    } catch (Exception $e) {
                        $rLog[] = 'Error downloading file signature';
                        echo implode("\n", $rLog);
                        echo '</pre>';
                        die;
                    }
                }
                else
                    $rLog[] = 'Signature already downloaded';

                //Check integrity
                $rLog[] = 'Checking file signature';
                $check = $this->_checkSign(HLX_STORE.'update/OpenTPE'.$version.'.zip', HLX_STORE.'update/OpenTPE'.$version.'.sig', HLX_STORE.'key/pub_key.pem');
                if ($check)
                    $rLog[] = 'Signature OK';
                else {
                    $rLog[] = 'Signature KO';
                    $rLog[] = 'Process aborted';
                    echo implode("\n", $rLog);
                    echo '</pre>';
                    die;
                }

                $zipHandle = zip_open(HLX_STORE.'update/OpenTPE'.$version.'.zip');

                while ($zip = zip_read($zipHandle) )
                {
                    $filename   = zip_entry_name($zip);
                    $dir        = dirname($filename);

                    //Continue if its not a file
                    if ( substr($filename, -1, 1) == '/')
                        continue;

                    //Make the directory if we need to...
                    if ( ! is_dir( $dir ) )
                    {
                        //TODO chmod ?
                        if ( ! @mkdir($dir)) {
                            $rLog[] = 'Error Creating Directory : '.$dir;
                            //Trying creating subdir
                            $rDir = explode('/', $dir);
                            $subdir = '';
                            foreach($rDir as $dir)
                            {
                                if ($subdir != '')
                                    $subdir = $subdir.'/'.$dir;
                                else
                                    $subdir = $dir;

                                if ( ! is_dir($subdir))
                                {
                                    if ( ! @mkdir($subdir))
                                    {
                                        $rLog[] = 'Error Creating SubDirectory : '.$subdir;
                                        $rLog[] = 'Process aborted';
                                        die;
                                    }
                                    $rLog[] = 'SubDirectory Created : '.$subdir;
                                }

                            }

                        }
                        else
                            $rLog[] = 'Directory Created : '.$dir;
                    }

                    //Overwrite the file
                    if ( ! is_dir($filename) ) {

                        //don't update config files
                        if ($filename == 'config/hlx.conf.php' || $filename == 'config/bfwk.conf.php')
                        {
                            $rLog[] = str_pad($filename, 128 - strlen('ignore'), '.').'<b>ignore</b>';
                        }
                        else
                        {
                            //Update file
                            try {

                                $contents = zip_entry_read($zip, zip_entry_filesize($zip));
                                $fUpdate = @fopen($filename, 'w');
                                if ( ! $fUpdate)
                                    throw new Exception('Error opening file');
                                if ( ! fwrite($fUpdate, $contents))
                                    throw new Exception('Error writing file');
                                if ( ! fclose($fUpdate))
                                    throw new Exception('Error closing file');
                                unset($contents, $fUpdate);

                                $rLog[] = str_pad($filename, 128 - strlen('update'), '.').'<b>update</b>';

                            } catch (Exception $e) {
                                $rLog[] = str_pad($filename, 128 - strlen('update'), '.').'<b>error</b>';
                                $rLog[] = 'Process aborted';
                                echo implode("\n", $rLog);
                                echo '</pre>';
                                die;
                            }
                        }
                    }
                }

                if (is_file('upgrade.php'))
                {
                    $rLog[] = 'Call \'upgrade\' script';
                    include('upgrade.php');
                    $rLog[] = 'Remove file';
                    unlink('upgrade.php');
                }

                $rLog[] = 'Delete update archive and signature';
                unlink(HLX_STORE.'update/OpenTPE'.$version.'.zip');
                unlink(HLX_STORE.'update/OpenTPE'.$version.'.sig');

                $rLog[] = 'Successful update';
            }

            if ($found == false)
            {
                $rLog[] = 'No update available';
            }
        }
        else
            $rLog[] = 'Could not find latest realeases';

        echo implode("\n", $rLog);
        echo '</pre>';

        exit;

    }

    private function _checkSign($file, $sign, $pubKey, $algo = 'sha256WithRSAEncryption')
    {
        $data       = file_get_contents($file);
        $signature  = file_get_contents($sign);
        $publicKey  = openssl_pkey_get_public('file://'.$pubKey);

        $ok = openssl_verify($data, $signature, $publicKey, $algo);
        if ($ok == 1)
            return true;
        else
            return false;
        /*TODO debug error ? echo "error: ".openssl_error_string();*/
    }

}