<?php

/**
 *
 * */

class Product extends HLX_Controller
{

    function index($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();

        if (isset($_SESSION['msg']) && $_SESSION['msg'] != '')//TODO find another way to carry message
        {
            $data['msg'] = htmlspecialchars(t($_SESSION['msg']));
            unset($_SESSION['msg']);
        }
        if (isset($_SESSION['err']) && $_SESSION['err'] != '')
        {
            $data['err'] = htmlspecialchars(t($_SESSION['err']));
            unset($_SESSION['err']);
        }

        require_once('models/settingModel.php');
        $data['settings'] = SettingModel::load(array('GENERAL'));

        //Options for navBar
        $data['navBar'] = navBarVars('product');
        $data['urlLoadList']        = BFWK_SERVER_ROOT.'?/product/loadList/';
        $data['urlNewProduct']      = BFWK_SERVER_ROOT.'?/product/newProduct/';
        $data['urlViewProduct']     = BFWK_SERVER_ROOT.'?/product/viewProduct/';
        $data['urlEditProduct']     = BFWK_SERVER_ROOT.'?/product/editProduct/';
        $data['urlDeleteProduct']   = BFWK_SERVER_ROOT.'?/product/deleteProduct/';
        $data['token'] = '';

        return $data;

    }

    function viewProduct($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        if ( ! isset($args[0]))
        {
            //TODO error
        }
        else
            $id = (int) $args[0];

        $data = array();
        //Options for navBar
        $data['navBar'] = navBarVars('product');

        require_once('models/productModel.php');
        $data['item'] = ProductModel::load($id);

        require_once('models/settingModel.php');
        $data['settings'] = SettingModel::load(array('GENERAL'));

        $data['urlEditProduct']     = BFWK_SERVER_ROOT.'?/product/editProduct/'.$id;
        $data['urlDeleteProduct']   = BFWK_SERVER_ROOT.'?/product/deleteProduct/'.$id;

        return $data;

    }

    function editProduct($args)
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        if ( ! isset($args[0]))
        {
            //TODO error
        }
        else
            $id = (int) $args[0];

        $data = array();
        //Options for navBar
        $data['navBar'] = navBarVars('product');

        require_once('models/productModel.php');
        $data['item'] = ProductModel::load($id);
        //TODO redirrection si erreur de chargement

        require_once('models/settingModel.php');
        $data['settings'] = SettingModel::load(array('GENERAL','UNITY'));

        if ($data['settings']['GENERAL']['UseVAT'] == 1)
        {
            require_once('models/taxModel.php');
            $data['taxes'] = TaxModel::loadAll();
        }
        else
            $data['taxes'] = array();

        $data['urlSave']    = BFWK_SERVER_ROOT.'?/product/saveProduct/';
        $data['urlCancel']  = BFWK_SERVER_ROOT.'?/product/viewProduct/'.$id;

        return $data;
    }

    function newProduct()
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();
        //Options for navBar
        $data['navBar'] = navBarVars('product');

        $id = 0;

        require_once('models/settingModel.php');
        $data['settings'] = SettingModel::load(array('GENERAL', 'UNITY'));

        require_once('models/productModel.php');
        $data['item'] = ProductModel::load($id);

        $data['item']['TaxID']          = $data['settings']['GENERAL']['ProductDefaultTaxSell'];
        $data['item']['PurchaseTaxID']  = $data['settings']['GENERAL']['ProductDefaultTaxBuy'];
        $data['item']['UnityID']        = $data['settings']['GENERAL']['ProductDefaultUnity'];



        require_once('models/taxModel.php');
        $data['taxes'] = TaxModel::loadAll();
        $data['urlSave']    = BFWK_SERVER_ROOT.'?/product/saveProduct/';
        $data['urlCancel'] = BFWK_SERVER_ROOT.'?/product/index/';

        $this->_display('product_editProduct', $data);
        die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
    }

    function deleteProduct($args)
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        if ( ! isset($args[0]))
        {
            //TODO error
        }
        else
            $id = (int) $args[0];

        require_once('models/productModel.php');
        $res = ProductModel::delete($id);

        if ($res === true)
        {
            $_SESSION['msg'] = 'ProductDeleted';
            header('Location: '.BFWK_SERVER_ROOT.'?/product/index');
        }
        else
        { //TODO si mode édition retour sur la page édition non pas accueil
            $_SESSION['err'] = 'ErrorDeletingProduct';
            header('Location: '.BFWK_SERVER_ROOT.'?/product/index');
        }
        die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
    }

    /**
     * Ajax call for main List
     * */
    function loadList()
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        //En fonction de Post récupère les données
        $args = $_POST;
        require_once('models/productModel.php');
        echo json_encode(ProductModel::getList($args));

        exit;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer

    }

    function saveProduct()
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        //TODO need check Token

        require_once('models/productModel.php');
        $res = ProductModel::save($_POST);

        if ($res == false)
        {
            //TODO error
        }

        header('Location: '.BFWK_SERVER_ROOT.'?/product/viewProduct/'.$res['ID']);
        return;
    }

}
