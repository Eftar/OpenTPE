<?php

class Setup extends HLX_Controller
{

    private $errors = 0;

    public function __construct()
    {
        parent::__construct();

        session_start();

        if (isset($_SESSION['lang']))
        {
            load_language($_SESSION['lang']);
        }
        else
        {
            load_language();
        }
    }

    public function index()
    {
        header('Location: '.BFWK_SERVER_ROOT.'?/setup/language');
    }

    public function language()
    {
        //only access if there isn't a configured db
        if (HLX_DBDSN != '')
        {
            die(t('DbAlreadyConfigured'));
        }
        $rLanguage = get_languages();

        if (isset($_POST['lang']) && isset($rLanguage[ $_POST['lang'] ]))
        {
            //load lang
            $_SESSION['lang'] = $_POST['lang'];
            unset($_POST);
            //redirect prerequisite
            header('Location: '.BFWK_SERVER_ROOT.'?/setup/prerequisite');
        }
        $data = array();
        $data['rLlanguage'] = $rLanguage;
        $data['urlForm']    = BFWK_SERVER_ROOT.'?/setup/language';

        return $data;
    }

    public function prerequisite()
    {

        //only access if there isn't a configured db
        if (HLX_DBDSN != '')
        {
            die(t('DbAlreadyConfigured'));
        }

        if (isset($_POST['btnContinue']))
        {
            unset($_POST);
            //redirect database
            header('Location: '.BFWK_SERVER_ROOT.'?/setup/database');
        }

        $data = array();
        $data['basics']     = $this->checkBasics();
        $data['writables']  = $this->checkWritables();
        $data['errors']     = $this->errors;
        $data['urlForm']    = BFWK_SERVER_ROOT.'?/setup/prerequisite';

        return $data;
    }

    public function database()
    {
        if (HLX_DBDSN != '')
        {
            die(t('DbAlreadyConfigured'));
        }

        $dsn    = HLX_DBDSN;
        $user   = HLX_DBUSER;
        $pass   = HLX_DBPWD;

        if (isset($_POST['btnCreateTable']))
        {
            $dsn = $_SESSION['dsn'];
            $user = $_SESSION['user'];
            $pass = $_SESSION['pass'];
            //write file
            if ($this->writeDatabaseConfig($dsn, $user, $pass))
            {
                if ($this->createTables($dsn, $user, $pass))
                {
                    //redirect
                    header('Location: '.BFWK_SERVER_ROOT.'?/setup/user');
                }
            }
        }
        elseif (isset($_POST['databaseType']))
        {
            if ($_POST['databaseType'] == 'sqlite' && isset($_POST['databasePath']) && $_POST['databasePath'] != '')
            {
                $dsn = 'sqlite:'.$_POST['databasePath'];
                $_SESSION['dsn'] = $dsn;
                $_SESSION['user'] = $user;
                $_SESSION['pass'] = $pass;
            }
            elseif ($_POST['databaseType'] == 'mysql'
                    && isset($_POST['databaseHost']) && $_POST['databaseHost'] != ''
                    && isset($_POST['databaseUser']) && $_POST['databaseUser'] != ''
                    && isset($_POST['databaseName']) && $_POST['databaseName'] != ''
                    && isset($_POST['databasePassword']))
            {
                $dsn = 'mysql:dbname='.$_POST['databaseName'].';host='.$_POST['databaseHost'].';charset=UTF8';
                $user = $_POST['databaseUser'];
                $pass = $_POST['databasePassword'];
                $_SESSION['dsn'] = $dsn;
                $_SESSION['user'] = $user;
                $_SESSION['pass'] = $pass;
            }
        }

        $data = array();
        $data['firstTry']   = empty($_POST);
        $data['database']   = $this->checkDatabase($dsn, $user, $pass);
        $data['errors']     = $this->errors;
        $data['urlForm']    = BFWK_SERVER_ROOT.'?/setup/database';

        return $data;

    }

    public function user()
    {
        //si les variables de session liées au setup n'existent pas
        if ( ! isset($_SESSION['lang']) || ! isset($_SESSION['dsn']))
        {
            die('No setup running, forbidden');
        }

        if (isset($_POST['Email']) && $_POST['Email'] != '' && isset($_POST['Password']) && $_POST['Password'] != '' && isset($_POST['Name']) && $_POST['Name'] != '')
        {
            $pdo = new PDO(HLX_DBDSN, HLX_DBUSER, HLX_DBPWD);

            $date = date('Y-m-d H:i:s');
            $sql = "INSERT INTO hlx_users (Name, Email, Password, Role, Active, DateCreated) VALUES (
                        ".$pdo->quote($_POST['Name']).",
                        ".$pdo->quote($_POST['Email']).",
                        '".password_hash($_POST['Password'], PASSWORD_DEFAULT)."',
                        'admin',
                        '1',
                        ".$pdo->quote($date)."
                    )";
            $res = $pdo->query($sql);

            //redirect
            header('Location: '.BFWK_SERVER_ROOT.'?/setup/complete');
        }

        $data = array();
        $data['urlForm'] = BFWK_SERVER_ROOT.'?/setup/user';

        return $data;
    }

    public function complete()
    {
        //si les variables de session liées au setup n'existent pas
        if ( ! isset($_SESSION['lang']) || ! isset($_SESSION['dsn']))
        {
            die('No setup running, forbidden');
        }

        $data = array();
        $data['save'] = false;
        if (isset($_POST['CompanyName']))
        {
            $pdo = new PDO(HLX_DBDSN, HLX_DBUSER, HLX_DBPWD);
            foreach($_POST as $name => $value)
            {
                if (in_array($name, array('CompanyName', 'CompanyType', 'CompanyRegistrationNumber', 'CompanyRegister', 'CompanyCapital', 'CompanyClassification', 'CompanyTaxNumber',
                                            'CompanyAddress1', 'CompanyAddress2', 'CompanyAddress3', 'CompanyPostalCode',
                                            'CompanyCity', 'CompanyPhone', 'CompanyFax', 'CompanyEmail', 'CompanyWeb', 'UseBillingAddress',
                                            'UseDeliveryAddress', 'UseVAT', 'UseDiscount')))
                {
                    $sql = "UPDATE hlx_settings SET Value = ".$pdo->quote($value)." WHERE Type ='GENERAL' AND Name = '".$name."'";
                    $res = $pdo->query($sql);
                }
            }

            if (isset($_POST['AutoSettings']) && $_POST['AutoSettings'] != '')
            {
                switch($_POST['AutoSettings']) {
                    case 'FR_SARL':
                        //Fill footer notes
                        $pdo->query("UPDATE hlx_settings SET Value = ".$pdo->quote("Aucun escompte ne sera accordé\nPénalités de retard 3 fois le taux d'intérêt légal\nEn cas de retard de paiement, application d'une indemnité forfaitaire pour frais de recouvrement de 40 € selon l'article D. 441-5 du code du commerce.\n{CompanyName} {CompanyType} au capital de {CompanyCapital} - SIRET {CompanyRegistrationNumber} - APE {CompanyClassification} - {CompanyRegister} - TVA {CompanyTaxNumber}\n")." WHERE Type ='GENERAL' AND Name = 'QuoteNote'");
                        $pdo->query("UPDATE hlx_settings SET Value = ".$pdo->quote("Aucun escompte ne sera accordé\nPénalités de retard 3 fois le taux d'intérêt légal\nEn cas de retard de paiement, application d'une indemnité forfaitaire pour frais de recouvrement de 40 € selon l'article D. 441-5 du code du commerce.\n{CompanyName} {CompanyType} au capital de {CompanyCapital} - SIRET {CompanyRegistrationNumber} - APE {CompanyClassification} - {CompanyRegister} - TVA {CompanyTaxNumber}\n")." WHERE Type ='GENERAL' AND Name = 'OrderNote'");
                        $pdo->query("UPDATE hlx_settings SET Value = ".$pdo->quote("Aucun escompte ne sera accordé\nPénalités de retard 3 fois le taux d'intérêt légal\nEn cas de retard de paiement, application d'une indemnité forfaitaire pour frais de recouvrement de 40 € selon l'article D. 441-5 du code du commerce.\n{CompanyName} {CompanyType} au capital de {CompanyCapital} - SIRET {CompanyRegistrationNumber} - APE {CompanyClassification} - {CompanyRegister} - TVA {CompanyTaxNumber}\n")." WHERE Type ='GENERAL' AND Name = 'DepositNote'");
                        $pdo->query("UPDATE hlx_settings SET Value = ".$pdo->quote("Aucun escompte ne sera accordé\nPénalités de retard 3 fois le taux d'intérêt légal\nEn cas de retard de paiement, application d'une indemnité forfaitaire pour frais de recouvrement de 40 € selon l'article D. 441-5 du code du commerce.\n{CompanyName} {CompanyType} au capital de {CompanyCapital} - SIRET {CompanyRegistrationNumber} - APE {CompanyClassification} - {CompanyRegister} - TVA {CompanyTaxNumber}\n")." WHERE Type ='GENERAL' AND Name = 'InvoiceNote'");
                        break;
                    case 'FR_EI':
                        $pdo->query("UPDATE hlx_settings SET Value = ".$pdo->quote("Aucun escompte ne sera accordé\nPénalités de retard 3 fois le taux d'intérêt légal\nEn cas de retard de paiement, application d'une indemnité forfaitaire pour frais de recouvrement de 40 € selon l'article D. 441-5 du code du commerce.\n{CompanyName} {CompanyType} - SIRET {CompanyRegistrationNumber} - APE {CompanyClassification} - {CompanyRegister} - TVA {CompanyTaxNumber}\n")." WHERE Type ='GENERAL' AND Name = 'QuoteNote'");
                        $pdo->query("UPDATE hlx_settings SET Value = ".$pdo->quote("Aucun escompte ne sera accordé\nPénalités de retard 3 fois le taux d'intérêt légal\nEn cas de retard de paiement, application d'une indemnité forfaitaire pour frais de recouvrement de 40 € selon l'article D. 441-5 du code du commerce.\n{CompanyName} {CompanyType} - SIRET {CompanyRegistrationNumber} - APE {CompanyClassification} - {CompanyRegister} - TVA {CompanyTaxNumber}\n")." WHERE Type ='GENERAL' AND Name = 'OrderNote'");
                        $pdo->query("UPDATE hlx_settings SET Value = ".$pdo->quote("Aucun escompte ne sera accordé\nPénalités de retard 3 fois le taux d'intérêt légal\nEn cas de retard de paiement, application d'une indemnité forfaitaire pour frais de recouvrement de 40 € selon l'article D. 441-5 du code du commerce.\n{CompanyName} {CompanyType} - SIRET {CompanyRegistrationNumber} - APE {CompanyClassification} - {CompanyRegister} - TVA {CompanyTaxNumber}\n")." WHERE Type ='GENERAL' AND Name = 'DepositNote'");
                        $pdo->query("UPDATE hlx_settings SET Value = ".$pdo->quote("Aucun escompte ne sera accordé\nPénalités de retard 3 fois le taux d'intérêt légal\nEn cas de retard de paiement, application d'une indemnité forfaitaire pour frais de recouvrement de 40 € selon l'article D. 441-5 du code du commerce.\n{CompanyName} {CompanyType} - SIRET {CompanyRegistrationNumber} - APE {CompanyClassification} - {CompanyRegister} - TVA {CompanyTaxNumber}\n")." WHERE Type ='GENERAL' AND Name = 'InvoiceNote'");
                        break;
                    case 'FR_MICRO':
                        $pdo->query("UPDATE hlx_settings SET Value = ".$pdo->quote("Aucun escompte ne sera accordé\nPénalités de retard 3 fois le taux d'intérêt légal\nEn cas de retard de paiement, application d'une indemnité forfaitaire pour frais de recouvrement de 40 € selon l'article D. 441-5 du code du commerce.\n{CompanyName} {CompanyType} - SIRET {CompanyRegistrationNumber} - APE {CompanyClassification} - {CompanyRegister} - Franchise de TVA, art. 293B du CGI\n")." WHERE Type ='GENERAL' AND Name = 'QuoteNote'");
                        $pdo->query("UPDATE hlx_settings SET Value = ".$pdo->quote("Aucun escompte ne sera accordé\nPénalités de retard 3 fois le taux d'intérêt légal\nEn cas de retard de paiement, application d'une indemnité forfaitaire pour frais de recouvrement de 40 € selon l'article D. 441-5 du code du commerce.\n{CompanyName} {CompanyType} - SIRET {CompanyRegistrationNumber} - APE {CompanyClassification} - {CompanyRegister} - Franchise de TVA, art. 293B du CGI\n")." WHERE Type ='GENERAL' AND Name = 'OrderNote'");
                        $pdo->query("UPDATE hlx_settings SET Value = ".$pdo->quote("Aucun escompte ne sera accordé\nPénalités de retard 3 fois le taux d'intérêt légal\nEn cas de retard de paiement, application d'une indemnité forfaitaire pour frais de recouvrement de 40 € selon l'article D. 441-5 du code du commerce.\n{CompanyName} {CompanyType} - SIRET {CompanyRegistrationNumber} - APE {CompanyClassification} - {CompanyRegister} - Franchise de TVA, art. 293B du CGI\n")." WHERE Type ='GENERAL' AND Name = 'DepositNote'");
                        $pdo->query("UPDATE hlx_settings SET Value = ".$pdo->quote("Aucun escompte ne sera accordé\nPénalités de retard 3 fois le taux d'intérêt légal\nEn cas de retard de paiement, application d'une indemnité forfaitaire pour frais de recouvrement de 40 € selon l'article D. 441-5 du code du commerce.\n{CompanyName} {CompanyType} - SIRET {CompanyRegistrationNumber} - APE {CompanyClassification} - {CompanyRegister} - Franchise de TVA, art. 293B du CGI\n")." WHERE Type ='GENERAL' AND Name = 'InvoiceNote'");
                        break;
                }
            }
            $data['save'] = true;
        }

        $data['urlForm']    = BFWK_SERVER_ROOT.'?/setup/complete';
        $data['urlLogin']   = BFWK_SERVER_ROOT.'?/main/login';

        return $data;
    }

    private function createTables($dsn, $user, $pass)
    {
        try {

            $pdo = new PDO($dsn, $user, $pass);
            if ( ! $pdo)
            {
                $this->errors = 1;
                return false;
            }

            $rSearch = array(
                '$Draft',
                '$Validated',
                '$Sent',
                '$Viewed',
                '$Approved',
                '$Rejected',
                '$Canceled',
                '$Closed',
                '$Paid',
                '$Invoiced',
                '$PendingReceipt',
                '$Received',
                '$ExportSells',
                '$ExportSellDesc',
                '$StartDate',
                '$EndDate',
                '$DatePayment',
                '$Reference',
                '$Customer',
                '$Nature',
                '$Amount',
                '$PaymentMethod',
                '$ExportPurchases',
                '$ExportPurchaseDesc',
                '$Supplier',
                '$DatePayment',
                '$Reference',
                '$Supplier',
                '$Nature',
                '$Amount',
                '$PaymentMethod',
                '$Product',
                '$Service',
                '$Transfer',
                '$Piece'
            );
            $rReplace = array(
                t('Draft'),
                t('Validated'),
                t('Sent'),
                t('Viewed'),
                t('Approved'),
                t('Rejected'),
                t('Canceled'),
                t('Closed'),
                t('Paid'),
                t('Invoiced'),
                t('PendingReceipt'),
                t('Received'),
                t('ExportSells'),
                t('ExportSellDesc'),
                t('StartDate'),
                t('EndDate'),
                t('DatePayment'),
                t('Reference'),
                t('Customer'),
                t('Nature'),
                t('Amount'),
                t('PaymentMethod'),
                t('ExportPurchases'),
                t('ExportPurchaseDesc'),
                t('Supplier'),
                t('DatePayment'),
                t('Reference'),
                t('Supplier'),
                t('Nature'),
                t('Amount'),
                t('PaymentMethod'),
                t('Product'),
                t('Service'),
                t('Transfer'),
                t('Piece')
            );

            $rSearch[] = '$Lang';
            $rReplace[] = $_SESSION['lang'];

            //TODO see what country use same as fr
            if (in_array($_SESSION['lang'], array('FR', 'BE', 'ES', 'IT')))
            {
                $rSearch[] = '$FirstDayOfWeek';
                $rReplace[] = 1;
                $rSearch[] = '$DateFormat';
                $rReplace[] = 'd-m-Y';
                $rSearch[] = '$CurrencySymbol';
                $rReplace[] = '€';
                $rSearch[] = '$CurrencyPlacement';
                $rReplace[] = 'after';
                $rSearch[] = '$Decimals';
                $rReplace[] = 2;
                $rSearch[] = '$DecimalSeparator';
                $rReplace[] = ',';
            }
            else
            {
                $rSearch[] = '$FirstDayOfWeek';
                $rReplace[] = 0;
                $rSearch[] = '$DateFormat';
                $rReplace[] = 'm-d-Y';
                $rSearch[] = '$CurrencySymbol';
                $rReplace[] = '$';
                $rSearch[] = '$CurrencyPlacement';
                $rReplace[] = 'before';
                $rSearch[] = '$Decimals';
                $rReplace[] = 2;
                $rSearch[] = '$DecimalSeparator';
                $rReplace[] = '.';
            }

            if (substr($dsn, 0, 6) == 'sqlite')
            {
                $sql = file_get_contents('store/sql/sqlite.database.sql');
                //replace some values
                $sql = str_replace($rSearch, $rReplace, $sql);
                $pdo->exec($sql);
            }
            elseif (substr($dsn, 0, 5) == 'mysql')
            {
                $sql = file_get_contents('store/sql/mysql.database.sql');
                //replace some values
                $sql = str_replace($rSearch, $rReplace, $sql);
                $pdo->exec($sql);
            }

            //add language into DB
            $rLanguage = get_languages();
            foreach ($rLanguage as $code => $libelle)
            {
                $sql = "INSERT INTO hlx_settings VALUES (NULL, 'LANGUAGE', ".$pdo->quote($libelle).", ".$pdo->quote($code).", '0000-00-00', '0000-00-00')";
                $pdo->query($sql);
            }

            //add taxes if fr && TODO if Taxe enabled
            if ($_SESSION['lang'] == 'FR')
            {
                $sql = "INSERT INTO hlx_taxes VALUES
                    (1, 'TVA 2.1%', 2.10, '0000-00-00', '0000-00-00'),
                    (3, 'TVA 20 %', 20.00, '0000-00-00', '0000-00-00'),
                    (2, 'TVA 5,5 %', 5.50, '0000-00-00', '0000-00-00')";
                $pdo->query($sql);
            }

            return true;

        }
        catch(Exception $e) {

            $this->errors = 1;
            return false;

        }
    }

    private function checkWritables()
    {
        $checks = array();
        /*
        $writables = array(
            '.',
            './store',
            './store/db',
            './store/documents',
            './store/documents/deposit',
            './store/documents/invoice',
            './store/documents/order',
            './store/documents/purchase',
            './store/documents/quote',
            './store/logo',
            './store/templates',
            './store/templates/invoice',
            './store/templates/order',
            './store/templates/purchase',
            './store/templates/quote',
            './config/hlx.conf.php'
        );

        foreach ($writables as $writable) {
            if ( ! is_writable($writable)) {
                $checks[] = array(
                    'message' => $writable . ' ' . t('isNotWritable'),
                    'success' => 0
                );
                $error = true;
                $this->errors += 1;
                break;
            }
        }
        if ($error == false) {
            $checks[] = array(
                'message' => t('AllDirAreWritable'),
                'success' => 1
            );
        }
        */

        if ( ! is_writable_r('.')) {
            $checks[] = array(
                'message' => t('SomeDirAreNotWritable'),
                'success' => 0
            );
            $this->errors += 1;
        }
        else {
            $checks[] = array(
                'message' => t('AllDirAreWritable'),
                'success' => 1
            );
        }

        return $checks;
    }

    private function checkBasics()
    {
        $checks = array();

        $php_required = '5.3';
        $php_installed = PHP_MAJOR_VERSION . '.' . PHP_MINOR_VERSION;

        if ($php_installed < $php_required) {
            $this->errors += 1;

            $checks[] = array(
                'message' => t('PHPVersionFail', $php_installed, $php_required),
                'success' => 0,
                'warning' => 0
            );
        } else {
            $checks[] = array(
                'message' => t('PHPVersionSuccess'),
                'success' => 1,
                'warning' => 0
            );
        }

        if ( ! ini_get('date.timezone')) {
            $checks[] = array(
                'message' => t('PHPTimezoneWarn', date_default_timezone_get()),
                'success' => 0,
                'warning' => 1
            );
        } else {
            $checks[] = array(
                'message' => t('PHPTimezoneSuccess'),
                'success' => 1,
                'warning' => 0
            );
        }

        if ( ! extension_loaded('zip')) {
            $checks[] = array(
                'message' => t('PHPZIPWarn'),
                'success' => 0,
                'warning' => 1
            );
        } else {
            $checks[] = array(
                'message' => t('PHPZIPSuccess'),
                'success' => 1,
                'warning' => 0
            );
        }

        if ( ! extension_loaded('pdo')) {
            $checks[] = array(
                'message' => t('PHPPDOFail'),
                'success' => 0,
                'warning' => 0
            );
        } else {
            $checks[] = array(
                'message' => t('PHPPDOSuccess'),
                'success' => 1,
                'warning' => 0
            );
        }

        return $checks;
    }

    private function checkDatabase($dsn, $user, $pass)
    {
        //Create DB connection
        try {

            $pdo = new PDO($dsn, $user, $pass);
            if ( ! $pdo)
            {
                $this->errors = 1;
                return array(
                    'message' => t('CannotConnectToDatabase'),
                    'success' => 0,
                    'warning' => 0
                );
            }

        }
        catch(Exception $e) {

            $this->errors = 1;
            return array(
                'message' => t('CannotConnectToDatabase'),
                'success' => 0,
                'warning' => 0
            );

        }

        return array(
            'message' => t('DatabaseProperlyConfigured'),
            'success' => 1,
            'warning' => 0
        );

    }

    private function writeDatabaseConfig($dsn, $user, $pass)
    {
        $confFile = "<?php

/**
 * DSN for database
 */
define('HLX_DBDSN', '".addslashes($dsn)."');

/**
 * User for database
 */
define('HLX_DBUSER', '".addslashes($user)."');

/**
 * Pass for database
 */
define('HLX_DBPWD', '".addslashes($pass)."');";

        return file_put_contents('config/hlx.conf.php', $confFile);

    }

}