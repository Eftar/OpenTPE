<?php

/**
 *
 * */

class Setting extends HLX_Controller
{

    function edit($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        //TODO add message erreur de role
        if ( ! $this->hlx_motor->user_isAdmin())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/home');

        $data = array();

        require_once('models/settingModel.php');
        $data['settings'] = SettingModel::load(array('GENERAL', 'PAYMENT', 'LANGUAGE', 'UNITY'), false);
        $data['settings']['GENERAL']['SMTPPassword'] = '';

        $data['quoteTemplates']     = SettingModel::getDocumentTemplates('quote');
        $data['orderTemplates']     = SettingModel::getDocumentTemplates('order');
        $data['invoiceTemplates']   = SettingModel::getDocumentTemplates('invoice');
        $data['purchaseTemplates']  = SettingModel::getDocumentTemplates('purchase');

        $data['companyLogo']        = SettingModel::getLogo();

        require_once('libs/HLX_Country.class.php');
        $data['countries'] = HLX_Country::getCountryList();

        require_once('models/taxModel.php');
        $data['taxes'] = TaxModel::loadAll(false);

        require_once('models/documentModel.php');
        require_once('models/documentStatusModel.php');
        $data['quoteStatutes']      = DocumentStatusModel::loadAll(DocumentModel::DOC_TYPE_QUOTE);
        $data['orderStatutes']      = DocumentStatusModel::loadAll(DocumentModel::DOC_TYPE_ORDER);
        $data['invoiceStatutes']    = DocumentStatusModel::loadAll(DocumentModel::DOC_TYPE_INVOICE);
        $data['purchaseStatutes']   = DocumentStatusModel::loadAll(DocumentModel::DOC_TYPE_PURCHASE);

        //Options for navBar
        $data['navBar']         = navBarVars('setting');
        $data['urlUpdate']      = BFWK_SERVER_ROOT.'?/update/index';
        $data['urlUpdateJson']  = BFWK_SERVER_ROOT.'?/update/getUpdateJson';
        $data['urlBackup']      = BFWK_SERVER_ROOT.'?/setting/backup';
        $data['urlRemoveLogo']  = BFWK_SERVER_ROOT.'?/setting/removeLogo';
        $data['urlSave']        = BFWK_SERVER_ROOT.'?/setting/save';
        $data['urlCancel']      = BFWK_SERVER_ROOT.'?/setting/edit';

        return $data;

    }

    function save($args)
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        //TODO add message erreur de role
        if ( ! $this->hlx_motor->user_isAdmin())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/home');

        require_once('models/settingModel.php');

        $data['GENERAL']['Language'] = (isset($_POST['Language']) ? $_POST['Language'] : '');
        $data['GENERAL']['FirstDayOfWeek'] = (isset($_POST['FirstDayOfWeek']) ? $_POST['FirstDayOfWeek'] : '');
        $data['GENERAL']['DateFormat'] = (isset($_POST['DateFormat']) ? $_POST['DateFormat'] : '');
        $data['GENERAL']['CurrencySymbol'] = (isset($_POST['CurrencySymbol']) ? $_POST['CurrencySymbol'] : '');
        $data['GENERAL']['CurrencyPlacement'] = (isset($_POST['CurrencyPlacement']) ? $_POST['CurrencyPlacement'] : '');
        $data['GENERAL']['Decimals'] = (isset($_POST['Decimals']) ? $_POST['Decimals'] : '');
        $data['GENERAL']['DecimalSeparator'] = (isset($_POST['DecimalSeparator']) ? $_POST['DecimalSeparator'] : '');
        $data['GENERAL']['UseBillingAddress'] = (isset($_POST['UseBillingAddress']) ? $_POST['UseBillingAddress'] : '');
        $data['GENERAL']['UseDeliveryAddress'] = (isset($_POST['UseDeliveryAddress']) ? $_POST['UseDeliveryAddress'] : '');
        $data['GENERAL']['UseDiscount'] = (isset($_POST['UseDiscount']) ? $_POST['UseDiscount'] : '');
        $data['GENERAL']['UseVAT'] = (isset($_POST['UseVAT']) ? $_POST['UseVAT'] : '');
        $data['GENERAL']['CompanyName'] = (isset($_POST['CompanyName']) ? $_POST['CompanyName'] : '');
        $data['GENERAL']['CompanyType'] = (isset($_POST['CompanyType']) ? $_POST['CompanyType'] : '');
        $data['GENERAL']['CompanyCapital'] = (isset($_POST['CompanyCapital']) ? $_POST['CompanyCapital'] : '');
        $data['GENERAL']['CompanyRegistrationNumber'] = (isset($_POST['CompanyRegistrationNumber']) ? $_POST['CompanyRegistrationNumber'] : '');
        $data['GENERAL']['CompanyRegister'] = (isset($_POST['CompanyRegister']) ? $_POST['CompanyRegister'] : '');
        $data['GENERAL']['CompanyClassification'] = (isset($_POST['CompanyClassification']) ? $_POST['CompanyClassification'] : '');
        $data['GENERAL']['CompanyTaxNumber'] = (isset($_POST['CompanyTaxNumber']) ? $_POST['CompanyTaxNumber'] : '');
        $data['GENERAL']['CompanyAddress1'] = (isset($_POST['CompanyAddress1']) ? $_POST['CompanyAddress1'] : '');
        $data['GENERAL']['CompanyAddress2'] = (isset($_POST['CompanyAddress2']) ? $_POST['CompanyAddress2'] : '');
        $data['GENERAL']['CompanyAddress3'] = (isset($_POST['CompanyAddress3']) ? $_POST['CompanyAddress3'] : '');
        $data['GENERAL']['CompanyPostalCode'] = (isset($_POST['CompanyPostalCode']) ? $_POST['CompanyPostalCode'] : '');
        $data['GENERAL']['CompanyCity'] = (isset($_POST['CompanyCity']) ? $_POST['CompanyCity'] : '');
        $data['GENERAL']['CompanyState'] = (isset($_POST['CompanyState']) ? $_POST['CompanyState'] : '');
        $data['GENERAL']['CompanyCountryID'] = (isset($_POST['CompanyCountryID']) ? $_POST['CompanyCountryID'] : '');
        $data['GENERAL']['CompanyPhone'] = (isset($_POST['CompanyPhone']) ? $_POST['CompanyPhone'] : '');
        $data['GENERAL']['CompanyFax'] = (isset($_POST['CompanyFax']) ? $_POST['CompanyFax'] : '');
        $data['GENERAL']['CompanyEmail'] = (isset($_POST['CompanyEmail']) ? $_POST['CompanyEmail'] : '');
        $data['GENERAL']['CompanyWeb'] = (isset($_POST['CompanyWeb']) ? $_POST['CompanyWeb'] : '');
        $data['GENERAL']['QuoteReferenceFormat'] = (isset($_POST['QuoteReferenceFormat']) ? $_POST['QuoteReferenceFormat'] : '');
        $data['GENERAL']['QuoteTaxDefault'] = (isset($_POST['QuoteTaxDefault']) ? $_POST['QuoteTaxDefault'] : '');
        $data['GENERAL']['QuoteExpirationDelay'] = (isset($_POST['QuoteExpirationDelay']) ? $_POST['QuoteExpirationDelay'] : '');
        $data['GENERAL']['QuoteExpirationDelay'] = (isset($_POST['QuoteExpirationDelay']) ? $_POST['QuoteExpirationDelay'] : '');
        $data['GENERAL']['QuoteNote'] = (isset($_POST['QuoteNote']) ? $_POST['QuoteNote'] : '');
        $data['GENERAL']['QuoteDefaultStatus'] = (isset($_POST['QuoteDefaultStatus']) ? $_POST['QuoteDefaultStatus'] : '');
        $data['GENERAL']['QuoteTemplate'] = (isset($_POST['QuoteTemplate']) ? $_POST['QuoteTemplate'] : '');
        $data['GENERAL']['OrderReferenceFormat'] = (isset($_POST['OrderReferenceFormat']) ? $_POST['OrderReferenceFormat'] : '');
        $data['GENERAL']['OrderTaxDefault'] = (isset($_POST['OrderTaxDefault']) ? $_POST['OrderTaxDefault'] : '');
        $data['GENERAL']['OrderExpirationDelay'] = (isset($_POST['OrderExpirationDelay']) ? $_POST['OrderExpirationDelay'] : '');
        $data['GENERAL']['OrderExpirationDelay'] = (isset($_POST['OrderExpirationDelay']) ? $_POST['OrderExpirationDelay'] : '');
        $data['GENERAL']['OrderNote'] = (isset($_POST['OrderNote']) ? $_POST['OrderNote'] : '');
        $data['GENERAL']['OrderDefaultStatus'] = (isset($_POST['OrderDefaultStatus']) ? $_POST['OrderDefaultStatus'] : '');
        $data['GENERAL']['OrderTemplate'] = (isset($_POST['OrderTemplate']) ? $_POST['OrderTemplate'] : '');
        $data['GENERAL']['DepositExpirationDelay'] = (isset($_POST['DepositExpirationDelay']) ? $_POST['DepositExpirationDelay'] : '');
        $data['GENERAL']['DepositNote'] = (isset($_POST['DepositNote']) ? $_POST['DepositNote'] : '');
        $data['GENERAL']['DepositDefaultUnity'] = (isset($_POST['DepositDefaultUnity']) ? $_POST['DepositDefaultUnity'] : '');
        $data['GENERAL']['InvoiceReferenceFormat'] = (isset($_POST['InvoiceReferenceFormat']) ? $_POST['InvoiceReferenceFormat'] : '');
        $data['GENERAL']['InvoiceTaxDefault'] = (isset($_POST['InvoiceTaxDefault']) ? $_POST['InvoiceTaxDefault'] : '');
        $data['GENERAL']['InvoiceDueDelay'] = (isset($_POST['InvoiceDueDelay']) ? $_POST['InvoiceDueDelay'] : '');
        $data['GENERAL']['InvoiceNote'] = (isset($_POST['InvoiceNote']) ? $_POST['InvoiceNote'] : '');
        $data['GENERAL']['InvoiceDefaultStatus'] = (isset($_POST['InvoiceDefaultStatus']) ? $_POST['InvoiceDefaultStatus'] : '');
        $data['GENERAL']['InvoiceNote'] = (isset($_POST['InvoiceNote']) ? $_POST['InvoiceNote'] : '');
        $data['GENERAL']['InvoiceTemplate'] = (isset($_POST['InvoiceTemplate']) ? $_POST['InvoiceTemplate'] : '');
        $data['GENERAL']['PurchaseReferenceFormat'] = (isset($_POST['PurchaseReferenceFormat']) ? $_POST['PurchaseReferenceFormat'] : '');
        $data['GENERAL']['PurchaseTaxDefault'] = (isset($_POST['PurchaseTaxDefault']) ? $_POST['PurchaseTaxDefault'] : '');
        $data['GENERAL']['PurchaseDefaultStatus'] = (isset($_POST['PurchaseDefaultStatus']) ? $_POST['PurchaseDefaultStatus'] : '');
        $data['GENERAL']['PurchaseTemplate'] = (isset($_POST['PurchaseTemplate']) ? $_POST['PurchaseTemplate'] : '');
        $data['GENERAL']['ProductDefaultTaxSell'] = (isset($_POST['ProductDefaultTaxSell']) ? $_POST['ProductDefaultTaxSell'] : '');
        $data['GENERAL']['ProductDefaultTaxBuy'] = (isset($_POST['ProductDefaultTaxBuy']) ? $_POST['ProductDefaultTaxBuy'] : '');
        $data['GENERAL']['ProductDefaultUnity'] = (isset($_POST['ProductDefaultUnity']) ? $_POST['ProductDefaultUnity'] : '');
        $data['GENERAL']['MailCopyAdmin'] = (isset($_POST['MailCopyAdmin']) ? $_POST['MailCopyAdmin'] : '');
        $data['GENERAL']['MailMethod'] = (isset($_POST['MailMethod']) ? $_POST['MailMethod'] : '');
        $data['GENERAL']['SMTPURL'] = (isset($_POST['SMTPURL']) ? $_POST['SMTPURL'] : '');
        $data['GENERAL']['SMTPNeedAuth'] = (isset($_POST['SMTPNeedAuth']) ? $_POST['SMTPNeedAuth'] : '');
        $data['GENERAL']['SMTPUser'] = (isset($_POST['SMTPUser']) ? $_POST['SMTPUser'] : '');
        $data['GENERAL']['SMTPPassword'] = (isset($_POST['SMTPPassword']) && $_POST['SMTPPassword'] != '' ? $_POST['SMTPPassword'] : '');
        if (isset($_POST['SMTPPort']) && $_POST['SMTPPort'] != '')
            $data['GENERAL']['SMTPPort'] = $_POST['SMTPPort'];
        $data['GENERAL']['SMTPSecurity'] = (isset($_POST['SMTPSecurity']) ? $_POST['SMTPSecurity'] : '');

        if (isset($_POST['IDPayment']) && is_array($_POST['IDPayment'])
            && isset($_POST['NamePayment']) && is_array($_POST['NamePayment'])
            && isset($_POST['ActiveFromPayment']) && is_array($_POST['ActiveFromPayment'])
            && isset($_POST['ActiveToPayment']) && is_array($_POST['ActiveToPayment']))
        {
            $data['PAYMENT'] = array();
            $i = 0;
            $c = min(count($_POST['IDPayment']), count($_POST['NamePayment']), count($_POST['ActiveFromPayment']), count($_POST['ActiveToPayment']));
            while ($i < $c)
            {
                if ($_POST['NamePayment'][ $i ] != '')
                {
                    $data['PAYMENT'][] = array(
                        'ID'            => (isset($_POST['IDPayment'][ $i ]) ? $_POST['IDPayment'][ $i ] : ''),
                        'Name'          => (isset($_POST['NamePayment'][ $i ]) ? $_POST['NamePayment'][ $i ] : ''),
                        'Value'         => (isset($_POST['NamePayment'][ $i ]) ? $_POST['NamePayment'][ $i ] : ''),
                        'ActiveFrom'    => (isset($_POST['ActiveFromPayment'][ $i ]) ? $_POST['ActiveFromPayment'][ $i ] : ''),
                        'ActiveTo'      => (isset($_POST['ActiveToPayment'][ $i ]) ? $_POST['ActiveToPayment'][ $i ] : '')
                    );
                }
                $i++;
            }
        }
        if (isset($_POST['IDUnity']) && is_array($_POST['IDUnity'])
            && isset($_POST['NameUnity']) && is_array($_POST['NameUnity'])
            && isset($_POST['ActiveFromUnity']) && is_array($_POST['ActiveFromUnity'])
            && isset($_POST['ActiveToUnity']) && is_array($_POST['ActiveToUnity']))
        {
            $data['UNITY'] = array();
            $i = 0;
            $c = min(count($_POST['IDUnity']), count($_POST['NameUnity']), count($_POST['ActiveFromUnity']), count($_POST['ActiveToUnity']));
            while ($i < $c)
            {
                if ($_POST['NameUnity'][ $i ] != '')
                {
                    $data['UNITY'][] = array(
                        'ID'            => (isset($_POST['IDUnity'][ $i ]) ? $_POST['IDUnity'][ $i ] : ''),
                        'Name'          => (isset($_POST['NameUnity'][ $i ]) ? $_POST['NameUnity'][ $i ] : ''),
                        'Value'         => (isset($_POST['NameUnity'][ $i ]) ? $_POST['NameUnity'][ $i ] : ''),
                        'ActiveFrom'    => (isset($_POST['ActiveFromUnity'][ $i ]) ? $_POST['ActiveFromUnity'][ $i ] : ''),
                        'ActiveTo'      => (isset($_POST['ActiveToUnity'][ $i ]) ? $_POST['ActiveToUnity'][ $i ] : '')
                    );
                }
                $i++;
            }
        }

        SettingModel::save($data);

        require_once('models/taxModel.php');

        $taxes = array();
        if (isset($_POST['IDTax']) && is_array($_POST['IDTax'])
            && isset($_POST['NameTax']) && is_array($_POST['NameTax'])
            && isset($_POST['RateTax']) && is_array($_POST['RateTax'])
            && isset($_POST['ActiveFromTax']) && is_array($_POST['ActiveFromTax'])
            && isset($_POST['ActiveToTax']) && is_array($_POST['ActiveToTax']))
        {
            $i = 0;
            $c = min(count($_POST['IDTax']), count($_POST['NameTax']), count($_POST['RateTax']), count($_POST['ActiveFromTax']), count($_POST['ActiveToTax']));
            while ($i < $c)
            {
                if ($_POST['NameTax'][ $i ] != '' && $_POST['RateTax'][ $i ] != '')
                {
                    $taxes[] = array(
                        'ID'            => (isset($_POST['IDTax'][ $i ]) ? $_POST['IDTax'][ $i ] : ''),
                        'Name'          => (isset($_POST['NameTax'][ $i ]) ? $_POST['NameTax'][ $i ] : ''),
                        'Rate'          => (isset($_POST['RateTax'][ $i ]) ? $_POST['RateTax'][ $i ] : ''),
                        'ActiveFrom'    => (isset($_POST['ActiveFromTax'][ $i ]) ? $_POST['ActiveFromTax'][ $i ] : ''),
                        'ActiveTo'      => (isset($_POST['ActiveToTax'][ $i ]) ? $_POST['ActiveToTax'][ $i ] : '')
                    );
                }
                $i++;
            }
        }

        TaxModel::saveAll($taxes);

        //Upload company logo
        if (isset($_FILES['CompanyLogo']) && $_FILES['CompanyLogo']['error'] == UPLOAD_ERR_OK)
        {

            $finfo = new finfo(FILEINFO_MIME_TYPE);
            $mime = $finfo->file($_FILES['CompanyLogo']['tmp_name']);
            $ext = array_search($mime, array(
                'jpg' => 'image/jpeg',
                'png' => 'image/png',
                'gif' => 'image/gif'
            ));
            if ($ext) {

                move_uploaded_file($_FILES['CompanyLogo']['tmp_name'], HLX_STORE.'logo/logo.'.$ext);
                chmod(HLX_STORE.'logo/logo.'.$ext, 0775);
            }
        }

        header('Location: '.BFWK_SERVER_ROOT.'?/setting/edit');
        exit;

    }

    function removeLogo()
    {

        //TODO add message erreur de role
        if ( ! $this->hlx_motor->user_isAdmin())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/home');

        @unlink(HLX_STORE.'logo/logo.jpg');
        @unlink(HLX_STORE.'logo/logo.png');
        @unlink(HLX_STORE.'logo/logo.gif');

        header('Location: '.BFWK_SERVER_ROOT.'?/setting/edit');
        exit;

    }

    function backup()
    {

        //TODO add message erreur de role
        if ( ! $this->hlx_motor->user_isAdmin())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/home');

        $date = date('YmdHis');

        //Do zip archive of generated document
        $zipFile = HLX_STORE.'backup/backup-'.$date.'.zip';
        $zipArchive = new ZipArchive();
        if ( ! $zipArchive->open($zipFile, ZipArchive::CREATE | ZIPARCHIVE::OVERWRITE))
        {
            echo '{"error": 1,"backup": ""}';
            die;
        }

        //Do dump of sql files
        require_once('libs/HLX_DBBackup.class.php');
        $dbBackup = new HLX_DBBackup(HLX_DBDSN, HLX_DBUSER, HLX_DBPWD);
        $dbBackup->backupTables('*', HLX_STORE.'backup/db-'.HLX_VERSION.'-'.$date.'.sql');
        //Add db to archive
        $zipArchive->addFile(HLX_STORE.'backup/db-'.HLX_VERSION.'-'.$date.'.sql', 'db-'.HLX_VERSION.'-'.$date.'.sql');

        //Do zip archive of data in csv
        $this->exportData('customers', HLX_STORE.'backup/customers-'.HLX_VERSION.'-'.$date.'.csv');
        $this->exportData('suppliers', HLX_STORE.'backup/suppliers-'.HLX_VERSION.'-'.$date.'.csv');
        $this->exportData('contacts', HLX_STORE.'backup/contacts-'.HLX_VERSION.'-'.$date.'.csv');
        $this->exportData('documents', HLX_STORE.'backup/documents-'.HLX_VERSION.'-'.$date.'.csv');
        $this->exportData('payments', HLX_STORE.'backup/payments-'.HLX_VERSION.'-'.$date.'.csv');
        $this->exportData('products', HLX_STORE.'backup/products-'.HLX_VERSION.'-'.$date.'.csv');
        //Add csv to archive
        $zipArchive->addFile(HLX_STORE.'backup/customers-'.HLX_VERSION.'-'.$date.'.csv',    'customers-'.HLX_VERSION.'-'.$date.'.csv');
        $zipArchive->addFile(HLX_STORE.'backup/suppliers-'.HLX_VERSION.'-'.$date.'.csv',    'suppliers-'.HLX_VERSION.'-'.$date.'.csv');
        $zipArchive->addFile(HLX_STORE.'backup/contacts-'.HLX_VERSION.'-'.$date.'.csv',     'contacts-'.HLX_VERSION.'-'.$date.'.csv');
        $zipArchive->addFile(HLX_STORE.'backup/documents-'.HLX_VERSION.'-'.$date.'.csv',    'documents-'.HLX_VERSION.'-'.$date.'.csv');
        $zipArchive->addFile(HLX_STORE.'backup/payments-'.HLX_VERSION.'-'.$date.'.csv',     'payments-'.HLX_VERSION.'-'.$date.'.csv');
        $zipArchive->addFile(HLX_STORE.'backup/products-'.HLX_VERSION.'-'.$date.'.csv',     'products-'.HLX_VERSION.'-'.$date.'.csv');

        $rootPath = realpath(HLX_STORE.'/documents/');

        // Create recursive directory iterator
        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($rootPath),
            RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file)
        {
            if ( ! $file->isDir())
            {
                if (basename($name) != 'index.html')
                {
                    // Get real and relative path for current file
                    $filePath = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($rootPath) + 1);

                    // Add current file to archive
                    $zipArchive->addFile($filePath, $relativePath);
                }
            }
        }

        $zipArchive->close();

        unlink(HLX_STORE.'backup/db-'.HLX_VERSION.'-'.$date.'.sql');
        unlink(HLX_STORE.'backup/customers-'.HLX_VERSION.'-'.$date.'.csv');
        unlink(HLX_STORE.'backup/suppliers-'.HLX_VERSION.'-'.$date.'.csv');
        unlink(HLX_STORE.'backup/contacts-'.HLX_VERSION.'-'.$date.'.csv');
        unlink(HLX_STORE.'backup/documents-'.HLX_VERSION.'-'.$date.'.csv');
        unlink(HLX_STORE.'backup/payments-'.HLX_VERSION.'-'.$date.'.csv');
        unlink(HLX_STORE.'backup/products-'.HLX_VERSION.'-'.$date.'.csv');

        echo '{"error":0,"file": "'.$zipFile.'"}';
        die;

    }

    private function exportData($type, $filename) {

        $sql = false;

        switch($type) {
            case 'customers':
                $sql = "SELECT * FROM hlx_thirdparties WHERE Type = 'C'";
                break;
            case 'suppliers':
                $sql = "SELECT * FROM hlx_thirdparties WHERE Type = 'S'";
                break;
            case 'contacts':
                $sql = "SELECT * FROM hlx_contacts";
                break;
            case 'documents':
                $sql = "SELECT * FROM hlx_documents AS d LEFT JOIN hlx_documents_lines AS dl ON d.ID = dl.DocumentID";
                break;
            case 'payments':
                $sql = "SELECT * FROM hlx_payments AS p LEFT JOIN hlx_documents AS d ON p.DocumentID = d.ID";
                break;
            case 'products':
                $sql = "SELECT * FROM hlx_products";
                break;
        }

        if ($sql)
        {
            $db = HLX_Motor::getInstance()->getDatabase();
            $res = $db->query($sql);
            $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC) : false;
            if ( $rows === false)
            {
                return false;
            }

            $csv = '';
            foreach($rows as $row) {
                if ($csv == '')
                {
                    foreach ($row as $key => $col) {
                        $csv .= '"'.str_replace('"', '""', $key).'";';
                    }
                    $csv .= "\n";
                }
                foreach ($row as $col) {
                    $csv .= '"'.str_replace('"', '""', $col).'";';
                }
                $csv .= "\n";
            }

            file_put_contents($filename, $csv);
            return true;
        }
        return false;
    }

}