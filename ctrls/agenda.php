<?php

/**
 *
 * */

class Agenda extends HLX_Controller
{

    function index($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();

        //Options for navBar
        $data['navBar'] = navBarVars('agenda');
        $data['header']['headEnd'] = '<link rel="stylesheet" href="views/css/fullcalendar.css" type="text/css" />
    <script type="text/javascript" src="views/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="views/js/fullcalendar.min.js"></script>
    <script type="text/javascript" src="views/js/fullcalendar-lang-all.js"></script>';

        return $data;

    }

}
