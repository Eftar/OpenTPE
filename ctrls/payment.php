<?php

/**
 *
 * */

class Payment extends HLX_Controller
{

    function index($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();

        if (isset($_SESSION['msg']) && $_SESSION['msg'] != '')//TODO find another way to carry message
        {
            $data['msg'] = htmlspecialchars(t($_SESSION['msg']));
            unset($_SESSION['msg']);
        }
        if (isset($_SESSION['err']) && $_SESSION['err'] != '')
        {
            $data['err'] = htmlspecialchars(t($_SESSION['err']));
            unset($_SESSION['err']);
        }

        //Options for navBar
        $data['navBar'] = navBarVars('payment');

        require_once('models/settingModel.php');
        $data['settings'] = SettingModel::load(array('GENERAL'));

        $data['urlLoadList']        = BFWK_SERVER_ROOT.'?/payment/loadList/';
        $data['urlNewPayment']      = BFWK_SERVER_ROOT.'?/payment/newPayment/';
        $data['urlViewInvoice']     = BFWK_SERVER_ROOT.'?/document/edit/invoice/';
        $data['urlViewPurchase']    = BFWK_SERVER_ROOT.'?/document/editPurchase/';
        $data['urlViewCustomer']    = BFWK_SERVER_ROOT.'?/thirdParty/viewCustomer/';
        $data['urlViewSupplier']    = BFWK_SERVER_ROOT.'?/thirdParty/viewSupplier/';
        $data['urlViewPayment']     = BFWK_SERVER_ROOT.'?/payment/viewPayment/';

        return $data;

    }

    function newPayment($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();

        require_once('models/documentModel.php');

        if ( isset($args[0]) && isset($args[1]))
        {
            if ($args[0] == 'thirdParty')
            {
                $thirdPartyID = (int) $args[1];
                //Load not paid documents of this customer
                $data['documents'] = documentModel::loadUnpaidDocuments($thirdPartyID);
            }
            else
            {
                $documentID = (int) $args[1];
                $data['documents'] = array();
                $document = documentModel::load($documentID, false);
                $data['documents'][] = $document;
            }
        }
        else
        {
            //Load not paid documents
            $data['documents'] = documentModel::loadUnpaidDocuments();
        }

        require_once('models/settingModel.php');
        $data['settings']  = SettingModel::load(array('GENERAL', 'PAYMENT'));

        //Options for navBar
        $data['navBar'] = navBarVars('payment');
        $data['urlSave'] = BFWK_SERVER_ROOT.'?/payment/savePayment/';

        return $data;

    }

    function viewPayment($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();

        if ( ! isset($args[0]))
        {
            //TODO error
        }

        require_once('models/paymentModel.php');
        $data['item']  = PaymentModel::load($args[0]);

        if ( ! $data['item'])
        {
            //TODO error
        }

        require_once('models/documentModel.php');
        $data['documents'] = array();
        $data['documents'][] = documentModel::load($data['item']['DocumentID'], false);
        $data['thirdPartyID'] = $data['documents'][0]['ThirdPartyID'];

        require_once('models/settingModel.php');
        $data['settings']  = SettingModel::load(array('GENERAL', 'PAYMENT'));

        //Options for navBar
        $data['navBar']         = navBarVars('payment');
        $data['urlViewInvoice'] = BFWK_SERVER_ROOT.'?/document/edit/invoice/';

        return $data;

    }

    function deletePayment($args)
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        if ( ! isset($args[0]))
        {
            //TODO error
        }
        else
            $id = (int) $args[0];

        require_once('models/paymentModel.php');
        $res = PaymentModel::delete($id);

        if ($res === true)
        {
            $_SESSION['msg'] = 'Payment deleted';
            header('Location: '.BFWK_SERVER_ROOT.'?/payment/index');
        }
        else
        { //TODO si mode édition retour sur la page édition non pas accueil
            $_SESSION['err'] = 'Error deleting payement';
            header('Location: '.BFWK_SERVER_ROOT.'?/payment/index');
        }
        die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
    }

    function savePayment()
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        //TODO need check Token

        require_once('models/paymentModel.php');
        $res = PaymentModel::save($_POST);

        if ($res == false)
        {
            //TODO error
        }

        header('Location: '.BFWK_SERVER_ROOT.'?/payment/index/');
        return;
    }

    /**
     * Ajax call for main List
     * */
    function loadList()
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        //En fonction de Post récupère les données
        $args = $_POST;
        require_once('models/paymentModel.php');
        echo json_encode(PaymentModel::getList($args));

        exit;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer

    }

}
