<?php

/**
 *
 * */

class Help extends HLX_Controller
{

    function documentation($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();

        //Options for navBar
        $data['navBar'] = navBarVars('documentation');

        //switch according language
        if ($this->settings['GENERAL']['Language'] != '')
        {
            if ($this->settings['GENERAL']['Language'] == 'FR')
            {
                $data['urlPDF'] = '../../../../help/helpfr.pdf';
                $this->_display('help_fr', $data);
                die;
            }
            elseif ($this->settings['GENERAL']['Language'] == 'EN')
            {//TODO add english language
                $data['urlPDF'] = BFWK_SERVER_ROOT.'/help/helpfr.pdf';
                $this->_display('help_fr', $data);
                die;
            }
        }

        //default language
        $this->_display('help_en', $data);
        die;

    }

    function myNote($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();

        if (isset($_SESSION['msg']) && $_SESSION['msg'] != '')//TODO find another way to carry message
        {
            $data['msg'] = htmlspecialchars(t($_SESSION['msg']));
            unset($_SESSION['msg']);
        }
        if (isset($_SESSION['err']) && $_SESSION['err'] != '')
        {
            $data['err'] = htmlspecialchars(t($_SESSION['err']));
            unset($_SESSION['err']);
        }

        //Options for navBar
        $data['navBar'] = navBarVars('myNote');

        require_once('models/noteModel.php');
        $data['rNotes']         = NoteModel::loadAll();
        $data['rTags']          = NoteModel::loadAllTags();
        $data['urlSaveNote']    = BFWK_SERVER_ROOT.'?/help/saveNote/';
        $data['urlDeleteNote']  = BFWK_SERVER_ROOT.'?/help/deleteNote/';

        return $data;

    }

    function about($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();

        //Options for navBar
        $data['navBar'] = navBarVars('about');

        return $data;

    }

    function saveNote($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        require_once('models/noteModel.php');
        $result = NoteModel::save($_POST);

        if ( ! $result)
        {
            $data['error'] = t('ErrorSavingNote');
            $this->_display('error_page', $data);
            die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
        }

        header('Location: '.BFWK_SERVER_ROOT.'?/help/myNote/');

    }

    function deleteNote($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        if ( ! isset($args[0]))
        {
            //TODO error
        }
        else
            $id = (int) $args[0];

        require_once('models/noteModel.php');
        $result = NoteModel::delete($id);

        if ($result === true)
        {
            $_SESSION['msg'] = 'NoteDeleted';
            header('Location: '.BFWK_SERVER_ROOT.'?/help/myNote');
        }
        else
        { //TODO si mode édition retour sur la page édition non pas accueil
            $_SESSION['err'] = 'ErrorDeletingNote';
            header('Location: '.BFWK_SERVER_ROOT.'?/help/myNote');
        }
        die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer

    }

}
