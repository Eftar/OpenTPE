<?php

/**
 *
 * */

class ThirdParty extends HLX_Controller
{

    function index($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();
        //Options for navBar
        $data['navBar'] = navBarVars('thirdParty');

        //Getting default configuration for list
        $data['listOptionType']     = 'customers';
        $data['listOptionFilter']   = 'active';

        if (isset($_COOKIE['thirdPartyList'])) {
            $options = json_decode($_COOKIE['thirdPartyList'], true);
            if ($options)
            {
                $data['listOptionType']     = $options['type'];
                $data['listOptionFilter']   = htmlspecialchars($options['filterBy']);
            }
        }

        if (isset($args[0]) && $args[0] != '') {
            $data['listOptionType'] = $args[0];

            if (isset($args[1]) && $args[1] != '')
                $data['listOptionFilter'] = htmlspecialchars($args[1]);
        }

        if ( ! in_array($data['listOptionType'], array('customers', 'suppliers', 'contacts')))
            $data['listOptionType'] = 'customers';

        require_once('models/settingModel.php');
        $data['settings'] = SettingModel::load(array('GENERAL'));

        $data['urlLoadList']        = BFWK_SERVER_ROOT.'?/thirdParty/loadList/';
        $data['urlNewCustomer']     = BFWK_SERVER_ROOT.'?/thirdParty/newCustomer/';
        $data['urlViewCustomer']    = BFWK_SERVER_ROOT.'?/thirdParty/viewCustomer/';
        $data['urlEditCustomer']    = BFWK_SERVER_ROOT.'?/thirdParty/editCustomer/';
        $data['urlNewContact']      = BFWK_SERVER_ROOT.'?/thirdParty/newContact/';
        $data['urlViewContact']     = BFWK_SERVER_ROOT.'?/thirdParty/viewContact/';
        $data['urlEditContact']     = BFWK_SERVER_ROOT.'?/thirdParty/editContact/';
        $data['urlNewSupplier']     = BFWK_SERVER_ROOT.'?/thirdParty/newSupplier/';
        $data['urlViewSupplier']    = BFWK_SERVER_ROOT.'?/thirdParty/viewSupplier/';
        $data['urlEditSupplier']    = BFWK_SERVER_ROOT.'?/thirdParty/editSupplier/';
        $data['urlNewPurchase']     = BFWK_SERVER_ROOT.'?/document/newPurchase/';
        $data['urlNewQuote']        = BFWK_SERVER_ROOT.'?/document/newDocument/quote/';
        $data['urlNewOrder']        = BFWK_SERVER_ROOT.'?/document/newDocument/order/';
        $data['urlNewInvoice']      = BFWK_SERVER_ROOT.'?/document/newDocument/invoice/';
        $data['token'] = '';

        return $data;

    }

    function viewCustomer($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        if ( ! isset($args[0]))
        {
            //TODO error
        }
        else
            $id = (int) $args[0];

        $data = array();
        //Options for navBar
        $data['navBar'] = navBarVars('thirdParty');

        require_once('models/documentStatusModel.php');
        $data['quoteStatutes']      = DocumentStatusModel::loadAll(1);//TODO use get method with cache
        $data['orderStatutes']      = DocumentStatusModel::loadAll(2);
        $data['invoiceStatutes']    = DocumentStatusModel::loadAll(4);

        require_once('models/thirdPartyModel.php');
        $data['item'] = ThirdPartyModel::load($id);

        require_once('models/documentModel.php');
        $rDocTypesRaw = DocumentModel::getDocumentTypes();
        $data['rDocTypes'] = array_map('t', $rDocTypesRaw);

        //Getting default configuration for document list
        $data['listDocOptionType']     = '1';
        $data['listDocOptionFilter']   = 'all';

        if (isset($_COOKIE['thirdDocumentList'])) {
            $options = json_decode($_COOKIE['thirdDocumentList'], true);
            if ($options)
            {
                $data['listDocOptionType']     = $options['type'];
                $data['listDocOptionFilter']   = htmlspecialchars($options['filterBy']);
            }
        }

        if ( ! in_array($data['listDocOptionType'], array_keys($rDocTypesRaw)))
            $data['listDocOptionType'] = key($rDocTypesRaw);

        require_once('models/settingModel.php');
        $data['settings'] = SettingModel::load(array('GENERAL'));

        require_once('libs/HLX_Country.class.php');

        $data['urlEditCustomer']    = BFWK_SERVER_ROOT.'?/thirdParty/editCustomer/'.$id;
        $data['urlViewContact']     = BFWK_SERVER_ROOT.'?/thirdParty/viewContact/';
        $data['urlEditContact']     = BFWK_SERVER_ROOT.'?/thirdParty/editContact/';
        $data['urlNewContact']      = BFWK_SERVER_ROOT.'?/thirdParty/newContact/'.$id;
        $data['urlNewQuote']        = BFWK_SERVER_ROOT.'?/document/newDocument/quote/'.$id;
        $data['urlNewInvoice']      = BFWK_SERVER_ROOT.'?/document/newDocument/invoice/'.$id;
        $data['urlNewOrder']        = BFWK_SERVER_ROOT.'?/document/newDocument/order/'.$id;
        $data['urlNewPayment']      = BFWK_SERVER_ROOT.'?/payment/newPayment/thirdParty/'.$id;

        $data['urlLoadContacts']    = BFWK_SERVER_ROOT.'?/thirdParty/loadContacts/'.$id;
        $data['urlLoadDocuments']   = BFWK_SERVER_ROOT.'?/thirdParty/loadDocuments/'.$id;
        $data['urlLoadPayments']    = BFWK_SERVER_ROOT.'?/thirdParty/loadPayments/'.$id;

        $data['urlEditQuote']       = BFWK_SERVER_ROOT.'?/document/edit/quote/';
        $data['urlPDFQuote']        = BFWK_SERVER_ROOT.'?/document/pdf/quote/';
        $data['urlSendQuote']       = BFWK_SERVER_ROOT.'?/document/send/quote/';
        $data['urlDeleteQuote']     = BFWK_SERVER_ROOT.'?/document/delete/quote/';

        $data['urlEditOrder']       = BFWK_SERVER_ROOT.'?/document/edit/order/';
        $data['urlPDFOrder']        = BFWK_SERVER_ROOT.'?/document/viewPdf/order/';
        $data['urlSendOrder']       = BFWK_SERVER_ROOT.'?/document/send/order/';

        $data['urlEditInvoice']     = BFWK_SERVER_ROOT.'?/document/edit/invoice/';
        $data['urlPDFInvoice']      = BFWK_SERVER_ROOT.'?/document/viewPdf/invoice/';
        $data['urlSendInvoice']     = BFWK_SERVER_ROOT.'?/document/send/invoice/';
        $data['urlNewDocPayment']   = BFWK_SERVER_ROOT.'?/payment/newPayment/document/';

        $data['urlViewCustomer']    = BFWK_SERVER_ROOT.'?/thirdParty/viewCustomer/';
        $data['urlViewInvoice']     = BFWK_SERVER_ROOT.'?/document/edit/invoice/';
        $data['urlViewDeposit']     = BFWK_SERVER_ROOT.'?/document/edit/deposit/';
        $data['urlViewPayment']     = BFWK_SERVER_ROOT.'?/payment/viewPayment/';
        $data['urlDeletePayment']   = BFWK_SERVER_ROOT.'?/payment/deletePayment/';

        return $data;

    }

    function viewSupplier($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        if ( ! isset($args[0]))
        {
            //TODO error
        }
        else
            $id = (int) $args[0];

        $data = array();
        //Options for navBar
        $data['navBar'] = navBarVars('thirdParty');

        require_once('models/documentStatusModel.php');
        $data['purchaseStatutes'] = DocumentStatusModel::loadAll(5);

        require_once('models/thirdPartyModel.php');
        $data['item'] = ThirdPartyModel::load($id);

        //Getting default configuration for document list
        $data['listDocOptionType']     = '5';
        $data['listDocOptionFilter']   = 'all';

        if (isset($_COOKIE['thirdPurchaseList'])) {
            $options = json_decode($_COOKIE['thirdPurchaseList'], true);
            if ($options)
            {
                $data['listDocOptionFilter'] = htmlspecialchars($options['filterBy']);
            }
        }

        require_once('models/settingModel.php');
        $data['settings'] = SettingModel::load(array('GENERAL'));

        require_once('libs/HLX_Country.class.php');

        $data['urlEditSupplier']        = BFWK_SERVER_ROOT.'?/thirdParty/editSupplier/'.$id;
        $data['urlViewContact']         = BFWK_SERVER_ROOT.'?/thirdParty/viewContact/';
        $data['urlEditContact']         = BFWK_SERVER_ROOT.'?/thirdParty/editContact/';
        $data['urlNewContact']          = BFWK_SERVER_ROOT.'?/thirdParty/newContact/'.$id;
        $data['urlNewPurchase']         = BFWK_SERVER_ROOT.'?/document/newPurchase/'.$id;
        $data['urlNewPayment']          = BFWK_SERVER_ROOT.'?/payment/newPayment/thirdParty/'.$id;

        $data['urlLoadContacts']        = BFWK_SERVER_ROOT.'?/thirdParty/loadContacts/'.$id;
        $data['urlLoadDocuments']       = BFWK_SERVER_ROOT.'?/thirdParty/loadDocuments/'.$id;
        $data['urlLoadPayments']        = BFWK_SERVER_ROOT.'?/thirdParty/loadPayments/'.$id;

        $data['urlViewSupplier']        = BFWK_SERVER_ROOT.'?/thirdParty/viewSupplier/';
        $data['urlEditPurchase']        = BFWK_SERVER_ROOT.'?/document/editPurchase/';
        $data['urlPDFPurchase']         = BFWK_SERVER_ROOT.'?/document/pdf/purchase/';
        $data['urlSendPurchase']        = BFWK_SERVER_ROOT.'?/document/send/purchase/';
        $data['urlNewPurchasePayment']  = BFWK_SERVER_ROOT.'?/payment/newPayment/document/';
        $data['urlReceive']             = BFWK_SERVER_ROOT.'?/document/receive/';

        $data['urlViewSupplier']        = BFWK_SERVER_ROOT.'?/thirdParty/viewSupplier/';
        $data['urlViewPurchase']        = BFWK_SERVER_ROOT.'?/document/editPurchase/';
        $data['urlViewPayment']         = BFWK_SERVER_ROOT.'?/payment/viewPayment/';

        return $data;

    }

    function viewContact($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        if ( ! isset($args[0]))
        {
            //TODO error
        }
        else
            $id = (int) $args[0];

        $data = array();
        //Options for navBar
        $data['navBar'] = navBarVars('thirdParty');

        require_once('models/contactModel.php');
        $data['item'] = ContactModel::load($id);

        $data['urlEditContact']     = BFWK_SERVER_ROOT.'?/thirdParty/editContact/'.$id;
        $data['urlViewCustomer']    = BFWK_SERVER_ROOT.'?/thirdParty/viewCustomer/';

        return $data;

    }

    function editCustomer($args)
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        if ( ! isset($args[0]))
        {
            //TODO error
        }
        else
            $id = (int) $args[0];

        $data = array();
        //Options for navBar
        $data['navBar'] = navBarVars('thirdParty');

        require_once('models/thirdPartyModel.php');
        $data['item'] = ThirdPartyModel::load($id);

        require_once('libs/HLX_Country.class.php');
        $data['countries'] = HLX_Country::getCountryList();

        $data['urlSave']   = BFWK_SERVER_ROOT.'?/thirdParty/saveCustomer/';
        $data['urlCancel'] = BFWK_SERVER_ROOT.'?/thirdParty/viewCustomer/'.$id;

        return $data;
    }

    function editContact($args)
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        if ( ! isset($args[0]))
        {
            //TODO error
        }
        else
            $id = (int) $args[0];

        $data = array();
        //Options for navBar
        $data['navBar'] = navBarVars('thirdParty');

        require_once('models/contactModel.php');
        $data['item'] = ContactModel::load($id);

        $data['urlSave']    = BFWK_SERVER_ROOT.'?/thirdParty/saveContact/'.$data['item']['ThirdPartyID'];
        $data['urlCancel']  = BFWK_SERVER_ROOT.'?/thirdParty/viewContact/'.$id;

        return $data;
    }

    function editSupplier($args)
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        if ( ! isset($args[0]))
        {
            //TODO error
        }
        else
            $id = (int) $args[0];

        $data = array();
        //Options for navBar
        $data['navBar'] = navBarVars('thirdParty');

        require_once('models/thirdPartyModel.php');
        $data['item'] = ThirdPartyModel::load($id);

        require_once('libs/HLX_Country.class.php');
        $data['countries'] = HLX_Country::getCountryList();

        $data['urlSave']   = BFWK_SERVER_ROOT.'?/thirdParty/saveSupplier/';
        $data['urlCancel'] = BFWK_SERVER_ROOT.'?/thirdParty/viewSupplier/'.$id;

        return $data;
    }

    function newCustomer()
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();
        //Options for navBar
        $data['navBar'] = navBarVars('thirdParty');

        $id = 0;

        require_once('models/thirdPartyModel.php');
        $data['item'] = ThirdPartyModel::load($id);

        require_once('models/settingModel.php');
        $settings = SettingModel::load(array('GENERAL'));
        $data['item']['CountryID'] = $settings['GENERAL']['Language'];

        require_once('libs/HLX_Country.class.php');
        $data['countries'] = HLX_Country::getCountryList();

        $data['urlSave']    = BFWK_SERVER_ROOT.'?/thirdParty/saveCustomer/';
        $data['urlCancel']  = BFWK_SERVER_ROOT.'?/thirdParty/index/';

        $this->_display('thirdParty_editCustomer', $data);
        die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
    }

    function newSupplier()
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        $data = array();
        //Options for navBar
        $data['navBar'] = navBarVars('thirdParty');

        $id = 0;

        require_once('models/thirdPartyModel.php');
        $data['item'] = ThirdPartyModel::load($id);

        require_once('models/settingModel.php');
        $settings = SettingModel::load(array('GENERAL'));
        $data['item']['CountryID'] = $settings['GENERAL']['Language'];

        require_once('libs/HLX_Country.class.php');
        $data['countries'] = HLX_Country::getCountryList();

        $data['urlSave']    = BFWK_SERVER_ROOT.'?/thirdParty/saveSupplier/';
        $data['urlCancel']  = BFWK_SERVER_ROOT.'?/thirdParty/index/';

        $this->_display('thirdParty_editSupplier', $data);
        die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
    }

    function newContact($args)
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        if ( ! isset($args[0]))
        {
            //TODO error
        }
        else
            $thirdPartyID = (int) $args[0];

        $id = 0;

        $data = array();
        //Options for navBar
        $data['navBar'] = navBarVars('thirdParty');

        require_once('models/contactModel.php');
        $data['item'] = ContactModel::load($id);

        $data['urlSave']    = BFWK_SERVER_ROOT.'?/thirdParty/saveContact/'.$thirdPartyID;
        $data['urlCancel']  = BFWK_SERVER_ROOT.'?/thirdParty/index/';

        $this->_display('thirdParty_editContact', $data);
        die;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer
    }

    function saveCustomer()
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        //TODO need check Token

        require_once('models/thirdPartyModel.php');
        $res = ThirdPartyModel::save($_POST);

        if ($res == false)
        {
            //TODO error
        }

        header('Location: '.BFWK_SERVER_ROOT.'?/thirdParty/viewCustomer/'.$res['ID']);
        return;
    }

    function saveSupplier()
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        //TODO need check Token

        require_once('models/thirdPartyModel.php');
        $res = ThirdPartyModel::save($_POST);

        if ($res == false)
        {
            //TODO error
        }

        header('Location: '.BFWK_SERVER_ROOT.'?/thirdParty/viewSupplier/'.$res['ID']);
        return;
    }

    function saveContact($args)
    {
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        if ( ! isset($args[0]))
        {
            //TODO error
        }
        else
            $_POST['ThirdPartyID'] = (int) $args[0];

        //TODO need check Token

        require_once('models/contactModel.php');
        $res = ContactModel::save($_POST);

        if ($res == false)
        {
            //TODO error
        }

        header('Location: '.BFWK_SERVER_ROOT.'?/thirdParty/viewContact/'.$res['ID']);
        return;
    }

    /**
     * Ajax call for main List
     * */
    function loadList()
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        //TODO check parameters else retrun error

        //En fonction de Post récupère les données
        $args = $_POST;
        switch($args['type']) {
            case 'contacts':
                require_once('models/contactModel.php');
                echo json_encode(ContactModel::getList($args));
                break;
            case 'suppliers':
            case 'customers':
            default:
                require_once('models/thirdPartyModel.php');
                echo json_encode(ThirdPartyModel::getList($args));
        }

        exit;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer

    }

    /**
     * Ajax call for List new contact && document
     * */
    function loadCustomer()
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        require_once('models/thirdPartyModel.php');
        echo json_encode(ThirdPartyModel::loadAll('customers'));

        exit;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer

    }

    function loadSupplier()
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        require_once('models/thirdPartyModel.php');
        echo json_encode(ThirdPartyModel::loadAll('suppliers'));

        exit;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer

    }

    /**
     * Ajax call for document List linked to a ThirdParty
     * */
    function loadDocuments($args)
    {
        //TODO move in document controller ?
        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        if ( ! isset($args[0]))
        {
            //TODO error
        }
        else
            $id = (int) $args[0];

        //En fonction de Post récupère les données
        $args = $_POST;

        require_once('models/documentModel.php');
        echo json_encode(DocumentModel::getList($args, $id));

        exit;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer

    }

    /**
     * Ajax call for contact List of a Customer
     * */
    function loadContacts($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        if ( ! isset($args[0]))
        {
            //TODO error
        }
        else
            $id = (int) $args[0];

        //En fonction de Post récupère les données
        $args = $_POST;

        require_once('models/contactModel.php');
        echo json_encode(ContactModel::getList($args, $id));

        exit;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer

    }

    /**
     * Ajax call for contact List of a Customer
     * */
    function loadPayments($args)
    {

        if ( ! $this->hlx_motor->user_isConnected())
            header('Location: '.BFWK_SERVER_ROOT.'?/main/login&msg=UserNotConnected');

        if ( ! isset($args[0]))
        {
            //TODO error
        }
        else
            $id = (int) $args[0];

        //En fonction de Post récupère les données
        $args = $_POST;

        require_once('models/paymentModel.php');
        echo json_encode(PaymentModel::getList($args, $id));

        exit;//TODO Autoriser return null ou false pour ne pas afficher de vue ++ ajouter un blocage du buffer

    }
}
