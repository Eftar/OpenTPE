<?php

/**
 * BFWK_MODE
 * BFWK behaviour
 * - 0 = development : bfwk tries to generate missing code to help
 *       programmers and designers, display PHP errors/warning/notices
 * - 1 = testing : bfwk shows missing code and display PHP errors
 * - 2 = production : security mode for production environment
 */
define('BFWK_MODE',                 2);

/**
 * BFWK_SERVER_ROOT
 * root path for current installation
 */
define('BFWK_SERVER_ROOT',          'http'.(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 's' : '' ).'://'.$_SERVER['HTTP_HOST'].rtrim(dirname($_SERVER['SCRIPT_NAME']), '\\/').'/');


/**
 * BFWK_DEFAULT_CTRL
 * controller used if HTTP query string is empty
 */
define('BFWK_DEFAULT_CTRL',         'main');

/**
 * BFWK_DEFAULT_VIEW
 * view used if HTTP query string contains only a controller name
 */
define('BFWK_DEFAULT_VIEW',         'index');

/**
 * BFWK_CTRL_PATH
 * relative or absolute path to controllers directory
 */
define('BFWK_CTRL_PATH',            'ctrls/');

/**
 * BFWK_VIEW_PATH
 * relative or absolute path to views directory
 */
define('BFWK_VIEW_PATH',            'views/');

/**
 * BFWK_LIB_PATH //TODO voir pour le retirer ou ajouter aussi model et fournir des methodes dans le ctrl standard pour les requires
 * relative or absolute path to classes directory
 */
define('BFWK_LIB_PATH',             'libs/');

/**
 * BFWK_PHP_EXT
 * PHP file extension (added to controller and view name)
 * could be '.php', '.phtml', '.inc'...
 */
define('BFWK_PHP_EXT',              '.php');

/**
 * BFWK_DEBUG_VAR
 * name of variable to enter debug mode on view which has not a controller action yet
 * this variable is added by developper at the end of url :
 * http://www.domain.tld/ctrl/view/param&debug
 * This mode permits to display php variable and code used by a view page.
 */
define('BFWK_DEBUG_VAR',            'debug');

/**
 * BFWK_DEFAULT_LANG
 * ISO 3166 code of default langage.
 * HTTP_ACCEPT_LANGUAGE is use first to find preferred lang in lang folder
 */
define('BFWK_DEFAULT_LANG',         'fr');
