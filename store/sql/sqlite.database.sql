CREATE TABLE IF NOT EXISTS `hlx_contacts` (
  `ID` integer NOT NULL PRIMARY KEY AUTOINCREMENT
,  `TitleID` integer NOT NULL DEFAULT 0
,  `Name` varchar(128) NOT NULL DEFAULT ''
,  `Firstname` varchar(128) NOT NULL DEFAULT ''
,  `Function` varchar(128) NOT NULL DEFAULT ''
,  `ThirdPartyID` integer  NOT NULL DEFAULT 0
,  `Phone` varchar(32) NOT NULL DEFAULT ''
,  `Fax` varchar(32) NOT NULL DEFAULT ''
,  `Mobile` varchar(32) NOT NULL DEFAULT ''
,  `Email` varchar(128) NOT NULL DEFAULT ''
,  `Active` integer NOT NULL DEFAULT 0
);
CREATE TABLE IF NOT EXISTS `hlx_documents` (
  `ID` integer NOT NULL PRIMARY KEY AUTOINCREMENT
,  `Reference` varchar(128) NOT NULL DEFAULT ''
,  `ExternalReference` varchar(128) NOT NULL DEFAULT ''
,  `Serial` varchar(10) NOT NULL DEFAULT ''
,  `QuoteID` integer NOT NULL DEFAULT 0
,  `OrderID` integer NOT NULL DEFAULT 0
,  `InvoiceID` integer NOT NULL DEFAULT 0
,  `Ordered` integer NOT NULL DEFAULT 0
,  `Invoiced` integer NOT NULL DEFAULT 0
,  `Received` integer NOT NULL DEFAULT 0
,  `IsDeposit` integer NOT NULL DEFAULT 0
,  `ThirdPartyID` integer NOT NULL DEFAULT 0
,  `UserID` integer NOT NULL DEFAULT 0
,  `DocStatusID` integer NOT NULL DEFAULT 0
,  `DocTypeID` integer NOT NULL DEFAULT 0
,  `DateCreated` datetime NOT NULL
,  `DateUpdated` datetime NOT NULL
,  `DateValidated` datetime NOT NULL
,  `DateReceived` date NOT NULL
,  `DateDue` date NOT NULL
,  `InvoiceName` varchar(128) NOT NULL DEFAULT ''
,  `InvoiceFirstname` varchar(128) NOT NULL DEFAULT ''
,  `InvoiceAddress1` varchar(128) NOT NULL DEFAULT ''
,  `InvoiceAddress2` varchar(128) NOT NULL DEFAULT ''
,  `InvoiceAddress3` varchar(128) NOT NULL DEFAULT ''
,  `InvoicePostalCode` varchar(32) NOT NULL DEFAULT ''
,  `InvoiceCity` varchar(128) NOT NULL DEFAULT ''
,  `InvoiceState` varchar(128) NOT NULL DEFAULT ''
,  `InvoiceCountryID` varchar(10) NOT NULL DEFAULT ''
,  `DeliveryName` varchar(128) NOT NULL DEFAULT ''
,  `DeliveryFirstname` varchar(128) NOT NULL DEFAULT ''
,  `DeliveryAddress1` varchar(128) NOT NULL DEFAULT ''
,  `DeliveryAddress2` varchar(128) NOT NULL DEFAULT ''
,  `DeliveryAddress3` varchar(128) NOT NULL DEFAULT ''
,  `DeliveryPostalCode` varchar(32) NOT NULL DEFAULT ''
,  `DeliveryCity` varchar(128) NOT NULL DEFAULT ''
,  `DeliveryState` varchar(128) NOT NULL DEFAULT ''
,  `DeliveryCountryID` varchar(10) NOT NULL DEFAULT ''
,  `Total` decimal(10,2) NOT NULL DEFAULT 0.00
,  `Paid` decimal(10,2) NOT NULL DEFAULT 0.00
,  `TaxID` integer NOT NULL DEFAULT 0
,  `DiscountType` char(1) NOT NULL DEFAULT '%'
,  `DiscountValue` decimal(10,2) NOT NULL DEFAULT 0.00
,  `Terms` text NOT NULL DEFAULT ''
,  `DocumentKey` varchar(64) NOT NULL DEFAULT ''
);
CREATE TABLE IF NOT EXISTS `hlx_documents_lines` (
  `DocumentID` integer NOT NULL
,  `Number` integer  NOT NULL DEFAULT 0
,  `Type` integer NOT NULL DEFAULT 0
,  `Reference` varchar(128) NOT NULL DEFAULT ''
,  `Name` varchar(128) NOT NULL DEFAULT ''
,  `Description` text NOT NULL DEFAULT ''
,  `Quantity` decimal(10,2) NOT NULL DEFAULT 0.00
,  `UnityID` integer  NOT NULL DEFAULT 0
,  `Price` decimal(10,2) NOT NULL DEFAULT 0.00
,  `TaxID` integer  NOT NULL DEFAULT 0
,  `DiscountType` char(1) NOT NULL DEFAULT '%'
,  `DiscountValue` decimal(10,2) NOT NULL DEFAULT 0.00
,  PRIMARY KEY (`DocumentID`,`Number`)
);

CREATE TABLE IF NOT EXISTS `hlx_documents_statutes` (
  `ID` integer  NOT NULL PRIMARY KEY AUTOINCREMENT
,  `DocTypeID` integer  NOT NULL DEFAULT 0
,  `Name` varchar(128) NOT NULL DEFAULT ''
,  `Position` integer  NOT NULL DEFAULT 0
,  `Color` varchar(6) NOT NULL DEFAULT ''
,  `ReadOnly` integer  NOT NULL DEFAULT 0
,  `Draft` integer  NOT NULL DEFAULT 0
,  `Validated` integer  NOT NULL DEFAULT 0
,  `Closed` integer  NOT NULL DEFAULT 0
,  `Received` integer  NOT NULL DEFAULT 0
);
INSERT INTO `hlx_documents_statutes` (`ID`, `DocTypeID`, `Name`, `Position`, `Color`, `ReadOnly`, `Draft`, `Validated`, `Closed`, `Received`) VALUES
(1, 1, '$Draft', 1, 'b3b3b3', 0, 1, 0, 0, 0),
(2, 1, '$Validated', 2, '54a0c6', 0, 0, 1, 0, 0),
(3, 1, '$Sent', 3, '54a0c6', 0, 0, 1, 0, 0),
(5, 1, '$Approved', 5, '58a959', 1, 0, 1, 1, 0),
(6, 1, '$Rejected', 6, 'c76e6d', 1, 0, 1, 1, 0),
(7, 1, '$Canceled', 7, 'ffa500', 1, 0, 0, 1, 0),
(8, 1, '$Closed', 8, '4d4d4d', 1, 0, 0, 1, 0),
(9, 4, '$Draft', 1, 'b3b3b3', 0, 1, 0, 0, 0),
(10, 4, '$Validated', 2, '54a0c6', 0, 0, 1, 0, 0),
(31, 4, '$Sent', 3, '54a0c6', 0, 0, 1, 0, 0),
(11, 4, '$Paid', 4, '58a959', 1, 0, 1, 1, 0),
(12, 2, '$Draft', 1, 'b3b3b3', 0, 1, 0, 0, 0),
(13, 2, '$Sent', 3, '54a0c6', 0, 0, 1, 0, 0),
(14, 2, '$Approved', 4, '58a959', 1, 0, 1, 1, 0),
(15, 2, '$Rejected', 5, 'c76e6d', 1, 0, 1, 1, 0),
(16, 2, '$Invoiced', 6, '58a959', 1, 0, 1, 1, 0),
(17, 2, '$Canceled', 7, 'ffa500', 1, 0, 0, 1, 0),
(22, 5, '$Draft', 1, 'b3b3b3', 0, 1, 0, 0, 0),
(23, 5, '$Validated', 2, '54a0c6', 0, 0, 1, 0, 0),
(24, 5, '$PendingReceipt ', 5, 'c4941b', 0, 0, 1, 0, 0),
(25, 5, '$Received', 6, '58a959', 0, 0, 1, 1, 1),
(26, 5, '$Canceled', 7, 'ffa500', 0, 0, 1, 1, 0),
(27, 5, '$Paid', 4, '58a959', 1, 0, 1, 1, 0),
(32, 5, '$Sent', 3, '54a0c6', 0, 0, 1, 0, 0),
(28, 2, '$Validated', 2, '58a959', 0, 0, 1, 0, 0),
(30, 3, '$Validated', 2, '54a0c6', 0, 0, 1, 0, 0);

CREATE TABLE IF NOT EXISTS `hlx_notes` (
  `ID` integer NOT NULL PRIMARY KEY AUTOINCREMENT
, `Name` varchar(256) NOT NULL DEFAULT ''
, `Tags` varchar(256) NOT NULL DEFAULT ''
, `Description` text NOT NULL DEFAULT ''
);

CREATE TABLE IF NOT EXISTS `hlx_payments` (
  `ID` integer  NOT NULL PRIMARY KEY AUTOINCREMENT
,  `Serial` varchar(10) NOT NULL DEFAULT ''
,  `DocumentID` integer  NOT NULL DEFAULT 0
,  `DocTypeID` integer  NOT NULL DEFAULT 0
,  `PaymentMethodID` integer  NOT NULL DEFAULT ''
,  `Date` datetime NOT NULL
,  `Amount` decimal(10,2) NOT NULL DEFAULT 0.00
,  `Note` text NOT NULL DEFAULT ''
);

CREATE TABLE IF NOT EXISTS `hlx_products` (
  `ID` integer  NOT NULL PRIMARY KEY AUTOINCREMENT
,  `Type` integer NOT NULL DEFAULT 0
,  `Reference` varchar(128) NOT NULL DEFAULT ''
,  `Name` varchar(128) NOT NULL DEFAULT ''
,  `Description` text NOT NULL DEFAULT ''
,  `Price` decimal(10,2) NOT NULL DEFAULT 0.00
,  `PurchasePrice` decimal(10,2) NOT NULL DEFAULT 0.00
,  `UnityID` integer  NOT NULL DEFAULT 0
,  `TaxID` integer  NOT NULL DEFAULT 0
,  `PurchaseTaxID` integer  NOT NULL DEFAULT 0
,  `OnSale` integer NOT NULL DEFAULT 0
,  `Active` integer  NOT NULL DEFAULT 0
);

CREATE TABLE IF NOT EXISTS `hlx_settings` (
  `ID` integer  NOT NULL PRIMARY KEY AUTOINCREMENT
,  `Type` varchar(16) NOT NULL DEFAULT ''
,  `Name` varchar(128) NOT NULL DEFAULT ''
,  `Value` text NOT NULL DEFAULT ''
,  `ActiveFrom` date NOT NULL
,  `ActiveTo` date NOT NULL
);
INSERT INTO `hlx_settings` (`ID`, `Type`, `Name`, `Value`, `ActiveFrom`, `ActiveTo`) VALUES
(NULL, 'COUNTER', '1', '0', '0000-00-00', '0000-00-00'),
(NULL, 'COUNTER', '2', '0', '0000-00-00', '0000-00-00'),
(NULL, 'COUNTER', '3', '0', '0000-00-00', '0000-00-00'),
(NULL, 'COUNTER', '4', '0', '0000-00-00', '0000-00-00'),
(NULL, 'COUNTER', '5', '0', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'Language', '$Lang', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'FirstDayOfWeek', '$FirstDayOfWeek', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'DateFormat', '$DateFormat', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'CurrencySymbol', '$CurrencySymbol', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'CurrencyPlacement', '$CurrencyPlacement', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'Decimals', '$Decimals', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'DecimalSeparator', '$DecimalSeparator', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'QuoteExpirationDelay', '30', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'QuoteNote', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'InvoiceDueDelay', '30', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'InvoiceNote', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'InvoiceDefaultStatus', '9', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'MailCopyAdmin', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'MailMethod', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'SMTPURL', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'SMTPNeedAuth', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'SMTPUser', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'SMTPPassword', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'SMTPPort', '445', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'SMTPSecurity', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'QuoteReferenceFormat', 'QUO{id}', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'InvoiceReferenceFormat', 'INV{id}', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'QuoteTaxDefault', '0', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'InvoiceTaxDefault', '0', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'ProductDefaultTaxSell', '0', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'ProductDefaultTaxBuy', '0', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'ProductDefaultUnity', '0', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'CompanyName', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'CompanyAddress1', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'CompanyAddress2', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'CompanyAddress3', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'CompanyType', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'CompanyCapital', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'CompanyRegistrationNumber', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'CompanyRegister', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'CompanyClassification', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'CompanyTaxNumber', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'CompanyPostalCode', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'CompanyCity', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'CompanyState', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'CompanyCountry', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'CompanyPhone', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'CompanyFax', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'CompanyEmail', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'CompanyWeb', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'QuoteTemplate', 'default', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'QuoteDefaultStatus', '1', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'InvoiceTemplate', 'default', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'OrderReferenceFormat', 'ORD{id}', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'OrderTaxDefault', '0', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'OrderDueDelay', '30', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'OrderDefaultStatus', '12', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'OrderTemplate', 'default', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'DepositDueDelay', '10', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'DepositNote', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'DepositDefaultUnity', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'OrderNote', '', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'UseBillingAddress', '1', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'UseDeliveryAddress', '1', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'UseDiscount', '1', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'UseVAT', '1', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'PurchaseReferenceFormat', 'PUR{id}', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'PurchaseTaxDefault', '0', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'PurchaseDefaultStatus', '22', '0000-00-00', '0000-00-00'),
(NULL, 'GENERAL', 'PurchaseTemplate', 'default', '0000-00-00', '0000-00-00'),
(NULL, 'PAYMENT', '$Transfer', '$Transfer', '0000-00-00', '0000-00-00'),
(NULL, 'UNITY', '$Piece', '$Piece', '0000-00-00', '0000-00-00');

CREATE TABLE IF NOT EXISTS `hlx_taxes` (
  `ID` integer  NOT NULL PRIMARY KEY AUTOINCREMENT
,  `Name` varchar(128) NOT NULL DEFAULT ''
,  `Rate` decimal(10,2) NOT NULL DEFAULT 0.00
,  `ActiveFrom` date NOT NULL
,  `ActiveTo` date NOT NULL
);

CREATE TABLE IF NOT EXISTS `hlx_thirdparties` (
  `ID` integer  NOT NULL PRIMARY KEY AUTOINCREMENT
,  `Type` char(1) NOT NULL DEFAULT ''
,  `TitleID` integer  NOT NULL DEFAULT 0
,  `Name` varchar(128) NOT NULL DEFAULT ''
,  `Firstname` varchar(128) NOT NULL DEFAULT ''
,  `Address1` varchar(128) NOT NULL DEFAULT ''
,  `Address2` varchar(128) NOT NULL DEFAULT ''
,  `Address3` varchar(128) NOT NULL DEFAULT ''
,  `PostalCode` varchar(32) NOT NULL DEFAULT ''
,  `City` varchar(128) NOT NULL DEFAULT ''
,  `State` varchar(128) NOT NULL DEFAULT ''
,  `CountryID` varchar(10) NOT NULL DEFAULT ''
,  `Phone` varchar(32) NOT NULL DEFAULT ''
,  `Fax` varchar(32) NOT NULL DEFAULT ''
,  `Mobile` varchar(32) NOT NULL DEFAULT ''
,  `Email` varchar(128) NOT NULL DEFAULT ''
,  `Web` varchar(256) NOT NULL DEFAULT ''
,  `VAT` varchar(128) NOT NULL DEFAULT ''
,  `Invoiced` decimal(10,2) NOT NULL DEFAULT 0.00
,  `Paid` decimal(10,2) NOT NULL DEFAULT 0.00
,  `Purchased` decimal(10,2) NOT NULL DEFAULT 0.00
,  `Active` integer  NOT NULL DEFAULT 0
);

CREATE TABLE IF NOT EXISTS `hlx_users` (
  `ID` integer  NOT NULL PRIMARY KEY AUTOINCREMENT
,  `Name` varchar(128) NOT NULL DEFAULT ''
,  `Email` varchar(128) NOT NULL DEFAULT ''
,  `Password` varchar(255) NOT NULL DEFAULT ''
,  `Role` varchar(10) NOT NULL DEFAULT ''
,  `Active` integer  NOT NULL DEFAULT 0
,  `DateCreated` datetime NOT NULL
,  UNIQUE (`Email`)
);

CREATE TABLE `hlx_reports` (
  `ID` integer NOT NULL PRIMARY KEY AUTOINCREMENT
,  `Name` varchar(128) NOT NULL DEFAULT ''
,  `Description` text NOT NULL DEFAULT ''
,  `Variables` text NOT NULL DEFAULT ''
,  `Query` text NOT NULL DEFAULT ''
);
INSERT INTO `hlx_reports` (`ID`, `Name`, `Description`, `Variables`, `Query`) VALUES
(NULL, '$ExportSells', '$ExportSellDesc', '[{"name":"startDate","label":"$StartDate","type":"date","default":"startY","placeholder":""},{"name":"endDate","label":"$EndDate","type":"date","default":"endY","placeholder":""}]', 'SELECT
    p.Date                                      AS "$DatePayment",
    d.Reference                                 AS "$Reference",
    d.InvoiceFirstname || " " || d.InvoiceName  AS "$Customer",
    GROUP_CONCAT(DISTINCT   CASE
                                WHEN dl.Type = 1 THEN "$Product"
                                WHEN dl.Type = 2 THEN "$Service"
                            END)                AS "$Nature",
    Amount                                      AS "$Amount",
    s.Value                                     AS "$PaymentMethod"

FROM hlx_payments AS p  LEFT JOIN hlx_settings AS s ON p.PaymentMethodID = s.ID AND s.Type = "PAYMENT"
                        LEFT JOIN hlx_documents AS d ON p.DocumentID = d.ID
                        LEFT JOIN hlx_documents_lines AS dl ON d.ID = dl.DocumentID AND dl.Type IN (1, 2)
WHERE   d.DocTypeID IN (4, 3)
        AND p.Date >= "{startDate}" AND p.Date <= "{endDate}"
GROUP BY p.ID
ORDER BY p.Date ASC'),
(NULL, '$ExportPurchases', '$ExportPurchaseDesc', '[{"name":"startDate","label":"$StartDate","type":"date","default":"startY","placeholder":""},{"name":"endDate","label":"$EndDate","type":"date","default":"endY","placeholder":""}]', 'SELECT
    p.Date                                  AS "$DatePayment",
    d.Reference ||  CASE
                        WHEN d.ExternalReference <> ""
                            THEN " - " || d.ExternalReference
                        ELSE ""
                    END                     AS "$Reference",
    d.InvoiceFirstname || d.InvoiceName     AS "$Supplier",
    GROUP_CONCAT(DISTINCT   CASE
                                WHEN dl.Type = 1 THEN "$Product"
                                WHEN dl.Type = 2 THEN "$Service"
                            END)            AS "$Nature",
    Amount                                  AS "$Amount",
    s.Value                                 AS "$PaymentMethod"

FROM hlx_payments AS p  LEFT JOIN hlx_settings AS s ON p.PaymentMethodID = s.ID AND s.Type = "PAYMENT"
                        LEFT JOIN hlx_documents AS d ON p.DocumentID = d.ID
                        LEFT JOIN hlx_documents_lines AS dl ON d.ID = dl.DocumentID AND dl.Type IN (1, 2)
WHERE   d.DocTypeID IN (5)
        AND p.Date >= "{startDate}" AND p.Date <= "{endDate}"
GROUP BY p.ID
ORDER BY p.Date ASC');

CREATE INDEX "idx_hlx_thirdparties_Name" ON "hlx_thirdparties" (`Name`);
CREATE INDEX "idx_hlx_documents_Ref" ON "hlx_documents" (`Reference`);
CREATE INDEX "idx_hlx_contacts_Name" ON "hlx_contacts" (`Name`);
