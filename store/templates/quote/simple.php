<!doctype html>
<html>
<head>
    <!--DESC La description de ce template -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?=ucfirst($type)?> <?=$document['Reference']?></title>
</head>

<body>
    <style type="text/css">
        body {
            font-family: Helvetica;
            font-size:0.7em;
            margin:0;
            margin-bottom:10px;
            padding:0;
        }
        @page {
            margin: 20px 20px !important;
        }
        .hidden {
            display:none;
        }
        .text-right {
            text-align:right;
            padding-right:2px;
        }
        .text-center {
            text-align:center;
        }
        .valign-top {
            vertical-align: top;
        }
        .valign-bottom {
            vertical-align: bottom;
        }
        table.products {
            border-collapse:collapse;
        }
        table.products th {
            border-bottom: 1px solid #000;
            text-align:center;
        }
        table.products th.first {
            text-align:left;
        }
        table.products td {
            border-bottom:1px solid #969696;
            padding: 4px 0;
            vertical-align: top;
            font-size: 0.9em;
        }
        table.products td + td {
            padding-right:10px;
        }
        table.products td.last {
            padding-right:0 !important;
        }
        table.tax {
            border-collapse:collapse;
        }
        table.tax th, table.tax td {
            border: 1px solid #969696;
            font-size: 0.9em;
        }
        /*p {
            page-break-after: always;
        }*/
        .pushLeft {
            float:left;
        }
        .company {
            font-size: 1.2em;
            padding: 0;
        }
        .company strong {
            font-size: 1.1em;
        }
        .logo {
            float:left;
            margin: 0;
            margin-right: 10px;
        }
        .address {
            clear:both;
        }
        .billingAddress p, .deliveryAddress p {
            font-size: 1.2em;
        }
        .summary {
            float:right;
            margin-top: 5px;
        }
        .summary td {
            text-align: right;
            width: 80px;
        }
        .summary th {
            text-align: right;
        }
    </style>
    <table cellpadding="0" cellspacing="0" style="width:100%" >
        <tr>
            <td colspan="2" class="company">
                <img src="<?=$companyLogo?>" class="logo" />
                <strong><?=$settings['GENERAL']['CompanyName']?></strong><br />
                <?=($settings['GENERAL']['CompanyAddress1'] != '' ? $settings['GENERAL']['CompanyAddress1'].'<br />':'')?>
                <?=($settings['GENERAL']['CompanyAddress2'] != '' ? $settings['GENERAL']['CompanyAddress2'].'<br />':'')?>
                <?=($settings['GENERAL']['CompanyAddress3'] != '' ? $settings['GENERAL']['CompanyAddress3'].'<br />':'')?>
                <?=$settings['GENERAL']['CompanyPostalCode'].' '.$settings['GENERAL']['CompanyCity']?><br />
                <?=($settings['GENERAL']['CompanyCountry'] != '' ? $settings['GENERAL']['CompanyCountry'].'<br />':'')?><br />
                <?=($settings['GENERAL']['CompanyPhone'] != '' ? t('Phone').' : '.$settings['GENERAL']['CompanyPhone'].'<br />':'')?>
                <?=($settings['GENERAL']['CompanyFax'] != '' ? t('Fax').' : '.$settings['GENERAL']['CompanyFax'].'<br />':'')?>
                <?=($settings['GENERAL']['CompanyEmail'] != '' ? t('Email').' : '.$settings['GENERAL']['CompanyEmail'].'<br />':'')?>
                <?=($settings['GENERAL']['CompanyWeb'] != '' ? t('Web').' : '.$settings['GENERAL']['CompanyWeb'].'<br />':'')?><br />
                <br />
                </p>
            </td>
        </tr>
        <tr class="address">
            <?php if ($settings['GENERAL']['UseDeliveryAddress'] == 1) { ?>
                <td style="width:58%" class="deliveryAddress">
                    <?php if ($settings['GENERAL']['UseBillingAddress'] == 1) { ?>
                        <strong><?=t('BillingAddress')?> :</strong><br />
                        <p>
                            <strong><?=$document['InvoiceName'].' '.$document['InvoiceFirstname']?></strong><br />
                            <?=($document['InvoiceAddress1'] != '' ? $document['InvoiceAddress1'].'<br />':'')?>
                            <?=($document['InvoiceAddress2'] != '' ? $document['InvoiceAddress2'].'<br />':'')?>
                            <?=($document['InvoiceAddress3'] != '' ? $document['InvoiceAddress3'].'<br />':'')?>
                            <?=$document['InvoicePostalCode'].' '.$document['InvoiceCity']?><br />
                            <?=($document['InvoiceCountryID'] != '0' ? HLX_Country::getCountryName($document['InvoiceCountryID']).'<br />' : '')?>
                        </p>
                    <?php } else { ?>
                        <strong><?=t('BillingAddress')?> :</strong><br />
                        <p>
                            <strong><?=$document['ThirdPartyName'].' '.$document['ThirdPartyFirstname']?></strong><br />
                            <?=($document['ThirdPartyAddress1'] != '' ? $document['ThirdPartyAddress1'].'<br />':'')?>
                            <?=($document['ThirdPartyAddress2'] != '' ? $document['ThirdPartyAddress2'].'<br />':'')?>
                            <?=($document['ThirdPartyAddress3'] != '' ? $document['ThirdPartyAddress3'].'<br />':'')?>
                            <?=$document['ThirdPartyPostalCode'].' '.$document['ThirdPartyCity']?><br />
                            <?=($document['ThirdPartyCountryID'] != '0' ? HLX_Country::getCountryName($document['ThirdPartyCountryID']).'<br />' : '')?>
                        </p>
                    <?php } ?>
                </td>
                <td class="billingAddress">
                    <strong><?=t('DeliveryAddress')?> :</strong><br />
                    <p>
                        <strong><?=$document['DeliveryName'].' '.$document['DeliveryFirstname']?></strong><br />
                        <?=($document['DeliveryAddress1'] != '' ? $document['DeliveryAddress1'].'<br />':'')?>
                        <?=($document['DeliveryAddress2'] != '' ? $document['DeliveryAddress2'].'<br />':'')?>
                        <?=($document['DeliveryAddress3'] != '' ? $document['DeliveryAddress3'].'<br />':'')?>
                        <?=$document['DeliveryPostalCode'].' '.$document['DeliveryCity']?><br />
                        <?=($document['DeliveryCountryID'] != '0' ? HLX_Country::getCountryName($document['DeliveryCountryID']).'<br />' : '')?>
                    </p>
                </td>
            <?php } else { ?>
                <td style="width:58%;vertical-align:bottom;">
                    <h2><?=strtoupper(t('Quote'))?> #<?=$document['Reference']?></h2>
                    <p>
                        <?=t('Created')?> : <?=printDate($settings['GENERAL']['DateFormat'], $document['DateCreated'])?><br>
                        <?=t('DateValidity')?> : <?=printDate($settings['GENERAL']['DateFormat'], $document['DateDue'])?>
                    </p>
                </td>
                <td class="billingAddress">
                    <?php if ($settings['GENERAL']['UseBillingAddress'] == 1) { ?>
                        <strong><?=t('BillingAddress')?> :</strong><br />
                        <p>
                            <strong><?=$document['InvoiceName'].' '.$document['InvoiceFirstname']?></strong><br />
                            <?=($document['InvoiceAddress1'] != '' ? $document['InvoiceAddress1'].'<br />':'')?>
                            <?=($document['InvoiceAddress2'] != '' ? $document['InvoiceAddress2'].'<br />':'')?>
                            <?=($document['InvoiceAddress3'] != '' ? $document['InvoiceAddress3'].'<br />':'')?>
                            <?=$document['InvoicePostalCode'].' '.$document['InvoiceCity']?><br />
                            <?=($document['InvoiceCountryID'] != '0' ? HLX_Country::getCountryName($document['InvoiceCountryID']).'<br />' : '')?>
                        </p>
                    <?php } else { ?>
                        <strong><?=t('BillingAddress')?> :</strong><br />
                        <p>
                            <strong><?=$document['ThirdPartyName'].' '.$document['ThirdPartyFirstname']?></strong><br />
                            <?=($document['ThirdPartyAddress1'] != '' ? $document['ThirdPartyAddress1'].'<br />':'')?>
                            <?=($document['ThirdPartyAddress2'] != '' ? $document['ThirdPartyAddress2'].'<br />':'')?>
                            <?=($document['ThirdPartyAddress3'] != '' ? $document['ThirdPartyAddress3'].'<br />':'')?>
                            <?=$document['ThirdPartyPostalCode'].' '.$document['ThirdPartyCity']?><br />
                            <?=($document['ThirdPartyCountryID'] != '0' ? HLX_Country::getCountryName($document['ThirdPartyCountryID']).'<br />' : '')?>
                        </p>
                    <?php } ?>
                </td>
            <?php } ?>
        </tr>
    </table>

    <?php if ($settings['GENERAL']['UseDeliveryAddress'] == 1) { ?>
        <h2><?=strtoupper(t('Quote'))?> #<?=$document['Reference']?></h2>

        <p>
            <?=t('Created')?> : <?=printDate($settings['GENERAL']['DateFormat'], $document['DateCreated'])?><br />
            <?=t('DateValidity')?> : <?=printDate($settings['GENERAL']['DateFormat'], $document['DateDue'])?>
        </p>
    <?php } ?>

    <br />

    <table class="products" style="width:100%;">
        <thead>
            <tr style="">
                <th class="first"><?=t('Name')?></th>
                <th style="width:60px;"><?=t('Qty')?></th>
                <th style="width:60px;"><?=t('Unity')?></th>
                <th style="width:60px;"><?=t('Price').' '.t('ExclVAT')?></th>
                <th style="width:50px; <?=($settings['GENERAL']['UseDiscount']==0?'display:none;':'')?>"><?=t('Disc.').' '.t('ExclVAT')?></th>
                <th style="width:40px; <?=($settings['GENERAL']['UseVAT']==0?'display:none;':'')?>"><?=t('VAT')?></th>
                <th style="width:80px;"><?=t('Total').' '.t('ExclVAT')?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($document['Lines'] as $line) { ?>
                <tr class="lineProduct">
                    <td class="">
                        <strong><?=trim($line['Reference'].' '.$line['Name'])?></strong><br />
                        <?=nl2br($line['Description'])?>
                    </td>
                    <?php if ($line['Type'] != 0) { ?>
                        <td class="text-right"><?=$line['Quantity']?></td>
                        <td class="text-center"><?=$line['UnityName']?></td>
                        <td class="text-right"><?=currency_format($line['Price'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></td>
                        <td class="text-right" style="<?=($settings['GENERAL']['UseDiscount']==0?'display:none;':'')?>"><?=($line['DiscountAmount'] != 0 ? currency_format($line['DiscountAmount'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement']) : '')?></td>
                        <td class="text-center" style="<?=($settings['GENERAL']['UseVAT']==0?'display:none;':'')?>"><?=$line['TaxID']?></td>
                        <td class="text-right last"><?=currency_format($line['SubtotalNet'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></td>
                    <?php } else { ?>
                        <td class="text-right"></td>
                        <td class="text-center"></td>
                        <td class="text-right"></td>
                        <td class="text-right" style="<?=($settings['GENERAL']['UseDiscount']==0?'display:none;':'')?>"></td>
                        <td class="text-center" style="<?=($settings['GENERAL']['UseVAT']==0?'display:none;':'')?>"></td>
                        <td class="text-right last"></td>
                    <?php } ?>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <table class="summary">
        <tr class="<?=($settings['GENERAL']['UseVAT']==0 && $settings['GENERAL']['UseDiscount']==0?'hidden':'')?>">
            <th class="first"><?=t('SubtotalItems')?> <?=($settings['GENERAL']['UseVAT']==1?t('ExclVAT'):'')?></th>
            <td class="first"><?=currency_format($document['SubtotalItems'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></td>
        </tr>
        <tr class="<?=($settings['GENERAL']['UseDiscount']==0?'hidden':'')?>">
            <th><?=t('Discount')?></th>
            <td><?=currency_format($document['DiscountAmount'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></td>
        </tr>
        <tr class="<?=($settings['GENERAL']['UseDiscount']==0 || ($settings['GENERAL']['UseDiscount']==1 && $settings['GENERAL']['UseVAT']==0) ?'hidden':'')?>">
            <th><?=t('Subtotal')?> <?=($settings['GENERAL']['UseVAT']==1?t('ExclVAT'):'')?></th>
            <td><?=currency_format($document['Subtotal'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></td>
        </tr>
        <tr class="<?=($settings['GENERAL']['UseVAT']==0?'hidden':'')?>">
            <th><?=t('VAT')?></th>
            <td><?=currency_format($document['TaxAmount'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></td>
        </tr>
        <tr>
            <th><?=t('Total')?> <?=($settings['GENERAL']['UseVAT']==1?t('InclVAT'):'')?></th>
            <td><?=currency_format($document['Total'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?></td>
        </tr>
    </table>

    <p style="clear:both;"></p>

    <table style="width:100%">
        <tr>
            <td style="width:50%;<?=(($settings['GENERAL']['UseVAT']==0 || empty($document['TaxSummary'])) ?'display:none;':'')?>">
                <label><strong><?=t('VAT')?> :</strong></label>
                <table class="tax" style="width:100%;margin-top:3px;">
                    <tr>
                        <th><?=t('ID')?></th>
                        <th><?=t('Base')?></th>
                        <th><?=t('Rate')?></th>
                        <th><?=t('Amount')?></th>
                    </tr>
                    <?php foreach($document['TaxSummary'] as $summary) { ?>
                        <tr>
                            <td class="text-center">
                                <?=$summary['ID']?>
                            </td>
                            <td class="text-right">
                                <?=currency_format($summary['Base'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?>
                            </td>
                            <td class="text-right">
                                <?=$summary['Rate']?>%
                            </td>
                            <td class="text-right">
                                <?=currency_format($summary['Amount'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </td>
            <td style="width:50%;">
                <?php if ( ! empty($document['Payments'])) { ?>
                    <label><strong><?=t('Payment')?> :</strong></label>
                    <table class="tax" style="width:100%;margin-top:3px;">
                        <tr>
                            <th><?=t('Date')?></th>
                            <th><?=t('Amount')?></th>
                            <th><?=t('PaymentMethod')?></th>
                        </tr>
                        <?php foreach($document['Payments'] as $payment) { ?>
                            <tr>
                                <td class="text-center">
                                    <?=printDate($settings['GENERAL']['DateFormat'], $payment['Date'])?>
                                </td>
                                <td class="text-right">
                                    <?=currency_format($payment['Amount'], $settings['GENERAL']['Decimals'], $settings['GENERAL']['DecimalSeparator'], '', $settings['GENERAL']['CurrencySymbol'], $settings['GENERAL']['CurrencyPlacement'])?>
                                </td>
                                <td class="text-center">
                                    <?=$payment['PaymentMethodName']?>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                <?php } ?>
            </td>
    </table>

    <?php if ($settings['GENERAL']['QuoteNote'] != '') { ?>
        <p style="position: absolute;bottom: 0;">
            <?=nl2br(str_replace(array_map(function($v) { return '{'.$v.'}';}, array_keys($settings['GENERAL'])), array_values($settings['GENERAL']), $settings['GENERAL']['QuoteNote']))?>
        </p>
    <?php } ?>

</body>
</html>
