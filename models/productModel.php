<?php

class ProductModel {

    static function load($id) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        if ($id == 0)
        {
            return array(
                'ID'                    => '0',
                'Reference'             => '',
                'Type'                  => '1',
                'Name'                  => '',
                'Description'           => '',
                'Price'                 => '0.00',
                'PriceTTC'              => '0.00',
                'PurchasePrice'         => '0.00',
                'PurchasePriceTTC'      => '0.00',
                'UnityID'               => '',
                'UnityValue'            => '',
                'TaxID'                 => '',
                'TaxName'               => '',
                'TaxRate'               => '',
                'PurchaseTaxID'         => '',
                'PurchaseTaxName'       => '',
                'PurchaseTaxRate'       => '',
                'OnSale'                => '1',
                'Active'                => '1'
            );
        }

        $sql = "SELECT
                    products.*,
                    unities.Value                   AS UnityValue,
                    taxes.Name                      AS TaxName,
                    COALESCE(taxes.Rate, 0)         AS TaxRate,
                    purchaseTaxes.Name              AS PurchaseTaxName,
                    COALESCE(purchaseTaxes.Rate, 0) AS PurchaseTaxRate
                FROM hlx_products AS products   LEFT JOIN hlx_taxes AS taxes ON (TaxID = taxes.ID)
                                                                    LEFT JOIN hlx_taxes AS purchaseTaxes ON (PurchaseTaxID = purchaseTaxes.ID)
                                                                    LEFT JOIN hlx_settings AS unities ON (unities.Type = 'UNITY' AND UnityID = unities.ID)
                WHERE products.ID = ".(int) $id;
        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC) : false;
        if ( $rows === false /*&&*/ )//TODO catch no result
        {
            return false;
        }

        $row = $rows[0];

        //Compute TTC Amount
        $row['PriceTTC'] = $row['Price'] * $row['TaxRate'] / 100 + $row['Price'];
        $row['PurchasePriceTTC'] = $row['PurchasePrice'] * $row['PurchaseTaxRate'] / 100 + $row['PurchasePrice'];

        $res->closeCursor();

        return $row;

    }

    static function save($data) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }
        //Correct nuber format
        $data['Price'] = str_replace(',', '.', $data['Price']);
        $data['PurchasePrice'] = str_replace(',', '.', $data['PurchasePrice']);

        if ($data['ID'] == 0)
        {
            $sql = "INSERT INTO hlx_products (Reference, Type, Name, Description, Price, PurchasePrice, UnityID, TaxID, PurchaseTaxID, OnSale, Active)
                        VALUES (
                            ".(isset($data['Reference']) ? $db->quote($data['Reference']) : "''").",
                            ".(isset($data['Type']) ? $db->quote($data['Type']) : "''").",
                            ".(isset($data['Name']) ? $db->quote($data['Name']) : "''").",
                            ".(isset($data['Description']) ? $db->quote($data['Description']) : "''").",
                            ".(isset($data['Price']) ? $data['Price'] *1.0 : "''").",
                            ".(isset($data['PurchasePrice']) ? $data['PurchasePrice']*1.0 : "''").",
                            ".(isset($data['UnityID']) ? $data['UnityID']*1 : "''").",
                            ".(isset($data['TaxID']) ? $data['TaxID']*1 : "''").",
                            ".(isset($data['PurchaseTaxID']) ? $data['PurchaseTaxID']*1 : "''").",
                            ".(isset($data['OnSale']) ? $data['OnSale']*1 : "''").",
                            ".(isset($data['Active']) ? $data['Active']*1 : "''")."
                        )";
            $res = $db->query($sql);
            if ( ! $res)
                return false;
            $data['ID'] = $db->lastInsertId();
        }
        else
        {
            $sql = "UPDATE hlx_products SET
                        Reference           = ".(isset($data['Reference']) ? $db->quote($data['Reference']) : "''").",
                        Type                = ".(isset($data['Type']) ? $db->quote($data['Type']) : "''").",
                        Name                = ".(isset($data['Name']) ? $db->quote($data['Name']) : "''").",
                        Description         = ".(isset($data['Description']) ? $db->quote($data['Description']) : "''").",
                        Price               = ".(isset($data['Price']) ? $data['Price']*1.0 : "''").",
                        PurchasePrice       = ".(isset($data['PurchasePrice']) ? $data['PurchasePrice']*1.0 : "''").",
                        UnityID             = ".(isset($data['UnityID']) ? $data['UnityID']*1 : "''").",
                        TaxID               = ".(isset($data['TaxID']) ? $data['TaxID']*1 : "''").",
                        PurchaseTaxID       = ".(isset($data['PurchaseTaxID']) ? $data['PurchaseTaxID']*1 : "''").",
                        OnSale              = ".(isset($data['OnSale']) ? $data['OnSale']*1 : "''").",
                        Active              = ".(isset($data['Active']) ? $data['Active']*1 : "''")."
                    WHERE ID = ".(int) $data['ID'];
            $res = $db->query($sql);
            if ( ! $res)
                return false;
        }

        return ProductModel::load($data['ID']);

    }

    static function delete($id) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        $sql = "DELETE FROM hlx_products WHERE ID = ".$id*1;
        $res = $db->query($sql);
        if ($res === false)
            return false;
        else
            return true;

    }

    static function getList($args) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        $where = '';

        switch($args['sortBy']) {
            case 'ID':
                $orderBy = 'ORDER BY products.ID';
                break;
             case 'Type':
                $orderBy = 'ORDER BY products.Type';
                break;
            case 'Name':
                $orderBy = 'ORDER BY products.Name';
                break;
            case 'UnityValue':
                $orderBy = 'ORDER BY UnityValue';
                break;
            case 'TaxName':
                $orderBy = 'ORDER BY TaxName';
                break;
            case 'Price':
                $orderBy = 'ORDER BY products.Price';
                break;
            case 'Active':
                $orderBy = 'ORDER BY products.Active';
                break;
            case 'Reference':
            default:
                $orderBy = 'ORDER BY LPAD(products.Reference, 50, \'0\')';
                //SQLITE hack
                $orderBy = 'ORDER BY SUBSTR("0000000000000000000000000000000000000000" || products.Reference, -40, 40)';
        }

        if (isset($args['sortDirection']) && $args['sortDirection'] == 'desc')
            $orderBy .= ' DESC';
        else
            $orderBy .= ' ASC';

        if ($args['searchBy'] != '')
        {

            $search = preg_replace('#[^a-zA-Z0-9 ]#', '', $args['searchBy']);
            $where = "WHERE (products.Name LIKE CONCAT ('%', ".$db->quote($args['searchBy']). ", '%')";
            $where .= " OR products.Description LIKE CONCAT ('%', ".$db->quote($args['searchBy']). ", '%')";
            $where .= " OR products.Reference LIKE CONCAT ('%', ".$db->quote($args['searchBy']). ", '%')";
            $where .= ")";
        }
        if ($args['filterBy'] != '')
        {
            //TODO fonction de filtre utilisateur actif /inactiff
            switch($args['filterBy']) {
                case 'active':
                    $where .= ($where != '' ? ' AND ':'WHERE ').'products.Active = 1';
                    break;
                case 'inactive':
                    $where .= ($where != '' ? ' AND ':'WHERE ').'products.Active = 0';
                    break;
                case 'onSale':
                    $where .= ($where != '' ? ' AND ':'WHERE ').'products.OnSale = 1';
                    break;
                case 'notOnSale':
                    $where .= ($where != '' ? ' AND ':'WHERE ').'products.OnSale = 0';
                    break;
                case 'all':
                default:
                    break;
            }
        }

        $sql = "SELECT
                        products.*,
                        unities.Value   AS UnityValue,
                        taxes.Name      AS TaxName,
                        taxes.Rate      AS TaxRate
                    FROM hlx_products AS products   LEFT JOIN hlx_taxes AS taxes ON (TaxID = taxes.ID)
                                                                        LEFT JOIN hlx_settings AS unities ON (unities.Type = 'UNITY' AND UnityID = unities.ID)
                    ".$where." ".$orderBy." LIMIT ".(int) $args['pageIndex'] * (int) $args['pageSize'].",  ".(int) $args['pageSize'];
        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC): false;
        if ( $rows === false)
        {
            return false;
        }
        $res->closeCursor();

        $sql = "SELECT COUNT(*) AS total
                    FROM hlx_products AS products   LEFT JOIN hlx_taxes AS taxes ON (TaxID = taxes.ID)
                                                                        LEFT JOIN hlx_settings AS unities ON (unities.Type = 'UNITY' AND UnityID = unities.ID)
                    ".$where;
        $res = $db->query($sql);
        $rTotal = $res ? $res->fetch(PDO::FETCH_ASSOC) : false;
        if ( $rTotal === false)
        {
            return false;
        }
        $res->closeCursor();

        return  array(
            'total' => $rTotal['total'],
            'items' => $rows
        );

    }

}
