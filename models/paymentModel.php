<?php

class PaymentModel {

    static function load($id) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        if ($id == 0)
        {
            return array(
                'ID'                    => '0',
                'Serial'                => '',
                'DocumentID'            => '',
                'DocumentReference'     => '',
                'DocumentTotal'         => '',
                'DocumentDateValidated' => '',
                'DocumentDateCreated'   => '',
                'Date'                  => '',
                'ThirdPartyID'          => '',
                'ThirdPartyName'        => '',
                'ThirdPartyFirstname'   => '',
                'Amount'                => '',
                'PaymentMethodID'       => '',
                'PaymentMethodName'     => '',
                'Note'                  => ''
            );
        }

        $sql = "SELECT
                    payments.*,
                    docs.DateCreated        AS DocumentDateCreated,
                    docs.DateValidated      AS DocumentDateValidated,
                    docs.Reference          AS DocumentReference,
                    docs.Total              AS DocumentTotal,
                    thirds.ID               AS ThirdPartyID,
                    thirds.Name             AS ThirdPartyName,
                    thirds.Firstname        AS ThirdPartyFirstname,
                    thirds.Firstname        AS ThirdPartyFirstname,
                    paymentMethods.Value    AS PaymentMethodName
                FROM hlx_payments AS payments   LEFT JOIN hlx_documents AS docs ON (DocumentID = docs.ID)
                                                LEFT JOIN hlx_thirdparties AS thirds ON (ThirdPartyID = thirds.ID)
                                                LEFT JOIN hlx_settings AS paymentMethods ON (paymentMethods.Type = 'PAYMENT' AND PaymentMethodID = paymentMethods.ID)
                WHERE payments.ID = ".(int) $id;
        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC) : false;
        if ( $rows === false /*&&*/ )//TODO catch no result
        {
            return false;
        }

        $row = $rows[0];

        $res->closeCursor();

        return $row;

    }

    static function save($data) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        //TODO Check mandatory field !!!

        require_once('models/settingModel.php');
        $settings  = SettingModel::load(array('GENERAL'));

        //Correct number format
        $data['Amount'] = str_replace(',', '.', $data['Amount']);

        if ($data['ID'] == 0)
        {
            $sql = "INSERT INTO hlx_payments (Serial, DocumentID, DocTypeID, PaymentMethodID, Date, Amount, Note)
                        VALUES (
                            (SELECT Serial FROM hlx_documents WHERE ID = ".($data['DocumentID']*1)."),
                            ".(isset($data['DocumentID']) ? $data['DocumentID']*1 : "''").",
                            (SELECT DocTypeID FROM hlx_documents WHERE ID = ".($data['DocumentID']*1)."),
                            ".(isset($data['PaymentMethodID']) ? $data['PaymentMethodID']*1 : "''").",
                            ".(isset($data['Date']) ? $db->quote(convertDateFormat($data['Date'], $settings['GENERAL']['DateFormat'], 'Y-m-d H:i:s', true)) : "''").",
                            ".(isset($data['Amount']) ? $data['Amount']*1.0 : "''").",
                            ".(isset($data['Note']) ? $db->quote($data['Note']) : "''")."
                        )";
            $res = $db->query($sql);
            if ( ! $res)
                return false;
            $data['ID'] = $db->lastInsertId();
        }
        else
        {
            $sql = "UPDATE hlx_payments SET
                        Serial              = (SELECT Serial FROM hlx_documents WHERE ID = ".($data['DocumentID']*1)."),
                        DocumentID          = ".(isset($data['DocumentID']) ? $data['DocumentID']*1 : "''").",
                        DocTypeID           = (SELECT DocTypeID FROM hlx_documents WHERE ID = ".($data['DocumentID']*1)."),
                        Date                = ".(isset($data['Date']) ? $db->quote(convertDateFormat($data['Date'], $settings['GENERAL']['DateFormat'], 'Y-m-d H:i:s', true)) : "''").",
                        PaymentMethodID     = ".(isset($data['PaymentMethodID']) ? $data['PaymentMethodID']*1 : "''").",
                        Amount              = ".(isset($data['Amount']) ? $data['Amount']*1.0 : "''").",
                        Note                = ".(isset($data['Note']) ? $db->quote($data['Note']) : "''")."
                    WHERE ID = ".(int) $data['ID'];
            $res = $db->query($sql);
            if ( ! $res)
                return false;
        }

        $payment = self::load($data['ID']);
        //Update document
        require_once('models/documentModel.php');
        $settings  = DocumentModel::updatePaidAmount($payment['Serial']);

        return $payment;

    }

    static function delete($id) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }
        //Get documentID
        $payment = self::load($id);

        $sql = "DELETE FROM hlx_payments WHERE ID = ".$id*1;
        $res = $db->query($sql);
        if ($res === false)
            return false;

        //Update document
        require_once('models/documentModel.php');
        $settings  = DocumentModel::updatePaidAmount($payment['Serial']);

        return true;

    }

    static function getList($args, $thirdPartyID = false) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        $where = '';

        switch($args['sortBy']) {
            case 'ID':
                $orderBy = 'ORDER BY payments.ID';
                break;
            case 'DocumentDateCreated':
                $orderBy = 'ORDER BY DocumentDateCreated';
                break;
            case 'DocumentReference':
                $orderBy = 'ORDER BY LPAD(DocumentReference, 50, \'0\')';
                //SQlite hack
                $orderBy = 'ORDER BY SUBSTR("0000000000000000000000000000000000000000" || DocumentReference, -40, 40)';
                break;
            case 'PaymentMethodName':
                $orderBy = 'ORDER BY PaymentMethodName';
                break;
            case 'ThirdPartyName':
                $orderBy = 'ORDER BY ThirdPartyName';
                break;
            case 'ID':
                $orderBy = 'ORDER BY payments.ID';
            case 'Date':
            default:
                $orderBy = 'ORDER BY payments.Date';
                break;
        }

        if (isset($args['sortDirection']) && $args['sortDirection'] == 'asc')
            $orderBy .= ' ASC';
        else
            $orderBy .= ' DESC';

        if ($args['searchBy'] != '')
        {
            $search = preg_replace('#[^a-zA-Z0-9 ]#', '', $args['searchBy']);
            $where = "WHERE (thirds.Name LIKE CONCAT ('%', ".$db->quote($args['searchBy']). ", '%')";
            $where .= " OR thirds.Firstname LIKE CONCAT ('%', ".$db->quote($args['searchBy']). ", '%')";
            $where .= " OR docs.Reference LIKE CONCAT ('%', ".$db->quote($args['searchBy']). ", '%')";
            $where .= ")";
        }

        if ($thirdPartyID !== false)
        {
            $where .= ($where != '' ? ' AND ':'WHERE ').'thirds.ID = '. (int) $thirdPartyID;
        }

        if ($args['filterBy'] != '')
        {

        }

        $sql = "SELECT
                    payments.*,
                    docs.DateCreated        AS DocumentDateCreated,
                    docs.Reference          AS DocumentReference,
                    thirds.ID               AS ThirdPartyID,
                    thirds.Name             AS ThirdPartyName,
                    thirds.Firstname        AS ThirdPartyFirstname,
                    thirds.Firstname        AS ThirdPartyFirstname,
                    paymentMethods.Value    AS PaymentMethodName
                FROM hlx_payments AS payments   LEFT JOIN hlx_documents AS docs ON (DocumentID = docs.ID)
                                                LEFT JOIN hlx_thirdparties AS thirds ON (ThirdPartyID = thirds.ID)
                                                LEFT JOIN hlx_settings AS paymentMethods ON (paymentMethods.Type = 'PAYMENT' AND paymentMethodID = paymentMethods.ID)
                ".$where." ".$orderBy." LIMIT ".(int) $args['pageIndex'] * (int) $args['pageSize'].",  ".(int) $args['pageSize'];
        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC): false;
        if ( $rows === false)
        {
            return false;
        }
        $res->closeCursor();

        $sql = "SELECT COUNT(*) AS total
                FROM hlx_payments AS payments   LEFT JOIN hlx_documents AS docs ON (DocumentID = docs.ID)
                                                LEFT JOIN hlx_thirdparties AS thirds ON (ThirdPartyID = thirds.ID)
                                                LEFT JOIN hlx_settings AS paymentMethods ON (paymentMethods.Type = 'PAYMENT' AND paymentMethodID = paymentMethods.ID)
                    ".$where;
        $res = $db->query($sql);
        $rTotal = $res ? $res->fetch(PDO::FETCH_ASSOC) : false;
        if ( $rTotal === false)
        {
            return false;
        }
        $res->closeCursor();

        return  array(
            'total' => $rTotal['total'],
            'items' => $rows
        );

    }

}
