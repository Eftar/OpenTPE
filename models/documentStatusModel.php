<?php

class DocumentStatusModel {

    const INVOICE_PAID      = 11;
    const INVOICE_VALIDATED = 10;
    const QUOTE_CLOSED      = 8;
    const ORDER_INVOICED    = 16;
    const PURCHASE_PAID     = 27;
    const PURCHASE_RECEIVED = 25;

    static function loadAll($docType = false) {

        static $rStatutes = array();

        if (empty($rStatutes))
        {
            $db = HLX_Motor::getInstance()->getDatabase();
            if ( ! $db)
            {
                return false;
            }

            $sql = "SELECT
                        *
                    FROM hlx_documents_statutes
                    ORDER BY DocTypeID ASC, Position ASC";

            $res = $db->query($sql);
            $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC) : false;
            if ($rows === false)
            {
                return false;
            }
            else
            {
                foreach ($rows as $status)
                {
                    $rStatutes[ $status['DocTypeID'] ][ $status['ID'] ] = $status;
                }
            }
        }
        if ($docType == false)
            return $rStatutes;
        else
            return $rStatutes[ $docType ];

    }

}
