<?php

class ThirdPartyModel {

    static function loadAll($thirdPartyType = 'customers') {

        $rResult = self::getList(array(
            'sortBy'        => 'Name',
            'sortDirection' => 'ASC',
            'type'          => $thirdPartyType,
            'searchBy'      => '',
            'filterBy'      => 'active',
            'pageIndex'     => 0,
            'pageSize'      => 1000
        ));

        return $rResult['items'];

    }

    static function load($id) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        if ($id == 0)
        {
            return array(
                'ID'            => '0',
                'Type'          => '',
                'Title'         => '',
                'Name'          => '',
                'Firstname'     => '',
                'Address1'      => '',
                'Address2'      => '',
                'Address3'      => '',
                'PostalCode'    => '',
                'City'          => '',
                'State'         => '',
                'CountryID'     => '',
                'Phone'         => '',
                'Fax'           => '',
                'Mobile'        => '',
                'Email'         => '',
                'Web'           => '',
                'VAT'           => '',
                'Active'        => '1',
                'Invoiced'      => '0.00',
                'Paid'          => '0.00',
                'Balance'       => '0.00',
                'Puchased'      => '0.00'
            );
        }

        $sql = "SELECT
                        thirds.*,
                        Invoiced - Paid AS Balance
                    FROM hlx_thirdparties thirds
                    WHERE thirds.ID = ".(int) $id;

        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC) : false;
        if ( $rows === false /*&&*/ )//TODO catch no result
        {
            return false;
        }
        $res->closeCursor();

        return $rows[0];

    }

    static function save($data) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        if ($data['ID'] == 0)
        {
            $sql = "INSERT INTO hlx_thirdparties (Type, TitleID, Name, Firstname, Address1, Address2,  Address3, PostalCode, City, State, CountryID, Phone, Fax, Mobile, Email, Web, VAT, Active)
                            VALUES (
                                ".(isset($data['Type']) ? $db->quote($data['Type']) : "''").",
                                ".(isset($data['TitleID']) ? $db->quote($data['TitleID']) : "''").",
                                ".(isset($data['Name']) ? $db->quote($data['Name']) : "''").",
                                ".(isset($data['Firstname']) ? $db->quote($data['Firstname']) : "''").",
                                ".(isset($data['Address1']) ? $db->quote($data['Address1']) : "''").",
                                ".(isset($data['Address2']) ? $db->quote($data['Address2']) : "''").",
                                ".(isset($data['Address3']) ? $db->quote($data['Address3']) : "''").",
                                ".(isset($data['PostalCode']) ? $db->quote($data['PostalCode']) : "''").",
                                ".(isset($data['City']) ? $db->quote($data['City']) : "''").",
                                ".(isset($data['State']) ? $db->quote($data['StateID']) : "''").",
                                ".(isset($data['CountryID']) ? $db->quote($data['CountryID']) : "''").",
                                ".(isset($data['Phone']) ? $db->quote($data['Phone']) : "''").",
                                ".(isset($data['Fax']) ? $db->quote($data['Fax']) : "''").",
                                ".(isset($data['Mobile']) ? $db->quote($data['Mobile']) : "''").",
                                ".(isset($data['Email']) ? $db->quote($data['Email']) : "''").",
                                ".(isset($data['Web']) ? $db->quote($data['Web']) : "''").",
                                ".(isset($data['VAT']) ? $db->quote($data['VAT']) : "''").",
                                ".(isset($data['Active']) ? $db->quote($data['Active']) : "''")."
                            )";
            $res = $db->query($sql);
            if ( ! $res)
                return false;
            $data['ID'] = $db->lastInsertId();
        }
        else
        {
            $sql = "UPDATE hlx_thirdparties SET
                            Type        = ".(isset($data['Type']) ? $db->quote($data['Type']) : "''").",
                            TitleID     = ".(isset($data['TitleID']) ? $db->quote($data['TitleID']) : "''").",
                            Name        = ".(isset($data['Name']) ? $db->quote($data['Name']) : "''").",
                            Firstname   = ".(isset($data['Firstname']) ? $db->quote($data['Firstname']) : "''").",
                            Address1    = ".(isset($data['Address1']) ? $db->quote($data['Address1']) : "''").",
                            Address2    = ".(isset($data['Address2']) ? $db->quote($data['Address2']) : "''").",
                            Address3    = ".(isset($data['Address3']) ? $db->quote($data['Address3']) : "''").",
                            PostalCode  = ".(isset($data['PostalCode']) ? $db->quote($data['PostalCode']) : "''").",
                            City        = ".(isset($data['City']) ? $db->quote($data['City']) : "''").",
                            State       = ".(isset($data['StateID']) ? $db->quote($data['StateID']) : "''").",
                            CountryID   = ".(isset($data['CountryID']) ? $db->quote($data['CountryID']) : "''").",
                            Phone       = ".(isset($data['Phone']) ? $db->quote($data['Phone']) : "''").",
                            Fax         = ".(isset($data['Fax']) ? $db->quote($data['Fax']) : "''").",
                            Mobile      = ".(isset($data['Mobile']) ? $db->quote($data['Mobile']) : "''").",
                            Email       = ".(isset($data['Email']) ? $db->quote($data['Email']) : "''").",
                            Web         = ".(isset($data['Web']) ? $db->quote($data['Web']) : "''").",
                            VAT         = ".(isset($data['VAT']) ? $db->quote($data['VAT']) : "''").",
                            Active      = ".(isset($data['Active']) ? $db->quote($data['Active']) : "''")."
                        WHERE ID = ".(int) $data['ID'];
            $res = $db->query($sql);
            if ( ! $res)
                return false;
        }

        return ThirdPartyModel::load($data['ID']);

    }

    static function getList($args) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        $where = '';

        switch($args['sortBy']) {
            case 'ID':
                $orderBy = 'ORDER BY ID';
                break;
            case 'Address':
                $orderBy = 'ORDER BY Address1';
                break;
            case 'Balance':
                $orderBy = 'ORDER BY Balance';
                break;
            case 'Active':
                $orderBy = 'ORDER BY Active';
                break;
            case 'Name':
            default:
                $orderBy = 'ORDER BY Name';
        }

        if (isset($args['sortDirection']) && $args['sortDirection'] == 'desc')
            $orderBy .= ' DESC';
        else
            $orderBy .= ' ASC';

        if ($args['searchBy'] != '')
        {

            $search = preg_replace('#[^a-zA-Z0-9 ]#', '', $args['searchBy']);
            $where = "WHERE (name LIKE CONCAT ('%', ".$db->quote($args['searchBy']). ", '%')";
            $where .= " OR city LIKE CONCAT('%', ".$db->quote($args['searchBy']). ", '%')";
            $where .= " OR address1 LIKE CONCAT('%', ".$db->quote($args['searchBy']). ", '%')";
            $where .= " OR address2 LIKE CONCAT('%', ".$db->quote($args['searchBy']). ", '%')";
            $where .= " OR address3 LIKE CONCAT('%', ".$db->quote($args['searchBy']). ", '%')";
            $where .= ")";
        }

        if ($args['filterBy'] != '')
        {
            //TODO fonction de filtre utilisateur actif /inactif
            switch($args['filterBy']) {
                case 'active':
                    $where .= ($where != '' ? ' AND ':'WHERE ').'Active = 1';
                    break;
                case 'inactive':
                    $where .= ($where != '' ? ' AND ':'WHERE ').'Active = 0';
                    break;
                case 'all':
                default:
                    break;
            }
        }

        if ($args['type'] != '')
        {
            switch($args['type']) {
                case 'suppliers':
                    $where .= ($where != '' ? ' AND ':'WHERE ')."Type = 'S'";
                    break;
                case 'customers':
                default:
                    $where .= ($where != '' ? ' AND ':'WHERE ')."Type = 'C'";
                    break;
            }
        }

        $sql = "SELECT
                    *,
                    Invoiced - Paid AS Balance
                FROM hlx_thirdparties
                ".$where." ".$orderBy."
                LIMIT ".(int) $args['pageIndex'] * (int) $args['pageSize'].",  ".(int) $args['pageSize'];
        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC) : false;
        if ( $rows === false)
        {
            return false;
        }
        $res->closeCursor();

        $sql = "SELECT COUNT(*) AS total FROM hlx_thirdparties ".$where;
        $res = $db->query($sql);
        $rTotal = $res ? $res->fetch(PDO::FETCH_ASSOC) : false;
        if ( $rTotal === false)
        {
            return false;
        }
        $res->closeCursor();

        return  array(
            'total' => $rTotal['total'],
            'items' => $rows
        );

    }

}
