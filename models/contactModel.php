<?php

class ContactModel {

    static function load($id) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        if ($id == 0)
        {
            return array(
                'ID'                => '0',
                'Title'             => '',
                'Name'              => '',
                'Firstname'         => '',
                'Function'          => '',
                'ThirdPartyID'      => '',
                'Phone'             => '',
                'Fax'               => '',
                'Mobile'            => '',
                'Email'             => '',
                'Active'            => '1',
                'CompanyName'       => '',
                'CompanyType'       => '',
                'CompanyID'         => '',
            );
        }

        $sql = "SELECT
                        contacts.*,
                        third.Name  AS CompanyName,
                        third.Type  AS CompanyType,
                        third.ID        AS CompanyID
                    FROM hlx_contacts AS contacts LEFT JOIN hlx_thirdparties AS third ON (ThirdPartyID = third.ID)
                    WHERE contacts.ID = ".(int) $id;
        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC) : false;
        if ( $rows === false /*&&*/ )//TODO catch no result
        {
            return false;
        }
        $res->closeCursor();

        return $rows[0];

    }

    static function save($data) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        if ($data['ID'] == 0)
        {
            $sql = "INSERT INTO hlx_contacts (TitleID, Name, Firstname, `Function`, ThirdPartyID, Phone, Fax, Mobile, Email, Active)
                            VALUES (
                                ".(isset($data['TitleID']) ? (int) $data['TitleID'] : "0").",
                                ".(isset($data['Name']) ? $db->quote($data['Name']) : "''").",
                                ".(isset($data['Firstname']) ? $db->quote($data['Firstname']) : "''").",
                                ".(isset($data['Function']) ? $db->quote($data['Function']) : "''").",
                                ".(isset($data['ThirdPartyID']) ? (int) $data['ThirdPartyID'] : "''").",
                                ".(isset($data['Phone']) ? $db->quote($data['Phone']) : "''").",
                                ".(isset($data['Fax']) ? $db->quote($data['Fax']) : "''").",
                                ".(isset($data['Mobile']) ? $db->quote($data['Mobile']) : "''").",
                                ".(isset($data['Email']) ? $db->quote($data['Email']) : "''").",
                                ".(isset($data['Active']) ? $db->quote($data['Active']) : "''")."
                            )";
            $res = $db->query($sql);
            if ( ! $res)
                return false;
            $data['ID'] = $db->lastInsertId();
        }
        else
        {
            $sql = "UPDATE hlx_contacts SET
                            TitleID         = ".(isset($data['TitleID']) ? (int) $data['TitleID'] : "''").",
                            Name            = ".(isset($data['Name']) ? $db->quote($data['Name']) : "''").",
                            Firstname       = ".(isset($data['Firstname']) ? $db->quote($data['Firstname']) : "''").",
                            `Function`      = ".(isset($data['Function']) ? $db->quote($data['Function']) : "''").",
                            ThirdPartyID    = ".(isset($data['ThirdPartyID']) ? (int) $data['ThirdPartyID'] : "''").",
                            Phone           = ".(isset($data['Phone']) ? $db->quote($data['Phone']) : "''").",
                            Fax             = ".(isset($data['Fax']) ? $db->quote($data['Fax']) : "''").",
                            Mobile          = ".(isset($data['Mobile']) ? $db->quote($data['Mobile']) : "''").",
                            Email           = ".(isset($data['Email']) ? $db->quote($data['Email']) : "''").",
                            Active          = ".(isset($data['Active']) ? $db->quote($data['Active']) : "''")."
                        WHERE ID = ".(int) $data['ID'];
            $res = $db->query($sql);
            if ( ! $res)
                return false;
        }

        return ContactModel::load($data['ID']);

    }

    static function getList($args, $thirdPartyID = false) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        $where = '';

        switch($args['sortBy']) {
            case 'ID':
                $orderBy = 'ORDER BY contacts.ID';
                break;
            case 'Function':
                $orderBy = 'ORDER BY Function';
                break;
            case 'Firstname':
                $orderBy = 'ORDER BY contacts.Firstname';
                break;
            case 'Company':
                $orderBy = 'ORDER BY Company';
                break;
            case 'Active':
                $orderBy = 'ORDER BY contacts.Active';
                break;
            case 'Name':
            default:
                $orderBy = 'ORDER BY contacts.Name';
        }

        if (isset($args['sortDirection']) && $args['sortDirection'] == 'desc')
            $orderBy .= ' DESC';
        else
            $orderBy .= ' ASC';

        if ($args['searchBy'] != '')
        {

            $search = preg_replace('#[^a-zA-Z0-9 ]#', '', $args['searchBy']);
            $where = "WHERE (contacts.Name LIKE CONCAT ('%', ".$db->quote($args['searchBy']). ", '%')";
            $where .= " OR Function LIKE CONCAT('%', ".$db->quote($args['searchBy']). ", '%')";
            $where .= " OR contacts.Firstname LIKE CONCAT('%', ".$db->quote($args['searchBy']). ", '%')";
            $where .= " OR third.Name LIKE CONCAT('%', ".$db->quote($args['searchBy']). ", '%')";
            $where .= ")";
        }
        if ($args['filterBy'] != '')
        {
            //TODO fonction de filtre utilisateur actif /inactiff
            switch($args['filterBy']) {
                case 'active':
                    $where .= ($where != '' ? ' AND ':'WHERE ').'contacts.Active = 1';
                    break;
                case 'inactive':
                    $where .= ($where != '' ? ' AND ':'WHERE ').'contacts.Active = 0';
                    break;
                case 'all':
                default:
                    break;
            }
        }

        if ($thirdPartyID !== false)
        {
            $where .= ($where != '' ? ' AND ':'WHERE ').'third.ID = '. (int) $thirdPartyID;
        }

        $sql = "SELECT
                        contacts.*,
                        third.Name  AS CompanyName,
                        third.Type  AS CompanyType,
                        third.ID    AS CompanyID
                    FROM hlx_contacts AS contacts   LEFT JOIN hlx_thirdparties AS third ON (ThirdPartyID = third.ID)
                    ".$where." ".$orderBy." LIMIT ".(int) $args['pageIndex'] * (int) $args['pageSize'].",  ".(int) $args['pageSize'];
        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC): false;
        if ( $rows === false)
        {
            return false;
        }
        $res->closeCursor();

        $sql = "SELECT COUNT(*) AS total
                    FROM hlx_contacts AS contacts LEFT JOIN hlx_thirdparties AS third ON (ThirdPartyID = third.ID)
                    ".$where;
        $res = $db->query($sql);
        $rTotal = $res ? $res->fetch(PDO::FETCH_ASSOC) : false;
        if ( $rTotal === false)
        {
            return false;
        }
        $res->closeCursor();

        return  array(
            'total' => $rTotal['total'],
            'items' => $rows
        );

    }

}
