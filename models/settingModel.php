<?php

class SettingModel {


    static $rCache = array();

    /**
     * TODO USE cache with curDate index
     * */
    static function load($rParam, $curDate = true) {

        $rResult = array();

        if ($curDate === true)
            $curDate = date('Y-m-d');
        elseif ($curDate !== false)
            $curDate = substr($curDate, 0, 10);

        foreach($rParam as $k => $param)
        {
            if (isset(self::$rCache[ $curDate ][ $param ]))
            {
                $rResult[ $param ] = self::$rCache[ $curDate ][ $param ];
                unset($rParam[ $k ]);
            }
        }

        if (empty($rParam))
            return $rResult;

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        if ( ! is_array($rParam) || count($rParam) == 0)
        {
            return false;
        }

        foreach($rParam as &$param)
        {
            $param = $db->quote($param);
        }

        $sql = "SELECT
                    settings.*
                FROM hlx_settings AS settings
                WHERE settings.Type IN (".implode(',', $rParam).")
                ORDER BY Name ASC";
        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC) : false;
        if ($rows === false /*&&*/ )//TODO catch no result
        {
            return false;
        }
        $res->closeCursor();

        foreach($rows as $row)
        {
            if ($row['Type'] == 'GENERAL' || $row['Type'] == 'COUNTER')
            {
                $rResult[ $row['Type'] ][ $row['Name'] ] = $row['Value'];
                self::$rCache[ $curDate ][ $row['Type'] ][ $row['Name'] ] = $row['Value'];
            }
            else
            {
                if ($curDate == false)
                {
                    $rResult[ $row['Type'] ][ $row['ID'] ] = $row;
                    self::$rCache[ $curDate ][ $row['Type'] ][ $row['ID'] ] = $row;
                }
                elseif ($row['ActiveFrom'] <= $curDate && $curDate <= ($row['ActiveTo'] != '0000-00-00' ? $row['ActiveTo'] : '9999-99-99'))
                {
                    $rResult[ $row['Type'] ][ $row['ID'] ] = $row;
                    self::$rCache[ $curDate ][ $row['Type'] ][ $row['ID'] ] = $row;
                }
            }
        }

        return $rResult;

    }

    static function save($data) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        $oldSettings = self::load(array('GENERAL'));

        foreach ($data as $type => $settings)
        {
            if ($type == 'GENERAL' || $type == 'COUNTER')
            {
                foreach ($settings as $key => $value)
                {
                    //Check some value
                    if ($key == 'DateFormat') {
                        $value = str_replace(array('D','M'), array('d','m'), $value);
                        $value = preg_replace('#([^dmyY\-/\\ .]*)#', '', $value);
                    }
                    if ($key == 'Decimals')
                        $value = (int) $value;
                    if ($key == 'DecimalSeparator' && ! in_array($value, array(',', '.')))
                        $value = '.';
                    $sql = "UPDATE hlx_settings SET Value = ".$db->quote($value)." WHERE Name = ".$db->quote($key)." AND Type = ".$db->quote($type);
                    $res = $db->query($sql);
                }
            }
            else
            {
                foreach ($settings as $setting)
                {
                    if (isset($setting['ID']) && $setting['ID'] != '')
                    {
                        $sql = "UPDATE hlx_settings SET
                                    Value       = ".$db->quote($setting['Value']).",
                                    Name        = ".$db->quote($setting['Name']).",
                                    ActiveFrom  = ".($setting['ActiveFrom'] == '' || $setting['ActiveFrom'] == '0000-00-00' ? "'0000-00-00'" : $db->quote(convertDateFormat($setting['ActiveFrom'], $oldSettings['GENERAL']['DateFormat'], 'Y-m-d', true))).",
                                    ActiveTo    = ".($setting['ActiveTo'] == '' || $setting['ActiveTo'] == '0000-00-00' ? "'0000-00-00'" : $db->quote(convertDateFormat($setting['ActiveTo'], $oldSettings['GENERAL']['DateFormat'], 'Y-m-d', true)))."
                                WHERE   ID = ".$db->quote($setting['ID'])."
                                        AND Type = ".$db->quote($type);
                        $res = $db->query($sql);
                    }
                    else
                    {
                        $sql = "INSERT INTO hlx_settings (Type, Name, Value, ActiveFrom, ActiveTo) VALUES (
                                    ".$db->quote($type).",
                                    ".$db->quote($setting['Name']).",
                                    ".$db->quote($setting['Value']).",
                                    ".($setting['ActiveFrom'] == '' || $setting['ActiveFrom'] == '0000-00-00' ? "'0000-00-00'" : $db->quote(convertDateFormat($setting['ActiveFrom'], $oldSettings['GENERAL']['DateFormat'], 'Y-m-d', true))).",
                                    ".($setting['ActiveTo'] == '' || $setting['ActiveTo'] == '0000-00-00' ? "'0000-00-00'" : $db->quote(convertDateFormat($setting['ActiveTo'], $oldSettings['GENERAL']['DateFormat'], 'Y-m-d', true)))."
                                )";
                        $res = $db->query($sql);
                    }
                }
            }
        }

        //clear cache
        self::$rCache = array();

    }

    static function getDocumentTemplates($type, $desc = false)
    {
        $rTemplate = array();

        if ( ! in_array($type, array('quote', 'order', 'invoice', 'purchase')))
        {
            return false;
        }

        $path = HLX_STORE.'templates/'.$type.'/';

        foreach (glob($path."*.php") as $filename) {
            $name = substr(basename($filename), 0, -4);
            $rTemplate[ $name ] = array(
                'Path' => $filename,
                'Name' => $name,
                'Desc' => ($desc ? SettingModel::getDocumentTemplatesDesc($filename) : '')
            );
        }

        return $rTemplate;

    }

    static function getDocumentTemplatesDesc($filename)
    {

        $desc = '';
        $content = file_get_contents($filename);
        if ($content)
        {
            preg_match('`<!--DESC([^-->]*)-->`', $content, $matches);
            if ($matches && isset($matches[1]))
            {
                $desc = trim($matches[1]);
            }
        }
        return $desc;
    }

    static function getLogo()
    {
        if (is_file(HLX_STORE.'logo/logo.jpg'))
        {
            return HLX_STORE.'logo/logo.jpg';
        }
        elseif (is_file(HLX_STORE.'logo/logo.png'))
        {
            return HLX_STORE.'logo/logo.png';
        }
        elseif (is_file(HLX_STORE.'logo/logo.gif'))
        {
            return HLX_STORE.'logo/logo.gif';
        }
        return false;
    }


}
