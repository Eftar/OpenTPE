<?php

class NoteModel {

    static function loadAll() {

        $rResult = self::getList(array(
            'sortBy'        => 'ID',
            'sortDirection' => 'ASC',
            'searchBy'      => '',
            'filterBy'      => '',
            'pageIndex'     => 0,
            'pageSize'      => 1000
        ));

        if ( ! $rResult)
            return false;

        return $rResult['items'];

    }

    static function loadAllTags() {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        $sql = "SELECT
                    GROUP_CONCAT(Tags) AS Tags
                FROM hlx_notes AS notes
                WHERE Tags <> ''";
        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC) : false;
        if ( $rows === false)
        {
            return false;
        }
        $res->closeCursor();

        if (isset($rows[0]))
            return array_unique(explode(',', $rows[0]['Tags']));
        else
            return array();

    }

    static function load($id) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        if ($id == 0)
        {
            return array(
                'ID'                    => '0',
                'Tags'                  => array(),
                'Name'                  => '',
                'Description'           => ''
            );
        }

        $sql = "SELECT
                    notes.*
                FROM hlx_notes AS notes
                WHERE notes.ID = ".(int) $id;
        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC) : false;
        if ( $rows === false /*&&*/ )//TODO catch no result
        {
            return false;
        }

        $row = $rows[0];

        //Compute TTC Amount
        $row['Tags'] = explode(',', $row['Tags']);

        $res->closeCursor();

        return $row;

    }

    static function save($data) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        if ($data['ID'] == 0)
        {
            $sql = "INSERT INTO hlx_notes (Name, Tags, Description)
                        VALUES (
                            ".(isset($data['Name']) ? $db->quote($data['Name']) : "''").",
                            ".(isset($data['Tags']) ? $db->quote($data['Tags']) : "''").",
                            ".(isset($data['Description']) ? $db->quote($data['Description']) : "''")."
                        )";
            $res = $db->query($sql);
            if ( ! $res)
                return false;
            $data['ID'] = $db->lastInsertId();
        }
        else
        {
            $sql = "UPDATE hlx_notes SET
                        Name                = ".(isset($data['Name']) ? $db->quote($data['Name']) : "''").",
                        Tags                = ".(isset($data['Tags']) ? $db->quote($data['Tags']) : "''").",
                        Description         = ".(isset($data['Description']) ? $db->quote($data['Description']) : "''")."
                    WHERE ID = ".(int) $data['ID'];
            $res = $db->query($sql);
            if ( ! $res)
                return false;
        }

        return NoteModel::load($data['ID']);

    }

    static function delete($id) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        $sql = "DELETE FROM hlx_notes WHERE ID = ".$id*1;
        $res = $db->query($sql);
        if ($res === false)
            return false;
        else
            return true;

    }

    static function getList($args) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        $where = '';

        switch($args['sortBy']) {
            case 'ID':
                $orderBy = 'ORDER BY notes.ID';
                break;
            case 'Name':
            default:
                $orderBy = 'ORDER BY notes.Name';
                break;
        }

        if (isset($args['sortDirection']) && $args['sortDirection'] == 'desc')
            $orderBy .= ' DESC';
        else
            $orderBy .= ' ASC';

        if ($args['searchBy'] != '')
        {

            $search = preg_replace('#[^a-zA-Z0-9 ]#', '', $args['searchBy']);
            $where = "WHERE (notes.Name LIKE CONCAT ('%', ".$db->quote($args['searchBy']). ", '%')";
            $where .= " OR notes.Description LIKE CONCAT ('%', ".$db->quote($args['searchBy']). ", '%')";
            $where .= ")";
        }
        if ($args['filterBy'] != '')
        {
            //TODO fonction de filtre utilisateur actif /inactiff
            switch($args['filterBy']) {
                case 'all':
                default:
                    break;
            }
        }

        $sql = "SELECT
                    notes.*
                FROM hlx_notes AS notes
                ".$where." ".$orderBy." LIMIT ".(int) $args['pageIndex'] * (int) $args['pageSize'].",  ".(int) $args['pageSize'];
        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC): false;
        if ( $rows === false)
        {
            return false;
        }
        $res->closeCursor();

        foreach ($rows as &$row)
        {
            $row['Tags'] = explode(',', $row['Tags']);
        }

        $sql = "SELECT COUNT(*) AS total
                    FROM hlx_notes AS notes
                    ".$where;
        $res = $db->query($sql);
        $rTotal = $res ? $res->fetch(PDO::FETCH_ASSOC) : false;
        if ( $rTotal === false)
        {
            return false;
        }
        $res->closeCursor();

        return  array(
            'total' => $rTotal['total'],
            'items' => $rows
        );

    }

}
