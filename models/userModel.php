<?php

class UserModel {

    static function load($id) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        if ($id == 0)
        {
            return array(
                'ID'        => '0',
                'Name'      => '',
                'Login'     => '',
                'Password'  => '',
                'Role'      => '',
                'Active'    => '1'
            );
        }

        $sql = "SELECT
                        *
                    FROM hlx_users AS users
                    WHERE users.ID = ".(int) $id;
        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC) : false;
        if ( $rows === false /*&&*/ )//TODO catch no result
        {
            return false;
        }
        $res->closeCursor();

        return $rows[0];

    }

    static function saveAll($data) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        foreach ($data as $user)
        {
            $password = ($user['Password'] != '' ? password_hash($user['Password'], PASSWORD_DEFAULT) : false);

            if (isset($user['ID']) && $user['ID'] != '')
            {
                $sql = "UPDATE hlx_users SET
                            Name    = ".$db->quote($user['Name']).",
                            Email   = ".$db->quote($user['Email']).",
                            ".($password ? "Password = ".$db->quote($password)."," : "")."
                            Role    = ".$db->quote($user['Role']).",
                            Active  = ".$db->quote($user['Active'])."
                        WHERE ID = ".$db->quote($user['ID']);
                $res = $db->query($sql);
            }
            else
            {
                $date = date('Y-m-d H:i:s');
                $sql = "INSERT INTO hlx_users (Name, Email, Password, Role, Active, DateCreated) VALUES (
                            ".$db->quote($user['Name']).",
                            ".$db->quote($user['Email']).",
                            ".$db->quote($Password).",
                            ".$db->quote($user['Role']).",
                            ".$db->quote($user['Active']).",
                            ".$db->quote($date)."
                        )";
                $res = $db->query($sql);
            }
        }

        return true;

    }

    static function loadAll() {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        $sql = "SELECT * FROM hlx_users AS users ORDER BY Name ASC";
        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC) : false;
        if ( $rows === false /*&&*/ )//TODO catch no result
        {
            return false;
        }
        $res->closeCursor();
        foreach($rows as &$row)
        {
            $row['Password'] = '';
        }

        return $rows;

    }

}
