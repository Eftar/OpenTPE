<?php

class DocumentModel {

    const DOC_TYPE_QUOTE    = 1;
    const DOC_TYPE_ORDER    = 2;
    const DOC_TYPE_INVOICE  = 4;
    const DOC_TYPE_PURCHASE = 5;

    const LINE_TYPE_PRODUCT = 1;
    const LINE_TYPE_SERVICE = 2;
    const LINE_TYPE_COMMENT = 0;

    static function getDocumentTypes($group = 'sell') {

        if ( ! in_array($group, array('all', 'sell', 'purchase')))
            $group = 'sell';

        static $rDocTypes = array();

        if ( ! empty($rDocTypes))
            return $rDocTypes[ $group ];

        $rDocTypes['sell'] = array(
            self::DOC_TYPE_QUOTE    => 'quote',
            self::DOC_TYPE_ORDER    => 'order',
            self::DOC_TYPE_INVOICE  => 'invoice'
        );
        $rDocTypes['purchase'] = array(
            self::DOC_TYPE_PURCHASE   => 'purchase'
        );

        require_once('models/settingModel.php');
        $settings  = SettingModel::load(array('GENERAL'));

        $rDocTypes['all'] = $rDocTypes['sell'] + $rDocTypes['purchase'];

        return $rDocTypes[ $group ];

    }

    static function loadUnpaidDocuments($thirdPartyID = false) {

        $rResult = self::getList(array(
            'sortBy'        => 'DateValidated',
            'sortDirection' => 'ASC',
            'type'          => 'payable',
            'searchBy'      => '',
            'filterBy'      => 'unpaid',
            'pageIndex'     => 0,
            'pageSize'      => 100
        ), $thirdPartyID);

        return $rResult['items'];

    }

    //TODO rename fucntion
    static function updatePaidAmount($serial) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        //update invoice with payment
        $sql = "UPDATE hlx_documents SET
                    Paid = COALESCE((SELECT
                                        SUM(pay.Amount)
                                    FROM hlx_payments AS pay
                                    WHERE pay.DocumentID = hlx_documents.ID), 0.00)
                WHERE   hlx_documents.DocTypeID IN (".self::DOC_TYPE_INVOICE.", ".self::DOC_TYPE_PURCHASE.")
                        AND hlx_documents.Serial = ".$db->quote($serial);

        $res = $db->query($sql);
        if ($res === false /*&&*/ )//TODO catch no result
        {
            return false;
        }
        $res->closeCursor();

        require_once('models/documentStatusModel.php');

        //Set Status Paid (for purchase : according current status position < paid status position)
        $sql = "UPDATE
                    hlx_documents
                SET
                    DocStatusID =   CASE DocTypeID
                                        WHEN ".self::DOC_TYPE_INVOICE." THEN ".DocumentStatusModel::INVOICE_PAID."
                                        WHEN ".self::DOC_TYPE_PURCHASE." THEN CASE
                                                                            WHEN    (SELECT Position FROM hlx_documents_statutes AS ds WHERE ds.ID = hlx_documents.DocStatusID AND ds.DocTypeID = hlx_documents.DocTypeID)
                                                                                    <
                                                                                    (SELECT Position FROM hlx_documents_statutes WHERE ID = ".DocumentStatusModel::PURCHASE_PAID.")
                                                                                THEN ".DocumentStatusModel::PURCHASE_PAID."
                                                                            ELSE DocStatusID
                                                                        END
                                    END
                WHERE   DocTypeID IN (".self::DOC_TYPE_INVOICE.", ".self::DOC_TYPE_PURCHASE.")
                        AND Serial = ".$db->quote($serial)."
                        AND Total = Paid
                        AND Total <> 0";

        $res = $db->query($sql);
        if ($res === false /*&&*/ )//TODO catch no result
        {
            return false;
        }
        $res->closeCursor();

        //get thirdPartyID
        $sql = "SELECT
                    MIN(ThirdPartyID) AS ThirdPartyID
                FROM hlx_documents
                WHERE Serial = ".$db->quote($serial);

        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC) : false;
        if ($rows === false /*&&*/ )//TODO catch no result
        {
            return false;
        }
        $res->closeCursor();

        $thirdPartyID = $rows[0]['ThirdPartyID'];

        //UPDATE ThirdParty Amount
        $sql = "UPDATE hlx_thirdparties SET
                    Invoiced    = COALESCE((SELECT
                                                SUM(Total)
                                            FROM hlx_documents documents
                                            WHERE   documents.ThirdPartyID = ".($thirdPartyID * 1)."
                                                    AND DocTypeID = ".self::DOC_TYPE_INVOICE."), 0.00),
                    Paid        = COALESCE((SELECT
                                                SUM(Amount)
                                            FROM hlx_payments payments LEFT JOIN hlx_documents documents ON payments.DocumentID = documents.ID
                                            WHERE documents.ThirdPartyID = ".($thirdPartyID * 1)."), 0.00),
                    Purchased   = COALESCE((SELECT
                                                SUM(Total)
                                            FROM hlx_documents documents
                                            WHERE   documents.ThirdPartyID = ".($thirdPartyID * 1)."
                                                    AND DocTypeID = ".self::DOC_TYPE_PURCHASE."), 0.00)

                WHERE ID = ".($thirdPartyID * 1);

        $res = $db->query($sql);
        if ($res === false /*&&*/ )//TODO catch no result
        {
            return false;
        }
        $res->closeCursor();

    }

    static function load($id, $full = true) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        if ($id == 0)
        {//TODO faire coincider l'ordre des clefs avec celui des champs de la requête pour simplifier la maintenance
            return array(
                'ID'                    => '0',
                'Reference'             => '',
                'ExternalReference'     => '',
                'Serial'                => '',
                'QuoteID'               => '0',
                'OrderID'               => '0',
                'InvoiceID'             => '0',
                'Ordered'               => '0',
                'Invoiced'              => '0',
                'Received'              => '0',
                'IsDeposit'             => '0',
                'ThirdPartyID'          => '0',
                'ThirdPartyName'        => '',
                'ThirdPartyFirstname'   => '',
                'ThirdPartyAddress1'    => '',
                'ThirdPartyAddress2'    => '',
                'ThirdPartyAddress3'    => '',
                'ThirdPartyPostalCode'  => '',
                'ThirdPartyCity'        => '',
                'ThirdPartyState'       => '',
                'ThirdPartyCountryID'   => '0',//TODO put default language
                'ThirdPartyPhone'       => '',
                'ThirdPartyMobile'      => '',
                'ThirdPartyFax'         => '',
                'ThirdPartyEmail'       => '',
                'InvoiceName'           => '',
                'InvoiceFirstname'      => '',
                'InvoiceAddress1'       => '',
                'InvoiceAddress2'       => '',
                'InvoiceAddress3'       => '',
                'InvoicePostalCode'     => '',
                'InvoiceCity'           => '',
                'InvoiceState'          => '',
                'InvoiceCountryID'      => '0',//TODO put default language
                'DeliveryName'          => '',
                'DeliveryFirstname'     => '',
                'DeliveryAddress1'      => '',
                'DeliveryAddress2'      => '',
                'DeliveryAddress3'      => '',
                'DeliveryPostalCode'    => '',
                'DeliveryCity'          => '',
                'DeliveryState'         => '',
                'DeliveryCountryID'     => '0',//TODO put default language
                'UserID'                => '0',
                'UserName'              => '',
                'DocStatusID'           => '',
                'DocStatusName'         => '',
                'DocStatusPosition'     => '',
                'DocStatusColor'        => '',
                'DocStatusReadOnly'     => '',
                'DocStatusDraft'        => '',
                'DocStatusValidated'    => '',
                'DocStatusClosed'       => '',
                'DocTypeID'             => '0',
                'DateCreated'           => '',
                'DateUpdated'           => '',
                'DateValidated'         => '',
                'DateDue'               => '',
                'DateReceived'          => '',
                'TaxID'                 => '0',
                'TaxName'               => '',
                'TaxRate'               => '',
                'DiscountType'          => '%',
                'DiscountValue'         => '',
                'Terms'                 => '',
                'DocumentKey'           => '',
                'Lines'                 => array(),
                'SubtotalItems'         => 0.00,
                'DiscountAmount'        => 0.00,
                'DiscountTax'           => 0.00,
                'Subtotal'              => 0.00,
                'TaxAmount'             => 0.00,
                'TaxSummary'            => array(),
                'Total'                 => 0.00,
                'Paid'                  => 0.00
            );
        }

        $sql = "SELECT
                    docs.*,
                    COALESCE(taxes.Rate, 0)     AS TaxRate,
                    COALESCE(taxes.Name, '')    AS TaxName,
                    docStatutes.Name            AS DocStatusName,
                    docStatutes.Position        AS DocStatusPosition,
                    docStatutes.Color           AS DocStatusColor,
                    docStatutes.ReadOnly        AS DocStatusReadOnly,
                    docStatutes.Draft           AS DocStatusDraft,
                    docStatutes.Validated       AS DocStatusValidated,
                    docStatutes.Closed          AS DocStatusClosed,
                    thirds.Name                 AS ThirdPartyName,
                    thirds.Firstname            AS ThirdPartyFirstname,
                    thirds.Address1             AS ThirdPartyAddress1,
                    thirds.Address2             AS ThirdPartyAddress2,
                    thirds.Address3             AS ThirdPartyAddress3,
                    thirds.PostalCode           AS ThirdPartyPostalCode,
                    thirds.City                 AS ThirdPartyCity,
                    thirds.CountryID            AS ThirdPartyCountryID,
                    thirds.Phone                AS ThirdPartyPhone,
                    thirds.Mobile               AS ThirdPartyMobile,
                    thirds.Fax                  AS ThirdPartyFax,
                    thirds.Email                AS ThirdPartyEmail
                FROM hlx_documents AS docs  LEFT JOIN hlx_thirdparties          AS thirds ON (ThirdPartyID = thirds.ID)
                                            LEFT JOIN hlx_documents_statutes    AS docStatutes ON (docs.DocStatusID = docStatutes.ID)
                                            LEFT JOIN hlx_taxes                 AS taxes ON (docs.TaxID = taxes.ID)
                WHERE docs.ID = ".(int) $id;

        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC) : false;
        if ($rows === false /*&&*/ )//TODO catch no result
        {
            return false;
        }
        $res->closeCursor();

        $document = $rows[0];

        $document['SubtotalItems']      = 0.00;
        $document['DiscountAmount']     = 0.00;
        $document['DiscountTax']        = 0.00;
        $document['Subtotal']           = 0.00;
        $document['TaxAmount']          = 0.00;
        $document['TaxSummary']         = array();
        $document['Total']              = 0.00;

        //Chargement des lignes
        $sql = "SELECT
                    docLines.*,
                    COALESCE(taxes.Rate, 0)     AS TaxRate,
                    COALESCE(taxes.Name, '')    AS TaxName,
                    COALESCE(unities.Value, '') AS UnityName
                FROM hlx_documents_lines docLines   LEFT JOIN hlx_taxes AS taxes ON (docLines.TaxID = taxes.ID)
                                                    LEFT JOIN hlx_settings AS unities ON (unities.Type = 'UNITY' AND docLines.UnityID = unities.ID)
                WHERE DocumentID = ".(int) $id."
                ORDER BY Number";

        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC) : false;
        if ($rows === false)
        {
            $document['Lines'] = array();
        }
        else
        {

            foreach ($rows as $line)
            {

                $line['Subtotal']        = sprintf('%.2f', $line['Price'] * $line['Quantity']);
                $line['DiscountAmount']  = sprintf('%.2f', ($line['DiscountType'] == '%' ? $line['Subtotal'] * $line['DiscountValue'] / 100 : $line['DiscountValue']));
                $line['SubtotalNet']     = sprintf('%.2f', $line['Subtotal'] - $line['DiscountAmount']);
                $line['TaxAmount']       = sprintf('%.2f', ($line['Subtotal'] - $line['DiscountAmount']) * $line['TaxRate'] / 100);
                $line['Total']           = sprintf('%.2f', $line['Subtotal'] - $line['DiscountAmount'] + $line['TaxAmount']);

                if ($line['TaxID'] != '')
                {
                    if ( ! isset($document['TaxSummary'][ $line['TaxID'] ]))
                        $document['TaxSummary'][ $line['TaxID'] ] = array(
                            'ID'        => $line['TaxID'],
                            'Base'      => $line['Subtotal'] - $line['DiscountAmount'],
                            'Rate'      => $line['TaxRate'],
                            'Amount'    => $line['TaxAmount']
                        );
                    else
                    {
                        $document['TaxSummary'][ $line['TaxID'] ]['Amount'] += $line['TaxAmount'];
                        $document['TaxSummary'][ $line['TaxID'] ]['Base']   += $line['Subtotal'] - $line['DiscountAmount'];
                    }
                }
                $document['SubtotalItems']  += ($line['Subtotal'] - $line['DiscountAmount']);
                $document['TaxAmount']      += $line['TaxAmount'];

                $document['Lines'][] = $line;
            }

            $res->closeCursor();
        }

        $document['SubtotalItems']  = sprintf('%.2f', $document['SubtotalItems']);
        $document['DiscountAmount'] = sprintf('%.2f', ($document['DiscountType'] == '%' ? $document['SubtotalItems'] * $document['DiscountValue'] / 100 : $document['DiscountValue']));
        if ($document['DiscountAmount'] > 0)
        {
            $document['DiscountTax'] = sprintf('%.2f', $document['DiscountAmount'] * $document['TaxRate'] / 100);
            //Remove negative tax
            if ($document['TaxID'] != '')
            {
                if ( ! isset($document['TaxSummary'][ $document['TaxID'] ]))
                    $document['TaxSummary'][ $document['TaxID'] ] = array(
                        'ID'        => $document['TaxID'],
                        'Base'      => $document['DiscountAmount'] * -1,
                        'Rate'      => $document['TaxRate'],
                        'Amount'    => $document['DiscountTax'] * -1
                    );
                else
                {
                    $document['TaxSummary'][ $document['TaxID'] ]['Amount'] -= $document['DiscountTax'];
                    $document['TaxSummary'][ $document['TaxID'] ]['Base'] -= $document['DiscountAmount'];
                }
            }
        }
        else
            $document['DiscountTax'] = sprintf('%.2f', 0.00);

        //Clear empty tax
        foreach ($document['TaxSummary'] as $taxID => $taxDetails)
        {
            if ($taxDetails['Amount'] == 0)
                unset($document['TaxSummary'][ $taxID ]);
        }

        $document['Subtotal']       = sprintf('%.2f', ($document['SubtotalItems'] - $document['DiscountAmount']));
        $document['TaxAmount']      = sprintf('%.2f', $document['TaxAmount'] - $document['DiscountTax']);
        $document['Total']          = sprintf('%.2f', $document['Subtotal'] + $document['TaxAmount']);

        //get Other document and payment
        if ($full == true)
        {
            $document['Documents'] = array();
            //Load linked document
            $sql = "SELECT
                        ID
                    FROM hlx_documents
                    WHERE   Serial = ".$db->quote($document['Serial'])."
                            AND ID <> ".(int) $id."
                    ORDER BY DateCreated";

            $res = $db->query($sql);
            $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC) : false;
            if ($rows)
            {
                foreach ($rows as $doc)
                {
                    $document['Documents'][] = self::load($doc['ID'], false);
                }
                $res->closeCursor();
            }


            $document['Payments'] = array();
            require_once('models/paymentModel.php');
            //Load linked document
            $sql = "SELECT
                        ID
                    FROM hlx_payments
                    WHERE   DocumentID = ".($document['ID'] * 1)."
                            AND DocTypeID = ".($document['DocTypeID'] * 1)."
                    ORDER BY Date";

            $res = $db->query($sql);
            $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC) : false;
            if ($rows)
            {
                foreach ($rows as $pay)
                {
                    $document['Payments'][] = PaymentModel::load($pay['ID']);
                }
                $res->closeCursor();
            }

        }

        return $document;

    }

    static function save($data) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        if ( ! in_array($data['DocTypeID'], array(self::DOC_TYPE_QUOTE, self::DOC_TYPE_ORDER, self::DOC_TYPE_INVOICE, self::DOC_TYPE_PURCHASE)))
        {
            return false;
        }

        require_once('models/settingModel.php');
        $settings = SettingModel::load(array('GENERAL', 'COUNTER'));

        require_once('models/documentStatusModel.php');
        $docStatutes = DocumentStatusModel::loadAll($data['DocTypeID']);

        if (isset($docStatutes[ $data['DocStatusID'] ]))
        {
            $docStatus = $docStatutes[ $data['DocStatusID'] ];
        }
        else
        {
            $docStatus = array_shift($docStatutes);
            $data['DocStatusID'] = $docStatus['ID'];
        }

        //If there
        if ($data['Reference'] == '' && $docStatus['Draft'] == 0)
        {
            if ($data['DocTypeID'] == self::DOC_TYPE_QUOTE)
            {
                $format = $settings['GENERAL']['QuoteReferenceFormat'];
            }
            elseif ($data['DocTypeID'] == self::DOC_TYPE_ORDER)
            {
                $format = $settings['GENERAL']['OrderReferenceFormat'];
            }
            elseif ($data['DocTypeID'] == self::DOC_TYPE_INVOICE)
            {
                $format = $settings['GENERAL']['InvoiceReferenceFormat'];
            }
            elseif ($data['DocTypeID'] == self::DOC_TYPE_PURCHASE)
            {
                $format = $settings['GENERAL']['PurchaseReferenceFormat'];
            }

            $settings['COUNTER'][ $data['DocTypeID'] ] += 1;
            $counter = $settings['COUNTER'][ $data['DocTypeID'] ];
            SettingModel::save($settings);
            $counter = str_pad($counter, 4, '0', STR_PAD_LEFT);
            $data['Reference'] = str_replace(array('{id}', '{year}', '{YEAR}', '{month}'), array($counter, date('y'), date('Y'), date('m')), $format);
        }

        //Add date validation if status is validated
        if ($data['DateValidated'] == '' && $docStatus['Validated'] == 1)
        {
            $data['DateValidated'] = 'CURRENT_TIMESTAMP';
        }
        //Reassign value if record exist
        elseif ($data['DateValidated'] != '' && $data['ID'] != 0)
        {
            $data['DateValidated'] = 'DateValidated';
        }
        else
        {
            $data['DateValidated'] = "'0000-00-00'";
        }

        //For purchase document Add received flag if status is received
        if ($docStatus['Received'] == 1)
        {
            $received = 1;
        }
        //Reassign if record exist
        elseif ($data['ID'] != 0)
        {
            $received = 'Received';
        }
        //Defaul value for creation
        else
        {
            $received = 0;
        }

        //Correct nuber format
        $data['DiscountValue'] = str_replace(',', '.', $data['DiscountValue']);

        if ($data['ID'] == 0)
        {

            $sql = "INSERT INTO hlx_documents (Reference, ExternalReference, Serial, QuoteID, OrderID, InvoiceID, Ordered, Invoiced, Received, IsDeposit, ThirdPartyID, DocStatusID, DocTypeID, DateCreated, DateUpdated, DateValidated, DateDue, DateReceived,
                                                InvoiceName, InvoiceFirstname, InvoiceAddress1, InvoiceAddress2, InvoiceAddress3, InvoicePostalCode, InvoiceCity, InvoiceState, InvoiceCountryID,
                                                DeliveryName, DeliveryFirstname, DeliveryAddress1, DeliveryAddress2, DeliveryAddress3, DeliveryPostalCode, DeliveryCity, DeliveryState, DeliveryCountryID,
                                                TaxID, DiscountType, DiscountValue)
                            VALUES (
                                ".$db->quote($data['Reference']).",
                                ".$db->quote($data['ExternalReference']).",
                                ".(isset($data['Serial']) ? $db->quote($data['Serial']) : "'".uniqid('', true)."'").",
                                ".(isset($data['QuoteID']) ? $data['QuoteID']*1 : "0").",
                                ".(isset($data['OrderID']) ? $data['OrderID']*1 : "0").",
                                ".(isset($data['InvoiceID']) ? $data['InvoiceID']*1 : "0").",
                                ".(isset($data['Ordered']) ? $data['Ordered']*1 : "0").",
                                ".(isset($data['Invoiced']) ? $data['Invoiced']*1 : "0").",
                                $received,
                                ".(isset($data['IsDeposit']) ? $data['IsDeposit']*1 : "0").",
                                ".(isset($data['ThirdPartyID']) ? $data['ThirdPartyID']*1 : "0").",
                                ".(isset($data['DocStatusID']) ? $data['DocStatusID']*1 : "0").",
                                ".(isset($data['DocTypeID']) ? $data['DocTypeID']*1 : "0").",
                                CURRENT_TIMESTAMP,
                                CURRENT_TIMESTAMP,
                                ".$data['DateValidated'].",
                                ".(isset($data['DateDue']) ? $db->quote(convertDateFormat($data['DateDue'], $settings['GENERAL']['DateFormat'], 'Y-m-d', true)) : "''").",
                                ".((isset($data['DateReceived']) && $data['DateReceived'] != '') ? $db->quote(convertDateFormat($data['DateReceived'], $settings['GENERAL']['DateFormat'], 'Y-m-d', true)) : "0000-00-00").",
                                ".(isset($data['InvoiceName']) ? $db->quote($data['InvoiceName']) : "''").",
                                ".(isset($data['InvoiceFirstname']) ? $db->quote($data['InvoiceFirstname']) : "''").",
                                ".(isset($data['InvoiceAddress1']) ? $db->quote($data['InvoiceAddress1']) : "''").",
                                ".(isset($data['InvoiceAddress2']) ? $db->quote($data['InvoiceAddress2']) : "''").",
                                ".(isset($data['InvoiceAddress3']) ? $db->quote($data['InvoiceAddress3']) : "''").",
                                ".(isset($data['InvoicePostalCode']) ? $db->quote($data['InvoicePostalCode']) : "''").",
                                ".(isset($data['InvoiceCity']) ? $db->quote($data['InvoiceCity']) : "''").",
                                ".(isset($data['InvoiceState']) ? $db->quote($data['InvoiceState']) : "''").",
                                ".(isset($data['InvoiceCountryID']) ? $data['InvoiceCountryID'] * 1 : "''").",
                                ".(isset($data['DeliveryName']) ? $db->quote($data['DeliveryName']) : "''").",
                                ".(isset($data['DeliveryFirstname']) ? $db->quote($data['DeliveryFirstname']) : "''").",
                                ".(isset($data['DeliveryAddress1']) ? $db->quote($data['DeliveryAddress1']) : "''").",
                                ".(isset($data['DeliveryAddress2']) ? $db->quote($data['DeliveryAddress2']) : "''").",
                                ".(isset($data['DeliveryAddress3']) ? $db->quote($data['DeliveryAddress3']) : "''").",
                                ".(isset($data['DeliveryPostalCode']) ? $db->quote($data['DeliveryPostalCode']) : "''").",
                                ".(isset($data['DeliveryCity']) ? $db->quote($data['DeliveryCity']) : "''").",
                                ".(isset($data['DeliveryState']) ? $db->quote($data['DeliveryState']) : "''").",
                                ".(isset($data['DeliveryCountryID']) ? $data['DeliveryCountryID'] * 1 : "''").",
                                ".(isset($data['TaxID']) ? $data['TaxID']*1 : "0").",
                                ".(isset($data['DiscountType']) ? $db->quote($data['DiscountType']) : "''").",
                                ".(isset($data['DiscountValue']) ? $data['DiscountValue'] * 1.0: "0.0")."
                            )";
            $res = $db->query($sql);
            if ( ! $res)
                return false;
            $data['ID'] = $db->lastInsertId();
        }
        else
        {
            $sql = "UPDATE hlx_documents SET
                            Reference           = ".(isset($data['Reference']) ? $db->quote($data['Reference']) : "''").",
                            ExternalReference   = ".(isset($data['ExternalReference']) ? $db->quote($data['ExternalReference']) : "''").",
                            QuoteID             = ".(isset($data['QuoteID']) ? $data['QuoteID'] * 1 : "0").",
                            OrderID             = ".(isset($data['OrderID']) ? $data['OrderID'] * 1 : "0").",
                            InvoiceID           = ".(isset($data['InvoiceID']) ? $data['InvoiceID'] * 1 : "0").",
                            Ordered             = ".(isset($data['Ordered']) ? $data['Ordered'] * 1 : "0").",
                            Invoiced            = ".(isset($data['Invoiced']) ? $data['Invoiced'] * 1 : "0").",
                            Received            = ".$received.",
                            IsDeposit           = ".(isset($data['IsDeposit']) ? $data['IsDeposit'] * 1 : "0").",
                            ThirdPartyID        = ".(isset($data['ThirdPartyID']) ? $data['ThirdPartyID'] * 1 : "0").",
                            DocStatusID         = ".(isset($data['DocStatusID']) ? $data['DocStatusID'] * 1 : "0").",
                            DocTypeID           = ".(isset($data['DocTypeID']) ? $data['DocTypeID'] * 1 : "0").",
                            DateUpdated         = CURRENT_TIMESTAMP,
                            DateValidated       = ".$data['DateValidated'].",
                            DateDue             = ".(isset($data['DateDue']) ? $db->quote(convertDateFormat($data['DateDue'], $settings['GENERAL']['DateFormat'], 'Y-m-d', true)) : "''").",
                            DateReceived        = ".((isset($data['DateReceived']) && $data['DateReceived'] != '') ? $db->quote(convertDateFormat($data['DateReceived'], $settings['GENERAL']['DateFormat'], 'Y-m-d', true)) : "0000-00-00").",
                            InvoiceName         = ".(isset($data['InvoiceName']) ? $db->quote($data['InvoiceName']) : "''").",
                            InvoiceFirstname    = ".(isset($data['InvoiceFirstname']) ? $db->quote($data['InvoiceFirstname']) : "''").",
                            InvoiceAddress1     = ".(isset($data['InvoiceAddress1']) ? $db->quote($data['InvoiceAddress1']) : "''").",
                            InvoiceAddress2     = ".(isset($data['InvoiceAddress2']) ? $db->quote($data['InvoiceAddress2']) : "''").",
                            InvoiceAddress3     = ".(isset($data['InvoiceAddress3']) ? $db->quote($data['InvoiceAddress3']) : "''").",
                            InvoicePostalCode   = ".(isset($data['InvoicePostalCode']) ? $db->quote($data['InvoicePostalCode']) : "''").",
                            InvoiceCity         = ".(isset($data['InvoiceCity']) ? $db->quote($data['InvoiceCity']) : "''").",
                            InvoiceState        = ".(isset($data['InvoiceState']) ? $db->quote($data['InvoiceState']) : "''").",
                            InvoiceCountryID    = ".(isset($data['InvoiceCountryID']) ? $data['InvoiceCountryID'] * 1 : "0").",
                            DeliveryName        = ".(isset($data['DeliveryName']) ? $db->quote($data['DeliveryName']) : "''").",
                            DeliveryFirstname   = ".(isset($data['DeliveryFirstname']) ? $db->quote($data['DeliveryFirstname']) : "''").",
                            DeliveryAddress1    = ".(isset($data['DeliveryAddress1']) ? $db->quote($data['DeliveryAddress1']) : "''").",
                            DeliveryAddress2    = ".(isset($data['DeliveryAddress2']) ? $db->quote($data['DeliveryAddress2']) : "''").",
                            DeliveryAddress3    = ".(isset($data['DeliveryAddress3']) ? $db->quote($data['DeliveryAddress3']) : "''").",
                            DeliveryPostalCode  = ".(isset($data['DeliveryPostalCode']) ? $db->quote($data['DeliveryPostalCode']) : "''").",
                            DeliveryCity        = ".(isset($data['DeliveryCity']) ? $db->quote($data['DeliveryCity']) : "''").",
                            DeliveryState       = ".(isset($data['DeliveryState']) ? $db->quote($data['DeliveryState']) : "''").",
                            DeliveryCountryID   = ".(isset($data['DeliveryCountryID']) ? $data['DeliveryCountryID'] * 1 : "0").",
                            TaxID               = ".(isset($data['TaxID']) ? $data['TaxID'] * 1 : "0").",
                            DiscountType        = ".(isset($data['DiscountType']) ? $db->quote($data['DiscountType']) : "''").",
                            DiscountValue       = ".(isset($data['DiscountValue']) ? $data['DiscountValue'] * 1.0 : "0")."
                        WHERE ID = ".(int) $data['ID'];
            $res = $db->query($sql);
            if ( ! $res)
                return false;
        }

        if (isset($data['Lines']))
        {
            $res = $db->query("DELETE FROM hlx_documents_lines WHERE DocumentID = ".(int) $data['ID']);
            if ( ! $res)
                return false;
            $i = 0;
            foreach($data['Lines'] as $line)
            {

                $line['Price']          = str_replace(',', '.', $line['Price']);
                $line['DiscountValue']  = str_replace(',', '.', $line['DiscountValue']);
                $line['Quantity']       = str_replace(',', '.', $line['Quantity']);

                $sql = "INSERT INTO hlx_documents_lines (DocumentID, Number, Type, Reference, Name, Description, Quantity, UnityID, Price, TaxID, DiscountType, DiscountValue)
                            VALUES (
                                ".(int) $data['ID'].",
                                ".$i.",
                                ".(isset($line['Type']) ? $db->quote($line['Type']) : "'P'").",
                                ".(isset($line['Reference']) ? $db->quote($line['Reference']) : "''").",
                                ".(isset($line['Name']) ? $db->quote($line['Name']) : "''").",
                                ".(isset($line['Description']) ? $db->quote($line['Description']) : "''").",
                                ".(isset($line['Quantity']) ? $line['Quantity'] * 1.0 : "0.0").",
                                ".(isset($line['UnityID']) ? $line['UnityID'] * 1 : "0").",
                                ".(isset($line['Price']) ? $line['Price'] * 1.0 : "0.0").",
                                ".(isset($line['TaxID']) ? $line['TaxID'] * 1 : "0").",
                                ".(isset($line['DiscountType']) ? $db->quote($line['DiscountType']) : "''").",
                                ".(isset($line['DiscountValue']) ? $line['DiscountValue'] * 1.0 : "0.0")."
                            )";
                $res = $db->query($sql);
                if ( ! $res)
                    return false;
                $i++;
            }
        }

        $data = DocumentModel::load($data['ID']);

        //Update Total in hlx_documents
        $db->query("UPDATE hlx_documents SET Total = ".($data['Total'] * 1.0)." WHERE ID = ".(int) $data['ID']);

        //Update amount paid for this serial
        self::updatePaidAmount($data['Serial']);

        return $data;

    }

    static function delete($id) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        $sql = "DELETE FROM hlx_documents WHERE DoctypeID = ".self::DOC_TYPE_QUOTE." AND ID = ".$id*1;
        $res = $db->query($sql);
        if ($res === false)
            return false;
        //TODO only if first request remove something
        $sql = "DELETE FROM hlx_documents_lines WHERE DocumentID = ".$id * 1;
        $res = $db->query($sql);
        if ($res === false)
            return false;
        else
            return true;

    }

    static function receivePurchase($id) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        require_once('models/documentStatusModel.php');

        //Set received && set status received if current status position < status received position
        $sql = "UPDATE
                    hlx_documents
                SET
                    Received = 1,
                    DateReceived =  CASE
                                        WHEN DateReceived IS NOT NULL THEN DateReceived
                                        ELSE CURRENT_TIMESTAMP
                                    END,
                    DocStatusID =   CASE
                                        WHEN    (SELECT Position FROM hlx_documents_statutes AS ds WHERE ds.ID = hlx_documents.DocStatusID AND ds.DocTypeID = hlx_documents.DocTypeID)
                                                <
                                                (SELECT Position FROM hlx_documents_statutes WHERE ID = ".DocumentStatusModel::PURCHASE_RECEIVED.")
                                            THEN ".DocumentStatusModel::PURCHASE_RECEIVED."
                                        ELSE DocStatusID
                                    END
                WHERE   DocTypeID = ".self::DOC_TYPE_PURCHASE."
                        AND ID = ".$db->quote($id);

        $res = $db->query($sql);
        if ($res === false /*&&*/ )//TODO catch no result
        {
            return false;
        }
        $res->closeCursor();

        return self::load($id);

    }

    static function getList($args, $thirdPartyID = false, $serial = false) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        $where = '';

        switch($args['sortBy']) {
            case 'State':
                $orderBy = 'ORDER BY docStatutes.Position';
                break;
            case 'DateDue':
                $orderBy = 'ORDER BY docs.DateDue';
                break;
            case 'ThirdParty':
                $orderBy = 'ORDER BY ThirdPartyName';
                break;
            case 'Total':
                $orderBy = 'ORDER BY docs.Total';
                break;
            case 'Reference':
                $orderBy = 'ORDER BY LPAD(docs.Reference, 50, \'0\')';
                //SQLite LPAD http://stackoverflow.com/a/35060424
                $orderBy = 'ORDER BY SUBSTR("0000000000000000000000000000000000000000" || docs.Reference, -40, 40)';
                break;
            case 'DateValidated':
            default:
                $orderBy = 'ORDER BY COALESCE(docs.DateValidated, docs.DateCreated)';
                break;
        }

        if (isset($args['sortDirection']) && $args['sortDirection'] == 'asc')
            $orderBy .= ' ASC';
        else
            $orderBy .= ' DESC';

        if ($args['type'] == 'payable')
            $where = "WHERE docs.DocTypeID IN (".self::DOC_TYPE_INVOICE.", ".self::DOC_TYPE_PURCHASE.")";
        elseif ($args['type'] != '')
            $where = "WHERE docs.DocTypeID = ".$args['type'] * 1;

        if ($args['searchBy'] != '')
        {
            $search = preg_replace('#[^a-zA-Z0-9 ]#', '', $args['searchBy']);
            $where .= ($where != '' ? ' AND ':'WHERE ')."(docs.Reference LIKE CONCAT ('%', ".$db->quote($args['searchBy']). ", '%')";
            $where .= " OR thirds.Name LIKE CONCAT ('%', ".$db->quote($args['searchBy']). ", '%')";
            $where .= ")";
        }

        if ($args['filterBy'] == 'unpaid')
            $where .= ($where != '' ? ' AND ':'WHERE ').'docs.Total <> docs.Paid AND docStatutes.Draft = 0';
        elseif ($args['filterBy'] == 'overdue' || $args['filterBy'] == 'expired')
            $where .= ($where != '' ? ' AND ':'WHERE ').'docs.DateDue < CURRENT_TIMESTAMP AND docStatutes.Closed = 0 AND docStatutes.Draft = 0';
        elseif ($args['filterBy'] != 'all' && $args['filterBy'] != '')
            $where .= ($where != '' ? ' AND ':'WHERE ').'docs.DocStatusID = '.(int) $args['filterBy'];

        if ($thirdPartyID != false)
        {
            $where .= ($where != '' ? ' AND ':'WHERE ').'docs.ThirdPartyID = '.(int) $thirdPartyID;
        }

        if ($serial != false)
        {
            $where .= ($where != '' ? ' AND ':'WHERE ').'docs.Serial = '.$db->quote($serial);
        }

        $sql = "SELECT
                    docs.*,
                    docStatutes.Name        AS DocStatusName,
                    docStatutes.Position    AS DocStatusPosition,
                    docStatutes.Color       AS DocStatusColor,
                    docStatutes.ReadOnly    AS DocStatusReadOnly,
                    docStatutes.Draft       AS DocStatusDraft,
                    docStatutes.Closed      AS DocStatusClosed,
                    invCountry.Name         AS InvoiceCountryName,
                    delivCountry.Name       AS DeliveryCountryName,
                    thirds.Name             AS ThirdPartyName,
                    thirds.Firstname        AS ThirdPartyFirstname,
                    thirds.Phone            AS ThirdPartyPhone,
                    thirds.Mobile           AS ThirdPartyMobile,
                    thirds.Fax              AS ThirdPartyFax,
                    thirds.Email            AS ThirdPartyEmail
                FROM hlx_documents AS docs  LEFT JOIN hlx_thirdparties          AS thirds ON (ThirdPartyID = thirds.ID)
                                            LEFT JOIN hlx_documents_statutes    AS docStatutes ON (docs.DocStatusID = docStatutes.ID)
                                            LEFT JOIN hlx_settings              AS invCountry ON (invCountry.Type = 'COUNTRY' AND InvoiceCountryID = invCountry.ID)
                                            LEFT JOIN hlx_settings              AS delivCountry ON (delivCountry.Type = 'COUNTRY' AND DeliveryCountryID = delivCountry.ID)
                ".$where." ".$orderBy." LIMIT ".(int) $args['pageIndex'] * (int) $args['pageSize'].",  ".(int) $args['pageSize'];

        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC): false;
        if ($rows === false)
        {
            return false;
        }
        $res->closeCursor();

        $sql = "SELECT
                    COUNT(*) AS total
                FROM hlx_documents AS docs  LEFT JOIN hlx_thirdparties          AS thirds ON (ThirdPartyID = thirds.ID)
                                            LEFT JOIN hlx_documents_statutes    AS docStatutes ON (docs.DocStatusID = docStatutes.ID)
                ".$where;

        $res = $db->query($sql);
        $rTotal = $res ? $res->fetch(PDO::FETCH_ASSOC) : false;
        if ($rTotal === false)
        {
            return false;
        }
        $res->closeCursor();

        return  array(
            'total' => $rTotal['total'],
            'items' => $rows
        );

    }

    static function getTotalDocumentsYear() {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        $rReturn = array();
        $month = date('n', mktime(date('h'), date('i'), date('s'), date('n'), date('d'), date('Y')-1));
        if ($month == 12)
            $month = 1;
        else
            $month++;
        $i = 0;
        while($i < 12)
        {
            $rReturn['1'][ $month ] = 0;
            $rReturn['2'][ $month ] = 0;
            $rReturn['4'][ $month ] = 0;

            if ($month == 12)
                $month = 1;
            else
                $month++;
            $i++;
        }

        if ($db->getAttribute(PDO::ATTR_DRIVER_NAME) === 'sqlite') {
            $sql = "SELECT
                        docs.DocTypeID,
                        strftime('%Y', docs.DateCreated)  AS year,
                        strftime('%m', docs.DateCreated)  AS month,
                        SUM(docs.Total)         AS total
                    FROM hlx_documents AS docs LEFT JOIN hlx_documents_statutes AS statutes ON docs.DocStatusID = statutes.ID
                    WHERE   docs.DateCreated >= DATE('NOW', '-11 MONTHS')
                            AND statutes.Validated = 1
                    GROUP BY docs.DocTypeID, strftime('%Y', docs.DateCreated), strftime('%m', docs.DateCreated)
                    ORDER BY docs.DocTypeID, strftime('%Y', docs.DateCreated), strftime('%m', docs.DateCreated)";
        }
        else {
            $sql = "SELECT
                        docs.DocTypeID,
                        YEAR(docs.DateCreated)  AS year,
                        MONTH(docs.DateCreated) AS month,
                        SUM(docs.Total)         AS total
                    FROM hlx_documents AS docs LEFT JOIN hlx_documents_statutes AS statutes ON docs.DocStatusID = statutes.ID
                    WHERE   docs.DateCreated >= DATE_SUB(DATE_FORMAT(CURRENT_TIMESTAMP, '%Y-%m-01 00:00:00'),INTERVAL 11 MONTH)
                            AND statutes.Validated = 1
                    GROUP BY docs.DocTypeID, YEAR(docs.DateCreated), MONTH(docs.DateCreated)
                    ORDER BY docs.DocTypeID, YEAR(docs.DateCreated), MONTH(docs.DateCreated)";
        }
        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC): false;
        if ($rows === false)
        {
            return false;
        }
        $res->closeCursor();

        foreach($rows as $row) {

            $rReturn[ $row['DocTypeID'] ][ ltrim($row['month'], '0') ] = $row['total'];
        }

        return $rReturn;

    }

    static function getQuoteDivision() {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        $rReturn = array(
            'draft'     => 0,
            'canceled'  => 0,
            'open'      => 0,
            'closed'    => 0,
        );

        if ($db->getAttribute(PDO::ATTR_DRIVER_NAME) === 'sqlite') {
            $sql = "SELECT
                        CASE
                            WHEN docs.DocStatusID = 1 THEN 'draft'
                            WHEN docs.DocStatusID = 7 OR docs.DocStatusID = 6 THEN 'canceled'
                            WHEN docs.DocStatusID = 3 OR docs.DocStatusID = 4 OR docs.DocStatusID = 2 THEN 'open'
                            WHEN docs.DocStatusID = 8 OR docs.DocStatusID = 5 THEN 'closed'
                            ELSE NULL
                        END state,
                        COUNT(*) AS total
                    FROM hlx_documents AS docs
                    WHERE   docs.DocTypeID = ".self::DOC_TYPE_QUOTE."
                            AND docs.DateCreated >= strftime('%Y-%m-00 00:00:00', date('now'))
                    GROUP BY    CASE
                                    WHEN docs.DocStatusID = 1 THEN 'draft'
                                    WHEN docs.DocStatusID = 7 OR docs.DocStatusID = 6 THEN 'canceled'
                                    WHEN docs.DocStatusID = 3 OR docs.DocStatusID = 4 OR docs.DocStatusID = 2 THEN 'open'
                                    WHEN docs.DocStatusID = 8 OR docs.DocStatusID = 5 THEN 'closed'
                                    ELSE NULL
                                END
                    HAVING state IS NOT NULL";
        }
        else {
            $sql = "SELECT
                        CASE
                            WHEN docs.DocStatusID = 1 THEN 'draft'
                            WHEN docs.DocStatusID = 7 OR docs.DocStatusID = 6 THEN 'canceled'
                            WHEN docs.DocStatusID = 3 OR docs.DocStatusID = 4 OR docs.DocStatusID = 2 THEN 'open'
                            WHEN docs.DocStatusID = 8 OR docs.DocStatusID = 5 THEN 'closed'
                            ELSE NULL
                        END state,
                        COUNT(*) AS total
                    FROM hlx_documents AS docs
                    WHERE   docs.DocTypeID = ".self::DOC_TYPE_QUOTE."
                            AND docs.DateCreated >= DATE_FORMAT(CURRENT_TIMESTAMP, '%Y-%m-00 00:00:00')
                    GROUP BY    CASE
                                    WHEN docs.DocStatusID = 1 THEN 'draft'
                                    WHEN docs.DocStatusID = 7 OR docs.DocStatusID = 6 THEN 'canceled'
                                    WHEN docs.DocStatusID = 3 OR docs.DocStatusID = 4 OR docs.DocStatusID = 2 THEN 'open'
                                    WHEN docs.DocStatusID = 8 OR docs.DocStatusID = 5 THEN 'closed'
                                    ELSE NULL
                                END
                    HAVING state IS NOT NULL";
        }
        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC): false;
        if ($rows === false)
        {
            return false;
        }
        $res->closeCursor();

        foreach($rows as $row) {

            $rReturn[ $row['state'] ] = $row['total'];
        }

        return $rReturn;

    }

    static function getInvoiceDivision() {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        $rReturn = array(
            'draft'     => 0,
            'validated' => 0,
            'paid'      => 0,
            'overdue'   => 0,
        );

        if ($db->getAttribute(PDO::ATTR_DRIVER_NAME) === 'sqlite') {
            $sql = "SELECT
                        CASE
                            WHEN docs.DocStatusID = 9 THEN 'draft'
                            WHEN docs.DocStatusID = 10 THEN CASE
                                                                WHEN docs.DateDue >= CURRENT_TIMESTAMP THEN 'validated'
                                                                ELSE 'overdue'
                                                            END
                            WHEN docs.DocStatusID = 11 THEN 'paid'
                            ELSE NULL
                        END state,
                        COUNT(*) AS total
                    FROM hlx_documents AS docs
                    WHERE   docs.DocTypeID = ".self::DOC_TYPE_INVOICE."
                            AND docs.DateCreated >= strftime('%Y-%m-00 00:00:00', CURRENT_TIMESTAMP)
                    GROUP BY    CASE
                                    WHEN docs.DocStatusID = 9 THEN 'draft'
                                    WHEN docs.DocStatusID = 10 THEN CASE
                                                                        WHEN docs.DateDue >= CURRENT_TIMESTAMP THEN 'validated'
                                                                        ELSE 'overdue'
                                                                    END
                                    WHEN docs.DocStatusID = 11 THEN 'paid'
                                    ELSE NULL
                                END
                    HAVING state IS NOT NULL";
        }
        else {
            $sql = "SELECT
                        CASE
                            WHEN docs.DocStatusID = 9 THEN 'draft'
                            WHEN docs.DocStatusID = 10 THEN CASE
                                                                WHEN docs.DateDue >= CURRENT_TIMESTAMP THEN 'validated'
                                                                ELSE 'overdue'
                                                            END
                            WHEN docs.DocStatusID = 11 THEN 'paid'
                            ELSE NULL
                        END state,
                        COUNT(*) AS total
                    FROM hlx_documents AS docs
                    WHERE   docs.DocTypeID = ".self::DOC_TYPE_INVOICE."
                            AND docs.DateCreated >= DATE_FORMAT(CURRENT_TIMESTAMP, '%Y-%m-00 00:00:00')
                    GROUP BY    CASE
                                    WHEN docs.DocStatusID = 9 THEN 'draft'
                                    WHEN docs.DocStatusID = 10 THEN CASE
                                                                        WHEN docs.DateDue >= CURRENT_TIMESTAMP THEN 'validated'
                                                                        ELSE 'overdue'
                                                                    END
                                    WHEN docs.DocStatusID = 11 THEN 'paid'
                                    ELSE NULL
                                END
                    HAVING state IS NOT NULL";
        }
        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC): false;
        if ($rows === false)
        {
            return false;
        }
        $res->closeCursor();

        foreach($rows as $row) {

            $rReturn[ $row['state'] ] = $row['total'];
        }

        return $rReturn;

    }

    static function getTotalOverdueInvoice() {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        $sql = "SELECT
                   COUNT(*) AS total
                FROM hlx_documents AS docs LEFT JOIN hlx_documents_statutes AS docStatutes ON (docs.DocStatusID = docStatutes.ID)
                WHERE   docs.DocTypeID = ".self::DOC_TYPE_INVOICE."
                        AND docs.DateDue <= CURRENT_TIMESTAMP
                        AND docStatutes.Closed = 0 AND docStatutes.Draft = 0";

        $res = $db->query($sql);
        $row = $res ? $res->fetch(PDO::FETCH_ASSOC): false;
        if ($row === false)
        {
            return false;
        }
        $res->closeCursor();

        return $row['total'];

    }

    static function getTotalExpiredQuote() {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        $sql = "SELECT
                   COUNT(*) AS total
                FROM hlx_documents AS docs LEFT JOIN hlx_documents_statutes AS docStatutes ON (docs.DocStatusID = docStatutes.ID)
                WHERE   docs.DocTypeID = ".self::DOC_TYPE_QUOTE."
                        AND docs.DateDue <= CURRENT_TIMESTAMP
                        AND docStatutes.Closed = 0 AND docStatutes.Draft = 0";

        $res = $db->query($sql);
        $row = $res ? $res->fetch(PDO::FETCH_ASSOC): false;
        if ($row === false)
        {
            return false;
        }
        $res->closeCursor();

        return $row['total'];

    }

}