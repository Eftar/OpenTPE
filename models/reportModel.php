<?php

class ReportModel {

    static function loadAll() {

        $rResult = self::getList(array(
            'sortBy'        => 'Name',
            'sortDirection' => 'ASC',
            'searchBy'      => '',
            'filterBy'      => '',
            'pageIndex'     => 0,
            'pageSize'      => 100
        ));

        return $rResult['items'];

    }

    static function load($id) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        if ($id == 0)
        {
            return array(
                'ID'                => '0',
                'Name'              => '',
                'Description'       => '',
                'Query'             => '',
                'Variables'         => array()
            );
        }

        $sql = "SELECT
                    reports.*
                FROM hlx_reports AS reports
                WHERE reports.ID = ".(int) $id;
        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC) : false;
        if ( $rows === false /*&&*/ )//TODO catch no result
        {
            return false;
        }
        foreach ($rows as &$row)
        {
            $variables = false;

            if ($row['Variables'] != '')
                $variables = json_decode($row['Variables'], true);

            if ($variables === false || ! is_array($variables))
                $row['Variables'] = array();
            else
            {
                foreach($variables as &$variable)
                {
                    if ($variable['type'] == 'date')
                    {
                        if ($variable['default'] == 'startY')
                            $variable['default'] = date('Y-01-01');
                        if ($variable['default'] == 'startM')
                            $variable['default'] = date('Y-m-01');
                        if ($variable['default'] == 'today')
                            $variable['default'] = date('Y-m-d');
                        if ($variable['default'] == 'endY')
                            $variable['default'] = date('Y-12-31');
                        if ($variable['default'] == 'endM')
                            $variable['default'] = date('Y-m-t');
                    }
                }
                $row['Variables'] = $variables;
            }

        }
        $res->closeCursor();

        return $rows[0];

    }

    static function getList($args) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        $where = '';

        switch($args['sortBy']) {
            case 'ID':
                $orderBy = 'ORDER BY reports.ID';
                break;
            case 'Name':
            default:
                $orderBy = 'ORDER BY reports.Name';
                break;
        }

        if (isset($args['sortDirection']) && $args['sortDirection'] == 'desc')
            $orderBy .= ' DESC';
        else
            $orderBy .= ' ASC';


        $sql = "SELECT
                        reports.*
                    FROM hlx_reports AS reports
                    ".$where." ".$orderBy." LIMIT ".(int) $args['pageIndex'] * (int) $args['pageSize'].",  ".(int) $args['pageSize'];
        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC): false;
        if ( $rows === false)
        {
            return false;
        }

        foreach ($rows as &$row)
        {
            $variables = false;

            if ($row['Variables'] != '')
                $variables = @json_decode($row['Variables'], true);

            if ($variables === false || ! is_array($variables))
                $row['Variables'] = array();
            else
                $row['Variables'] = $variables;

        }
        $res->closeCursor();

        $sql = "SELECT COUNT(*) AS total
                    FROM hlx_reports AS reports
                    ".$where;
        $res = $db->query($sql);
        $rTotal = $res ? $res->fetch(PDO::FETCH_ASSOC) : false;
        if ( $rTotal === false)
        {
            return false;
        }
        $res->closeCursor();

        return  array(
            'total' => $rTotal['total'],
            'items' => $rows
        );

    }

}