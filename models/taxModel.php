<?php

class TaxModel {

    static function loadAll($curDate = true) {

        if ($curDate === true)
            $curDate = date('Y-m-d');
        elseif ($curDate !== false)
            $curDate = substr($curDate, 0, 10);

        $rResult = self::getList(array(
            'sortBy'        => 'Rate',
            'sortDirection' => 'ASC',
            'searchBy'      => '',
            'filterBy'      => $curDate,
            'pageIndex'     => 0,
            'pageSize'      => 100
        ));

        if ( ! $rResult)
            return false;

        return $rResult['items'];

    }

     static function saveAll($data) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        require_once('models/settingModel.php');
        $settings = SettingModel::load(array('GENERAL'));

        foreach ($data as $tax)
        {
            $tax['Rate'] = str_replace(',', '.', $tax['Rate']);

            if (isset($tax['ID']) && $tax['ID'] != '')
            {
                $sql = "UPDATE hlx_taxes SET
                            Rate        = ".($tax['Rate'] *1.0).",
                            Name        = ".$db->quote($tax['Name']).",
                            ActiveFrom  = ".($tax['ActiveFrom'] == '' || $tax['ActiveFrom'] == '0000-00-00' ? "'0000-00-00'" : $db->quote(convertDateFormat($tax['ActiveFrom'], $settings['GENERAL']['DateFormat'], 'Y-m-d', true))).",
                            ActiveTo    = ".($tax['ActiveTo'] == '' || $tax['ActiveTo'] == '0000-00-00' ? "'0000-00-00'" : $db->quote(convertDateFormat($tax['ActiveTo'], $settings['GENERAL']['DateFormat'], 'Y-m-d', true)))."
                        WHERE ID = ".$db->quote($tax['ID']);
                $res = $db->query($sql);
            }
            else
            {
                $sql = "INSERT INTO hlx_taxes (Name, Rate, ActiveFrom, ActiveTo) VALUES (
                            ".$db->quote($tax['Name']).",
                            ".($tax['Rate'] *1.0).",
                            ".($tax['ActiveFrom'] == '' || $tax['ActiveFrom'] == '0000-00-00' ? "'0000-00-00'" : $db->quote(convertDateFormat($tax['ActiveFrom'], $settings['GENERAL']['DateFormat'], 'Y-m-d', true))).",
                            ".($tax['ActiveTo'] == '' || $tax['ActiveTo'] == '0000-00-00' ? "'0000-00-00'" : $db->quote(convertDateFormat($tax['ActiveTo'], $settings['GENERAL']['DateFormat'], 'Y-m-d', true)))."
                        )";
                $res = $db->query($sql);
            }
        }

    }

    static function getList($args = false) {

        $db = HLX_Motor::getInstance()->getDatabase();
        if ( ! $db)
        {
            return false;
        }

        $where = '';

        switch($args['sortBy']) {
            case 'ID':
                $orderBy = 'ORDER BY ID';
                break;
            case 'Name':
                $orderBy = 'ORDER BY Name';
                break;
            case 'Rate':
            default:
                $orderBy = 'ORDER BY Rate';
                break;
        }

        if (isset($args['sortDirection']) && $args['sortDirection'] == 'desc')
            $orderBy .= ' DESC';
        else
            $orderBy .= ' ASC';

        if ($args['searchBy'] != '')
        {
            $where = "WHERE (Name LIKE CONCAT ('%', ".$db->quote($args['searchBy']). ", '%')";
            $where .= " OR Rate = ".$args['searchBy']*1.0;
            $where .= ")";
        }
        if ($args['filterBy'] != '')
        {
            $where .= ($where != '' ? ' AND ':'WHERE ').'taxes.ActiveFrom <= '.$db->quote($args['filterBy']).' AND (taxes.ActiveTo = \'0000-00-00\' OR taxes.ActiveTo >= '.$db->quote($args['filterBy']).')';
        }

        $sql = "SELECT
                        taxes.*
                    FROM hlx_taxes AS taxes
                    ".$where." ".$orderBy." LIMIT ".(int) $args['pageIndex'] * (int) $args['pageSize'].",  ".(int) $args['pageSize'];
        $res = $db->query($sql);
        $rows = $res ? $res->fetchAll(PDO::FETCH_ASSOC): false;
        if ( $rows === false)
        {
            return false;
        }
        $res->closeCursor();

        $sql = "SELECT COUNT(*) AS total
                    FROM hlx_taxes AS taxes
                    ".$where;
        $res = $db->query($sql);
        $rTotal = $res ? $res->fetch(PDO::FETCH_ASSOC) : false;
        if ( $rTotal === false)
        {
            return false;
        }
        $res->closeCursor();

        return  array(
            'total' => $rTotal['total'],
            'items' => $rows
        );

    }

}
