#Version 0.9.7-beta2
- [FIX] Upgrade.php correction de la vérification du numéro de version
#Version 0.9.7-beta1
- [CHG] Fusion du dossier de release beta/rc/stable
#Version 0.9.7
- [FIX] Les acomptes doivent utiliser la même numérotation que les factures
- [FIX] Correction du numéro de version pour la maj des tables
- [CHG] Le fichier de configuration n'est plus dans le dépôt GIT
#Version 0.9.6
- [FIX] Déplacement de l'inclusion de la librairie de fonction dans le controleur
- [FIX] Version Jquery sur la page de login
#Version 0.9.5
- [FIX] Nom de colonne ambigu dans requete statistique page d'accueil
#Version 0.9.4
- [FIX] Adresse document d'achat
- [ADD] Stockage de notes personnelles
