<?php

function __autoload($classname) {
    $filename = BFWK_LIB_PATH.$classname.'.class'.BFWK_PHP_EXT;
    include_once($filename);
}

class Application
{
    private $m_qstring='';

    public function __construct($qstring)
    {
        $this->m_qstring = trim($qstring);
    }

    public function run()
    {
        $ctrl='';
        $action='';
        $args=array();

        // Parsing Query String

        $qstring = $this->m_qstring;

        $aPos = strpos($qstring,'&');
        if($aPos!==false)
        {
            parse_str(substr($qstring,$aPos),$args);
            $qstring=substr($qstring,0,$aPos);  // removing args
        }

        $sPos = strpos($qstring,'/');
        if($sPos===0)
        {
            // remove first slash
            $qstring = substr($qstring,1);
            $sPos = strpos($qstring,'/');
        }
        if($qstring=='')
        {
            // default controller with default view
            $ctrl = BFWK_DEFAULT_CTRL;
            $action = BFWK_DEFAULT_VIEW;
        }
        else if($sPos===false)
        {
            // controller
            $ctrl = $qstring;
            // default view
            $action = BFWK_DEFAULT_VIEW;
        }
        else
        {
            // controller
            $ctrl = substr($qstring,0,$sPos);
            $qstring = substr($qstring,$sPos+1); // removing controller
            $sPos = strpos($qstring,'/');

            // view
            if($qstring=='')
            {
                // default view
                $action = BFWK_DEFAULT_VIEW;
            }
            else if($sPos===false)
            {
                // view
                $action = $qstring;
            }
            else
            {
                $action = substr($qstring,0,$sPos);
                $qstring = substr($qstring,$sPos+1);
                // explode end into args
                $args2 = explode('/',urldecode($qstring));
                $args = array_merge($args2, $args);
            }
        }

        // security check
        if(!preg_match('/^([0-9]|[a-z]|[A-Z]|-)*$/',$ctrl) || !preg_match('/^([0-9]|[a-z]|[A-Z]|-)*$/',$action))
        {
            $oCtrl = new Controller();
            $oCtrl->_error("Invalid controller / view !");
            return;
        }

        // Load controller
        if(file_exists(BFWK_CTRL_PATH.$ctrl.BFWK_PHP_EXT))
        {
            require(BFWK_CTRL_PATH.$ctrl.BFWK_PHP_EXT);
            $oCtrl = new $ctrl();
        }
        else
        {
            // not found controller, used default one
            $oCtrl = new Controller();
        }

        $oCtrl->_setName($ctrl);

        $data = $oCtrl->$action($args);
        if($data===false)
        {
            // controller has already generated the page
        }
        else
        {
            $oCtrl->_display($ctrl.'_'.$action,$data);
        }
    }

}