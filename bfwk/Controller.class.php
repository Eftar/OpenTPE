<?php

class Controller
{

    private $m_ctrlname='';
    private $m_devHelp='';
    /*AutoLoad Language from browser settings*/
    protected $m_autoLang=true;

    public function __construct()
    {
        if ($this->m_autoLang == true)
            load_language();
    }

    public function __destruct()
    {
    }

    public function _setName($ctrlname)
    {
        $this->m_ctrlname = $ctrlname;
    }

    /**
     * Load developper view if no action found
     *
     * @param string $name
     * @param array $arguments
     */
    public function __call($name, $arguments)
    {
        switch(BFWK_MODE)
        {
            case 0: // dev
                return $this->_devNoAction($name, $arguments);
            case 1: // test
                return $this->_testNoAction($name, $arguments);
            case 2: // prod
            default:
                return $this->_errNoAction($name, $arguments);
        }
    }


    function _error($errorMsg)
    {
        switch(BFWK_MODE)
        {
            case 0: // dev
            case 1: // test
                echo "<html><body><h1>$errorMsg</h1></body></html>";
                break;
            case 2: // prod
            default:
                echo "<html><body></body></html>$errorMsg";
        }
        return false;
    }

    function _display($viewpage,$data)
    {
        header("Content-Type: text/html; charset=UTF-8");
        // Load view
        if(file_exists(BFWK_VIEW_PATH.$viewpage.BFWK_PHP_EXT))
        {
            $_ctrl = $this->m_ctrlname;
            $_viewpage = $viewpage;
            if(is_array($data))
                extract($data,EXTR_OVERWRITE);
            require(BFWK_VIEW_PATH.$_viewpage.BFWK_PHP_EXT);
        }
        else
        {
            switch(BFWK_MODE)
            {
                case 0: // dev
                    return $this->_devNoView($viewpage, $data);
                case 1: // test
                    return $this->_testNoView($viewpage, $data);
                case 2: // prod
                default:
                    return $this->_errNoView($viewpage, $data);
            }
        }
        return false;
    }

    function _location($url)
    {
        header('Location: '.$url);
        exit;
    }

    function _pregcallback_var($matches)
    {
        // as usual: $matches[0] is the complete match
        // $matches[1] the match for the first subpattern
        // enclosed in '(...)' and so on
        if(substr($matches[0],0,3)=='<'.'?=')
        {
            if(strpos($matches[0],'//')===false)
            {
                $this->m_devHelp.="\r\nVar : ".htmlentities($matches[2])."\r\n";
                return $matches[2];
            }
            else
            {
                $this->m_devHelp.="\r\nVar : ".htmlentities($matches[2])."//".htmlentities($matches[3])."\r\n";
                return $matches[3];
            }
        }
        else
        {
            if(strpos($matches[0],'//')===false)
            {
                $this->m_devHelp.="\r\nCode : ".htmlentities($matches[2])."\r\n";
                return "<"."?php ".$matches[2]." ?".">";
            }
            else
            {
                $this->m_devHelp.="\r\nCode : ".htmlentities($matches[2])."\r\n";
                return "<"."?php ".$matches[3]." ?".">";
            }
        }
    }

    function _pregcallback_require($matches)
    {
        // (< \?php(.*)require\((.*)\)(.*)\? >)
        //           1            2     3
        // as usual: $matches[0] is the complete match
        // $matches[1] the match for the first subpattern
        // enclosed in '(...)' and so on

        $filename = str_replace(array('"',"'"),'',$matches[2]);
        $reqpage = @file_get_contents(BFWK_VIEW_PATH.$filename);
        if($reqpage===false)
        {
            $this->m_devHelp.="\r\nRequire missing : ".htmlentities($matches[2])."\r\n";
            return $matches[0];
        }
        return $reqpage;
    }

    // Missing controller action
    function _devNoAction($name, $arguments)
    {
        if(!file_exists(BFWK_VIEW_PATH.$this->m_ctrlname.'_'.$name.BFWK_PHP_EXT))
        {
            return $this->_error("Neither controller <em>".$this->m_ctrlname."-&gt;".$name."</em> nor view <em>".$this->m_ctrlname."_".$name."</em> !");
        }
        // loading view page
        $page = file_get_contents((BFWK_VIEW_PATH.$this->m_ctrlname.'_'.$name.BFWK_PHP_EXT));
        // include required file (header, footer...) into page
        $page = preg_replace_callback("#<\?"."php(.*)require\((.*)\)(.*)\?".">#U", array($this, '_pregcallback_require'), $page);
        if(isset($arguments[0][BFWK_DEBUG_VAR]))    // developper debug mode
        {
            $this->m_devHelp='';
            // append a comment mark at the end of php tags to be sure they will be grabbed by next regex
            $page = preg_replace("#<\?=(.*)\?".">#U", "<"."?=\\1//?".">", $page);
            $page = preg_replace("#<\?php(.*)\?".">#U", "<"."?php\\1//?".">", $page);
        }
        $page = preg_replace_callback("#<\?(=|php)(.*)//(.*)\?".">#U", array($this, '_pregcallback_var'), $page);

        if(isset($arguments[0][BFWK_DEBUG_VAR]))    // developper debug mode
        {
            echo "<html><body>";
            echo "<h1>Message to developper</h1>\r\n";
            echo "<p>You have to write a controller action in <strong>".BFWK_CTRL_PATH.$this->m_ctrlname.BFWK_PHP_EXT."</strong> called <strong>function $name (\$args)</strong> returning an array with data for this code :</p>\r\n";
            echo "<pre>".$this->m_devHelp."</pre>";
        }
        else    // designer simulation mode
        {
            // simulate loop(n)
            $var = 'a';
            $oldpage=-1;
            while ($oldpage!=$page)
            {
                $oldpage=$page;
                $page = preg_replace("(loop\(([0-9]*)\))", 'for($____'.$var.'=0;$____'.$var.'<\\1;$____'.$var.'++)', $page, 1);
                $var=chr(ord($var)+1);//change 'for' var name to allow inner loop
            }
            echo eval("?".">".$page);
        }
        return false;
    }

    // Missing controller action
    function _testNoAction($name, $arguments)
    {
        if(!file_exists(BFWK_VIEW_PATH.$this->m_ctrlname.'_'.$name.BFWK_PHP_EXT))
        {
            return $this->_error("Neither controller <em>".$this->m_ctrlname."-&gt;".$name."</em> nor view <em>".$this->m_ctrlname."_".$name."</em> !");
        }
    }

    // Missing controller action
    function _errNoAction($viewpage, $data)
    {
        echo "<html>";
        echo "<h1>Sorry 404</h1>";
        echo "</html>";
        return false;
    }

    // Missing view page
    function _devNoView($viewpage, $data)
    {
        echo "<html><body>";
        echo "<h1>Message to designer</h1>\r\n";
        echo "<p>You have to write a view called <strong>".BFWK_VIEW_PATH.$viewpage.BFWK_PHP_EXT."</strong> using these data :</p>\r\n";
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
        echo "</body></html>";
        return false;
    }

    function _testNoView($viewpage, $data)
    {
        echo "<html>";
        echo "<h1>VIEW $viewpage IS MISSING</h1>";
        echo "</html>";
        return false;
    }

    function _errNoView($viewpage, $data)
    {
        echo "<html>";
        echo "<h1>Sorry 404</h1>";
        echo "</html>";
        return false;
    }

}
