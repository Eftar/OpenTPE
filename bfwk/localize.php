<?php

function load_language($lang = false)
{
    define('BFWK_LANG', load_translations($lang));
}

/**
 * Load translated texte according to user agent preferences
 */

function load_translations($lang = false)
{
    $rLang = array();
    //Forced language
    if ($lang != false)
    {
        $rLang[$lang]=$lang;
    }
    //Browser Language
    if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
    {
        preg_match_all('/(\W|^)([a-z]{2})([^a-z]|$)/six', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $m, PREG_PATTERN_ORDER);
        if(count($m[2])>0)
            foreach($m[2] as $lang)
                $rLang[$lang]=$lang;
    }
    //Default Language
    $rLang[ BFWK_DEFAULT_LANG ] = BFWK_DEFAULT_LANG;
    $rLang = array_map('strtoupper', $rLang);
    foreach($rLang as $lang)
    {
        if (is_file('lang/'.$lang.'.txt'))
        {
            $rTrans = @parse_ini_file('lang/'.$lang.'.txt');

            if($rTrans!==false)
            {
                foreach($rTrans as $k => $v)
                    define('T__'.$k, $v);
                return $lang;
            }
        }
    }

    echo "Unknown language $lang !";
    exit;

}

function get_languages()
{
    $rLangue = array();

    $path = 'lang/';

    foreach (glob($path."*.txt") as $filename) {
        $code = substr(basename($filename), 0, -4);
        $name = $code;
        $rTrans = @parse_ini_file('lang/'.$code.'.txt');
        if ($rTrans != false)
        {
            foreach($rTrans as $k => $v)
            {
                if ($k == 'Lang')
                {
                    $name = $v;
                    break;
                }
            }
        }
        $rTemplate[ $code ] = $name;
    }

    return $rTemplate;

}

/**
 * Get text translation
 * @param string $id : translation Id
 * @param ... :  text parameters for %1, %2 replacement
 * @return string translated text
 */

function t($id)
{
    if(defined('T__'.$id))
    {
        $txt = constant('T__'.$id);
        for($p = 1; $p < func_num_args(); $p++)
            $txt = str_replace('%'.$p, func_get_arg($p), $txt);
    }
    else
    {
        $txt = '_'.$id.'_';
        for($p = 1; $p < func_num_args(); $p++)
            $txt .= '_'.func_get_arg($p);
    }
    return $txt;
}
