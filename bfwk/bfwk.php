<?php

switch(BFWK_MODE)
{
	case 0:	// dev
		error_reporting(E_ALL);
		ini_set('display_errors','stdout');
		break;
	case 1:	// test
		error_reporting(E_ERROR | E_PARSE);
		ini_set('display_errors','stdout');
		break;
	case 2:	// prod
	default:
		error_reporting(0);
		ini_set('display_errors',0);
}


require_once("Application.class.php");
require_once("Controller.class.php");
require_once("localize.php");

$queryString = (isset($_SERVER["QUERY_STRING"]) ? $_SERVER["QUERY_STRING"] : (isset($argv) ? $argv : ''));

$app = new Application($queryString);

$app->run();

