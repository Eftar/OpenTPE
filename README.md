# OpenTPE

Logiciel libre de gestion pour les TPE
https://www.opentpe.fr

## Liste des bibliothèques tierces utilisées dans le projet :
| Bibliothèque                                                                | Licence                         |
|-----------------------------------------------------------------------------|---------------------------------|
| [Bfwk](http://www.befox.fr)                                                 |                                 |
| [Bootstrap](http://getbootstrap.com/)                                       | MIT                             |
| [Bootstrap-datepicker](http://eonasdan.github.io/bootstrap-datetimepicker/) | MIT                             |
| [Bootstrap-select](https://silviomoreto.github.io/bootstrap-select/)        | MIT                             |
| [Chartjs](http://www.chartjs.org/)                                          | MIT                             |
| [DOMPDF](http://dompdf.github.io/)                                          | GNU/LGPLv2.1                    |
| [Fontawesome](http://fontawesome.io/)                                       | SIL OFL 1.1                     |
| [FuelUX](http://getfuelux.com/)                                             | BSD 3-clause "New" or "Revised" |
| [Fullcalendar](https://fullcalendar.io/)                                    | MIT                             |
| [Glyphicons](http://glyphicons.com/)                                        | CC BY 3.0                       |
| [Jquery](https://jquery.com/)                                               | MIT                             |
| [momentjs](https://momentjs.com/)                                           | MIT                             |
| [npm](https://www.npmjs.com/)                                               | The Artistic License 2.0        |
| [pdfjs](https://github.com/mozilla/pdf.js)                                  | Apache License 2.0              |
| [PHPMailer](https://github.com/PHPMailer/PHPMailer)                         | GNU/GPL v2.1                    |
| [Sortable](https://github.com/RubaXa/Sortable)                              | MIT                             |

## Icône
https://www.webdesignerdepot.com/

## Licence
GPLv3